package id.ac.tazkia.aplikasikeuangan.dao;


import id.ac.tazkia.aplikasikeuangan.RunWith;
import id.ac.tazkia.aplikasikeuangan.Test;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDao;
import org.jboss.jandex.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AnggaranDaoTest {

    @Autowired
    private AnggaranDao anggaranDao;

    @Test
    public void testTotalAnggaran(){
        List<Object[]> totalPengajuan = anggaranDao.getAnggaranDepartemen("1123", Arrays.asList("96c491cf-f095-11e9-9a47-6466b30294e6"));
        for (Object[] row: totalPengajuan) {
            Integer x = 0;
            for (Object data : row) {
                System.out.println(x++);
                System.out.println("Tipe data : " + data.getClass().getName());
                System.out.println(data);
            }
        }
    }

}
