package id.ac.tazkia.aplikasikeuangan.dao;

import id.ac.tazkia.aplikasikeuangan.RunWith;
import id.ac.tazkia.aplikasikeuangan.Test;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailDao;
import id.ac.tazkia.aplikasikeuangan.dto.setting.AnggaranDetailDto;
import junit.framework.Assert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AnggaranDetailDaoTests {
    @Autowired private AnggaranDetailDao anggaranDetailDao;

    @Test
    public void testMappingKeDto() {
        List<AnggaranDetailDto> hasil = anggaranDetailDao.getDetailAnggaran("afa60ff6-d726-451f-b053-e11f60f4f4b6");
        Assert.assertNotNull(hasil);
        Assert.assertFalse(hasil.isEmpty());
        for (AnggaranDetailDto dto: hasil) {
            System.out.println("Deskripsi : "+dto.getDeskripsi());
            System.out.println("Anggaran : "+dto.getAnggaran().setScale(2, BigDecimal.ROUND_HALF_EVEN));
            System.out.println("Total : "+dto.getTotalPengajuanDana());
            System.out.println("Sisa : "+dto.getSisa().setScale(2, BigDecimal.ROUND_HALF_EVEN));
        }
    }
}
