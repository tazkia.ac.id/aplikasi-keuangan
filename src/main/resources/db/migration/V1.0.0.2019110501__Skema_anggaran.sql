CREATE TABLE `anggaran` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_periode_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_departemen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` longtext COLLATE utf8_unicode_ci,
  `amount` decimal(19,2) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE `anggaran_detail` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci,
  `kuantitas` decimal(19,2) DEFAULT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `anggaran`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `anggaran_detail`
  ADD PRIMARY KEY (`id`);
