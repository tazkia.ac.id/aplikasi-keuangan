CREATE TABLE `departemen` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_departemen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_aktif` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_departemen` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `karyawan` (
  `id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nik` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_karyawan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gelar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_aktif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nidn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `rfid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_absen` int(11) DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `mata_anggaran` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode_mata_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_mata_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_aktif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `pagu_anggaran` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_periode_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_departemen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pagu_anggaran_amount` decimal(19,2) DEFAULT NULL,
  `persentase_pagu_anggaran` decimal(19,2) DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocking` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `penerimaan` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_periode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_mata_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_rencana_penerimaan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` decimal(19,0) DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_penerimaan` date DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_peneriman` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `periode_anggaran` (
  `id` int(11) NOT NULL,
  `kode_periode_anggaran` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_periode_anggaran` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_mulai_perencanaan` date DEFAULT NULL,
  `tanggal_selesai_perencanaan` date DEFAULT NULL,
  `tanggal_mulai_anggaran` date DEFAULT NULL,
  `tanggal_selesai_anggaran` date DEFAULT NULL,
  `status_aktif` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `pic_anggaran` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_periode_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_departemen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_karyawan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `rencana_penerimaan` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_periode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_mata_anggaran` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persentase_pagu` decimal(19,2) DEFAULT NULL,
  `pagu` decimal(19,2) DEFAULT NULL,
  `persentase_anggaran` decimal(19,2) DEFAULT NULL,
  `anggaran` decimal(19,2) DEFAULT NULL,
  `selisih` decimal(19,2) DEFAULT NULL,
  `status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `s_permission` (
  `id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `s_role` (
  `id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `s_role_permission` (
  `id_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_permission` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `s_user` (
  `id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `s_user_password` (
  `id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `departemen`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `mata_anggaran`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pagu_anggaran`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `penerimaan`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `periode_anggaran`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pic_anggaran`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `rencana_penerimaan`
  ADD PRIMARY KEY (`id`);

