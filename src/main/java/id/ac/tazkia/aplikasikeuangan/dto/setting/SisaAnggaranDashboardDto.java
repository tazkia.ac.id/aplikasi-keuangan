package id.ac.tazkia.aplikasikeuangan.dto.setting;

import java.math.BigDecimal;

public interface SisaAnggaranDashboardDto {

    String getId();
    String getNamaDepartemen();
    BigDecimal getTotalAnggaran();
    BigDecimal getPengeluaran();
    BigDecimal getPersentase();
    BigDecimal getPaguAnggaran();
    BigDecimal getSisaPagu();

}
