package id.ac.tazkia.aplikasikeuangan.dto.export;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface ExportAnggaranDetailEstimasiDepartemenDto {

    String getKodeAnggaran();
    String getNamaAnggaran();
    String getProgramKerja();
    String getDeskripsiDetail();
    String getDeskripsiEstimasi();
    LocalDate getTanggalEstimasi();
    BigDecimal getKuantitas();
    String getSatuan();
    BigDecimal getAmount();
    BigDecimal getJumlah();

}
