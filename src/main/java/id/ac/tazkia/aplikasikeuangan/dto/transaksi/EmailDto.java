package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;

public interface EmailDto {

    String getNama();
    String getItem();
    BigDecimal getKuantitas();
    BigDecimal getAmount();
    String getSatuan();
    BigDecimal getJumlah();
    String getEmail();
    String getJabatan();
    String getTanggal();
    String getAnggaran();
    String getId();

}
