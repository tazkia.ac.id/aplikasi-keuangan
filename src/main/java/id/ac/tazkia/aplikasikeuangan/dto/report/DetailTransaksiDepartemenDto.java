package id.ac.tazkia.aplikasikeuangan.dto.report;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface DetailTransaksiDepartemenDto {

    String getDeskripsiBiaya();
    LocalDateTime getTanggalPengajuan();
    BigDecimal getPengajuan();
    LocalDateTime getTanggalLaporan();
    BigDecimal getRealisasi();
    Integer getBulan();
    Integer getTahun();
    BigDecimal getTotal();
    Integer getNomor();
    String getIdDepartemen();

}
