package id.ac.tazkia.aplikasikeuangan.dto.setting;

import java.math.BigDecimal;

public interface BelumAdaEstimasiDto {
     
     String getId();
     String getIdAnggaran();
     String getIdAnggaranProker();
     BigDecimal getTotalAnggaranDetail();
     BigDecimal getTotalEstimasi();
     
}
