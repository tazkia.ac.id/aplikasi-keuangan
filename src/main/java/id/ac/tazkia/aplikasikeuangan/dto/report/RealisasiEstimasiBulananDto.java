package id.ac.tazkia.aplikasikeuangan.dto.report;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface RealisasiEstimasiBulananDto {

    String getId();
    LocalDate getTanggalEstimasi();
    String getNamaDepartemen();
    String getKodeAnggaran();
    String getNamaAnggaran();
    String getDeskripsi();
    String getEstimasi();
    BigDecimal getTotalEstimasi();
    BigDecimal getRealisasi();
    String getStatus();
}
