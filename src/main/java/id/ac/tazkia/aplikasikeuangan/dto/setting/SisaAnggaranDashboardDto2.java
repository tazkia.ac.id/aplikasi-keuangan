package id.ac.tazkia.aplikasikeuangan.dto.setting;

import java.math.BigDecimal;

public interface SisaAnggaranDashboardDto2 {

    String getId();

    String getIdDepartemen();
    String getNamaDepartemen();
    BigDecimal getTotalAnggaran();
    BigDecimal getPengeluaran();
    BigDecimal getPersentase();
    BigDecimal getPaguAnggaran();
    BigDecimal getSisaPagu();
    String getKeterangan();

}
