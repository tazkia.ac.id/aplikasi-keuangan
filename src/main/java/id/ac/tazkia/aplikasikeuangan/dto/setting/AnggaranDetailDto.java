package id.ac.tazkia.aplikasikeuangan.dto.setting;

import java.math.BigDecimal;

public interface AnggaranDetailDto {
    String getId();
    String getProgramKerja();
    String getDeskripsi();
    BigDecimal getAnggaran();
    BigDecimal getTotalPengajuanDana();
    BigDecimal getSisa();
}
