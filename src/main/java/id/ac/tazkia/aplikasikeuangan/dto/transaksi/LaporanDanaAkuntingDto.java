package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface LaporanDanaAkuntingDto {
    String getId();
    String getDeskripsi();
    LocalDateTime getTanggalLaporan();
    BigDecimal getJumlah();
    String getPosting();
    String getPilihan();
    String getTemplateJurnal();
}
