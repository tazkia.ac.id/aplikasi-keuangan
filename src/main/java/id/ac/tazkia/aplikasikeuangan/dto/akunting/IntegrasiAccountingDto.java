package id.ac.tazkia.aplikasikeuangan.dto.akunting;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

@Data
public class IntegrasiAccountingDto {
    private String responseCode;
    private String responseMessage;
    private String codeTemplate;
    private LocalDate dateTransaction;
    private String description;
    private String institut;

    private BigDecimal amounts;
    private String tags;
}
