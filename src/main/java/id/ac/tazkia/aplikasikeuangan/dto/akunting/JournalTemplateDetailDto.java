package id.ac.tazkia.aplikasikeuangan.dto.akunting;

import lombok.Data;

@Data
public class JournalTemplateDetailDto {

    private String sequence;
    private String idAccount;
    private String account;
    private String balanceType;

}
