package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;

public interface SisaSaldoBankDto {

    BigDecimal getMasuk();
    BigDecimal getKeluar();

    BigDecimal getSisa();

    String getInstansi();

}
