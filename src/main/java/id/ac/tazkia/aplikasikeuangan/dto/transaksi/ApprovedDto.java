package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface ApprovedDto {

    String getId();
    String getIdpengajuandana();
    String getNamadepartemen();
    LocalDateTime getTanggalpengajuan();
    String getFile();
    String getKodeanggaran();
    String getNamaanggaran();
    String getNamakaryawan();
    String getDeskripsi();
    String getDeskripsibiaya();
    BigDecimal getKuantitas();
    String getSatuan();
    BigDecimal getJumlah();
    Integer getNomor();
    Integer getNomorurut();
    BigDecimal getAmount();
    String getRequestFor();
    String getStatus();
    String getTotalNomorUrut();
    String getNomorUrut();

}
