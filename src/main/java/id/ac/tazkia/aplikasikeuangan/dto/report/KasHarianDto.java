package id.ac.tazkia.aplikasikeuangan.dto.report;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface KasHarianDto {

    String getTanggalpengajuan();
    LocalDateTime getTanggalpencairan();
    LocalDateTime getTanggalcair();
    LocalDateTime getTanggallaporan();
    String getNama();
    String getKode();
    String getName();
    String getDeskripsi();
    BigDecimal getPengajuan();
    BigDecimal getDisbursereq();
    BigDecimal getDisburse();
    BigDecimal getRealisasi();
    BigDecimal getSelisih();
    String getIdPencairanDana();
    String getBuktiPencairan();
    String getBuktiMintaPencairan();
    String getStatusAkunting();

}
