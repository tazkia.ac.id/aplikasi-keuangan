package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface PengembalianDanaProsesAkuntingDto {
    String getId();
    LocalDateTime getTanggalPengembalian();
    String getKeteranganPengembalian();
    BigDecimal getNominalPengembalian();
    String getPilihan();
    String getTemplateJurnal();
}
