package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;

public interface PengembalianDanaDto {

    String getId();
    String getKeteranganPengajuan();
    BigDecimal getPengembalian();
    BigDecimal getPencairan();
    BigDecimal getJumlahLaporan();

}
