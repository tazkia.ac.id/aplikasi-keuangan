package id.ac.tazkia.aplikasikeuangan.dto.setting;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface SisaAnggaranDashboardKodeDto {

    String getId();
    String getNamaDepartemen();
    BigDecimal getTotalAnggaran();
    BigDecimal getPengeluaran();
    BigDecimal getPersentase();
    BigDecimal getPaguAnggaran();
    BigDecimal getSisaPagu();
    String getKode();
    String getIdDepartemen();

    LocalDateTime getTanggalUpdate();

}
