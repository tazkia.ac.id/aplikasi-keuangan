package id.ac.tazkia.aplikasikeuangan.dto.report;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface EstimasiPengeluaranDto {

    String getKodeAnggaran();
    String getNamaAnggaran();
    String getDesDetail();
    String getDesEstimasi();
    LocalDate getTanggalEstimasi();
    BigDecimal getKuantitas();
    String getSatuan();
    BigDecimal getAmount();
    BigDecimal getJumlah();

}
