package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface PencairanDanaAkuntingDto {
    String getId();
    LocalDateTime getTanggalPencairan();
    String getDeskripsi();
    BigDecimal getNominalPencairan();
    String getTemplateJurnal();
}
