package id.ac.tazkia.aplikasikeuangan.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface PrintLaporanDanaDto {

    String getDepartemen();
    String getKaryawan();
    String getKode();
    String getDetailAnggaran();
    BigDecimal getPengajuan();
    BigDecimal getPencairan();
    BigDecimal getLaporan();
    BigDecimal getSisa();
    LocalDate getTanggal();

}
