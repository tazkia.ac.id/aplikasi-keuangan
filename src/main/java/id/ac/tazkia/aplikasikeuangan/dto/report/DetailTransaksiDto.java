package id.ac.tazkia.aplikasikeuangan.dto.report;

import java.awt.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface DetailTransaksiDto {

    String getId();
    LocalDateTime getTanggalPengajuan();
    String getNamaDepartemen();
    String getNamaKaryawan();
    String getKodeAnggaran();
    String getDetailAnggaran();
    String getDeskripsiBiaya();
    BigDecimal getPengajuan();
    BigDecimal getRealisasi();
    BigDecimal getSelisih();

}
