package id.ac.tazkia.aplikasikeuangan.dto.akunting;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SaveAccountingDto {
    private String idPencairan;
    private String idLaporanDana;

    private String journalTemplate;
    private String description;
    private String[] amounts;

    private String tahunSelected;
    private String bulanSelected;
}
