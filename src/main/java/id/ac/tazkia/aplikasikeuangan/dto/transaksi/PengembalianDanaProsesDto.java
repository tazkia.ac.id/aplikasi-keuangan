package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;

public interface PengembalianDanaProsesDto {

    String getId();
    String getPengembalianDanaProses();
    String getFile();
    String getKeteranganPengajuan();
    BigDecimal getPengembalian();
    BigDecimal getPencairan();
    BigDecimal getJumlahLaporan();

}
