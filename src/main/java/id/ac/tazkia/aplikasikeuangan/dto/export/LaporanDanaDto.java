package id.ac.tazkia.aplikasikeuangan.dto.export;

import java.math.BigDecimal;

public interface LaporanDanaDto {

    BigDecimal getNominal();
}
