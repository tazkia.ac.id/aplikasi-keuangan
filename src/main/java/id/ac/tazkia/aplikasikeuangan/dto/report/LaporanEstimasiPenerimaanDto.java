package id.ac.tazkia.aplikasikeuangan.dto.report;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface LaporanEstimasiPenerimaanDto {

    LocalDate getTanggalPenerimaan();
    String getKodeMataAnggaran();
    String getNamaMataAnggaran();
    String getKeterangan();
    BigDecimal getKuantitas();
    String getSatuan();
    BigDecimal getAmount();
    BigDecimal getTotal();

}
