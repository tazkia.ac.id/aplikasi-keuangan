package id.ac.tazkia.aplikasikeuangan.dto.setting;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class AnggaranDetailResponsDto {

    private String result;
    private List<AnggaranDetailResponsDto> items=new ArrayList<>();



}
