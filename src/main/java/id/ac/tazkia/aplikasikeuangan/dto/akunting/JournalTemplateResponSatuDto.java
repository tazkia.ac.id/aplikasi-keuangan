package id.ac.tazkia.aplikasikeuangan.dto.akunting;

import lombok.Data;

import java.util.List;

@Data
public class JournalTemplateResponSatuDto {

    private String responseCode;
    private String responseMessage;
    private JournalTemplateDto data;
}
