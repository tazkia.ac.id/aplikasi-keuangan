package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

public interface PenerimaanBalanceDto {

    String getId();
    String getKodeMataAnggaran();
    String getNamaMataAnggaran();
    String getEstimasi();
    String getPenerimaan();
    String getMargin();

}
