package id.ac.tazkia.aplikasikeuangan.dto.setting;

import java.math.BigDecimal;

public interface DetailAnggaranPorsiDto {

    String getId();
    String getNamaDepartemen();
    String getKodeAnggaran();
    String getNamaAnggaran();
    String getDeskripsi();
    BigDecimal getJumlah();
    String getPorsi();

}
