package id.ac.tazkia.aplikasikeuangan.dto.akunting;

import lombok.Data;

import java.util.List;

@Data
public class JournalTemplateResponsDto {

        private String responseCode;
        private String responseMessage;
        private List<JournalTemplateDto> data;

}
