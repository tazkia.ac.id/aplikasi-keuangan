package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

public interface PenerimaanBalanceDetailDto {

    String getId();
    String getKodeMataAnggaran();
    String getKeterangan();
    String getEstimasi();
    String getPenerimaan();
    String getMargin();

}
