package id.ac.tazkia.aplikasikeuangan.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DashboardDto {

    private String namaPeriodeAnggaran;
    private BigDecimal totalBudget;
    private BigDecimal requestFund;
    private BigDecimal totalPenerimaan;
    private BigDecimal budgetBalance;

}
