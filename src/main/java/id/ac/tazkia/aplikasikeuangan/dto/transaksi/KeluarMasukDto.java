package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;

public interface KeluarMasukDto {

    Integer getAda();

    String getNama();

    BigDecimal getPenerimaan();

    BigDecimal getPengeluaran();
}
