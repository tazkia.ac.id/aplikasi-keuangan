package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;

public interface TransaksiSetahunDto {

    BigDecimal getTotal();

    String getBulan();

}
