package id.ac.tazkia.aplikasikeuangan.dto.akunting;

import lombok.Data;

@Data
public class JournalTemplateDto {

    private String id;
    private String code;
    private String name;
    private String category;

}
