package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface SaldoPendapatanTerkaitDto {

    String getId();
    LocalDate getTgl();
    LocalDateTime getTanggalInsert();
    String getDeskripsi();
    String getFile();
    BigDecimal getJumlah();
    String getSatuan();
    BigDecimal getMasuk();
    BigDecimal getKeluar();

    BigDecimal getSaldo();

    String getStatus();

}
