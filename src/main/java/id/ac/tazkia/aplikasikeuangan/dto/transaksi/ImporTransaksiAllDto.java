package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface ImporTransaksiAllDto {

    String getId();
    String getIdDepartemen();
    LocalDateTime getTanggalPengajuan();
    String getDeskripsiBiaya();
    BigDecimal getPengajuan();
    LocalDateTime getTanggalLaporan();

    BigDecimal getRealisasi();

}
