package id.ac.tazkia.aplikasikeuangan.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransaksiSetahunDuaDto {

    private BigDecimal total;
    private String bulan;

}
