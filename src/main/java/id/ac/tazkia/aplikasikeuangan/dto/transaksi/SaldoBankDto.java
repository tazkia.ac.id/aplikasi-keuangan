package id.ac.tazkia.aplikasikeuangan.dto.transaksi;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface SaldoBankDto {

    String getId();
    String getInstansi();
    LocalDate getTanggal();
    BigDecimal getAmount();
    String getDescription();
    String getNama();
    String getNamaPeriode();
}
