package id.ac.tazkia.aplikasikeuangan.services;


import id.ac.tazkia.aplikasikeuangan.dao.HistoryDetailTransaksiDao;
import id.ac.tazkia.aplikasikeuangan.dao.HistorySaldoHarianDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.LaporanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardDto;
import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardDto2;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.ImporTransaksiAllDto;
import id.ac.tazkia.aplikasikeuangan.entity.HistoryDetailTransaksi;
import id.ac.tazkia.aplikasikeuangan.entity.HistorySaldoHarian;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
// import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@EnableScheduling
public class UpdateSaldoHarianService {


    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;
    @Autowired
    private HistorySaldoHarianDao historySaldoHarianDao;

    @Autowired
    private HistoryDetailTransaksiDao historyDetailTransaksiDao;
    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;


    @Scheduled(cron = "0 0 4 * * *", zone = "Asia/Jakarta")
    public void updateSaldoDepartementSchedule(){

        System.out.println("Update Saldo Jalan");
        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        List<SisaAnggaranDashboardDto> sisaAnggaranDashboardDtos = anggaranCrudDao.getSisaAnggaranPerDepartemen(periodeAnggaran1);
        LocalDateTime tanggalTerakhir = LocalDateTime.now();

        if(sisaAnggaranDashboardDtos != null) {
            historySaldoHarianDao.deleteDataYangSama(tanggalTerakhir);
            historyDetailTransaksiDao.deleteDataTransaksiYangSama(tanggalTerakhir);
            for (SisaAnggaranDashboardDto sisaAnggaranDashboardDto : sisaAnggaranDashboardDtos) {
                HistorySaldoHarian historySaldoHarian = new HistorySaldoHarian();
                historySaldoHarian.setDepartemen(departemenDao.findByStatusAndId(StatusRecord.AKTIF, sisaAnggaranDashboardDto.getId()));
                historySaldoHarian.setNamaDepartemen(sisaAnggaranDashboardDto.getNamaDepartemen());
                historySaldoHarian.setPaguAnggaran(sisaAnggaranDashboardDto.getPaguAnggaran());
                historySaldoHarian.setTotalAnggaran(sisaAnggaranDashboardDto.getTotalAnggaran());
                historySaldoHarian.setSisaPagu(sisaAnggaranDashboardDto.getSisaPagu());
                historySaldoHarian.setUsed(sisaAnggaranDashboardDto.getPengeluaran());
                historySaldoHarian.setPersentase(sisaAnggaranDashboardDto.getPersentase());
                historySaldoHarian.setTanggalUpdate(tanggalTerakhir);
                historySaldoHarian.setStatus(StatusRecord.AKTIF);
                historySaldoHarian.setKeterangan("");
                historySaldoHarianDao.save(historySaldoHarian);

            }
            List<ImporTransaksiAllDto> history = anggaranCrudDao.detailTransaksiAll(periodeAnggaran1, tanggalTerakhir);
            if(history != null){
                for (ImporTransaksiAllDto imporTransaksiAllDto : history){
                    HistoryDetailTransaksi historyDetailTransaksi = new HistoryDetailTransaksi();
                    historyDetailTransaksi.setDepartemen(departemenDao.findByStatusAndId(StatusRecord.AKTIF,imporTransaksiAllDto.getIdDepartemen()));
                    historyDetailTransaksi.setTanggalUpdate(tanggalTerakhir);
                    historyDetailTransaksi.setStatus(StatusRecord.AKTIF);
                    historyDetailTransaksi.setPengajuan(imporTransaksiAllDto.getPengajuan());
                    historyDetailTransaksi.setRealisasi(imporTransaksiAllDto.getRealisasi());
                    historyDetailTransaksi.setDeskripsiBiaya(imporTransaksiAllDto.getDeskripsiBiaya());
                    historyDetailTransaksi.setTanggalPengajuan(imporTransaksiAllDto.getTanggalPengajuan());
                    historyDetailTransaksi.setTanggalLaporan(imporTransaksiAllDto.getTanggalLaporan());
                    historyDetailTransaksi.setPengajuanDana(pengajuanDanaDao.findById(imporTransaksiAllDto.getId()).get());
                    historyDetailTransaksiDao.save(historyDetailTransaksi);
                }
            }
        }

        System.out.println("Update Saldo Selesai");

    }


    @Scheduled(cron = "0 0 17 * * *", zone = "Asia/Jakarta")
    public void updateSaldoDepartementSchedule2(){

        System.out.println("Update Saldo Jalan");
        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        List<SisaAnggaranDashboardDto> sisaAnggaranDashboardDtos = anggaranCrudDao.getSisaAnggaranPerDepartemen(periodeAnggaran1);
        LocalDateTime tanggalTerakhir = LocalDateTime.now();

        if(sisaAnggaranDashboardDtos != null) {
            historySaldoHarianDao.deleteDataYangSama(tanggalTerakhir);
            historyDetailTransaksiDao.deleteDataTransaksiYangSama(tanggalTerakhir);
            for (SisaAnggaranDashboardDto sisaAnggaranDashboardDto : sisaAnggaranDashboardDtos) {
                HistorySaldoHarian historySaldoHarian = new HistorySaldoHarian();
                historySaldoHarian.setDepartemen(departemenDao.findByStatusAndId(StatusRecord.AKTIF, sisaAnggaranDashboardDto.getId()));
                historySaldoHarian.setNamaDepartemen(sisaAnggaranDashboardDto.getNamaDepartemen());
                historySaldoHarian.setPaguAnggaran(sisaAnggaranDashboardDto.getPaguAnggaran());
                historySaldoHarian.setTotalAnggaran(sisaAnggaranDashboardDto.getTotalAnggaran());
                historySaldoHarian.setSisaPagu(sisaAnggaranDashboardDto.getSisaPagu());
                historySaldoHarian.setUsed(sisaAnggaranDashboardDto.getPengeluaran());
                historySaldoHarian.setPersentase(sisaAnggaranDashboardDto.getPersentase());
                historySaldoHarian.setTanggalUpdate(tanggalTerakhir);
                historySaldoHarian.setStatus(StatusRecord.AKTIF);
                historySaldoHarian.setKeterangan("");
                historySaldoHarianDao.save(historySaldoHarian);

            }
            List<ImporTransaksiAllDto> history = anggaranCrudDao.detailTransaksiAll(periodeAnggaran1, tanggalTerakhir);
            if(history != null){
                for (ImporTransaksiAllDto imporTransaksiAllDto : history){
                    HistoryDetailTransaksi historyDetailTransaksi = new HistoryDetailTransaksi();
                    historyDetailTransaksi.setDepartemen(departemenDao.findByStatusAndId(StatusRecord.AKTIF,imporTransaksiAllDto.getIdDepartemen()));
                    historyDetailTransaksi.setTanggalUpdate(tanggalTerakhir);
                    historyDetailTransaksi.setStatus(StatusRecord.AKTIF);
                    historyDetailTransaksi.setPengajuan(imporTransaksiAllDto.getPengajuan());
                    historyDetailTransaksi.setRealisasi(imporTransaksiAllDto.getRealisasi());
                    historyDetailTransaksi.setDeskripsiBiaya(imporTransaksiAllDto.getDeskripsiBiaya());
                    historyDetailTransaksi.setTanggalPengajuan(imporTransaksiAllDto.getTanggalPengajuan());
                    historyDetailTransaksi.setTanggalLaporan(imporTransaksiAllDto.getTanggalLaporan());
                    historyDetailTransaksi.setPengajuanDana(pengajuanDanaDao.findById(imporTransaksiAllDto.getId()).get());
                    historyDetailTransaksiDao.save(historyDetailTransaksi);
                }
            }
        }

        System.out.println("Update Saldo Selesai");

    }


}
