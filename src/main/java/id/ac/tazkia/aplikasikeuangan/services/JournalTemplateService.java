package id.ac.tazkia.aplikasikeuangan.services;

import id.ac.tazkia.aplikasikeuangan.dto.akunting.JournalTemplateDetailDto;

import java.util.List;

public interface JournalTemplateService {
    List<JournalTemplateDetailDto> getDetailsByTemplateId(String templateId);
}
