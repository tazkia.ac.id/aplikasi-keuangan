package id.ac.tazkia.aplikasikeuangan.services;

import id.ac.tazkia.aplikasikeuangan.dto.akunting.JournalTemplateDetailDto;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.JournalTemplateDetailResponsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Service
public class JournalTemplateServiceImpl implements JournalTemplateService {
    @Autowired
    @Value("${api.akunting}")
    private String lokasiAkunting;

    private final RestTemplate restTemplate;
    private final String apiBaseUrl = lokasiAkunting;  // Sesuaikan dengan URL API Anda

    public JournalTemplateServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<JournalTemplateDetailDto> getDetailsByTemplateId(String templateId) {
        try {
//            String apiUrl = lokasiAkunting + "/public/api/journal-template/detail/" + templateId;
            String apiUrl  = lokasiAkunting + "/public/api/journal-template?detail=" + templateId;
            System.out.println("url : " + apiUrl);
            JournalTemplateDetailResponsDto response = restTemplate.getForObject(apiUrl, JournalTemplateDetailResponsDto.class);
            return response != null ? response.getData() : Collections.emptyList();
        } catch (Exception e) {
            // Tangani kesalahan ketika API tidak dapat diakses
            System.err.println("Error fetching journal template details: " + e.getMessage());
            return Collections.emptyList();
        }
    }

}
