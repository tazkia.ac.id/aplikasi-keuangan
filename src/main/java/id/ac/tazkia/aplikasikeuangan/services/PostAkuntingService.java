package id.ac.tazkia.aplikasikeuangan.services;

import id.ac.tazkia.aplikasikeuangan.dao.transaksi.LaporanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengembalianDanaProsesDao;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.IntegrasiAccountingDto;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.SaveAccountingDto;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.SavedAccountingDto;
import id.ac.tazkia.aplikasikeuangan.dto.export.LaporanDanaDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.LaporanDanaAkuntingDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.PencairanDanaAkuntingDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.PengembalianDanaProsesAkuntingDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusAccounting;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDanaProses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service @Slf4j
public class PostAkuntingService {
    @Autowired
    @Value("${api.akunting}")
    private String lokasiAkunting;

    @Autowired
    private PencairanDanaDao pencairanDanaDao;

    @Autowired
    private LaporanDanaDao laporanDanaDao;

    @Autowired
    private PengembalianDanaProsesDao pengembalianDanaProsesDao;

    WebClient webClient = WebClient.builder()
            .baseUrl(lokasiAkunting)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    @Scheduled(cron = "${scheduled.sendAkuntingPencairan}", zone = "Asia/Jakarta")
    private void postAkuntingPencairan(){
        System.out.println("Posting akunting pencairan start");
        List<PencairanDanaAkuntingDto> pencairanDanaAkuntingDtoList = pencairanDanaDao.listPencairanAkuntingDto();
        for (PencairanDanaAkuntingDto dto : pencairanDanaAkuntingDtoList) {
            LocalDate tanggalTransaksi = dto.getTanggalPencairan().toLocalDate();
            Optional<PencairanDana> pencairanDanaOptional = pencairanDanaDao.findById(dto.getId());
            if (pencairanDanaOptional.isPresent()) {
                PencairanDana pencairanDana = pencairanDanaOptional.get();
                pencairanDana.setStatusAkunting(StatusAccounting.POSTED);
                pencairanDanaDao.save(pencairanDana);
                IntegrasiAccountingDto integrasiAccountingDto = new IntegrasiAccountingDto();
                integrasiAccountingDto.setCodeTemplate(dto.getTemplateJurnal());
                integrasiAccountingDto.setDescription(dto.getDeskripsi());
                integrasiAccountingDto.setDateTransaction(tanggalTransaksi);
                integrasiAccountingDto.setInstitut("bed6a06c-afe9-4345-8b3f-ebe2cbb49a51");
                integrasiAccountingDto.setAmounts(dto.getNominalPencairan());
                integrasiAccountingDto.setTags("Finance - Pencairan Dana" + " - " + dto.getId());

                IntegrasiAccountingDto res = webClient.post()
                        .uri(lokasiAkunting + "/public/api/journal")
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(integrasiAccountingDto)
                        .retrieve()
                        .bodyToMono(IntegrasiAccountingDto.class)
                        .block();

                hasilIntegrasiPencairan(dto.getId(), res);
            }else {
                log.info("[Data Laporan Dana ] : {} ", "Tidak Ditemukan");
            }
        }
        System.out.println("Posting akunting pencairan finished");
    }

    @Scheduled(cron = "${scheduled.sendLaporanAkunting}", zone = "Asia/Jakarta")
    public void postAkuntingLaporan() {
        System.out.println("Posting akunting pelaporan start");
        List<LaporanDanaAkuntingDto> list = laporanDanaDao.laporanDanaListAkunting();
        for(LaporanDanaAkuntingDto dto : list) {

            LocalDate tanggalTransaksi = dto.getTanggalLaporan().toLocalDate();

            Optional<LaporanDana> laporanDanaOptional = laporanDanaDao.findById(dto.getId());
            if (laporanDanaOptional.isPresent()) {
                LaporanDana laporanDana = laporanDanaOptional.get();
                laporanDana.setPosting(LaporanDana.Posting.POSTED);
                laporanDanaDao.save(laporanDana);
                IntegrasiAccountingDto integrasiAccountingDto = new IntegrasiAccountingDto();
                integrasiAccountingDto.setCodeTemplate(dto.getTemplateJurnal());
                integrasiAccountingDto.setDescription(dto.getDeskripsi());
                integrasiAccountingDto.setDateTransaction(tanggalTransaksi);
                integrasiAccountingDto.setInstitut("bed6a06c-afe9-4345-8b3f-ebe2cbb49a51");
                integrasiAccountingDto.setAmounts(dto.getJumlah());
                integrasiAccountingDto.setTags("Finance - Laporan Dana - " + dto.getId());

                IntegrasiAccountingDto res = webClient.post()
                        .uri(lokasiAkunting + "/public/api/journal")
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(integrasiAccountingDto)
                        .retrieve()
                        .bodyToMono(IntegrasiAccountingDto.class)
                        .block();

                hasilIntegrasiLaporan(dto.getId(), res);
            }else {
                log.info("[Data Laporan Dana ] : {} ", "Tidak Ditemukan");
            }
        }
        System.out.println("Posting akunting pelaporan finished");
    }

    private void hasilIntegrasiLaporan(String id, IntegrasiAccountingDto res) {
        if (Objects.equals(res.getResponseMessage(), "Balance")) {
            log.info("[Berhasil] : {} ", res);
        } else {
            // Jika terjadi error dari API, batalkan update status
            batalUpdateStatusPostingLaporan(id);
            log.info("[Gagal] : {} ", res);
        }
    }



    private void batalUpdateStatusPostingLaporan(String idlaporan) {
        LaporanDana laporanDana = laporanDanaDao.findById(idlaporan).get();
        laporanDana.setPosting(LaporanDana.Posting.WAITING);
        laporanDanaDao.save(laporanDana);
    }





    private void hasilIntegrasiPencairan(String id, IntegrasiAccountingDto res) {
        if (Objects.equals(res.getResponseMessage(), "Balance")) {
            log.info("[Berhasil] : {} ", res);
        } else {
            // Jika terjadi error dari API, batalkan update status
            batalUpdateStatusPostingPencairan(id);
            log.info("[Gagal] : {} ", res);
        }
    }

    private void batalUpdateStatusPostingPencairan(String idpencairan) {
        PencairanDana pencairanDana = pencairanDanaDao.findById(idpencairan).get();
        pencairanDana.setStatusAkunting(StatusAccounting.WAITING);
        pencairanDanaDao.save(pencairanDana);
    }

    @Scheduled(cron = "${scheduled.sendPengembalianDanaAkunting}", zone = "Asia/Jakarta")
    private void postPengembalianDanaAkunting(){
        System.out.println("Posting akunting pengembalian start");
        List<PengembalianDanaProsesAkuntingDto> pengembalianDanaProsesAkuntingDtos = pengembalianDanaProsesDao.listPengembalianDana();
        for (PengembalianDanaProsesAkuntingDto dto : pengembalianDanaProsesAkuntingDtos){
            LocalDate tanggalTransaksi = dto.getTanggalPengembalian().toLocalDate();
            Optional<PengembalianDanaProses> pengembalianDanaProses = pengembalianDanaProsesDao.findById(dto.getId());
            if (pengembalianDanaProses.isPresent()) {
                PengembalianDanaProses pengembalianDanaProsesUpdate = pengembalianDanaProses.get();
                pengembalianDanaProsesUpdate.setStatusAkunting(StatusAccounting.POSTED);
                pengembalianDanaProsesDao.save(pengembalianDanaProsesUpdate);

                IntegrasiAccountingDto integrasiAccountingDto = new IntegrasiAccountingDto();

                integrasiAccountingDto.setCodeTemplate(dto.getTemplateJurnal());
                integrasiAccountingDto.setDescription(dto.getKeteranganPengembalian());
                integrasiAccountingDto.setDateTransaction(tanggalTransaksi);
                integrasiAccountingDto.setInstitut("bed6a06c-afe9-4345-8b3f-ebe2cbb49a51");
                integrasiAccountingDto.setAmounts(dto.getNominalPengembalian());
                integrasiAccountingDto.setTags("Finance - Pengembalian Dana" + " - " + dto.getId());
                IntegrasiAccountingDto res = webClient.post()
                        .uri(lokasiAkunting + "/public/api/journal")
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(integrasiAccountingDto)
                        .retrieve()
                        .bodyToMono(IntegrasiAccountingDto.class)
                        .block();
                hasilIntegrasiPengembalian(dto.getId(), res);

            }
        }
        System.out.println("Posting akunting pengembalian finished");
    }
    private void hasilIntegrasiPengembalian(String id, IntegrasiAccountingDto res) {
        if (Objects.equals(res.getResponseMessage(), "Balance")) {
            log.info("[Berhasil] : {} ", res);
        } else {
            // Jika terjadi error dari API, batalkan update status
            batalUpdateStatusPostingPengembalian(id);
            log.info("[Gagal] : {} ", res);
        }
    }

    private void batalUpdateStatusPostingPengembalian(String idpencairan) {
        PengembalianDanaProses pengembalianDanaProses = pengembalianDanaProsesDao.findById(idpencairan).get();
        pengembalianDanaProses.setStatusAkunting(StatusAccounting.WAITING);
        pengembalianDanaProsesDao.save(pengembalianDanaProses);
    }
}
