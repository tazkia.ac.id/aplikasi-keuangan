package id.ac.tazkia.aplikasikeuangan.api;

import id.ac.tazkia.aplikasikeuangan.dao.HistorySaldoHarianDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.*;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dto.DashboardDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.KeluarMasukDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.KeluarMasukTransaksiDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
// import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Controller
public class DashboardSpmApi {

    @Autowired
    private HistorySaldoHarianDao historySaldoHarianDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PenerimaanDao penerimaanDao;

    @GetMapping("/api/transaksi/enambulan")
    @ResponseBody
    public ResponseEntity<List<KeluarMasukTransaksiDto>> keluarMasukTransaksiDto(){

        List<KeluarMasukDto> keluarMasukDtos = pengajuanDanaDao.dataTransaksiEnamBulan();
        List<KeluarMasukTransaksiDto> keluarMasukTransaksiDtos = new ArrayList<>();
        for(KeluarMasukDto keluarMasukDto : keluarMasukDtos){
            KeluarMasukTransaksiDto keluarMasukTransaksiDto = new KeluarMasukTransaksiDto();
            keluarMasukTransaksiDto.setAda(keluarMasukDto.getAda());
            keluarMasukTransaksiDto.setNama(keluarMasukDto.getNama());
            keluarMasukTransaksiDto.setPenerimaan(keluarMasukDto.getPenerimaan());
            keluarMasukTransaksiDto.setPengeluaran(keluarMasukDto.getPengeluaran());
            keluarMasukTransaksiDtos.add(keluarMasukTransaksiDto);
        }

        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(keluarMasukTransaksiDtos, httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/api/dashboard/keuangan")
    @ResponseBody
    public DashboardDto cariDetailAnggaran(){

        LocalDateTime localDateTime = historySaldoHarianDao.tanggalUpdateTerakhir();
        // String periodeAnggaranAktif1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        String periodeBerjalan = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        BigDecimal totalBudget =  anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeBerjalan);
        BigDecimal totalPengajuanDanaAktif = historySaldoHarianDao.totalPengeluaran(localDateTime);
        if (totalBudget == null){
            totalBudget = BigDecimal.ZERO;
        }

        if (totalPengajuanDanaAktif == null){
            totalPengajuanDanaAktif = BigDecimal.ZERO;
        }
        BigDecimal totalBudgetBalance = totalBudget.subtract(totalPengajuanDanaAktif);

        DashboardDto dashboardDto = new DashboardDto();
        dashboardDto.setTotalBudget(totalBudget);
        dashboardDto.setRequestFund(totalPengajuanDanaAktif);
        dashboardDto.setNamaPeriodeAnggaran(periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeBerjalan).getNamaPeriodeAnggaran());
        dashboardDto.setTotalPenerimaan(penerimaanDao.getTotalPenerimaan(periodeBerjalan));
        dashboardDto.setBudgetBalance(totalBudgetBalance);

        return dashboardDto;

    }



}
