package id.ac.tazkia.aplikasikeuangan.api;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailDao;
import id.ac.tazkia.aplikasikeuangan.dto.BaseResponseApiDto;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.JournalTemplateDto;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.JournalTemplateResponsDto;
import id.ac.tazkia.aplikasikeuangan.entity.PilihanEnum;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/api/v1/settingtemplate")
public class SettingTemplateApi {

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    @Value("${api.akunting}")
    private String lokasiAkunting;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/all")
    public ResponseEntity<BaseResponseApiDto> getAnggaranDetail(@RequestParam String departemen, 
                        @RequestParam String periode) {
        return ResponseEntity.ok()
            .body(BaseResponseApiDto.builder()
                .data(anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(
                    StatusRecord.AKTIF, periode, departemen)).build()
                );
    }

    @GetMapping("/temp")
    public ResponseEntity<BaseResponseApiDto> getTemplate() {
        String apiUrl = lokasiAkunting + "/public/api/journal-template?type=FINANCE";
        JournalTemplateResponsDto response = restTemplate.getForObject(apiUrl, JournalTemplateResponsDto.class);
        List<JournalTemplateDto> journalTemplatesDto = response != null ? response.getData() : Collections.emptyList();

        return ResponseEntity.ok()
            .body(BaseResponseApiDto.builder()
            .data(journalTemplatesDto)
            .build());
    }

    @GetMapping("/selecttemp")
    public ResponseEntity<BaseResponseApiDto> updateTemplate(@RequestParam String id, @RequestParam String codeTemp) {
        Optional<AnggaranDetail> anggaranDetaiOptional = anggaranDetailDao.findById(id);
        if (anggaranDetaiOptional.isPresent()) {
            AnggaranDetail anggaranDetailUpdate = anggaranDetaiOptional.get();
            anggaranDetailUpdate.setTemplateJurnal(codeTemp);
            anggaranDetailDao.save(anggaranDetailUpdate);

            return ResponseEntity.ok()
                .body(BaseResponseApiDto.builder()
                .responseMessage("success").build());
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/pilihan")
    public ResponseEntity<BaseResponseApiDto> updatePilihan(@RequestParam String id, @RequestParam PilihanEnum pilihan) {
        Optional<AnggaranDetail> anggaranDetaiOptional = anggaranDetailDao.findById(id);
        if (anggaranDetaiOptional.isPresent()) {
            AnggaranDetail anggaranDetailUpdate = anggaranDetaiOptional.get();
            anggaranDetailUpdate.setPilihan(pilihan);
            anggaranDetailDao.save(anggaranDetailUpdate);

            return ResponseEntity.ok()
                .body(BaseResponseApiDto.builder()
                .responseMessage("success").build());
        }else {
            return ResponseEntity.notFound().build();
        }
    }
    
    
    
    

}
