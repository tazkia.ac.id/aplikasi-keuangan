package id.ac.tazkia.aplikasikeuangan.api;


import id.ac.tazkia.aplikasikeuangan.dao.transaksi.LaporanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.*;
import id.ac.tazkia.aplikasikeuangan.entity.StatusAccounting;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Controller@Slf4j
public class AccountingApi {

    @Autowired
    @Value("${api.akunting}")
    private String lokasiAkunting;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LaporanDanaDao laporanDanaDao;

    @Autowired
    private PencairanDanaDao pencairanDanaDao;

    WebClient webClient = WebClient.builder()
            .baseUrl(lokasiAkunting)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    @PostMapping("/accounting/post")
    public String integrasiAkunting(@Valid SaveAccountingDto saveAccountingDto, RedirectAttributes redirectAttributes) {
        if (saveAccountingDto.getIdPencairan() != null) {
            Optional<PencairanDana> pencairanDanaOpt = pencairanDanaDao.findById(saveAccountingDto.getIdPencairan());
            if (pencairanDanaOpt.isPresent()) {
                PencairanDana pencairanDana = pencairanDanaOpt.get();
                pencairanDana.setStatusAkunting(StatusAccounting.POSTED);
                pencairanDanaDao.save(pencairanDana); // Simpan status pencairan dana

                SavedAccountingDto res = prosesIntegrasi(saveAccountingDto, pencairanDana.getTanggalPencairan().toLocalDate());
                hasilIntegrasi(saveAccountingDto, res, redirectAttributes);
            } else {
                // Jika pencairan dana tidak ditemukan, set error
                redirectAttributes.addFlashAttribute("error", "Pencairan dana tidak ditemukan.");
                return "redirect:/reports/dailycash?tahun=" + saveAccountingDto.getTahunSelected() + "&bulan=" + saveAccountingDto.getBulanSelected();
            }
        }

        if (saveAccountingDto.getIdLaporanDana() != null) {
            Optional<LaporanDana> laporanDanaOpt = laporanDanaDao.findById(saveAccountingDto.getIdLaporanDana());
            if (laporanDanaOpt.isPresent()) {
                LaporanDana laporanDana = laporanDanaOpt.get();
                laporanDana.setPosting(LaporanDana.Posting.POSTED);
                laporanDanaDao.save(laporanDana); // Simpan status laporan dana

                SavedAccountingDto res = prosesIntegrasi(saveAccountingDto, laporanDana.getTanggalLaporan().toLocalDate());
                hasilIntegrasi(saveAccountingDto, res, redirectAttributes);
            } else {
                redirectAttributes.addFlashAttribute("error", "Laporan dana tidak ditemukan.");
                return "redirect:/reports/dailycash?tahun=" + saveAccountingDto.getTahunSelected() + "&bulan=" + saveAccountingDto.getBulanSelected();
            }
        }

        return "redirect:/reports/dailycash?tahun=" + saveAccountingDto.getTahunSelected() + "&bulan=" + saveAccountingDto.getBulanSelected();
    }

    private RedirectAttributes hasilIntegrasi(SaveAccountingDto saveAccountingDto, SavedAccountingDto res, RedirectAttributes redirectAttributes) {
        if (Objects.equals(res.getResponseCode(), "200")) {
            redirectAttributes.addFlashAttribute("success", res.getResponseMessage());
        } else {
            // Jika terjadi error dari API, batalkan update status
            batalUpdateStatusPosting(saveAccountingDto);
            redirectAttributes.addFlashAttribute("error", res.getResponseMessage());
        }
        return redirectAttributes;
    }

    private void batalUpdateStatusPosting(SaveAccountingDto saveAccountingDto) {
        // Pembatalan status pencairan dana jika terjadi error
        if (saveAccountingDto.getIdPencairan() != null) {
            PencairanDana pencairanDana = pencairanDanaDao.findById(saveAccountingDto.getIdPencairan()).get();
            pencairanDana.setStatusAkunting(StatusAccounting.WAITING);
            pencairanDanaDao.save(pencairanDana);
            log.info("Batal update status pencairan dana : " + pencairanDana.getKeteranganPencairan() + " - jadi masih : " + pencairanDana.getStatusAkunting());
        }

        // Pembatalan status laporan dana jika terjadi error
        if (saveAccountingDto.getIdLaporanDana() != null) {
            LaporanDana laporanDana = laporanDanaDao.findById(saveAccountingDto.getIdLaporanDana()).get();
            laporanDana.setPosting(LaporanDana.Posting.WAITING);
            laporanDanaDao.save(laporanDana);
            log.info("Batal update status laporan dana : " + laporanDana.getKeterangan() + " - jadi masih : " + laporanDana.getPosting());
        }
    }

    private SavedAccountingDto prosesIntegrasi(SaveAccountingDto saveAccountingDto, LocalDate dateTransaction) {
        String apiUrlSatu = lokasiAkunting + "/public/api/journal-template?id=" + saveAccountingDto.getJournalTemplate();
        JournalTemplateResponSatuDto responseSatu = restTemplate.getForObject(apiUrlSatu, JournalTemplateResponSatuDto.class);
        JournalTemplateDto journalTemplateDto = responseSatu != null ? responseSatu.getData() : null;

        ArrayList<BigDecimal> amounts = new ArrayList<>();
        for (String data : saveAccountingDto.getAmounts()) {
            String amountSting = cleanAndFormatValue(data);
            BigDecimal amount = new BigDecimal(amountSting);
            amounts.add(amount);
        }

        SavedAccountingDto savedAccountingDto = new SavedAccountingDto();
        savedAccountingDto.setCodeTemplate(journalTemplateDto.getCode());
        savedAccountingDto.setDateTransaction(dateTransaction);
        savedAccountingDto.setDescription(saveAccountingDto.getDescription());
        savedAccountingDto.setInstitut("bed6a06c-afe9-4345-8b3f-ebe2cbb49a51");
        savedAccountingDto.setAmounts(amounts);

        // Mengirim data ke API
        SavedAccountingDto res = webClient.post()
                .uri(lokasiAkunting + "/public/api/journal-array")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(savedAccountingDto)
                .retrieve()
                .bodyToMono(SavedAccountingDto.class)
                .block();

        log.info("Log Integrasi : " + res);
        return res;
    }

    public String cleanAndFormatValue(String value) {
        if (value == null || value.trim().isEmpty()) {
            return "0";
        }
        // Remove thousand separators ('.') but keep decimal point (',')
        value = value.replaceAll("\\.(?=.*\\d)", "");
        // Replace decimal point (',') with '.'
        value = value.replace(",", ".");
        return value;
    }
}
