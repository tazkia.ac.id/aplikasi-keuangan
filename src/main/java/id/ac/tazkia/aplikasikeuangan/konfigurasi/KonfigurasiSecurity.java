package id.ac.tazkia.aplikasikeuangan.konfigurasi;

import id.ac.tazkia.aplikasikeuangan.dao.config.UserDao;
import id.ac.tazkia.aplikasikeuangan.entity.config.Permission;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
// import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
// import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.extras.springsecurity6.dialect.SpringSecurityDialect;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
// @EnableGlobalMethodSecurity(prePostEnabled = true)
public class KonfigurasiSecurity{

    @Autowired
    private UserDao userDao;


    @Bean @Order(2)
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth
                .requestMatchers("/login",
                "/bower_components/**",
                "/build/**",
                "/dist/**",
                "/css/**",
                "/acet/**",
                "/bootstrap52/**",
                "/documentation/**",
                "/plugins/**",
                "/",
                "/404",
                "/api/dashboard/keuangan",
                "/api/pengajuandana/setahun",
                "/api/transaksi/setahun",
                "/api/transaksi/enambulan",
                "/images/**").permitAll()
                                .requestMatchers("/setting/pagu").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
                .requestMatchers("/keuangan/pencairan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
                .requestMatchers("/transaksi/penerimaanreal").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_REVENUES")
                .requestMatchers("/setting/rencanapenerimaan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_REVENUES")
                .requestMatchers("/setting/paguanggaran").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER")
                .requestMatchers("/setting/penerimaan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_REVENUES")
                .requestMatchers("/setting/picanggaran").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
                .requestMatchers("/setting/periodeanggaran").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
                .requestMatchers("/masterdata/karyawan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_SDM")
                .requestMatchers("/masterdata/departemen").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_SDM")
                .requestMatchers("/masterdata/jabatan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_SDM")
                .requestMatchers("/masterdata/user").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_SDM")
                .requestMatchers("/masterdata/mataanggaran").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
                .requestMatchers("/transaksi/kaskecil").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
                .requestMatchers("/reports/estimasi_revenue").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                .requestMatchers("/reports/estimasi").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                .requestMatchers("/reports/dailycash").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                .requestMatchers("/reports/dailycash/home").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                .requestMatchers("/reports/disbursement").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                .requestMatchers("/reports/disbursementfund").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                .requestMatchers("/reports/requestfund").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                .requestMatchers("/reports/realizedrevenues").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                .requestMatchers("/reports/dashboard").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
                        .anyRequest().authenticated()
                )
                .headers().frameOptions().sameOrigin()
                .and().logout().permitAll()
                .and().oauth2Login().loginPage("/login").permitAll()
                .userInfoEndpoint()
                .userAuthoritiesMapper(authoritiesMapper())
                .and().defaultSuccessUrl("/dashboard", true);
        return http.build();
    }

    // @Override
    // protected void configure(HttpSecurity http) throws Exception {
    //     http
    //             .authorizeRequests()
    //             .antMatchers("/setting/pagu").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
    //             .antMatchers("/keuangan/pencairan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
    //             .antMatchers("/transaksi/penerimaanreal").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_REVENUES")
    //             .antMatchers("/setting/rencanapenerimaan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_REVENUES")
    //             .antMatchers("/setting/paguanggaran").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER")
    //             .antMatchers("/setting/penerimaan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_REVENUES")
    //             .antMatchers("/setting/picanggaran").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
    //             .antMatchers("/setting/periodeanggaran").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
    //             .antMatchers("/masterdata/karyawan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_SDM")
    //             .antMatchers("/masterdata/departemen").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_SDM")
    //             .antMatchers("/masterdata/jabatan").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_SDM")
    //             .antMatchers("/masterdata/user").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_SDM")
    //             .antMatchers("/masterdata/mataanggaran").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
    //             .antMatchers("/transaksi/kaskecil").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN")
    //             .antMatchers("/reports/estimasi_revenue").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .antMatchers("/reports/estimasi").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .antMatchers("/reports/dailycash").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .antMatchers("/reports/dailycash/home").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .antMatchers("/reports/disbursement").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .antMatchers("/reports/disbursementfund").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .antMatchers("/reports/requestfund").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .antMatchers("/reports/realizedrevenues").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .antMatchers("/reports/dashboard").hasAnyAuthority("VIEW_WAREK2","VIEW_SUPERUSER","VIEW_KEUANGAN","VIEW_AUDIT")
    //             .anyRequest().authenticated()
    //             .and().logout().permitAll()
    //             .and().oauth2Login().loginPage("/login").permitAll()
    //             .userInfoEndpoint()
    //             .userAuthoritiesMapper(authoritiesMapper())
    //             .and().defaultSuccessUrl("/dashboard", true);
    // }

    private GrantedAuthoritiesMapper authoritiesMapper(){
        return (authorities) -> {
            String emailAttrName = "email";
            String email = authorities.stream()
                    .filter(OAuth2UserAuthority.class::isInstance)
                    .map(OAuth2UserAuthority.class::cast)
                    .filter(userAuthority -> userAuthority.getAttributes().containsKey(emailAttrName))
                    .map(userAuthority -> userAuthority.getAttributes().get(emailAttrName).toString())
                    .findFirst()
                    .orElse(null);

            if (email == null) {
                return authorities;     // data email tidak ada di userInfo dari Google
            }

            User user = userDao.findByUsername(email);
            if(user == null) {
                throw new IllegalStateException("Email "+email+" tidak ada dalam database");
//                return null;
//                return authorities;     // email user ini belum terdaftar di database
            }

            Set<Permission> userAuthorities = user.getRole().getPermissions();
            if (userAuthorities.isEmpty()) {
                return authorities;     // authorities defaultnya ROLE_USER
            }

            return Stream.concat(
                    authorities.stream(),
                    userAuthorities.stream()
                            .map(Permission::getValue)
                            .map(SimpleGrantedAuthority::new)
            ).collect(Collectors.toCollection(ArrayList::new));
        };
    }

    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }

    // @Override
    // public void configure(WebSecurity web) {
    //     web.ignoring()
    //             .antMatchers("/login")
    //             .antMatchers("/bower_components/**")
    //             .antMatchers("/build/**")
    //             .antMatchers("/dist/**")
    //             .antMatchers("/css/**")
    //             .antMatchers("/acet/**")
    //             .antMatchers("/bootstrap52/**")
    //             .antMatchers("/documentation/**")
    //             .antMatchers("/plugins/**")
    //             .antMatchers("/")
    //             .antMatchers("/404")
    //             .antMatchers("/api/dashboard/keuangan")
    //             .antMatchers("/api/pengajuandana/setahun")
    //             .antMatchers("/api/transaksi/setahun")
    //             .antMatchers("/api/transaksi/enambulan")
    //             .antMatchers("/images/**");
    // }


    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
