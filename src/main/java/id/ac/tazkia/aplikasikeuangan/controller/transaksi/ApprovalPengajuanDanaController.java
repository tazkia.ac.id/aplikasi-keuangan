package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.SettingApprovalPengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaApproveDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.SaldoBankDao;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.EmailDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.SisaSaldoBankDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.KaryawanJabatan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDanaApprove;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import id.ac.tazkia.aplikasikeuangan.services.GmailApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ApprovalPengajuanDanaController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PengajuanDanaApproveDao pengajuanDanaApproveDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private SettingApprovalPengajuanDanaDao settingApprovalPengajuanDanaDao;

    @Autowired
    private GmailApiService gmailApiService;

    @Autowired
    private MustacheFactory mustacheFactory;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;


    @GetMapping("/transaksi/approval/pengajuandana/detail")
    public String detailApprovalPengajuanDana(Model model,
                                              @RequestParam(required = false)String detail,
                                              @PageableDefault(size = 10) Pageable page,
                                              @RequestParam(required = false) String search,
                                              @RequestParam(required = false)String id){

        PengajuanDanaApprove pengajuanDanaApprove = pengajuanDanaApproveDao.findById(id).get();
        model.addAttribute("pengajuanDanaApprove", pengajuanDanaApprove);

        model.addAttribute("transactions", "active");
        model.addAttribute("approvalf", "active");
        model.addAttribute("approvalRequest", "active");
        model.addAttribute("page", page);
        model.addAttribute("search", search);
        return "transaksi/approval/pengajuandana/detail";

    }

    @GetMapping("/history/detail/penerima")
    public String historyDetailPenerima(Model model,
                                        @PageableDefault(size = 20) Pageable page,
                                        @RequestParam(required = true) Karyawan karyawan,
                                        @RequestParam(required = true) PengajuanDana pengajuanDana){

        List<StatusRecord> statusRecords = new ArrayList<>();
        statusRecords.add(StatusRecord.AKTIF);
        statusRecords.add(StatusRecord.APPROVED);
        statusRecords.add(StatusRecord.CLOSED);

        List<StatusRecord> status = new ArrayList<>();
        status.add(StatusRecord.AKTIF);
        status.add(StatusRecord.WAITING);
        status.add(StatusRecord.APPROVED);

        System.out.println("status : "+ status);

//        List<String> idPengajuanDana = pengajuanDanaDao.listHistory(status, pengajuanDana.getId(), karyawan.getId());

        model.addAttribute("listHistory", pengajuanDanaDao.findByStatusInAndRequestForOrderByTanggalPengajuanDesc(status, karyawan, page));
        model.addAttribute("karyawan", karyawan);

        return "history/detail_penerima";
    }

    @PostMapping("/transaksi/approval/pengajuandana/revisi")
    public String revisiPengajuanDanaApproval(@RequestParam PengajuanDanaApprove pengajuanDanaApprove,
                                              @ModelAttribute PengajuanDana pengajuanDana,
                                              @RequestParam(required = false) Integer page,
                                               @RequestParam(required = false) Integer size,
                                               @RequestParam(required = false) String search,
                                               Authentication authentication,
                                              RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        PengajuanDana pengajuanDana1 = pengajuanDanaDao.findById(pengajuanDanaApprove.getPengajuanDana().getId()).get();

        pengajuanDanaApprove.setStatusApprove(StatusRecord.APPROVED);
        pengajuanDanaApprove.setTanggalApproval(LocalDateTime.now().plusHours(7).toString());
        pengajuanDanaApprove.setTanggalApprove(LocalDateTime.now().plusHours(7));
        pengajuanDanaApprove.setUserApprove(karyawan);
        pengajuanDanaApproveDao.save(pengajuanDanaApprove);


        if(pengajuanDana.getAmount()!= null) {
            if(pengajuanDana.getKuantitas()!= null) {
                BigDecimal getSisa = anggaranDetailDao.getSisaDetailAnggaran(pengajuanDana1.getAnggaranDetail().getId());
                if (getSisa.compareTo(BigDecimal.ZERO) < 0){
                    attribute.addFlashAttribute("gagal", "Save Data Gagal");
                    return "redirect:../pengajuandana?page"+ page;
                }
                BigDecimal jumlah = pengajuanDana.getAmount().multiply(pengajuanDana.getKuantitas());
                BigDecimal jumlah2 = jumlah.subtract(pengajuanDana1.getJumlah());
                if (getSisa.subtract(jumlah2).compareTo(BigDecimal.ZERO) < 0){
                    attribute.addFlashAttribute("gagal", "Save Data Gagal");
                    return "redirect:../pengajuandana?page"+ page;
                }
                pengajuanDana1.setAmount(pengajuanDana.getAmount());
                pengajuanDana1.setKuantitas(pengajuanDana.getKuantitas());
                pengajuanDana1.setJumlah(pengajuanDana.getAmount().multiply(pengajuanDana.getKuantitas()));
            }else{
                BigDecimal getSisa = anggaranDetailDao.getSisaDetailAnggaran(pengajuanDana1.getAnggaranDetail().getId());
                if (getSisa.compareTo(BigDecimal.ZERO) < 0){
                    attribute.addFlashAttribute("gagal", "Save Data Gagal");
                    return "redirect:../pengajuandana?page"+ page;
                }
                BigDecimal jumlah = pengajuanDana.getAmount().multiply(pengajuanDana1.getKuantitas());
                BigDecimal jumlah2 = jumlah.subtract(pengajuanDana1.getJumlah());
                if (getSisa.subtract(jumlah2).compareTo(BigDecimal.ZERO) < 0){
                    attribute.addFlashAttribute("gagal", "Save Data Gagal");
                    return "redirect:../pengajuandana?page"+ page;
                }
                pengajuanDana1.setAmount(pengajuanDana.getAmount());
                pengajuanDana1.setJumlah(pengajuanDana.getAmount().multiply(pengajuanDana1.getKuantitas()));
            }
        }else{
            if(pengajuanDana.getKuantitas()!= null) {
                BigDecimal getSisa = anggaranDetailDao.getSisaDetailAnggaran(pengajuanDana1.getAnggaranDetail().getId());
                if (getSisa.compareTo(BigDecimal.ZERO) < 0){
                    attribute.addFlashAttribute("gagal", "Save Data Gagal");
                    return "redirect:../pengajuandana?page"+ page;
                }
                BigDecimal jumlah = pengajuanDana1.getAmount().multiply(pengajuanDana.getKuantitas());
                BigDecimal jumlah2 = jumlah.subtract(pengajuanDana1.getJumlah());
                if (getSisa.subtract(jumlah2).compareTo(BigDecimal.ZERO) < 0){
                    attribute.addFlashAttribute("gagal", "Save Data Gagal");
                    return "redirect:../pengajuandana?page"+ page;
                }
                pengajuanDana1.setKuantitas(pengajuanDana.getKuantitas());
                pengajuanDana1.setJumlah(pengajuanDana1.getAmount().multiply(pengajuanDana.getKuantitas()));
            }
        }


        Integer nomorUrut = settingApprovalPengajuanDanaDao.nomorUrut(pengajuanDanaApprove.getPengajuanDana().getId());
        if (nomorUrut > 0){
            pengajuanDana1.setNomorUrut(nomorUrut);
        }else {
            pengajuanDana1.setNomorUrut(pengajuanDanaApprove.getNomorUrut() + 1);
        }
//        pengajuanDana1.setNomorUrut(pengajuanDanaApprove.getNomorUrut()+1);
        pengajuanDanaDao.save(pengajuanDana1);


//        return "redirect:../pengajuandana?page"+ page;

        if(page != null || size > 0) {
            if(search == null) {
                return "redirect:../pengajuandana?page=" + page + "&size="+ size;
            }else{
                return "redirect:../pengajuandana?page=" + page + "&size="+ size + "&search=" + search;
            }
        }else{
            if(search == null) {
                return "redirect:../pengajuandana";
            }else{
                return "redirect:../pengajuandana?search=" + search;
            }
        }
    }

    @PostMapping("/transaksi/approval/pengajuandana/approve_checked")
    public String approvePengajuanDanaApproval(@RequestParam(required = false) List<String> request,
                                               @RequestParam(required = false) Integer page,
                                               @RequestParam(required = false) Integer size,
                                               @RequestParam(required = false) String search,
                                               RedirectAttributes redirectAttributes,
                                               Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        if(request == null){

            redirectAttributes.addFlashAttribute("status", "Belum ada pengajuan yang di centang");
            if(page != null || size > 0) {
                if(search == null) {
                    return "redirect:../pengajuandana_check?page=" + page + "&size="+ size;
                }else{
                    return "redirect:../pengajuandana_check?page=" + page + "&size="+ size + "&search=" + search;
                }
            }else{
                if(search == null) {
                    return "redirect:../pengajuandana_check";
                }else{
                    return "redirect:../pengajuandana_check?search=" + search;
                }
            } 
        }

        if (request != null) {
            BigDecimal pengajuanJml = BigDecimal.ZERO;
            String idPeriode = "";
//            for(String req2 : request){
//
//                PengajuanDanaApprove pengajuanDanaApprove = pengajuanDanaApproveDao.findByStatusAndId(StatusRecord.AKTIF, req2);
//                String idPengajuanDana = pengajuanDanaApprove.getPengajuanDana().getId();
//                PengajuanDana pengajuanDana = pengajuanDanaDao.findById(idPengajuanDana).get();
//
//                if(pengajuanDana.getNomorUrut().equals(pengajuanDana.getTotalNomorUrut())) {
//                    pengajuanJml = pengajuanJml.add(pengajuanDana.getJumlah());
//                    idPeriode = pengajuanDana.getPeriodeAnggaran().getId();
//                }
//            }

//            SisaSaldoBankDto sisaSaldoBankDto = saldoBankDao.sisaSaldoBank(idPeriode, );





            for (String req : request) {
                PengajuanDanaApprove pengajuanDanaApprove = pengajuanDanaApproveDao.findByStatusAndId(StatusRecord.AKTIF, req);
                String idPengajuanDana = pengajuanDanaApprove.getPengajuanDana().getId();
                PengajuanDana pengajuanDana = pengajuanDanaDao.findById(idPengajuanDana).get();
                SisaSaldoBankDto sisaSaldoBankDto = saldoBankDao.sisaSaldoBank(pengajuanDana.getAnggaranDetail().getAnggaran().getPeriodeAnggaran().getId(), pengajuanDana.getAnggaranDetail().getAnggaran().getDepartemen().getInstansi().getId());

                if (sisaSaldoBankDto.getSisa().subtract(pengajuanJml).compareTo(BigDecimal.ZERO) < 0) {

                    redirectAttributes.addFlashAttribute("gagale", "Data pengajuan dana dengan keterangan " + pengajuanDana.getDeskripsiBiaya() + " tidak bisa diapprove karena sudah melebihi saldo bank pada instansi terkait ");
                    return "redirect:/pengajuandana_check";
                }

                pengajuanDanaApprove.setStatusApprove(StatusRecord.APPROVED);
                pengajuanDanaApprove.setTanggalApproval(LocalDateTime.now().plusHours(7).toString());
                pengajuanDanaApprove.setTanggalApprove(LocalDateTime.now().plusHours(7));
                pengajuanDanaApprove.setUserApprove(karyawan);

                System.out.println("karyawan : " + karyawan.getId());

                pengajuanDanaApproveDao.save(pengajuanDanaApprove);
                Integer nomorUrut = settingApprovalPengajuanDanaDao.nomorUrut(pengajuanDanaApprove.getPengajuanDana().getId());

                if (nomorUrut > 0) {
                    pengajuanDana.setNomorUrut(nomorUrut);
                } else {
                    pengajuanDana.setNomorUrut(pengajuanDanaApprove.getNomorUrut() + 1);
                }
                pengajuanDanaDao.save(pengajuanDana);

                //EmailDto pengajuanDanaApproves = pengajuanDanaApproveDao.pengajuanDanaNotifikasi(pengajuanDana.getId());
                if (nomorUrut > 0) {
                    PengajuanDanaApprove pengajuanDanaApprove1 = pengajuanDanaApproveDao.findTopByPengajuanDanaAndNomorUrutAndStatusAndStatusApprove(pengajuanDana, nomorUrut, StatusRecord.AKTIF, StatusRecord.WAITING);

                    Mustache templateEmail = mustacheFactory.compile("invoice.html");
                    Map<String, String> data = new HashMap<>();
                    data.put("nama", pengajuanDanaApprove1.getUserApprove().getNamaKaryawan());
                    data.put("item", pengajuanDanaApprove1.getPengajuanDana().getDeskripsiBiaya());
                    data.put("kuantitas", pengajuanDanaApprove1.getPengajuanDana().getKuantitas().toString());
                    data.put("amount", pengajuanDanaApprove1.getPengajuanDana().getAmount().toString());
                    data.put("satuan", pengajuanDanaApprove1.getPengajuanDana().getSatuan());
                    data.put("jumlah", pengajuanDanaApprove1.getPengajuanDana().getJumlah().toString());
                    data.put("jabatan", pengajuanDanaApprove1.getJabatanApprove().getNamaJabatan());
                    data.put("tanggal", pengajuanDanaApprove1.getPengajuanDana().getTanggalPengajuan().toString());
                    data.put("anggaran", pengajuanDanaApprove1.getPengajuanDana().getAnggaranDetail().getDeskripsi());
                    data.put("id", pengajuanDana.getId());
                    data.put("dari", pengajuanDanaApprove1.getPengajuanDana().getKaryawanPengaju().getNamaKaryawan());
                    data.put("departemendari", pengajuanDanaApprove1.getPengajuanDana().getAnggaranDetail().getAnggaran().getDepartemen().getNamaDepartemen());

                    StringWriter output = new StringWriter();
                    templateEmail.execute(output, data);

                    gmailApiService.kirimEmail(
                            "Notifikasi Pengajuan Dana",
                            pengajuanDanaApprove1.getUserApprove().getEmail(),
                            "Approval Pengajuan dana",
                            output.toString());

                }

                if (nomorUrut > pengajuanDana.getTotalNomorUrut()) {

                    Mustache templateEmail = mustacheFactory.compile("approve.html");
                    Map<String, String> data = new HashMap<>();
                    data.put("nama", pengajuanDana.getKaryawanPengaju().getNamaKaryawan());
                    data.put("item", pengajuanDana.getDeskripsiBiaya());
                    data.put("kuantitas", pengajuanDana.getKuantitas().toString());
                    data.put("amount", pengajuanDana.getAmount().toString());
                    data.put("satuan", pengajuanDana.getSatuan());
                    data.put("jumlah", pengajuanDana.getJumlah().toString());
                    data.put("jabatan", "");
                    data.put("tanggal", pengajuanDana.getTanggalPengajuan().toString());
                    data.put("anggaran", pengajuanDana.getAnggaranDetail().getDeskripsi());
                    data.put("id", pengajuanDana.getId());
                    data.put("dari", pengajuanDana.getKaryawanPengaju().getNamaKaryawan());
                    data.put("departemendari", pengajuanDana.getAnggaranDetail().getAnggaran().getDepartemen().getNamaDepartemen());

                    StringWriter output = new StringWriter();
                    templateEmail.execute(output, data);

                    gmailApiService.kirimEmail(
                            "Notifikasi Pengajuan Dana",
                            pengajuanDana.getKaryawanPengaju().getEmail(),
                            "Approval Pengajuan dana",
                            output.toString());

                }
            }
        }
        if(page != null || size > 0) {
            if(search == null) {
                return "redirect:../pengajuandana_check?page=" + page + "&size="+ size;
            }else{
                return "redirect:../pengajuandana_check?page=" + page + "&size="+ size + "&search=" + search;
            }
        }else{
            if(search == null) {
                return "redirect:../pengajuandana_check";
            }else{
                return "redirect:../pengajuandana_check?search=" + search;
            }
        }

//        return "redirect:../pengajuandana_check?page"+ page;
    }

    @Autowired
    private SaldoBankDao saldoBankDao;

    @PostMapping("/transaksi/approval/pengajuandana/approve")
    public String approvePengajuanDanaApproval(@RequestParam PengajuanDanaApprove pengajuanDanaApprove,
                                               @RequestParam(required = false) Integer page,
                                               @RequestParam(required = false) Integer size,
                                               @RequestParam (required = false) String search,
                                               RedirectAttributes redirectAttributes,
                                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String idPengajuanDana=pengajuanDanaApprove.getPengajuanDana().getId();
        PengajuanDana pengajuanDana = pengajuanDanaDao.findById(idPengajuanDana).get();

        SisaSaldoBankDto sisaSaldoBankDto = saldoBankDao.sisaSaldoBank(pengajuanDana.getAnggaranDetail().getAnggaran().getPeriodeAnggaran().getId(), pengajuanDana.getAnggaranDetail().getAnggaran().getDepartemen().getInstansi().getId());

        if(pengajuanDana.getNomorUrut().equals(pengajuanDana.getTotalNomorUrut())) {
            if (sisaSaldoBankDto.getSisa().subtract(pengajuanDana.getJumlah()).compareTo(BigDecimal.ZERO) < 0) {

                redirectAttributes.addFlashAttribute("gagale", "Approval gagal sudah melebihi saldo yang tersedia");
                return "redirect:../pengajuandana";
            }
        }

        pengajuanDanaApprove.setStatusApprove(StatusRecord.APPROVED);
        pengajuanDanaApprove.setTanggalApproval(LocalDateTime.now().plusHours(7).toString());
        pengajuanDanaApprove.setTanggalApprove(LocalDateTime.now().plusHours(7));
        pengajuanDanaApprove.setUserApprove(karyawan);

        System.out.println("karyawan : "+ karyawan.getId());

        pengajuanDanaApproveDao.save(pengajuanDanaApprove);
        Integer nomorUrut = settingApprovalPengajuanDanaDao.nomorUrut(pengajuanDanaApprove.getPengajuanDana().getId());

        if (nomorUrut > 0){
            pengajuanDana.setNomorUrut(nomorUrut);
        }else {
            pengajuanDana.setNomorUrut(pengajuanDanaApprove.getNomorUrut() + 1);
        }
        pengajuanDanaDao.save(pengajuanDana);

            //EmailDto pengajuanDanaApproves = pengajuanDanaApproveDao.pengajuanDanaNotifikasi(pengajuanDana.getId());
        if (nomorUrut > 0) {
            PengajuanDanaApprove pengajuanDanaApprove1 = pengajuanDanaApproveDao.findTopByPengajuanDanaAndNomorUrutAndStatusAndStatusApprove(pengajuanDana, nomorUrut, StatusRecord.AKTIF, StatusRecord.WAITING);

            Mustache templateEmail = mustacheFactory.compile("invoice.html");
            Map<String, String> data = new HashMap<>();
            data.put("nama", pengajuanDanaApprove1.getUserApprove().getNamaKaryawan());
            data.put("item", pengajuanDanaApprove1.getPengajuanDana().getDeskripsiBiaya());
            data.put("kuantitas", pengajuanDanaApprove1.getPengajuanDana().getKuantitas().toString());
            data.put("amount", pengajuanDanaApprove1.getPengajuanDana().getAmount().toString());
            data.put("satuan", pengajuanDanaApprove1.getPengajuanDana().getSatuan());
            data.put("jumlah", pengajuanDanaApprove1.getPengajuanDana().getJumlah().toString());
            data.put("jabatan", pengajuanDanaApprove1.getJabatanApprove().getNamaJabatan());
            data.put("tanggal", pengajuanDanaApprove1.getPengajuanDana().getTanggalPengajuan().toString());
            data.put("anggaran", pengajuanDanaApprove1.getPengajuanDana().getAnggaranDetail().getDeskripsi());
            data.put("id", pengajuanDana.getId());
            data.put("dari", pengajuanDanaApprove1.getPengajuanDana().getKaryawanPengaju().getNamaKaryawan());
            data.put("departemendari", pengajuanDanaApprove1.getPengajuanDana().getAnggaranDetail().getAnggaran().getDepartemen().getNamaDepartemen());

            StringWriter output = new StringWriter();
            templateEmail.execute(output, data);

            gmailApiService.kirimEmail(
                    "Notifikasi Pengajuan Dana",
                    pengajuanDanaApprove1.getUserApprove().getEmail(),
                    "Approval Pengajuan dana",
                    output.toString());

        }

        if (nomorUrut > pengajuanDana.getTotalNomorUrut()) {

            Mustache templateEmail = mustacheFactory.compile("approve.html");
            Map<String, String> data = new HashMap<>();
            data.put("nama", pengajuanDana.getKaryawanPengaju().getNamaKaryawan());
            data.put("item", pengajuanDana.getDeskripsiBiaya());
            data.put("kuantitas", pengajuanDana.getKuantitas().toString());
            data.put("amount", pengajuanDana.getAmount().toString());
            data.put("satuan", pengajuanDana.getSatuan());
            data.put("jumlah", pengajuanDana.getJumlah().toString());
            data.put("jabatan", "");
            data.put("tanggal", pengajuanDana.getTanggalPengajuan().toString());
            data.put("anggaran", pengajuanDana.getAnggaranDetail().getDeskripsi());
            data.put("id", pengajuanDana.getId());
            data.put("dari", pengajuanDana.getKaryawanPengaju().getNamaKaryawan());
            data.put("departemendari", pengajuanDana.getAnggaranDetail().getAnggaran().getDepartemen().getNamaDepartemen());

            StringWriter output = new StringWriter();
            templateEmail.execute(output, data);

            gmailApiService.kirimEmail(
                    "Notifikasi Pengajuan Dana",
                   pengajuanDana.getKaryawanPengaju().getEmail(),
                    "Approval Pengajuan dana",
                    output.toString());

        }

        System.out.println("page = "+ page);

        if(page != null || size > 0) {
            if(search == null) {
                return "redirect:../pengajuandana?page=" + page + "&size="+ size;
            }else{
                return "redirect:../pengajuandana?page=" + page + "&size="+ size + "&search=" + search;
            }
        }else{
            if(search == null) {
                return "redirect:../pengajuandana";
            }else{
                return "redirect:../pengajuandana?search=" + search;
            }
        }

    }

    @PostMapping("/transaksi/approval/pengajuandana/reject")
    public String approvePengajuanDanaReject(@RequestParam PengajuanDanaApprove pengajuanDanaApprove1,
                                             @RequestParam(required = false) Integer page,
                                             @RequestParam(required = false) Integer size,
                                             @RequestParam(required = false) String search,
                                             @ModelAttribute PengajuanDanaApprove pengajuanDanaApprove,
                                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String idPengajuanDana=pengajuanDanaApprove1.getPengajuanDana().getId();
        PengajuanDana pengajuanDana = pengajuanDanaDao.findById(idPengajuanDana).get();

        pengajuanDanaApprove1.setStatusApprove(StatusRecord.REJECTED);
        pengajuanDanaApprove1.setKeterangan(pengajuanDanaApprove.getKeterangan());
        pengajuanDanaApprove1.setTanggalApproval(LocalDateTime.now().plusHours(7).toString());
        pengajuanDanaApprove1.setTanggalApprove(LocalDateTime.now().plusHours(7));
        pengajuanDanaApprove1.setUserApprove(karyawan);

        System.out.println("karyawan : "+ karyawan.getId());

        pengajuanDanaApproveDao.save(pengajuanDanaApprove1);

        pengajuanDana.setNomorUrut(0);
        pengajuanDana.setStatus(StatusRecord.REJECTED);
        pengajuanDanaDao.save(pengajuanDana);

//        return "redirect:../pengajuandana?page="+ page;
        if(page != null || size > 0) {
            if(search == null) {
                return "redirect:../pengajuandana?page=" + page + "&size="+ size;
            }else{
                return "redirect:../pengajuandana?page=" + page + "&size="+ size + "&search=" + search;
            }
        }else{
            if(search == null) {
                return "redirect:../pengajuandana";
            }else{
                return "redirect:../pengajuandana?search=" + search;
            }
        }

    }


    @PostMapping("/transaksi/approval/pengajuandana/rev")
    public String revisiPengajuanDana(Model model,
                                      @RequestParam PengajuanDanaApprove pengajuanDanaApprove,
                                      @RequestParam Integer page,
                                      Authentication authentication){

        PengajuanDana pengajuanDana = pengajuanDanaDao.findById(pengajuanDanaApprove.getPengajuanDana().getId()).get();

        return "../";

    }

}
