package id.ac.tazkia.aplikasikeuangan.controller.keuangan;

import id.ac.tazkia.aplikasikeuangan.controller.transaksi.PengajuanDanaController;
import id.ac.tazkia.aplikasikeuangan.dao.BulanDao;
import id.ac.tazkia.aplikasikeuangan.dao.TahunDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.UrutanDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.*;
import id.ac.tazkia.aplikasikeuangan.entity.Bulan;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.Tahun;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;

import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Urutan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.*;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lowagie.text.DocumentException;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
public class PencairanController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PengajuanDanaController.class);

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private LaporanDanaDao laporanDanaDao;

    @Autowired
    private PencairanDanaDetailDao pencairanDanaDetailDao;

    @Autowired
    @Value("${upload.pencairandanacair}")
    private String uploadFolderCair;

    @Autowired
    @Value("${upload.pengembalian}")
    private String uploadFolderKembali;

    @Autowired
    private PencairanDanaDao pencairanDanaDao;

    @Autowired
    private UrutanDao urutanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private PengembalianDanaProsesDao pengembalianDanaProsesDao;

    @Autowired
    private PengembalianDanaProsesDetailDao pengembalianDanaProsesDetailDao;

    @Autowired
    private PengembalianDanaDao pengembalianDanaDao;

    @GetMapping("/keuangan/pencairan")
    private String pencairanDana(Model model,
                                 @RequestParam(required = false) Urutan urutan,
                                 @RequestParam(required = false) String range,
                                 @PageableDefault(size = 10)Pageable page,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        String nows = sdf.format(now.getTime()).toString();

        model.addAttribute("selectedSorting", urutan);
        model.addAttribute("listSorting", urutanDao.findByStatusOrderByNomorUrut(StatusRecord.AKTIF));
        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
        if(urutan == null){
            if (StringUtils.hasText(range)) {
                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
                model.addAttribute("range", range);
                model.addAttribute("pencairanDana", new PencairanDana());
                model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndTanggalMintaPencairanBetweenAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, localDatea, localDateb, cariIdInstansi, page));
            }else {
                model.addAttribute("pencairanDana", new PencairanDana());
                model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));
            }
        }else {
            if (urutan.getInisial().equalsIgnoreCase("terlama")) {
                model.addAttribute("pencairanDana", new PencairanDana());
                model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("terbaru")){
                model.addAttribute("pencairanDana", new PencairanDana());
                model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairanDesc(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("tertinggi")){
                model.addAttribute("pencairanDana", new PencairanDana());
                model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByNominalPencairanDesc(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("terendah")){
                model.addAttribute("pencairanDana", new PencairanDana());
                model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByNominalPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("departemen")){
                model.addAttribute("pencairanDana", new PencairanDana());
                model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByPengajuanDanaDepartemenNamaDepartemen(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("karyawan")){
                model.addAttribute("pencairanDana", new PencairanDana());
                model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByUserMintaPencairanNamaKaryawan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));
            }else {
                if (StringUtils.hasText(range)) {
                    String datea = range;
                    String tahuna = datea.substring(6, 10);
                    String bulana = datea.substring(0, 2);
                    String tanggala = datea.substring(3, 5);
                    String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                    String dateb = range;
                    String tahunb = dateb.substring(19, 23);
                    String bulanb = dateb.substring(13, 15);
                    String tanggalb = dateb.substring(16, 18);
                    String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);
                    DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
                    model.addAttribute("range", range);
                    model.addAttribute("pencairanDana", new PencairanDana());
                    model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndTanggalMintaPencairanBetweenAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, localDatea, localDateb,cariIdInstansi, page));
                } else {
                    model.addAttribute("pencairanDana", new PencairanDana());
                    model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));
                }
            }
        }

        return "keuangan/pencairan/list";
    }

    @PostMapping("/keuangan/pencairan/reject")
    private String rejectPencairanDana(Model model,
                                       @RequestParam(required = false) PencairanDana pencairanDana1,
                                       @ModelAttribute PencairanDana pencairanDana,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        PencairanDana pencairanDana2 = pencairanDanaDao.findById(pencairanDana1.getId()).get();
        List<PencairanDanaDetail> pencairanDanaDetail = pencairanDanaDetailDao.findByPencairanDana(pencairanDana2);

        pencairanDana2.setStatusCair(StatusRecord.REJECTED);
        pencairanDana2.setStatusPencairan(StatusRecord.REJECTED);
        pencairanDana2.setUserPencairan(karyawan);
        pencairanDana2.setKeteranganPencairan(pencairanDana.getKeteranganPencairan());
        pencairanDanaDao.save(pencairanDana1);

        for(PencairanDanaDetail s : pencairanDanaDetail){
            PengajuanDana pengajuanDana = pengajuanDanaDao.findByStatusAndId(StatusRecord.OPEN, s.getPengajuanDana().getId());
            pengajuanDana.setStatus(StatusRecord.REJECTED);
            pengajuanDanaDao.save(pengajuanDana);
        }

        return "redirect:../pencairan";
    }

    @GetMapping("/keuangan/pencairan/open")
    private String pencairanDanaOpen(Model model, @PageableDefault(size = 10)Pageable page,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));

        model.addAttribute("listPencairanDanaWaiting", pencairanDanaDao.findByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi, page));

        return "keuangan/pencairan/listopen";
    }

    @GetMapping("/keuangan/pencairan/rejected")
    private String pencairanDanaRejected(Model model,
                                         @RequestParam(required = false) Urutan urutan,
                                         @RequestParam(required = false) String range,
                                         @PageableDefault(size = 10)Pageable page,
                                         Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        String nows = sdf.format(now.getTime()).toString();

        model.addAttribute("selectedSorting", urutan);
        model.addAttribute("listSorting", urutanDao.findByStatusOrderByNomorUrut(StatusRecord.AKTIF));

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));

        if(urutan == null){
            if (StringUtils.hasText(range)) {
                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
                model.addAttribute("range", range);
                model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndTanggalMintaPencairanBetweenAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.REJECTED, localDatea, localDateb, cariIdInstansi, page));
            }else {
                model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));
            }
        }else {
            if (urutan.getInisial().equalsIgnoreCase("terlama")) {
                model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("terbaru")){
                model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairanDesc(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("tertinggi")){
                model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByNominalPencairanDesc(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("terendah")){
                model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByNominalPencairan(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("departemen")){
                model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByPengajuanDanaDepartemenNamaDepartemen(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));
            }else if(urutan.getInisial().equalsIgnoreCase("karyawan")){
                model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByUserMintaPencairanNamaKaryawan(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));
            }else {
                if (StringUtils.hasText(range)) {
                    String datea = range;
                    String tahuna = datea.substring(6, 10);
                    String bulana = datea.substring(0, 2);
                    String tanggala = datea.substring(3, 5);
                    String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                    String dateb = range;
                    String tahunb = dateb.substring(19, 23);
                    String bulanb = dateb.substring(13, 15);
                    String tanggalb = dateb.substring(16, 18);
                    String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);
                    DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
                    model.addAttribute("range", range);
                    model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndTanggalMintaPencairanBetweenAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.REJECTED, localDatea, localDateb, cariIdInstansi, page));
                } else {
                    model.addAttribute("listPencairanDanaRejected", pencairanDanaDao.findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));
                }
            }
        }
        return "keuangan/pencairan/listrejected";
    }

    @GetMapping("/keuangan/pencairan/closed")
    private String pencairanDanaClosed(Model model, @PageableDefault(size = 10)Pageable page,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));

//        model.addAttribute("listPencairanDanaClosed", pencairanDanaDao.findByStatusAndStatusCairOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CLOSED, page));

        model.addAttribute("listPelaporanDanaClosed", laporanDanaDao.findByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi, page));

        return "keuangan/pencairan/listclosed";

    }

    @GetMapping("/keuangan/pencairan/waitingreport")
    private String pencairanDanaWairingReports(Model model, @PageableDefault(size = 10)Pageable page,
                                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));


        model.addAttribute("listPencairanDanaClosed", pencairanDanaDao.findByStatusAndStatusPencairanAndStatusCairOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, page));

        return "keuangan/pencairan/listreport";

    }

    @GetMapping("/keuangan/pencairan/report")
    private String pencairanDanaReports(Model model, @PageableDefault(size = 100)Pageable page,
                                        @RequestParam(required = false) Bulan bulan,
                            @RequestParam(required = false) Tahun tahun,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        
        
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        
        
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));


            model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
            model.addAttribute("listBulan", bulanDao.findByStatusOrderByBulan(StatusRecord.AKTIF));
            model.addAttribute("laporanDana", new LaporanDana());

            if (bulan != null && tahun != null){
                model.addAttribute("tahunSelected", tahun);
                model.addAttribute("bulanSelected", bulan);

                Integer bulani = new Integer(bulan.getBulan().toString());
                Integer tahuni = new Integer(tahun.getTahun());

                LocalDate startOfMonth = YearMonth.of(tahuni, bulani).atDay(1);
                LocalDate endOfMonth = YearMonth.of(tahuni, bulani).atEndOfMonth();

                System.out.println("tanggal "+ startOfMonth + " - " + endOfMonth);
        
                LocalDate startDate = LocalDate.of(startOfMonth.getYear(), startOfMonth.getMonth(), startOfMonth.getDayOfMonth());
                LocalDate endDate = LocalDate.of(endOfMonth.getYear(), endOfMonth.getMonth(), endOfMonth.getDayOfMonth());

            // Page<LaporanDana> laporanDanaPage = laporanDanaRepository.findByStatusAndStatusLaporAndTanggalLaporanBetweenAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(
            //     StatusRecord.STATUS_AKTIF, StatusRecord.STATUS_LAPOR, startDate, endDate, instansiList, Pageable.ofSize(10));
                
//                model.addAttribute("listLaporanDana", laporanDanaDao.findByStatusAndStatusLaporAndTanggalLaporanBetweenAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, startDate, endDate, cariIdInstansi, page));

                model.addAttribute("listLaporanDana", laporanDanaDao.tampilkanLaporanDanaTanggal(cariIdInstansi, startDate, endDate, page));
            }else{
// model.addAttribute("listLaporanDana", laporanDanaDao.findByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi, page));

                model.addAttribute("listLaporanDana", laporanDanaDao.tampilkanLaporanDana(cariIdInstansi, page));
            }
        
        
        return "keuangan/pencairan/listwaiting";

    }

    @GetMapping("/pelaporan_bkd/download")
    public void generatePdfFile(HttpServletResponse response,
                                @RequestParam(required = true) LaporanDana laporanDana) throws DocumentException, IOException {

        response.setContentType("application/pdf");

        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD:HH:MM:SS");

        String currentDateTime = dateFormat.format(new Date());

        String headerkey = "Content-Disposition";

        String headervalue = "attachment; filename=Laporan_realisasi_" + laporanDana.getUserPelapor() +"_" + laporanDana.getTanggalLaporan() + ".pdf";

        response.setHeader(headerkey, headervalue);

        LaporanDana laporanDanas = laporanDanaDao.findByStatusAndId(StatusRecord.AKTIF,laporanDana.getId());

//        LaporanDana generator = new LaporanDana();
//
//        generator.generate(laporanDanas, response);

    }

    @PostMapping("/keuangan/laporandana/reject")
    public String rejectLaporanDana(Model model,
                                    @ModelAttribute LaporanDana laporanDana,
                                    @RequestParam(required = false)String laporanDana1,
                                    RedirectAttributes attributes,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        LaporanDana laporanDana2 = laporanDanaDao.findById(laporanDana1).get();
        String kkl = laporanDana.getKeterangan();

        if(kkl.isEmpty()){
            attributes.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:../pencairan/report";
        }

        laporanDana2.setStatusLapor(StatusRecord.REJECTED);
        laporanDana2.setKeterangan(laporanDana.getKeterangan());
        laporanDana2.setUserKonfirmasi(karyawan);
        laporanDanaDao.save(laporanDana2);

        return "redirect:../pencairan/report";
    }

    @GetMapping("/keuangan/pencairan/reportrejected")
    private String pencairanDanaReportsRejected(Model model, @PageableDefault(size = 10)Pageable page,
                                        Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));

//        model.addAttribute("laporanDana", new LaporanDana());
        model.addAttribute("listLaporanDana", laporanDanaDao.findByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi, page));

        return "keuangan/pencairan/reportrejected";

    }

    @GetMapping("/keuangan/pencairan/approval_refund")
    private String approvalRefund(Model model, @PageableDefault(size = 10) Pageable page,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));

        model.addAttribute("pageApprovalRefund", pengembalianDanaProsesDao.pageApprovalRefund(cariIdInstansi, "WAITING", page));
        model.addAttribute("listApprovalRefund", pengembalianDanaProsesDao.ListApprovalRefund(cariIdInstansi,"WAITING"));

        return "keuangan/pencairan/approval_refund";
    }

    @GetMapping("/keuangan/pencairan/refund_closed")
    private String refundClosed(Model model, @PageableDefault(size = 10) Pageable page,
                                  Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));

        model.addAttribute("pageApprovalRefund", pengembalianDanaProsesDao.pageApprovalRefund(cariIdInstansi, "CLOSED", page));
        model.addAttribute("listApprovalRefund", pengembalianDanaProsesDao.ListApprovalRefund(cariIdInstansi,"CLOSED"));

        return "keuangan/pencairan/refund_closed";
    }

    @GetMapping("/keuangan/pencairan/all")
    private String pencairanDanaAll(Model model, @PageableDefault(size = 10)Pageable page,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequest", pencairanDanaDao.countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, cariIdInstansi));
        model.addAttribute("jumlahOpenCair", pencairanDanaDao.countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahClosedPencairan", pencairanDanaDao.countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, StatusRecord.CAIR, cariIdInstansi));
        model.addAttribute("jumlahLaporan", laporanDanaDao.jumlahLaporanWaiting(cariIdInstansi));
        model.addAttribute("jumlahLaporanRejected", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.REJECTED, cariIdInstansi));
//        model.addAttribute("jumlahLaporanClosed", laporanDanaDao.countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord.AKTIF, StatusRecord.CLOSED, cariIdInstansi));
        model.addAttribute("jumlahApprovalRefund", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "WAITING"));
//        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.jumlahRefundWaiting(cariIdInstansi, "CLOSED"));

        return "keuangan/pencairan/listall";

    }

    @GetMapping("/keuangan/pencairan/approve")
    private String approvePencairanDana(Model model,
                                        @RequestParam(required = false)String pencairan){

        PencairanDana pencairanDana = pencairanDanaDao.findById(pencairan).get();
        model.addAttribute("listPencairanDanaDetail", pencairanDanaDetailDao.findByPencairanDana(pencairanDana));
        model.addAttribute("totalPengajuanDana", pencairanDanaDetailDao.totalPengajuanDana(pencairanDana.getId()));
        model.addAttribute("listPencairanDana", pencairanDana);
        model.addAttribute("pencairanDana", pencairanDana);
        return "keuangan/pencairan/form";

    }

    @PostMapping("/keuangan/pencairan/approve/save")
    private  String saveApprovePencairanDana(Model model,
                                             @ModelAttribute @Valid PencairanDana pencairanDana, BindingResult errors,
                                             @RequestParam("lampiran") MultipartFile file,
                                             Authentication authentication,
                                             RedirectAttributes attribute) throws IOException {

        PencairanDana pencairanDana2 = pencairanDanaDao.findById(pencairanDana.getId()).get();

        if((pencairanDana2.getNominalMintaPencairan().subtract(pencairanDana.getNominalPencairan())).intValue() < 0) {

            model.addAttribute("pencairanDana", pencairanDana);
            attribute.addFlashAttribute("lebih", "Save Data Gagal");
            return "redirect:../approve?pencairan="+ pencairanDana.getId();

        }

        if (pencairanDana.getTanggalan().trim().length() == 0 ){
            model.addAttribute("pencairanDana", pencairanDana);
            attribute.addFlashAttribute("gagaltanggal", "Save Data Gagal");
            return "redirect:../approve?pencairan="+ pencairanDana.getId();
        }

        if (pencairanDana.getJaman().trim().length() == 0 ){
            model.addAttribute("pencairanDana", pencairanDana);
            attribute.addFlashAttribute("gagaljam", "Save Data Gagal");
            return "redirect:../approve?pencairan="+ pencairanDana.getId();
        }


        String date = pencairanDana.getTanggalan();
        String jenisJam="AM";
        if (pencairanDana.getJaman().substring(2,3).equals(":")) {
            jenisJam = pencairanDana.getJaman().substring(6, 8);
        }else{
            jenisJam = pencairanDana.getJaman().substring(5, 7);
        }
        String jamas = "00:00:00";

        System.out.println("jam:"+ jenisJam);
        System.out.println("separator:" + pencairanDana.getJaman().substring(2,3));
        if(jenisJam.equals("AM")){
            String jam = "00:00";
            if (pencairanDana.getJaman().substring(2,3).equals(":")){
                jam = pencairanDana.getJaman().substring(0,5);
            }else{
                jam = "0" + pencairanDana.getJaman().substring(0,4);
            }
            jamas = jam + ":00";
        }else{

            String jama = "00";
            String jamb = "00";
            if (pencairanDana.getJaman().substring(2,3).equals(":")){
                jama = pencairanDana.getJaman().substring(0,2);
                jamb = pencairanDana.getJaman().substring(3,5);
            }else{
                jama = '0' + pencairanDana.getJaman().substring(0,1);
                jamb = pencairanDana.getJaman().substring(2,4);
            }

            int Jamc = new Integer(jama)+12;
            String Jamd = new String(String.valueOf(Jamc));
            jamas = Jamd + ':' + jamb + ":00";
        }

        String tahun = date.substring(6, 10);
        String bulan = date.substring(0, 2);
        String tanggal = date.substring(3, 5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal + ' ' + jamas;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.parse(tanggalan, formatter);

        User user = currentUserService.currentUser(authentication);
        System.out.println("userpencairan : "+pencairanDana.getId());
        Karyawan karyawan = karyawanDao.findByUser(user);
        PencairanDana pencairanDana1 = pencairanDanaDao.findById(pencairanDana.getId()).get();
        List<String> pencairanDanaDetails = pencairanDanaDetailDao.listDetailPencairanDana(pencairanDana1.getId());
        List<PengajuanDana> pengajuanDana = pengajuanDanaDao.findByStatusAndIdIn(StatusRecord.OPEN, pencairanDanaDetails);
//        PengajuanDana pengajuanDana = pengajuanDanaDao.findById(pencairanDana1.getPengajuanDana().getId()).get();

        if (errors.hasErrors()) {
            LOGGER.debug("Error input pengajuan : {}"+errors);
            return "keuangan/pencairan/form";
        }

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
//
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
//
        String idFile = UUID.randomUUID().toString();

        pencairanDana1.setStatusPencairan(StatusRecord.CAIR);
//        pencairanDana1.setTanggalPencairan(LocalDateTime.now().plusHours(7));
        pencairanDana1.setTanggalPencairan(localDateTime);
        pencairanDana1.setUserPencairan(karyawan);
        pencairanDana1.setKeteranganPencairan(pencairanDana.getKeteranganPencairan());
        pencairanDana1.setNominalPencairan(pencairanDana.getNominalPencairan());
        pencairanDana1.setTanggalan(pencairanDana.getTanggalan());
        pencairanDana1.setJaman(pencairanDana.getJaman());
        if (ukuran == 0){
            pencairanDana1.setBuktiPencairan("default.jpg");
        }else{
            pencairanDana1.setBuktiPencairan(idFile + "." + extension);
            new File(uploadFolderCair).mkdirs();
            File tujuan = new File(uploadFolderCair + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }
        pencairanDanaDao.save(pencairanDana1);

        for(PengajuanDana s : pengajuanDana) {
            PengajuanDana pengajuanDana1 = pengajuanDanaDao.findById(s.getId()).get();
            pengajuanDana1.setStatus(StatusRecord.CAIR);
            pengajuanDanaDao.save(pengajuanDana1);
        }

        attribute.addFlashAttribute("success", "Data berhasil di simpan!");
        return "redirect:../../pencairan";

    }

    @GetMapping("/keuangan/pencairan/approve_refund")
    public String approveRefundSave(@RequestParam PengembalianDanaProses pengembalianDanaProses,
                                    Authentication authentication,
                                    RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        pengembalianDanaProses.setUserPenerima(karyawan);
        pengembalianDanaProses.setTanggalTerima(LocalDateTime.now());
        pengembalianDanaProses.setStatusPengembalian(StatusRecord.CLOSED);

        List<PengembalianDanaProsesDetail> pengembalianDanaProsesDetails = pengembalianDanaProsesDetailDao.findByStatusAndPengembalianDanaProses(StatusRecord.AKTIF, pengembalianDanaProses);

        for(PengembalianDanaProsesDetail pdpd : pengembalianDanaProsesDetails){

            PengembalianDana pengembalianDana = pengembalianDanaDao.findById(pdpd.getPengembalianDana().getId()).get();
            pengembalianDana.setStatusPengembalian(StatusRecord.CLOSED);

            PencairanDana pencairanDana = pencairanDanaDao.findById(pengembalianDana.getPencairanDana().getId()).get();
            pencairanDana.setStatusCair(StatusRecord.CLOSED);

            List<PencairanDanaDetail> pencairanDanaDetails = pencairanDanaDetailDao.findByPencairanDana(pencairanDana);

            for(PencairanDanaDetail pcd : pencairanDanaDetails){

                PengajuanDana pengajuanDana = pengajuanDanaDao.findById(pcd.getPengajuanDana().getId()).get();
                pengajuanDana.setStatus(StatusRecord.CLOSED);
                pengajuanDanaDao.save(pengajuanDana);

            }

            pengembalianDanaDao.save(pengembalianDana);
            pencairanDanaDao.save(pencairanDana);

        }

        pengembalianDanaProsesDao.save(pengembalianDanaProses);

        attributes.addFlashAttribute("success", "Data berhasil di simpan!");
        return "redirect:../pencairan/approval_refund";

    }

    @PostMapping("/keuangan/pencairan/close")
    public String closePencairanDana(@RequestParam LaporanDana laporanDana,
                                     Authentication authentication){


        PencairanDana pencairanDana = pencairanDanaDao.findById(laporanDana.getPencairanDanaDetail().getPencairanDana().getId()).get();
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
//        PencairanDana pencairanDana = pencairanDanaDao.findById(laporanDana.getPencairanDana().getId()).get();
        PengajuanDana pengajuanDana = pengajuanDanaDao.findById(laporanDana.getPencairanDanaDetail().getPengajuanDana().getId()).get();

        laporanDana.setStatusLapor(StatusRecord.KEMBALI);
        laporanDana.setUserKonfirmasi(karyawan);
        laporanDana.setTanggalKonfirmasi(LocalDateTime.now().plusHours(7));
        laporanDanaDao.save(laporanDana);

        BigDecimal sisaPencairan = laporanDanaDao.sisaPencairan(laporanDana.getPencairanDanaDetail().getPencairanDana().getId());
        PengembalianDana pengembalianDana = pengembalianDanaDao.findByStatusAndPencairanDanaAndStatusPengembalian(StatusRecord.AKTIF, pencairanDana, StatusRecord.WAITING);

        if(pengembalianDana == null){
            if (sisaPencairan.intValue() > 0) {
                PengembalianDana pengembalianDana1 = new PengembalianDana();
                pengembalianDana1.setPencairanDana(pencairanDana);
                pengembalianDana1.setNominal(sisaPencairan);
                pengembalianDana1.setStatusPengembalian(StatusRecord.WAITING);
                pengembalianDana1.setStatus(StatusRecord.AKTIF);
                pengembalianDanaDao.save(pengembalianDana1);
            }else{
                pencairanDana.setStatusCair(StatusRecord.CLOSED);
                pencairanDanaDao.save(pencairanDana);

                pengajuanDana.setStatus(StatusRecord.CLOSED);
                pengajuanDanaDao.save(pengajuanDana);

                laporanDana.setStatusLapor(StatusRecord.CLOSED);
                laporanDana.setUserKonfirmasi(karyawan);
                laporanDana.setTanggalKonfirmasi(LocalDateTime.now().plusHours(7));
                laporanDanaDao.save(laporanDana);
            }
        }else{
            if (sisaPencairan.intValue() > 0) {
                PengembalianDana pengembalianDana1 = pengembalianDana;
                pengembalianDana1.setPencairanDana(pencairanDana);
                pengembalianDana1.setNominal(sisaPencairan);
                pengembalianDana1.setStatusPengembalian(StatusRecord.WAITING);
                pengembalianDana1.setStatus(StatusRecord.AKTIF);
                pengembalianDanaDao.save(pengembalianDana1);
            }else{
                PengembalianDana pengembalianDana1 = pengembalianDana;
                pengembalianDana1.setNominal(BigDecimal.ZERO);
                pengembalianDana1.setStatusPengembalian(StatusRecord.CLOSED);
                pengembalianDana1.setStatus(StatusRecord.AKTIF);
                pengembalianDanaDao.save(pengembalianDana1);

                pencairanDana.setStatusCair(StatusRecord.CLOSED);
                pencairanDanaDao.save(pencairanDana);

                pengajuanDana.setStatus(StatusRecord.CLOSED);
                pengajuanDanaDao.save(pengajuanDana);

                laporanDana.setStatusLapor(StatusRecord.CLOSED);
                laporanDana.setUserKonfirmasi(karyawan);
                laporanDana.setTanggalKonfirmasi(LocalDateTime.now().plusHours(7));
                laporanDanaDao.save(laporanDana);
            }
        }



        return "redirect:../pencairan/report";
    }

    @PostMapping("/transaksi/pencairandana/konfirmasi")
    public String konfirmasiPencairanDana(@RequestParam PencairanDana pencairan){

        PencairanDana pencairanDana=pencairanDanaDao.findById(pencairan.getId()).get();

        PengajuanDana pengajuanDana=pengajuanDanaDao.findById(pencairanDana.getPengajuanDana().getId()).get();
        pencairanDana.setStatusCair(StatusRecord.CAIR);
        pencairanDana.setStatusPencairan(StatusRecord.CLOSED);
        pencairanDanaDao.save(pencairanDana);

        pengajuanDana.setStatus(StatusRecord.LAPOR);
        pengajuanDanaDao.save(pengajuanDana);

        return "redirect:../pengajuandana/laporan";

    }















    @GetMapping("/pencairandanacair/{pencairanDana}/bukti/")
    public ResponseEntity<byte[]> tampilkanBuktiPencairan(@PathVariable PencairanDana pencairanDana) throws Exception {
        String lokasiFile = uploadFolderCair + File.separator + pencairanDana.getBuktiPencairan();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pencairanDana.getBuktiPencairan().toLowerCase().endsWith("jpeg") || pencairanDana.getBuktiPencairan().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pencairanDana.getBuktiPencairan().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pencairanDana.getBuktiPencairan().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/pengembalianDanaProses/{pengembalianDanaProses}/bukti/")
    public ResponseEntity<byte[]> tampilkanBuktiPengembalian(@PathVariable PengembalianDanaProses pengembalianDanaProses) throws Exception {
        String lokasiFile = uploadFolderKembali + File.separator + pengembalianDanaProses.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pengembalianDanaProses.getFile().toLowerCase().endsWith("jpeg") || pengembalianDanaProses.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pengembalianDanaProses.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pengembalianDanaProses.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
