package id.ac.tazkia.aplikasikeuangan.controller.reports;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

@Controller
public class RequestFundReportController {

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;


    @GetMapping("/reports/requestfund")
    private String reportRequestFund(Model model,
                                     @RequestParam(required = false) String range,
                                     @RequestParam(required = false) String search,
                                     Authentication authentication){

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        String nows = sdf.format(now.getTime()).toString();
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        if (StringUtils.hasText(range)) {

            String datea = range;
            String tahuna = datea.substring(6, 10);
            String bulana = datea.substring(0, 2);
            String tanggala = datea.substring(3, 5);
            String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

            String dateb = range;
            String tahunb = dateb.substring(19, 23);
            String bulanb = dateb.substring(13, 15);
            String tanggalb = dateb.substring(16, 18);
            String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
            DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
            model.addAttribute("search",search);
            model.addAttribute("range", range);
            model.addAttribute("listRequestFund", pengajuanDanaDao.listPengajuanDanaApproveTanggal(localDatea,localDateb));
            model.addAttribute("totalRequestFund", pengajuanDanaDao.totalPengajuanDanaApproveTanggal(localDatea,localDateb));

        }else{

            model.addAttribute("search",search);
            model.addAttribute("listRequestFund", pengajuanDanaDao.listPengajuanDanaApprove(LocalDateTime.now().plusHours(7)));
            model.addAttribute("totalRequestFund", pengajuanDanaDao.totalPengajuanDanaApprove(LocalDateTime.now().plusHours(7)));

        }



        return "reports/requestfund/list";
    }


}
