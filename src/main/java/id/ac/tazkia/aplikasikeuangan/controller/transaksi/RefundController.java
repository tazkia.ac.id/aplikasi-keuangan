package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.RekeningDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengembalianDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengembalianDanaProsesDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengembalianDanaProsesDetailDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Rekening;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDanaProses;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDanaProsesDetail;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Controller
public class RefundController {

    @Autowired
    private PengembalianDanaDao pengembalianDanaDao;

    @Autowired
    private PengembalianDanaProsesDao pengembalianDanaProsesDao;

    @Autowired
    private RekeningDao rekeningDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PengembalianDanaProsesDetailDao pengembalianDanaProsesDetailDao;

    @Autowired
    @Value("${upload.pengembalian}")
    private String uploadFolder;

    @GetMapping("/transaksi/refund/process")
    public String prosesRefund(Model model,
                               @RequestParam(required = false)String refund){

        model.addAttribute("dataRefund", pengembalianDanaDao.findById(refund).get());
        model.addAttribute("rekening", rekeningDao.findByStatusOrderByAtasNama(StatusRecord.AKTIF));
        model.addAttribute("pengembalianDanaProses", new PengembalianDanaProses());

        model.addAttribute("transactions", "active");
        return "transaksi/refund/process";
    }


    @GetMapping("/transaksi/refund/process2")
    public String prosesRefund2(Model model,
                                @RequestParam(required = false) List<String> refund){

        model.addAttribute("dataRefund", pengembalianDanaDao.findByIdIn(refund));
        model.addAttribute("totalNominal", pengembalianDanaDao.totalNominal(refund));
        model.addAttribute("rekening", rekeningDao.findByStatusOrderByAtasNama(StatusRecord.AKTIF));
        model.addAttribute("pengembalianDanaProses", new PengembalianDanaProses());

        model.addAttribute("transactions", "active");
        return "transaksi/refund/process2";
    }

    @GetMapping("/transaksi/refund/edit")
    public String editRefund(Model model,
                                @RequestParam(required = false) PengembalianDanaProses pengembalianDanaProses){

        List<String> pengembalianDanaProsesDetails = pengembalianDanaProsesDetailDao.idPengembalianDana(pengembalianDanaProses.getId(), "AKTIF");

        model.addAttribute("dataRefund", pengembalianDanaDao.findByIdIn(pengembalianDanaProsesDetails));
        model.addAttribute("totalNominal", pengembalianDanaDao.totalNominal(pengembalianDanaProsesDetails));
        model.addAttribute("rekening", rekeningDao.findByStatusOrderByAtasNama(StatusRecord.AKTIF));
        model.addAttribute("pengembalianDanaProses", pengembalianDanaProses);

        model.addAttribute("transactions", "active");
        return "transaksi/refund/edit";

    }



    @PostMapping("/transaksi/refund/save")
    public String saveRefund(Model model,
                             @RequestParam(required = false)String refund,
                             RedirectAttributes attributes,
                             @ModelAttribute @Valid PengembalianDanaProses pengembalianDanaProses,
                             BindingResult errors,
                             @RequestParam("lampiran") MultipartFile file,
                             Authentication authentication)throws Exception{

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        PengembalianDana pengembalianDana = pengembalianDanaDao.findById(refund).get();
        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        pengembalianDanaProses.setUserPengembali(karyawan);
        pengembalianDanaProses.setNominalPengembalian(pengembalianDana.getNominal());
        pengembalianDanaProses.setTanggalPengembalian(LocalDateTime.now());
//        pengembalianDanaProses.setPengembalianDana(pengembalianDana);
        pengembalianDanaProses.setStatus(StatusRecord.AKTIF);
        pengembalianDanaProses.setStatusPengembalian(StatusRecord.WAITING);
        if (ukuran == 0){
            pengembalianDanaProses.setFile("default.jpg");
        }else{
            pengembalianDanaProses.setFile(idFile + "." + extension);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }
        pengembalianDanaProsesDao.save(pengembalianDanaProses);

        pengembalianDana.setStatusPengembalian(StatusRecord.OPEN);
        pengembalianDanaDao.save(pengembalianDana);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../pengajuandana/waitingrefund";
    }

    @PostMapping("/transaksi/refund/save2")
    public String saveRefund2(Model model,
                                 RedirectAttributes attributes,
                                @ModelAttribute @Valid PengembalianDanaProses pengembalianDanaProses,
                                BindingResult errors,
                                @RequestParam(required = false) BigDecimal nominalP,
                                @RequestParam("lampiran") MultipartFile file,
                                @RequestParam(required = false)List<PengembalianDana> pengembalianDana,
                                Authentication authentication)throws Exception{

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<PengembalianDana> pengembalianDanas = pengembalianDana;
        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
//        pengembalianDanaProses.setId(idFile);
        pengembalianDanaProses.setUserPengembali(karyawan);
        pengembalianDanaProses.setNominalPengembalian(nominalP);
        pengembalianDanaProses.setTanggalPengembalian(LocalDateTime.now());
//        pengembalianDanaProses.setPengembalianDana(pengembalianDana);
        pengembalianDanaProses.setStatus(StatusRecord.AKTIF);
        pengembalianDanaProses.setStatusPengembalian(StatusRecord.WAITING);
        if (ukuran == 0){
            pengembalianDanaProses.setFile("default.jpg");
        }else{
            pengembalianDanaProses.setFile(idFile + "." + extension);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }



        for(PengembalianDana pd : pengembalianDanas) {

            PengembalianDanaProsesDetail pengembalianDanaProsesDetail = new PengembalianDanaProsesDetail();
            pengembalianDanaProsesDetail.setPengembalianDana(pd);
            pengembalianDanaProsesDetail.setPengembalianDanaProses(pengembalianDanaProses);
            pengembalianDanaProsesDetail.setStatus(StatusRecord.AKTIF);
            pengembalianDanaProses.getPengembalianDanaProsesDetail().add(pengembalianDanaProsesDetail);
            pengembalianDanaProsesDetailDao.save(pengembalianDanaProsesDetail);

            pd.setStatusPengembalian(StatusRecord.OPEN);
            pengembalianDanaDao.save(pd);

        }

        pengembalianDanaProsesDao.save(pengembalianDanaProses);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../pengajuandana/waitingrefund";
    }


    @PostMapping("/transaksi/refund/update")
    public String updateRefund(Model model,
                              RedirectAttributes attributes,
                              @ModelAttribute @Valid PengembalianDanaProses pengembalianDanaProses,
                              BindingResult errors,
                              @RequestParam(required = false) BigDecimal nominalP,
                              @RequestParam("lampiran") MultipartFile file,
                              @RequestParam(required = false)List<PengembalianDana> pengembalianDana,
                              Authentication authentication)throws Exception{

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<PengembalianDana> pengembalianDanas = pengembalianDana;
        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
//        pengembalianDanaProses.setId(idFile);
        pengembalianDanaProses.setUserPengembali(karyawan);
        pengembalianDanaProses.setNominalPengembalian(nominalP);
        pengembalianDanaProses.setTanggalPengembalian(LocalDateTime.now());
//        pengembalianDanaProses.setPengembalianDana(pengembalianDana);
        pengembalianDanaProses.setStatus(StatusRecord.AKTIF);
        pengembalianDanaProses.setStatusPengembalian(StatusRecord.WAITING);
        if (ukuran == 0){
            pengembalianDanaProses.setFile("default.jpg");
        }else{
            pengembalianDanaProses.setFile(idFile + "." + extension);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }



//        for(PengembalianDana pd : pengembalianDanas) {
//
//            PengembalianDanaProsesDetail pengembalianDanaProsesDetail = pengembalianDanaProsesDetailDao.findByStatusAndPengembalianDana(StatusRecord.AKTIF, pd);
//
//            pengembalianDanaProsesDetail.setPengembalianDanaProses(pengembalianDanaProses);
//            pengembalianDanaProsesDetail.setPengembalianDana(pd);
//            pengembalianDanaProsesDetail.setStatus(StatusRecord.AKTIF);
//
//            pd.setStatus(StatusRecord.OPEN);
//            pengembalianDanaDao.save(pd);
//
//        }

        for(PengembalianDana pd : pengembalianDanas) {

            PengembalianDanaProsesDetail pengembalianDanaProsesDetail = new PengembalianDanaProsesDetail();
            pengembalianDanaProsesDetail.setPengembalianDana(pd);
            pengembalianDanaProsesDetail.setPengembalianDanaProses(pengembalianDanaProses);
            pengembalianDanaProsesDetail.setStatus(StatusRecord.AKTIF);
            pengembalianDanaProses.getPengembalianDanaProsesDetail().add(pengembalianDanaProsesDetail);
            pengembalianDanaProsesDetailDao.save(pengembalianDanaProsesDetail);

            pd.setStatusPengembalian(StatusRecord.OPEN);
            pengembalianDanaDao.save(pd);

        }


        pengembalianDanaProsesDao.save(pengembalianDanaProses);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../pengajuandana/refund";
    }


    @GetMapping("/refund/{pengembalianDanaProses}/bukti/")
    public ResponseEntity<byte[]> tampilkanBuktiRefund(@PathVariable PengembalianDanaProses pengembalianDanaProses) throws Exception {


        String lokasiFile = uploadFolder + File.separator + pengembalianDanaProses.getFile();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pengembalianDanaProses.getFile().toLowerCase().endsWith("jpeg") || pengembalianDanaProses.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pengembalianDanaProses.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pengembalianDanaProses.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
