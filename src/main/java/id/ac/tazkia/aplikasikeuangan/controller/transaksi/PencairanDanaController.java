package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.BankDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDanaDetail;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import jakarta.validation.Valid;

@Controller
public class PencairanDanaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PengajuanDanaController.class);

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PencairanDanaDao pencairanDanaDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PencairanDanaDetailDao pencairanDanaDetailDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private BankDao bankDao;


    @Autowired
    @Value("${upload.pencairandana}")
    private String uploadFolder;


    @GetMapping("/transaksi/pencairandana")
    private String pencairanDana(Model model,
                                 @RequestParam(required = false)String pengajuan,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        PengajuanDana pengajuanDana = pengajuanDanaDao.findById(pengajuan).get();
        PencairanDana pencairanDana = pencairanDanaDao.findByStatusAndStatusPencairanAndPengajuanDanaId(StatusRecord.AKTIF, StatusRecord.WAITING, pengajuan);

        if (pencairanDana != null) {

            model.addAttribute("listBank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF));
            model.addAttribute("pengajuanDana", pengajuanDana);
            model.addAttribute("pencairanDana", pencairanDana);

            model.addAttribute("transactions", "active");
            return "transaksi/pencairandana/ada";

        }else{

            model.addAttribute("pengajuanDana", pengajuanDana);
            model.addAttribute("listBank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF));
            model.addAttribute("pencairanDana", new PencairanDana());

            model.addAttribute("transactions", "active");
            return "transaksi/pencairandana/form";

        }


    }

    @GetMapping("/transaksi/pencairandana/proses")
    public String prosesPencairanDana(Model model,
                                      @RequestParam(required = false) List<PengajuanDana> pengajuanDana,
                                      Authentication authentication,
                                      RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        if (pengajuanDana == null){
            attributes.addFlashAttribute("unprocess", "Process Data Failed");
            return "redirect:../pengajuandana/approved";
        }

        BigDecimal total = BigDecimal.ZERO;
        String ada = "";
        Integer isi = 0;
        for (PengajuanDana a : pengajuanDana){
            if (ada.equals(a.getAnggaranDetail().getAnggaran().getDepartemen().getInstansi().getId())){

            }else{
                isi = isi + 1;
            }
            ada = a.getAnggaranDetail().getAnggaran().getDepartemen().getInstansi().getId();
            total = total.add(a.getJumlah());
        }

        if (isi > 1){
            attributes.addFlashAttribute("tidaksama", "Process Data Failed");
            return "redirect:../pengajuandana/approved";
        }

        List<PencairanDana> pencairanDana = pencairanDanaDao.findByStatusAndStatusPencairanAndPengajuanDanaIn(StatusRecord.AKTIF, StatusRecord.WAITING, pengajuanDana);

            model.addAttribute("pengajuanDana", pengajuanDana);
            model.addAttribute("listBank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF));
            model.addAttribute("pencairanDana", new PencairanDana());
            model.addAttribute("totalPencairan", total);

            model.addAttribute("transactions", "active");
            return "transaksi/pencairandana/proses";

//        }

    }

    private void getAnggaranDetail() {
    }

    @GetMapping("/transaksi/pencairandana/edit")
    private String editPencairanDana(Model model,
                                     @RequestParam(required = false)PencairanDana pencairanDana,
                                     Authentication authentication){


        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        PengajuanDana pengajuanDana = pengajuanDanaDao.findById(pencairanDana.getPengajuanDana().getId()).get();

        PencairanDana pencairanDana1 = pencairanDanaDao.findById(pencairanDana.getId()).get();

        model.addAttribute("pencairanDana", pencairanDana1);
        model.addAttribute("listBank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF));
        model.addAttribute("pengajuanDana", pengajuanDana);

        model.addAttribute("transactions", "active");
        return "transaksi/pencairandana/form";
    }

    @GetMapping("/transaksi/pencairandana/edit2")
    private String editPencairanDana2(Model model,
                                     @RequestParam(required = false)PencairanDana pencairanDana,
                                     Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> pencairanDanaDetails = pencairanDanaDetailDao.listDetailPencairanDana(pencairanDana.getId());

        List<PengajuanDana> pengajuanDanas = pengajuanDanaDao.findByStatusAndIdIn(StatusRecord.OPEN, pencairanDanaDetails);

        model.addAttribute("pencairanDana", pencairanDana);
        model.addAttribute("listBank", bankDao.findByStatusOrderByNamaBank(StatusRecord.AKTIF));
        model.addAttribute("pengajuanDana", pengajuanDanas);

        model.addAttribute("transactions", "active");
        return "transaksi/pencairandana/prosesedit";
    }

    @PostMapping("/transaksi/pencairandana/save")
    private String savePencairanDana(Model model,
                                     @ModelAttribute  PencairanDana pencairanDana,
                                     BindingResult errors,
                                     @RequestParam("lampiran") MultipartFile file,
                                     @RequestParam(required = false)List<PengajuanDana> pengajuanDana,
                                     Authentication authentication,
                                     RedirectAttributes attributes) throws IOException {

        if (errors.hasErrors()) {
            LOGGER.debug("Error input pencairan : {}"+errors);

            model.addAttribute("transactions", "active");
            return "transaksi/pencairandana/form";
        }

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        BigDecimal jumlah = BigDecimal.ZERO;
        for(PengajuanDana j : pengajuanDana){
            jumlah = jumlah.add(j.getJumlah());
        }

        if(pencairanDana.getNominalMintaPencairan() == null){
            attributes.addFlashAttribute("gagalnol", "Process Data Failed");
            return "redirect:../pengajuandana/approved";
        }

        if(jumlah.subtract(pencairanDana.getNominalMintaPencairan()).longValue() < 0){
            attributes.addFlashAttribute("gagalselisih", "Process Data Failed");
            return "redirect:../pengajuandana/approved";
        }


        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
//
        String idFile = UUID.randomUUID().toString();
//        pencairanDana.setPengajuanDana(pengajuanDana);
        pencairanDana.setUserMintaPencairan(karyawan);
        if (ukuran == 0){
            pencairanDana.setFile("default.jpg");
        }else{
            pencairanDana.setFile(idFile + "." + extension);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        String idPencairanDana = UUID.randomUUID().toString();
        pencairanDana.setId(idPencairanDana);
        pencairanDanaDao.save(pencairanDana);
        PencairanDana pencairanDana1 = pencairanDanaDao.findById(idPencairanDana).get();

        for(PengajuanDana s : pengajuanDana){
            PencairanDanaDetail pencairanDanaDetail = new PencairanDanaDetail();
            pencairanDana1.setDepartemen(s.getDepartemen());
            pencairanDanaDetail.setPencairanDana(pencairanDana1);
            pencairanDanaDetail.setPengajuanDana(s);
            pencairanDanaDetailDao.save(pencairanDanaDetail);
            s.setStatus(StatusRecord.OPEN);
            pengajuanDanaDao.save(s);
        }
        pencairanDanaDao.save(pencairanDana1);
        attributes.addFlashAttribute("success", "Process Data Failed");
        return "redirect:../pengajuandana/approved";

    }


    @PostMapping("/transaksi/pencairandana/update")
    private String updatePencairanDana(Model model,
                                     @ModelAttribute @Valid PencairanDana pencairanDana,
                                     BindingResult errors,
                                     @RequestParam("lampiran") MultipartFile file,
                                     @RequestParam(required = false)PencairanDana idCair,
                                     RedirectAttributes attributes,
                                     Authentication authentication) throws IOException {

        if (errors.hasErrors()) {
            LOGGER.debug("Error input pencairan : {}"+errors);

            model.addAttribute("transactions", "active");
            return "transaksi/pencairandana/form";
        }

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        PencairanDana pencairanDana1=pencairanDanaDao.findById(idCair.getId()).get();

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
//
        String idFile = UUID.randomUUID().toString();
//        pencairanDana.setPengajuanDana(pengajuanDana);
        pencairanDana1.setUserMintaPencairan(karyawan);
        if (ukuran == 0){
            pencairanDana1.setFile("default.jpg");
        }else{
            pencairanDana1.setFile(idFile + "." + extension);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }


        pencairanDana1.setKeterangan(pencairanDana.getKeterangan());
        pencairanDana1.setNominalMintaPencairan(pencairanDana.getNominalMintaPencairan());
        pencairanDana1.setNomorRekening(pencairanDana.getNomorRekening());
        pencairanDana1.setJenisPencairan(pencairanDana.getJenisPencairan());
        pencairanDana1.setBank(pencairanDana.getBank());
        pencairanDanaDao.save(pencairanDana1);
        attributes.addFlashAttribute("success", "Process Data Failed");
        return "redirect:../pengajuandana/disbursment";

    }


    @GetMapping("/pencairandana/{pencairanDana}/bukti/")
    public ResponseEntity<byte[]> tampilkanBuktiPenerimaan(@PathVariable PencairanDana pencairanDana) throws Exception {
        String lokasiFile = uploadFolder + File.separator + pencairanDana.getFile();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pencairanDana.getFile().toLowerCase().endsWith("jpeg") || pencairanDana.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pencairanDana.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pencairanDana.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
