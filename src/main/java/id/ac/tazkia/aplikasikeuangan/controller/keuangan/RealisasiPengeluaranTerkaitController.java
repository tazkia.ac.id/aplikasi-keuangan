package id.ac.tazkia.aplikasikeuangan.controller.keuangan;

import id.ac.tazkia.aplikasikeuangan.controller.transaksi.PengajuanDanaController;
import id.ac.tazkia.aplikasikeuangan.dao.config.UserDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.InstansiDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPendapatanTerkaitDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPendapatanTerkaitDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PendapatanTerkaitDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengeluaranTerkaitDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengeluaranTerkait;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Controller
public class RealisasiPengeluaranTerkaitController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PengajuanDanaController.class);

    @Autowired
    private EstimasiPendapatanTerkaitDao estimasiPendapatanTerkaitDao;

    @Autowired
    private EstimasiPendapatanTerkaitDetailDao estimasiPendapatanTerkaitDetailDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private KaryawanDao karyawanDao;
    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;
    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private SatuanDao satuanDao;

    @Autowired
    private PengeluaranTerkaitDao pengeluaranTerkaitDao;

    @Autowired
    @Value("${upload.pengeluaranTerkait}")
    private String uploadFolder;

    @GetMapping("/transaksi/pengeluaran_terkait/add")
    public String pendapatanTerkaitList(Model model,
                                        @RequestParam(required = true) EstimasiPendapatanTerkait id,
                                        @PageableDefault(size = 10) Pageable pageable){

        model.addAttribute("listPengeluaranTerkait", pengeluaranTerkaitDao.findByStatusAndEstimasiPendapatanTerkaitOrderByTanggalDesc(StatusRecord.AKTIF, id, pageable));
        model.addAttribute("estimasiPendapatanTerkait", id);
        model.addAttribute("finance","active");
        model.addAttribute("pendapatan_terkait","active");
        return "keuangan/pengeluaran_terkait/realisasi/list_add";
    }

    @GetMapping("/transaksi/pengeluaran_terkait/new")
    public String pendapatanTerkaitNew(Model model,
                                       @RequestParam(required = true) EstimasiPendapatanTerkait estimasiPendapatanTerkait){

        model.addAttribute("pengeluaranTerkait", new PengeluaranTerkait());
        model.addAttribute("estimasiPendapatanTerkait", estimasiPendapatanTerkait);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("finance","active");
        model.addAttribute("pendapatan_terkait","active");
        return "keuangan/pengeluaran_terkait/realisasi/form";
    }


    @GetMapping("/transaksi/pengeluaran_terkait/edit")
    public String pendapatanTerkaitEdit(Model model,
                                        @RequestParam(required = true) PengeluaranTerkait id){

        model.addAttribute("pengeluaranTerkait", id);
        model.addAttribute("estimasiPendapatanTerkait", id.getEstimasiPendapatanTerkait());
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("finance","active");
        model.addAttribute("pendapatan_terkait","active");
        return "keuangan/pengeluaran_terkait/realisasi/form";
    }

    @PostMapping("/transaksi/pengeluaran_terkait/save")
    public String pendapatanTerkaitSave(@ModelAttribute @Valid PengeluaranTerkait pengeluaranTerkait,
                                        @RequestParam EstimasiPendapatanTerkait estimasiPendapatanTerkait,
                                        @RequestParam("lampiran") MultipartFile file,
                                        Authentication authentication,
                                        RedirectAttributes attribute) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        String date = pengeluaranTerkait.getTanggalString();
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        pengeluaranTerkait.setEstimasiPendapatanTerkait(estimasiPendapatanTerkait);
        pengeluaranTerkait.setSatuan(estimasiPendapatanTerkait.getSatuan());
        pengeluaranTerkait.setTotal(pengeluaranTerkait.getJumlah().multiply(pengeluaranTerkait.getNominalSatuan()));
        pengeluaranTerkait.setStatus(StatusRecord.AKTIF);
        pengeluaranTerkait.setTanggalInsert(LocalDateTime.now());
        pengeluaranTerkait.setUserInsert(user.getUsername());
        pengeluaranTerkait.setKaryawan(karyawan);
        pengeluaranTerkait.setTanggal(localDate);

        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
//
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
//
        String idFile = UUID.randomUUID().toString();
        if (ukuran == 0){
            pengeluaranTerkait.setFile("default.jpg");
        }else{
            pengeluaranTerkait.setFile(idFile + "." + extension);
        }
        System.out.println("file :"+ namaAsli);
        System.out.println("Ukuran :"+ ukuran);

        LOGGER.debug("Lokasi upload : {}", uploadFolder);
        new File(uploadFolder).mkdirs();
        File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);


        LOGGER.debug("Pengajuan dana sebelum save : {}", pengeluaranTerkait);
        pengeluaranTerkaitDao.save(pengeluaranTerkait);

        attribute.addFlashAttribute("success", "Data berhasil disimpan");
        return "redirect:../pengeluaran_terkait/add?id="+ estimasiPendapatanTerkait.getId();
    }


    @GetMapping("/pengeluaran_terkait/{pengeluaranTerkait}/attachement/")
    public ResponseEntity<byte[]> tampilkanBuktiPembayaran(@PathVariable PengeluaranTerkait pengeluaranTerkait) throws Exception {
        String lokasiFile = uploadFolder + File.separator + pengeluaranTerkait.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pengeluaranTerkait.getFile().toLowerCase().endsWith("jpeg") || pengeluaranTerkait.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pengeluaranTerkait.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pengeluaranTerkait.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
