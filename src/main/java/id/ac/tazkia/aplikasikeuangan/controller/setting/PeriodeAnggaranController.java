package id.ac.tazkia.aplikasikeuangan.controller.setting;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PaguAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PicAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PaguAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PicAnggaran;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Controller
public class PeriodeAnggaranController {

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PaguAnggaranDao paguAnggaranDao;

    @Autowired
    private PicAnggaranDao picAnggaranDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private AnggaranDao anggaranDao;

    @GetMapping("/setting/periodeanggaran")
    public String daftarPicAnggaran(Model model, @PageableDefault(size = 10) Pageable page, String search) {

        model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaran(StatusRecord.AKTIF, page));

        model.addAttribute("setting", "active");
        model.addAttribute("periodes", "active");
        return "setting/periode/list";
    }

    @GetMapping("/setting/periodeanggaran/baru")
    public String baruPeriodeAnggaran(Model model){

        model.addAttribute("periodeAnggaran", new PeriodeAnggaran());

        model.addAttribute("setting", "active");
        model.addAttribute("periodes", "active");
        return "setting/periode/form";

    }

    @GetMapping("/setting/periodeanggaran/edit")
    public String editPeriodeAnggaran(Model model,
                                      @RequestParam(required = false)String id){

        if (id != null && !id.isEmpty()) {
            PeriodeAnggaran periodeAnggaran = periodeAnggaranDao.findById(id).get();
            if (periodeAnggaran != null) {
                model.addAttribute("periodeAnggaran", periodeAnggaran);
                if (periodeAnggaran.getStatus() == null){
                    periodeAnggaran.setStatus(StatusRecord.HAPUS);
                }
                if (periodeAnggaran.getStatusAktif() == null){
                    periodeAnggaran.setStatusAktif(StatusRecord.NONAKTIF);
                }
            }
        }

        model.addAttribute("setting", "active");
        model.addAttribute("periodes", "active");
        return "setting/periode/edit";
    }


    @PostMapping("/setting/periodeanggaran/save")
    public String savePeriodeAnggaran(@ModelAttribute @Valid PeriodeAnggaran periodeAnggaran, Model model,
                                      BindingResult errors,
                                      RedirectAttributes attributes){

        String idFile = UUID.randomUUID().toString();

        String date = periodeAnggaran.getTanggalMulaiRencana();

        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        periodeAnggaran.setTanggalMulaiPerencanaan(localDate);
//        periodeAnggaran.setTanggalMulaiPerencanaan(localDate.plusDays(1));

        String date1 = periodeAnggaran.getTanggalSelesaiRencana();
        String tahun1 = date1.substring(0,4);
        String bulan1 = date1.substring(5,7);
        String tanggal1 = date1.substring(8,10);
//        String tahun1 = date1.substring(6,10);
//        String bulan1 = date1.substring(0,2);
//        String tanggal1 = date1.substring(3,5);
        String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);
        periodeAnggaran.setTanggalSelesaiPerencanaan(localDate1);
//        periodeAnggaran.setTanggalSelesaiPerencanaan(localDate1.plusDays(1));

        String date2 = periodeAnggaran.getTanggalMulaiAnggar();
        String tahun2 = date2.substring(0,4);
        String bulan2 = date2.substring(5,7);
        String tanggal2 = date2.substring(8,10);
//        String tahun2 = date2.substring(6,10);
//        String bulan2 = date2.substring(0,2);
//        String tanggal2 = date2.substring(3,5);
        String tanggalan2 = tahun2 + '-' + bulan2 + '-' + tanggal2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalan2, formatter2);
        periodeAnggaran.setTanggalMulaiAnggaran(localDate2);
//        periodeAnggaran.setTanggalMulaiAnggaran(localDate2.plusDays(1));

        String date3 = periodeAnggaran.getTanggalSelesaiAnggar();
        String tahun3 = date.substring(0,4);
        String bulan3 = date.substring(5,7);
        String tanggal3 = date.substring(8,10);

//        String tahun3 = date3.substring(6,10);
//        String bulan3 = date3.substring(0,2);
//        String tanggal3 = date3.substring(3,5);
        String tanggalan3 = tahun3 + '-' + bulan3 + '-' + tanggal3;
        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate3 = LocalDate.parse(tanggalan3, formatter3);
        periodeAnggaran.setTanggalSelesaiAnggaran(localDate3);
//        periodeAnggaran.setTanggalSelesaiAnggaran(localDate3.plusDays(1));


        if(errors.hasErrors()){

            model.addAttribute("setting", "active");
            model.addAttribute("periodes", "active");
            return "/setting/periode/form";
        }

        if (periodeAnggaran.getStatus()==null){
            periodeAnggaran.setStatus(StatusRecord.HAPUS);
        }
        if (periodeAnggaran.getStatusAktif() == null){
            periodeAnggaran.setStatusAktif(StatusRecord.NONAKTIF);
        }


//        if(periodeAnggaran.getId() == null){
            periodeAnggaran.setId(idFile);
            periodeAnggaranDao.save(periodeAnggaran);

            PeriodeAnggaran periodeAnggaran1 = periodeAnggaranDao.findById(idFile).get();
            Karyawan karyawan = karyawanDao.findById("default1").get();

            List<Departemen> departemens=departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF);
            for(Departemen d : departemens){

                PicAnggaran picAnggaran = new PicAnggaran();
                picAnggaran.setPeriodeAnggaran(periodeAnggaran1);
                picAnggaran.setDepartemen(d);
                picAnggaran.setKaryawan(karyawan);
                picAnggaran.setJumlah(BigDecimal.ZERO);
                picAnggaran.setStatus(StatusRecord.AKTIF);
                picAnggaranDao.save(picAnggaran);

                PaguAnggaran paguAnggaran = new PaguAnggaran();
                paguAnggaran.setPeriodeAnggaran(periodeAnggaran1);
                paguAnggaran.setPaguAnggaranAmount(BigDecimal.ZERO);
                paguAnggaran.setPersentasePaguAnggaran(BigDecimal.ZERO);
                paguAnggaran.setDepartemen(d);
                paguAnggaran.setKeterangan("-");
                paguAnggaran.setStatus(StatusRecord.AKTIF);
                paguAnggaran.setBlocking(StatusRecord.AKTIF);
                paguAnggaran.setStatus(StatusRecord.AKTIF);
                paguAnggaran.setAmbil("NETT");
                paguAnggaranDao.save(paguAnggaran);

            }


//        }else{
//
//            periodeAnggaranDao.save(periodeAnggaran);
//
//        }


        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../periodeanggaran";
    }


    @PostMapping("/setting/periodeanggaran/update")
    public String updatePeriodeAnggaran(@ModelAttribute @Valid PeriodeAnggaran periodeAnggaran, Model model,
                                      BindingResult errors,
                                      RedirectAttributes attributes){

        String idFile = UUID.randomUUID().toString();

        String date = periodeAnggaran.getTanggalMulaiRencana();
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        periodeAnggaran.setTanggalMulaiPerencanaan(localDate);
//        periodeAnggaran.setTanggalMulaiPerencanaan(localDate.plusDays(1));

        String date1 = periodeAnggaran.getTanggalSelesaiRencana();
        String tahun1 = date1.substring(0,4);
        String bulan1 = date1.substring(5,7);
        String tanggal1 = date1.substring(8,10);
//        String tahun1 = date1.substring(6,10);
//        String bulan1 = date1.substring(0,2);
//        String tanggal1 = date1.substring(3,5);
        String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);
        periodeAnggaran.setTanggalSelesaiPerencanaan(localDate1);
//        periodeAnggaran.setTanggalSelesaiPerencanaan(localDate1.plusDays(1));

        String date2 = periodeAnggaran.getTanggalMulaiAnggar();
        String tahun2 = date2.substring(0,4);
        String bulan2 = date2.substring(5,7);
        String tanggal2 = date2.substring(8,10);
//        String tahun2 = date2.substring(6,10);
//        String bulan2 = date2.substring(0,2);
//        String tanggal2 = date2.substring(3,5);
        String tanggalan2 = tahun2 + '-' + bulan2 + '-' + tanggal2;
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(tanggalan2, formatter2);
        periodeAnggaran.setTanggalMulaiAnggaran(localDate2);
//        periodeAnggaran.setTanggalMulaiAnggaran(localDate2.plusDays(1));

        String date3 = periodeAnggaran.getTanggalSelesaiAnggar();
        String tahun3 = date3.substring(0,4);
        String bulan3 = date3.substring(5,7);
        String tanggal3 = date3.substring(8,10);
//        String tahun3 = date3.substring(6,10);
//        String bulan3 = date3.substring(0,2);
//        String tanggal3 = date3.substring(3,5);
        String tanggalan3 = tahun3 + '-' + bulan3 + '-' + tanggal3;
        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate3 = LocalDate.parse(tanggalan3, formatter3);
        periodeAnggaran.setTanggalSelesaiAnggaran(localDate3);
//        periodeAnggaran.setTanggalSelesaiAnggaran(localDate3.plusDays(1));


        if(errors.hasErrors()){
            model.addAttribute("setting", "active");
            model.addAttribute("periodes", "active");
            return "/setting/periode/form";
        }

        if (periodeAnggaran.getStatus()==null){
            periodeAnggaran.setStatus(StatusRecord.HAPUS);
        }
        if (periodeAnggaran.getStatusAktif() == null){
            periodeAnggaran.setStatusAktif(StatusRecord.NONAKTIF);
        }

        periodeAnggaranDao.save(periodeAnggaran);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../periodeanggaran";
    }



    @PostMapping("/setting/periodeanggaran/hapus")
    public String hapusPeriodeAnggaran(@ModelAttribute @Valid PeriodeAnggaran periodeAnggaran, Model model,
                                      BindingResult errors,
                                      RedirectAttributes attributes){

        Integer anggaran = anggaranDao.countByStatusAndPeriodeAnggaran(StatusRecord.AKTIF, periodeAnggaran);

        if (anggaran > 0){
            model.addAttribute("setting", "active");
            model.addAttribute("periodes", "active");
            attributes.addFlashAttribute("anggaran", "Save Data Gagal");
            return "redirect:../periodeanggaran";
        }

        periodeAnggaran.setStatus(StatusRecord.HAPUS);
        periodeAnggaranDao.save(periodeAnggaran);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../periodeanggaran";
    }
}
