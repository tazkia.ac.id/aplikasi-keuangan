package id.ac.tazkia.aplikasikeuangan.controller.masterdata;

//import com.sun.org.apache.xpath.internal.objects.XString;
import id.ac.tazkia.aplikasikeuangan.dao.config.RoleDao;
import id.ac.tazkia.aplikasikeuangan.dao.config.UserDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class KaryawanController {

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/masterdata/karyawan")
    public String daftarKaryawan(Model model, @PageableDefault(size = 10) Pageable page, String search) {

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listKaryawan", karyawanDao.findByStatusAndNikContainingIgnoreCaseOrStatusAndNamaKaryawanContainingIgnoreCaseOrderByNamaKaryawan(StatusRecord.AKTIF, search, StatusRecord.AKTIF, search, page));
        } else {
            model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawan(StatusRecord.AKTIF, page));
        }

        model.addAttribute("setting", "active");
        model.addAttribute("employes", "active");
        return "masterdata/karyawan/list";
    }

    @GetMapping("/masterdata/karyawan/detail")
    public String detailKaryawan(Model model,
                                 @PageableDefault(size = 10) Pageable page,
                                 @RequestParam(required = false)String detail){

        Karyawan karyawan = karyawanDao.findById(detail).get();

        model.addAttribute("karyawan", karyawan);
        model.addAttribute("karyawanJabatan", karyawanJabatanDao.findByStatusAndJabatanStatusAndJabatanStatusAktifAndKaryawanIdOrderByMulaiBerlaku(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.AKTIF, karyawan.getId(), page ));

        model.addAttribute("setting", "active");
        model.addAttribute("employes", "active");
        return ("masterdata/karyawan/detail");
    }

    @GetMapping("/masterdata/karyawan/baru")
    public String baruKaryawan(Model model){

        model.addAttribute("karyawan", new Karyawan());

        model.addAttribute("setting", "active");
        model.addAttribute("employes", "active");
        return ("masterdata/karyawan/form");
    }

    @PostMapping("/masterdata/karyawan/save")
    public String saveKaryawan(@ModelAttribute @Valid Karyawan karyawan,
                               Authentication authentication,
                               Model model,
                               RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        User userbaru = karyawan.getUser();

        if(userbaru != null){
            User userada = userDao.findByUsernameAndIdNot(karyawan.getEmail(),userbaru.getId());
            if(userada != null){
                model.addAttribute("karyawan", karyawan);
                model.addAttribute("setting", "active");
                model.addAttribute("employes", "active");
                model.addAttribute("erorada", "email sudah di gunakan sebelum nya, coba gunakan email lain");
                return ("masterdata/karyawan/form");
            }
        }else{
            User userada = userDao.findByUsername(karyawan.getEmail());
            if(userada != null){
                model.addAttribute("karyawan", karyawan);
                model.addAttribute("setting", "active");
                model.addAttribute("employes", "active");
                model.addAttribute("erorada", "email sudah di gunakan sebelum nya, coba gunakan email lain");
                return ("masterdata/karyawan/form");
            }
        }

        if(userbaru==null) {
            userbaru = new User();
            userbaru.setUsername(karyawan.getEmail());
            userbaru.setRole(roleDao.findById("staff").get());
            userbaru.setActive(true);
        }else{
            userbaru.setUsername(karyawan.getEmail());
//            userbaru.setRole(roleDao.findById("staff").get());
            userbaru.setActive(true);
        }
        userDao.save(userbaru);

        String date = karyawan.getTanggalLahirString();
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;

//        String tanggalan = tanggalActive;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        karyawan.setTanggalLahir(localDate);
        karyawan.setStatus(StatusRecord.AKTIF);
        karyawan.setUser(userDao.findByUsername(karyawan.getEmail()));
        if (karyawan.getStatusAktif() == null){
            karyawan.setStatusAktif(StatusRecord.NONAKTIF);
        }
        karyawanDao.save(karyawan);

        attributes.addFlashAttribute("success","sukses");
        return "redirect:../karyawan";
    }

//    @PostMapping("/masterdata/karyawan/save")
//    public String saveKaryawan(@ModelAttribute @Valid Karyawan karyawan,
//                               Authentication authentication,
//                               RedirectAttributes attributes){
//
//        User user = currentUserService.currentUser(authentication);
//
//        String date = karyawan.getTanggalLahirString();
//        String tahun = date.substring(0,4);
//        String bulan = date.substring(5,7);
//        String tanggal = date.substring(8,10);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//
////        String tanggalan = tanggalActive;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
//
//        karyawan.setTanggalLahir(localDate);
//        karyawan.setStatus(StatusRecord.AKTIF);
//        if (karyawan.getStatusAktif() == null){
//            karyawan.setStatusAktif(StatusRecord.NONAKTIF);
//        }
//        karyawanDao.save(karyawan);
//
//        attributes.addFlashAttribute("success","sukses");
//        return "redirect:../karyawan";
//    }

    @GetMapping("/masterdata/karyawan/edit")
    public String editKaryawan(Model model,
                               @RequestParam(required = false)String id,
                               RedirectAttributes attributes){

        Karyawan karyawan = karyawanDao.findById(id).get();

        model.addAttribute("karyawan", karyawan);

        model.addAttribute("setting", "active");
        model.addAttribute("employes", "active");
        return ("masterdata/karyawan/form");
    }

    @PostMapping("/masterdata/karyawan/hapus")
    public String hapusKaryawan(@RequestParam Karyawan karyawan){

        karyawan.setStatus(StatusRecord.HAPUS);
        karyawanDao.save(karyawan);

        return "redirect:../karyawan";
    }

}
