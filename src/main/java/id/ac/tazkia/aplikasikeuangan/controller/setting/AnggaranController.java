package id.ac.tazkia.aplikasikeuangan.controller.setting;

import id.ac.tazkia.aplikasikeuangan.dao.config.RoleDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.*;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dto.export.ExportAnggaranDetailEstimasiDepartemenDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PicAnggaran;
import id.ac.tazkia.aplikasikeuangan.export.ExportEstimasiTransaksiDepartemen;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Controller
public class AnggaranController {

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PicAnggaranDao picAnggaranDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PaguAnggaranDao paguAnggaranDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private AnggaranDetailCrudDao anggaranDetailCrudDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private AnggaranProkerDao anggaranProkerDao;

    @Autowired
    private AnggaranDetailEstimasiDao anggaranDetailEstimasiDao;

    @GetMapping("/setting/anggaran")
    private String listAnggaran(Model model,
                                @PageableDefault(size = 10) Pageable page,
                                @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                String search,
                                Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
      //  List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        List<String> karyawanDepartemen = karyawanJabatanDao.cariIdDepartemen(karyawan.getId());
        List<String> idInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        String periodeAnggaranAktif = "";


        String  periodeAnggaranAktif1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        if(periodeAnggaran != null) {
            periodeAnggaranAktif = periodeAnggaran.getId();
            model.addAttribute("periodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaranAktif1));
            model.addAttribute("aktifa","NONAKTIF");
        }else{
            if (periodeAnggaranAktif1 == null) {
                periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
                model.addAttribute("aktifa","NONAKTIF");
            } else {
                periodeAnggaranAktif = periodeAnggaranAktif1;
                model.addAttribute("periodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaranAktif1));
                model.addAttribute("aktifa","AKTIF");
            }
        }

        System.out.println("user_role : "+ user.getRole());

        if (roleDao.findById("warek2").get() == user.getRole() ||  roleDao.findById("anggaran").get() == user.getRole() || roleDao.findById("keuangan").get() == user.getRole() || roleDao.findById("mutu").get() == user.getRole()){
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktifInstansi(periodeAnggaranAktif, idInstansi));
                model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord.AKTIF,idInstansi, periodeAnggaranAktif, search, page));
            } else {
                model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktifInstansi(periodeAnggaranAktif, idInstansi));
                model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord.AKTIF,idInstansi, periodeAnggaranAktif, page));
            }
        }else{
            if(roleDao.findById("superuser").get() == user.getRole()){
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaranAktif));
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaranAktif, search, page));
                } else {
                    model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaranAktif));
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaranAktif, page));
                }
            }else {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaranAktif));
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenIdInAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaranAktif, karyawanDepartemen, search, page));
                } else {
                    model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaranAktif));
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenIdInOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaranAktif, karyawanDepartemen, page));
                }
            }
        }


//        model.addAttribute("periodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaranAktif1));
        model.addAttribute("periodeAnggaranAktifS", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaranAktif));
        model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_budget","active");
        return "setting/anggaran/list";

    }

    @GetMapping("/setting/anggaran/setting")
    private String listSettingAnggaran(Model model, @PageableDefault(size = 10) Pageable page,
                                       @RequestParam(required = false)String departemen,
                                       @RequestParam(required = false)String periode, String search){

        String periodeAnggaranAktif = "";
        String periodeAnggaranAktif1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        if(periodeAnggaranAktif1 == null){
            periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranAktifA(LocalDate.now());
        }else{
            periodeAnggaranAktif = periodeAnggaranAktif1;
        }
        BigDecimal paguAnggaran = paguAnggaranDao.getTotalPaguAnggaranDepartemen(periodeAnggaranAktif, departemen);
        //BigDecimal pengajuanDanaTotal = pengajuanDanaDao.getSisaPaguAnggaran(periode, departemen);
        BigDecimal pengajuanDanaTotal = anggaranCrudDao.getSisaAnggaranPerDepartemenIn(periodeAnggaranAktif, departemen);

        if (pengajuanDanaTotal == null) {
            BigDecimal sisaAnggaran = paguAnggaran.subtract(BigDecimal.ZERO);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("departemen", departemen);
                model.addAttribute("departemens", departemenDao.findById(departemen).get());
                model.addAttribute("periode", periodeAnggaranAktif);
                model.addAttribute("paguAnggaran", paguAnggaran);
                model.addAttribute("pengajuanDana", pengajuanDanaTotal);
                model.addAttribute("sisaAnggaran", sisaAnggaran);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("totalAnggaran", anggaranCrudDao.getTotalAnggaran(departemen,periode));
                model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode, departemen));
                model.addAttribute("listAnggaran", anggaranDao.findByStatusAndDepartemenIdAndPeriodeAnggaranIdAndKodeAnggaranOrNamaAnggaranOrderByKodeAnggaran(StatusRecord.AKTIF, departemen, periode, search, search, page));
                model.addAttribute("listAnggaranProker", anggaranProkerDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode, departemen));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode, departemen));
            }else{
                model.addAttribute("departemen", departemen);
                model.addAttribute("departemens", departemenDao.findById(departemen).get());
                model.addAttribute("periode", periodeAnggaranAktif);
                model.addAttribute("paguAnggaran", paguAnggaran);
                model.addAttribute("pengajuanDana", pengajuanDanaTotal);
                model.addAttribute("sisaAnggaran", sisaAnggaran);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("totalAnggaran", anggaranCrudDao.getTotalAnggaran(departemen,periode));
                model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode, departemen));
                model.addAttribute("listAnggaran", anggaranDao.findByStatusAndDepartemenIdAndPeriodeAnggaranIdOrderByKodeAnggaran(StatusRecord.AKTIF, departemen, periode));
                model.addAttribute("listAnggaranProker", anggaranProkerDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode, departemen));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode, departemen));
            }

            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_budget","active");
            return "setting/anggaran/anggaranlist";

        }else{

            BigDecimal sisaAnggaran = paguAnggaran.subtract(pengajuanDanaTotal);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("departemen", departemen);
                model.addAttribute("departemens", departemenDao.findById(departemen).get());
                model.addAttribute("periode", periode);
                model.addAttribute("paguAnggaran", paguAnggaran);
                model.addAttribute("pengajuanDana", pengajuanDanaTotal);
                model.addAttribute("sisaAnggaran", sisaAnggaran);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("totalAnggaran", anggaranCrudDao.getTotalAnggaran(departemen,periode));
                model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode, departemen));
                model.addAttribute("listAnggaran", anggaranDao.findByStatusAndDepartemenIdAndPeriodeAnggaranIdAndKodeAnggaranOrNamaAnggaranOrderByKodeAnggaran(StatusRecord.AKTIF, departemen, periode, search, search, page));
                model.addAttribute("listAnggaranProker", anggaranProkerDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode, departemen));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode, departemen));
            }else{
                model.addAttribute("departemen", departemen);
                model.addAttribute("departemens", departemenDao.findById(departemen).get());
                model.addAttribute("periode", periode);
                model.addAttribute("paguAnggaran", paguAnggaran);
                model.addAttribute("pengajuanDana", pengajuanDanaTotal);
                model.addAttribute("sisaAnggaran", sisaAnggaran);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("totalAnggaran", anggaranCrudDao.getTotalAnggaran(departemen,periode));
                model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode, departemen));
                model.addAttribute("listAnggaran", anggaranDao.findByStatusAndDepartemenIdAndPeriodeAnggaranIdOrderByKodeAnggaran(StatusRecord.AKTIF, departemen, periode));
                model.addAttribute("listAnggaranProker", anggaranProkerDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode, departemen));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode, departemen));
            }

            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_budget","active");
            return "setting/anggaran/anggaranlist";
        }


    }


    @GetMapping("/setting/anggaran/baru")
    private String baruSettingAnggaran(Model model,@RequestParam(required = false)String id, @RequestParam(required = false)String departemen
            , @RequestParam(required = false)String periode) {

        String periodeAnggaranAktif = "";
        String periodeAnggaranAktif1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        if(periodeAnggaranAktif1 == null){
            periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        }else{
            periodeAnggaranAktif = periodeAnggaranAktif1;
        }
        model.addAttribute("departemen",departemen);
        model.addAttribute("departemens", departemenDao.findById(departemen).get());
        model.addAttribute("periode", periodeAnggaranAktif);
        model.addAttribute("anggaran", new Anggaran());
        model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen));

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_budget","active");
        return "setting/anggaran/anggaranform";

    }

    @GetMapping("/setting/anggaran/edit")
    private String editSettingAnggaran(Model model,
                                       @RequestParam(required = false)String data,
                                       @RequestParam(required = false)String departemen,
                                       @RequestParam(required = false)String periode) {

        String periodeAnggaranAktif = "";
        String periodeAnggaranAktif1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        if(periodeAnggaranAktif1 == null){
            periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        }else{
            periodeAnggaranAktif = periodeAnggaranAktif1;
        }
        if (data != null && !data.isEmpty()) {
            Anggaran anggaran = anggaranDao.findById(data).get();
            if (anggaran != null) {
                model.addAttribute("anggaran", anggaran);
                if (anggaran.getStatus() == null){
                    anggaran.setStatus(StatusRecord.HAPUS);
                }
            }
        }

        model.addAttribute("departemen",departemen);
        model.addAttribute("departemens", departemenDao.findById(departemen).get());
        model.addAttribute("periode", periodeAnggaranAktif);
        model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen));

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_budget","active");
        return "setting/anggaran/anggaranform";

    }


    @PostMapping("/setting/anggaran/save")
    public String prosesSaveAnggaran(Model model,
                                     @RequestParam(required = false)String departemen,
                                     @RequestParam(required = false)String periode,
                                     @ModelAttribute @Valid Anggaran anggaran,
                                     BindingResult errors,
                                     RedirectAttributes attributes){

        String periodeAnggaranAktif = "";
        String periodeAnggaranAktif1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        if(periodeAnggaranAktif1 == null){
            periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        }else{
            periodeAnggaranAktif = periodeAnggaranAktif1;
        }
        String idDepartemen = departemen;
        String idPeriode = periodeAnggaranAktif;

        model.addAttribute("departemen",departemen);
        model.addAttribute("departemens", departemenDao.findById(departemen).get());
        model.addAttribute("periode", periodeAnggaranAktif);
        model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen));

        String idAnggaran=anggaran.getId();

        if(idAnggaran==null){
            anggaran.setAmount(BigDecimal.ZERO);
        }else{
            BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
            if (totalAnggaran==null){
                anggaran.setAmount(BigDecimal.ZERO);
            }else{
                anggaran.setAmount(totalAnggaran);
            }
        }


        anggaran.setDepartemen(departemenDao.findById(departemen).get());
        anggaran.setPeriodeAnggaran(periodeAnggaranDao.findById(periodeAnggaranAktif).get());
        if (anggaran.getStatus()==null){
            anggaran.setStatus(StatusRecord.HAPUS);
        }

        if(errors.hasErrors()){
            model.addAttribute("anggaran_budget","active");
            return "setting/anggaran/anggaranform";
        }
        anggaranDao.save(anggaran);


        PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
        BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
        picAnggaran.setJumlah(totalAmountAnggaran);
        picAnggaranDao.save(picAnggaran);


        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;
    }

    @PostMapping("/setting/anggaran/hapus")
    public String deleteAnggaran(@RequestParam Anggaran anggaran, @RequestParam(required = false)String departemen
                                , @RequestParam(required = false)String periode,
                                 RedirectAttributes attributes){
        String periodeAnggaranAktif = "";
        String periodeAnggaranAktif1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        if(periodeAnggaranAktif1 == null){
            periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        }else{
            periodeAnggaranAktif = periodeAnggaranAktif1;
        }
        String idDepartemen = departemen;
        String idPeriode = periodeAnggaranAktif;

        BigDecimal totalPengjuanDana = pengajuanDanaDao.getTotalPengajuanDanaAnggaran(anggaran.getId());

        if ( totalPengjuanDana == null ) {

            String idAnggaran=anggaran.getId();
            if(idAnggaran==null){
                anggaran.setAmount(BigDecimal.ZERO);
            }else{
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if (totalAnggaran == null){
                    anggaran.setAmount(BigDecimal.ZERO);
                }else{
                    anggaran.setAmount(totalAnggaran);
                }
            }
            anggaran.setStatus(StatusRecord.HAPUS);
            anggaranDao.save(anggaran);

            PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
            BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode);
            if(totalAmountAnggaran == null){
                picAnggaran.setJumlah(BigDecimal.ZERO);
            }else{
                picAnggaran.setJumlah(totalAmountAnggaran);
            }

            picAnggaranDao.save(picAnggaran);

            attributes.addFlashAttribute("hapus", "Save Data Berhasil");
            return "redirect:setting?departemen="+idDepartemen+"&periode="+periodeAnggaranAktif;

        }else{

            attributes.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:setting?departemen="+idDepartemen+"&periode="+periodeAnggaranAktif;

        }


    }



    @GetMapping("/setting/anggaran/estimasi/export")
    public void exportToExcel(@RequestParam(required = true) Departemen departemen,
                              @RequestParam(required = true) PeriodeAnggaran periodeAnggaran,
                              HttpServletResponse response,
                              Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Employes_All_Company_" + currentDateTime + ".xlsx";
        headerValue = "attachment; filename=Estimated_Budget" + departemen.getNamaDepartemen() + "_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<ExportAnggaranDetailEstimasiDepartemenDto> anggaranDetailEstimasiDepartemenDtos = anggaranDetailEstimasiDao.exportAnggaranDetailEstimasiDepartemen(departemen.getId(), periodeAnggaran.getId());
        ExportEstimasiTransaksiDepartemen excelExporter = new ExportEstimasiTransaksiDepartemen(anggaranDetailEstimasiDepartemenDtos);
        excelExporter.export(response);

    }
}
