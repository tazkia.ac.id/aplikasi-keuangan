package id.ac.tazkia.aplikasikeuangan.controller.keuangan;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPendapatanTerkaitDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPendapatanTerkaitDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkaitDetail;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Controller
public class EstimasiPendapatanTerkaitDetailController {

    @Autowired
    private EstimasiPendapatanTerkaitDetailDao estimasiPendapatanTerkaitDetailDao;

    @Autowired
    private SatuanDao satuanDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EstimasiPendapatanTerkaitDao estimasiPendapatanTerkaitDao;

    @GetMapping("/finance/estimasi_pendapatan_terkait/detail")
    public String estimasiPendapatanTerkaitDetail(Model model, @RequestParam EstimasiPendapatanTerkait id){

        model.addAttribute("estimasiPendapatanTerkait",id);
        model.addAttribute("estimasiPendapatanTerkaitDetail", estimasiPendapatanTerkaitDetailDao.findByStatusAndEstimasiPendapatanTerkaitOrderByTanggalEstimasi(StatusRecord.AKTIF, id));

        model.addAttribute("pendapatanTerkait","active");
        return "keuangan/pendapatan_terkait/estimasi_detail/list";
    }


    @GetMapping("/finance/estimasi_pendapatan_terkait/detail/baru")
    public String estimasiPendapatanTerkaitDetailBaru(Model model, @RequestParam EstimasiPendapatanTerkait id){

        model.addAttribute("estimasiPendapatanTerkait",id);
        model.addAttribute("estimasiPendapatanTerkaitDetail", new EstimasiPendapatanTerkaitDetail());
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("status", "baru");

        model.addAttribute("pendapatanTerkait","active");
        return "keuangan/pendapatan_terkait/estimasi_detail/form";
    }

    @PostMapping("/finance/estimasi_pendapatan_terkait/detail/baru")
    public String simpanEstimasiPendapatanTerkaitDetail(@ModelAttribute EstimasiPendapatanTerkaitDetail estimasiPendapatanTerkaitDetail,
                                                        @RequestParam EstimasiPendapatanTerkait estimasiPendapatanTerkait, Authentication authentication){

        User user = currentUserService.currentUser(authentication);

        if(estimasiPendapatanTerkaitDetail.getEstimasiPendapatanTerkait() == null){
            estimasiPendapatanTerkaitDetail.setEstimasiPendapatanTerkait(estimasiPendapatanTerkait);
        }
        estimasiPendapatanTerkaitDetail.setNominalSatuan(estimasiPendapatanTerkait.getNominalSatuan());
        estimasiPendapatanTerkaitDetail.setNominalTotal(estimasiPendapatanTerkaitDetail.getNominalSatuan().multiply(estimasiPendapatanTerkaitDetail.getJumlah()));
        estimasiPendapatanTerkaitDetail.setTanggalInsert(LocalDateTime.now());
        estimasiPendapatanTerkaitDetail.setUserInsert(user.getUsername());
        estimasiPendapatanTerkaitDetailDao.save(estimasiPendapatanTerkaitDetail);

        BigDecimal totalDetail = estimasiPendapatanTerkaitDetailDao.totalEstimasiPendapatanTerkait(estimasiPendapatanTerkait.getId());
        if(totalDetail == null){
            totalDetail = BigDecimal.ZERO;
        }
        if(totalDetail.compareTo(BigDecimal.ZERO) > 0){
            estimasiPendapatanTerkait.setNominalTotal(totalDetail);
        }else {
            estimasiPendapatanTerkait.setNominalTotal(estimasiPendapatanTerkait.getNominalSatuan());
        }
        estimasiPendapatanTerkaitDao.save(estimasiPendapatanTerkait);

        return "redirect:/finance/estimasi_pendapatan_terkait/detail?id="+estimasiPendapatanTerkait.getId();
    }

}
