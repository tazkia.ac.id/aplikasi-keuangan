package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.MataAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PenerimaanRealDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PenerimaanReal;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Controller
public class PenerimaanRealController {

    @Autowired
    private PenerimaanRealDao penerimaanRealDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PenerimaanDao penerimaanDao;

    @Autowired
    private MataAnggaranDao mataAnggaranDao;

    @Autowired
    private SatuanDao satuanDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    @Value("${upload.penerimaan}")
    private String uploadFolder;


    @GetMapping("/transaksi/penerimaanreal")
    public String listPenerimaanRealList(Model model,
                                     String search,
                                     @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                     @RequestParam(required = false) MataAnggaran mataAnggaran,
                                     @PageableDefault(size = 10)Pageable page){

        model.addAttribute("selectedPeriode", periodeAnggaran);
        model.addAttribute("selectedMataAnggaran", mataAnggaran);
        if (periodeAnggaran != null) {
            String periodeAnggaran2 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
            List<String> periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif1(LocalDate.now());
            model.addAttribute("listPeriodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran2));
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusAndIdNotInOrderByKodePeriodeAnggaran(StatusRecord.AKTIF, periodeAnggaran1));

            if (mataAnggaran != null){
                model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranAndMataAnggaranAndKeteranganContainingIgnoreCaseOrderByTanggalPenerimaanDesc(StatusRecord.AKTIF, periodeAnggaran, mataAnggaran, search, page));
                }else{
                    model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranAndMataAnggaranOrderByTanggalPenerimaanDesc(StatusRecord.AKTIF, periodeAnggaran, mataAnggaran, page));
                }
            }else{
                model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranAndKeteranganContainingIgnoreCaseOrderByTanggalPenerimaanDesc(StatusRecord.AKTIF, periodeAnggaran, search, page));
                }else{
                    model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranOrderByTanggalPenerimaanDesc(StatusRecord.AKTIF, periodeAnggaran, page));
                }
            }
        }else{
            String periodeAnggaran2 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
            List<String> periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif1(LocalDate.now());
            model.addAttribute("listPeriodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran2));
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusAndIdNotInOrderByKodePeriodeAnggaran(StatusRecord.AKTIF, periodeAnggaran1));
            model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
            model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranIdOrderByMataAnggaran(StatusRecord.AKTIF, periodeAnggaran2, page));
        }

        model.addAttribute("finance", "active");
        model.addAttribute("penerimaanreal", "active");
        return "transaksi/penerimaanreal/real";
    }



    @GetMapping("/transaksi/penerimaanreal/add")
    public String addPenerimaanReal(Model model,
                                    @RequestParam(required = false) String estimasi){

        Penerimaan penerimaan = penerimaanDao.findById(estimasi).get();

        model.addAttribute("penerimaan", penerimaan);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("penerimaanReal", new PenerimaanReal());

        model.addAttribute("finance", "active");
        model.addAttribute("penerimaanreal", "active");
        return "transaksi/penerimaanreal/realadd";

    }

    @GetMapping("/transaksi/penerimaanreal/editreal")
    public String editPenerimaanReal(Model model,
                                     @RequestParam(required = false)String id,
                                     @RequestParam(required = false)String estimasi){

        Penerimaan penerimaan = penerimaanDao.findById(estimasi).get();

        model.addAttribute("penerimaan", penerimaan);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("penerimaanReal", penerimaanRealDao.findById(id).get());

        model.addAttribute("finance", "active");
        model.addAttribute("penerimaanreal", "active");
        return "transaksi/penerimaanreal/realadd";

    }



    @PostMapping("/transaksi/penerimaanreal/realsave")
    public String savePenerimaanReal(Model model,
                                     @RequestParam(required = false)Penerimaan penerimaan,
                                     @ModelAttribute @Valid PenerimaanReal penerimaanReal,
                                     Authentication authentication,
                                     BindingResult errors,
                                     @RequestParam("lampiran") MultipartFile file,
                                     RedirectAttributes attributes) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        if (errors.hasErrors()) {
            model.addAttribute("finance", "active");
            model.addAttribute("penerimaanreal", "active");
            return "transaksi/penerimaanreal/realadd";
        }


        String tahun = penerimaanReal.getTanggalan().substring(0,4);
        String bulan = penerimaanReal.getTanggalan().substring(5,7);
        String tanggal = penerimaanReal.getTanggalan().substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
//
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
//
        String idFile = UUID.randomUUID().toString();

        String date = penerimaanReal.getTanggalan();


//        String tahun = date.substring(6, 10);
//        String bulan = date.substring(0, 2);
//        String tanggal = date.substring(3, 5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
//        System.out.println("Input Date?= "+ tanggalan);
//        System.out.println("Converted Date?= " + localDate + "\n");
        if (ukuran == 0){
            if (penerimaanReal.getId() == null){
                penerimaanReal.setFile("default.jpg");
            }else{
                penerimaanReal.setFile(penerimaanReal.getFile());
            }
        }else{
            penerimaanReal.setFile(idFile + "." + extension);
        }

        penerimaanReal.setPenerimaan(penerimaan);
        penerimaanReal.setTanggal(localDate.plusDays(1));
        penerimaanReal.setTotal(penerimaanReal.getKuantitas().multiply(penerimaanReal.getAmount()));
        penerimaanReal.setUserUpdate(karyawan.getNamaKaryawan());
        penerimaanReal.setTanggalUpdate(LocalDateTime.now());

        new File(uploadFolder).mkdirs();
        File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);

        if (errors.hasErrors()) {

            model.addAttribute("penerimaan", penerimaanDao.findById(penerimaanReal.getPenerimaan().getId()).get());
            model.addAttribute("satuan", satuanDao.findAll());
            model.addAttribute("finance", "active");
            model.addAttribute("penerimaanreal", "active");
            return "setting/penerimaan/form";

        }
        penerimaanRealDao.save(penerimaanReal);
        BigDecimal totalRealisasi = penerimaanRealDao.getTotalPenerimaanReal(penerimaan.getId());
        if (totalRealisasi == null){
            penerimaan.setRealisasi(BigDecimal.ZERO);
        }else{
            penerimaan.setRealisasi(penerimaanRealDao.getTotalPenerimaanReal(penerimaan.getId()));
        }
        penerimaanDao.save(penerimaan);
        attributes.addFlashAttribute("success", "Input Data Berhasil");
        return "redirect:../penerimaanreal/detail?estimasi="+penerimaan.getId();

    }

    @GetMapping("/transaksi/penerimaan/realisasi")
    public String listPenerimaanReal(Model model,
                                     String search,
                                     @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                     @RequestParam(required = false) MataAnggaran mataAnggaran,
                                     @PageableDefault(size = 10)Pageable page){

        model.addAttribute("selectedPeriode", periodeAnggaran);
        model.addAttribute("selectedMataAnggaran", mataAnggaran);

        if (periodeAnggaran != null){
            String periodeAnggaran2 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
            List<String> periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif1(LocalDate.now());
            model.addAttribute("listPeriodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran2));
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusAndIdNotInOrderByKodePeriodeAnggaran(StatusRecord.AKTIF, periodeAnggaran1));
            if(mataAnggaran != null){
                model.addAttribute("listMataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listPenerimaanReal", penerimaanRealDao.findByStatusAndPenerimaanPeriodeAnggaranAndPenerimaanMataAnggaranAndKeteranganContainingIgnoreCaseOrderByTanggal(StatusRecord.AKTIF, periodeAnggaran, mataAnggaran, search, page));
                }else{
                    model.addAttribute("listPenerimaanReal", penerimaanRealDao.findByStatusAndPenerimaanPeriodeAnggaranAndPenerimaanMataAnggaranOrderByTanggal(StatusRecord.AKTIF, periodeAnggaran, mataAnggaran, page));
                }
            }else{
                model.addAttribute("listMataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
                model.addAttribute("listPenerimaanReal", penerimaanRealDao.findByStatusAndPenerimaanPeriodeAnggaranIdOrderByTanggal(StatusRecord.AKTIF, periodeAnggaran.getId(), page));
            }
        }else{
            String periodeAnggaran2 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
            List<String> periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif1(LocalDate.now());
            model.addAttribute("listPeriodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran2));
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusAndIdNotInOrderByKodePeriodeAnggaran(StatusRecord.AKTIF, periodeAnggaran1));
            model.addAttribute("listMataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
            model.addAttribute("listPenerimaanReal", penerimaanRealDao.findByStatusAndPenerimaanPeriodeAnggaranIdOrderByTanggal(StatusRecord.AKTIF, periodeAnggaran2, page));
        }

        model.addAttribute("finance", "active");
        model.addAttribute("penerimaanreal", "active");
        return "transaksi/penerimaanreal/list";
    }


    @GetMapping("/tranasaksi/penerimaan/realisasi/baru")
    public String baruRealisasiPenerimaan(Model model,
                                          @RequestParam(required = false)String id){
        String periodeAnggaran2 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranIdOrderByMataAnggaran(StatusRecord.AKTIF, periodeAnggaran2));
        model.addAttribute("penerimaanReal", new PenerimaanReal());

        model.addAttribute("finance", "active");
        model.addAttribute("penerimaanreal", "active");
        return "transaksi/penerimaanreal/form";

    }

    @GetMapping("/penerimaan/{penerimaanReal}/bukti/")
    public ResponseEntity<byte[]> tampilkanBuktiPenerimaan(@PathVariable PenerimaanReal penerimaanReal) throws Exception {
        String lokasiFile = uploadFolder + File.separator + penerimaanReal.getFile();

        try {
            HttpHeaders headers = new HttpHeaders();
            if (penerimaanReal.getFile().toLowerCase().endsWith("jpeg") || penerimaanReal.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (penerimaanReal.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}