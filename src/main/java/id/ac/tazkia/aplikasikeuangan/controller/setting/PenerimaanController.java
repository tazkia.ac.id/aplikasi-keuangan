package id.ac.tazkia.aplikasikeuangan.controller.setting;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.MataAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AkunLevelTigaDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.RencanaPenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PenerimaanRealDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPenerimaan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import jakarta.validation.Valid;

@Controller
public class PenerimaanController {

    @Autowired
    private RencanaPenerimaanDao rencanaPenerimaanDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private MataAnggaranDao mataAnggaranDao;

    @Autowired
    private PenerimaanDao penerimaanDao;

    @Autowired
    private SatuanDao satuanDao;

    @Autowired
    private DepartemenDao departementDao;

    @Autowired
    private EstimasiPenerimaanDao estimasiPenerimaanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private AkunLevelTigaDao akunLevelTigaDao;

    @Autowired
    private PenerimaanRealDao penerimaanRealDao;

    @GetMapping("/setting/penerimaan")
    public String daftarPenerimaan(Model model, @PageableDefault(size = 10)Pageable page, String search,
                                   @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                   @RequestParam(required = false) MataAnggaran mataAnggaran){

        model.addAttribute("selectedMataAnggaran", mataAnggaran);
        if (periodeAnggaran != null) {
//            model.addAttribute("selectedPeriode", periodeAnggaran);
            model.addAttribute("listPeriodeAnggaranAktif", periodeAnggaran);
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));
            if (mataAnggaran != null ){
                model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranAndMataAnggaranAndKeteranganContainingIgnoreCaseOrderByTanggalPenerimaanDesc(StatusRecord.AKTIF, periodeAnggaran, mataAnggaran, search, page));
                    model.addAttribute("totalPenerimaan", penerimaanDao.getTotalEstimasiPenerimaanPerAnggaran(periodeAnggaran.getId(), mataAnggaran.getId()));
                    model.addAttribute("totalPenerimaanDeviasi", penerimaanDao.getTotalEstimasiPenerimaanPerAnggaranDeviasi(periodeAnggaran.getId(), mataAnggaran.getId()));

                }else{

                    model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranAndMataAnggaranOrderByTanggalPenerimaanDesc(StatusRecord.AKTIF, periodeAnggaran, mataAnggaran, page));
                    model.addAttribute("totalPenerimaan", penerimaanDao.getTotalEstimasiPenerimaanPerAnggaran(periodeAnggaran.getId(), mataAnggaran.getId()));
                    model.addAttribute("totalPenerimaanDeviasi", penerimaanDao.getTotalEstimasiPenerimaanPerAnggaranDeviasi(periodeAnggaran.getId(), mataAnggaran.getId()));

                }
            }else{
                model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("totalPenerimaan", penerimaanDao.getTotalEstimasiPenerimaan(periodeAnggaran.getId()));
                    model.addAttribute("totalPenerimaanDeviasi", penerimaanDao.getTotalEstimasiPenerimaanDeviasi(periodeAnggaran.getId()));
                    model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranAndKeteranganContainingIgnoreCaseOrderByTanggalPenerimaanDesc(StatusRecord.AKTIF, periodeAnggaran, search, page));

                }else{
                    model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranOrderByTanggalPenerimaanDesc(StatusRecord.AKTIF, periodeAnggaran, page));
                    model.addAttribute("totalPenerimaan", penerimaanDao.getTotalEstimasiPenerimaan(periodeAnggaran.getId()));
                    model.addAttribute("totalPenerimaanDeviasi", penerimaanDao.getTotalEstimasiPenerimaanDeviasi(periodeAnggaran.getId()));
                }
            }
        }else{

            String periodeAnggaran2 = periodeAnggaranCrudDao.getAnggaranAktifA(LocalDate.now());
            List<String> periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif2(LocalDate.now());
            model.addAttribute("listPeriodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran2));
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusAndIdNotInOrderByKodePeriodeAnggaran(StatusRecord.AKTIF, periodeAnggaran1));
            model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
            model.addAttribute("listPenerimaan", penerimaanDao.findByStatusAndPeriodeAnggaranIdOrderByMataAnggaran(StatusRecord.AKTIF, periodeAnggaran2, page));
            model.addAttribute("totalPenerimaan", penerimaanDao.getTotalEstimasiPenerimaan(periodeAnggaran2));
            model.addAttribute("totalPenerimaanDeviasi", penerimaanDao.getTotalEstimasiPenerimaanDeviasi(periodeAnggaran2));
        }
        return "setting/penerimaan/list";
    }

    @GetMapping("/setting/penerimaan/baru")
    public String formPenerimaan(Model model, @RequestParam(required = false)String id){

        model.addAttribute("periode", periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));
        model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
//        model.addAttribute("listAkunLevelTiga", akunLevelTigaDao.findByStatusOrderByAkunLevelDuaAkunLevelSatuAkunNamaAkun(StatusRecord.AKTIF));
        model.addAttribute("listDepartement", departementDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("penerimaan", new Penerimaan());

        return "setting/penerimaan/form";

    }

    @GetMapping("/setting/penerimaan/edit")
    public String editPenerimaan(Model model, @RequestParam(required = false)String id){


        Penerimaan penerimaan = penerimaanDao.findById(id).get();
        if (penerimaan != null) {
            model.addAttribute("penerimaan", penerimaan);

            model.addAttribute("selectedPeriode", periodeAnggaranDao.findById(penerimaan.getPeriodeAnggaran().getId()));
            model.addAttribute("selectedMataAnggaran", mataAnggaranDao.findById(penerimaan.getMataAnggaran().getId()));
        }
        model.addAttribute("periode", periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));
        model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
//        model.addAttribute("listAkunLevelTiga", akunLevelTigaDao.findByStatusOrderByAkunLevelDuaAkunLevelSatuAkunNamaAkun(StatusRecord.AKTIF));
        model.addAttribute("listDepartement", departementDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));
        model.addAttribute("satuan", satuanDao.findAll());

        return "setting/penerimaan/form";
    }

    @GetMapping("/setting/penerimaan/estimasi")
    public String estimasiPenerimaan (Model model, @RequestParam(required = false)String id){

        Penerimaan penerimaan = penerimaanDao.findById(id).get();
        model.addAttribute("rencanaPenerimaan", penerimaan);
        model.addAttribute("listEstimasiPenerimaan", estimasiPenerimaanDao.findByStatusAndPenerimaanOrderByTanggal(StatusRecord.AKTIF, penerimaan));

        return "setting/penerimaan/estimasi/list";

    }


    @GetMapping("/setting/penerimaan/estimasi/baru")
    public String estimasiPenerimaanBaru (Model model, @RequestParam(required = false)String id){

        Penerimaan penerimaan = penerimaanDao.findById(id).get();
        model.addAttribute("rencanaPenerimaan", penerimaan);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("estimasiPenerimaan", new EstimasiPenerimaan());

        return "setting/penerimaan/estimasi/form";

    }

    @GetMapping("/setting/penerimaan/estimasi/edit")
    public String estimasiPenerimaanEdit (Model model, @RequestParam(required = false)EstimasiPenerimaan estimasiPenerimaan){

        Penerimaan penerimaan = penerimaanDao.findById(estimasiPenerimaan.getPenerimaan().getId()).get();
        model.addAttribute("rencanaPenerimaan", penerimaan);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("estimasiPenerimaan", estimasiPenerimaan);

        return "setting/penerimaan/estimasi/edit";

    }

    @PostMapping("/setting/penerimaan/estimasi/save")
    public String saveEstimasiPenerimaan(Model model,
                                         @RequestParam(required = true)Penerimaan penerimaan,
                                         @ModelAttribute @Valid EstimasiPenerimaan estimasiPenerimaan,
                                         BindingResult errors,
                                         RedirectAttributes attributes,
                                         Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
//        Penerimaan penerimaan = penerimaanDao.findById(penerimaan.getId()).get();

        
        BigDecimal totalRencana = estimasiPenerimaanDao.cariTotalPenerimaan(penerimaan.getId());
        if(totalRencana == null){
            totalRencana = BigDecimal.ZERO;
        }
        BigDecimal totalRencana2 = totalRencana.add(estimasiPenerimaan.getKuantitas().multiply(estimasiPenerimaan.getJumlahSatuan()));
        BigDecimal sisa = totalRencana2.subtract(penerimaan.getTotal());
        // if(sisa.compareTo(BigDecimal.ZERO) > 0){
        //     model.addAttribute("rencanaPenerimaan", penerimaan);
        //     model.addAttribute("satuan", satuanDao.findAll());
        //     model.addAttribute("estimasiPenerimaan", estimasiPenerimaan);
        //     model.addAttribute("error", "Total nominal estimasi penerimaan tidak boleh melebihi rencana awal penerimaan");
        //     return "setting/penerimaan/estimasi/form";
        // }

        String date = estimasiPenerimaan.getTanggalString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        estimasiPenerimaan.setTanggal(localDate);
        estimasiPenerimaan.setPenerimaan(penerimaan);
        estimasiPenerimaan.setTotal(estimasiPenerimaan.getKuantitas().multiply(estimasiPenerimaan.getJumlahSatuan()));

        BigDecimal totalEstimasiPenerimaan = estimasiPenerimaanDao.getTotalEstimasiPenerimaan(penerimaan.getId());
        BigDecimal kuantitasEstimasiPenerimaan = estimasiPenerimaanDao.getKuantitasEstimasiPenerimaan(penerimaan.getId());
        BigDecimal totalAmount = totalEstimasiPenerimaan.add(estimasiPenerimaan.getTotal());
        BigDecimal totalKuantitas = kuantitasEstimasiPenerimaan.add(estimasiPenerimaan.getKuantitas());

        penerimaan.setTotal(totalAmount);
        penerimaan.setKuantitas(totalKuantitas);
        penerimaan.setSatuan(estimasiPenerimaan.getSatuan());
        penerimaan.setAmount(estimasiPenerimaan.getJumlahSatuan());
        penerimaan.setUserEdit(karyawan.getNamaKaryawan());
        penerimaan.setTanggalEdit(LocalDateTime.now());

        estimasiPenerimaanDao.save(estimasiPenerimaan);
        penerimaanDao.save(penerimaan);

        attributes.addFlashAttribute("success", "Delete Data Berhasil");
        return "redirect:../estimasi?id="+ penerimaan.getId();

    }

    @PostMapping("/setting/penerimaan/estimasi/update")
    public String updateEstimasiPenerimaan(Model model,
                                         @RequestParam(required = false)Penerimaan penerimaan,
                                         @ModelAttribute @Valid EstimasiPenerimaan estimasiPenerimaan,
                                         BindingResult errors,
                                         RedirectAttributes attributes,
                                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
//        Penerimaan penerimaan = penerimaanDao.findById(penerimaan.getId()).get();

        EstimasiPenerimaan estimasiPenerimaan1 = estimasiPenerimaanDao.findById(estimasiPenerimaan.getId()).get();
        String date = estimasiPenerimaan.getTanggalString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        estimasiPenerimaan.setTanggal(localDate);
//        estimasiPenerimaan.setPenerimaan(penerimaan);
        estimasiPenerimaan.setTotal(estimasiPenerimaan.getKuantitas().multiply(estimasiPenerimaan.getJumlahSatuan()));

        BigDecimal totalEstimasiPenerimaan = estimasiPenerimaanDao.getTotalEstimasiPenerimaan(penerimaan.getId());
        BigDecimal totalAmount = (totalEstimasiPenerimaan.subtract(estimasiPenerimaan1.getTotal())).add(estimasiPenerimaan.getTotal());
        BigDecimal kuantitasEstimasiPenerimaan = estimasiPenerimaanDao.getKuantitasEstimasiPenerimaan(penerimaan.getId());
        BigDecimal totalKuantitas = (kuantitasEstimasiPenerimaan.subtract(estimasiPenerimaan1.getKuantitas())).add(estimasiPenerimaan.getKuantitas());
//        BigDecimal totalAmount = totalEstimasiPenerimaan.add(estimasiPenerimaan.getTotal());

        penerimaan.setTotal(totalAmount);
        penerimaan.setKuantitas(totalKuantitas);
        penerimaan.setSatuan(estimasiPenerimaan.getSatuan());
        penerimaan.setAmount(estimasiPenerimaan.getJumlahSatuan());
        penerimaan.setUserEdit(karyawan.getNamaKaryawan());
        penerimaan.setTanggalEdit(LocalDateTime.now());
        estimasiPenerimaanDao.save(estimasiPenerimaan);
        penerimaanDao.save(penerimaan);

        attributes.addFlashAttribute("success", "Delete Data Berhasil");
        return "redirect:../estimasi?id="+ penerimaan.getId();

    }

    @PostMapping("/setting/penerimaan/estimasi/hapus")
    public String hapusEstimasiPenerimaan(@RequestParam(required = false)EstimasiPenerimaan estimasiPenerimaan,
                                           RedirectAttributes attributes,
                                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
       Penerimaan penerimaan = penerimaanDao.findById(estimasiPenerimaan.getPenerimaan().getId()).get();

        EstimasiPenerimaan estimasiPenerimaan1 = estimasiPenerimaanDao.findById(estimasiPenerimaan.getId()).get();
//        String date = estimasiPenerimaan.getTanggalString();
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
//        estimasiPenerimaan.setTanggal(localDate);
//        estimasiPenerimaan.setPenerimaan(penerimaan);

//        estimasiPenerimaan.setTotal(estimasiPenerimaan.getKuantitas().multiply(estimasiPenerimaan.getJumlahSatuan()));
        BigDecimal totalEstimasiPenerimaan = estimasiPenerimaanDao.getTotalEstimasiPenerimaan(estimasiPenerimaan.getPenerimaan().getId());
        BigDecimal totalAmount = totalEstimasiPenerimaan.subtract(estimasiPenerimaan1.getTotal());
        BigDecimal kuantitasEstimasiPenerimaan = estimasiPenerimaanDao.getKuantitasEstimasiPenerimaan(estimasiPenerimaan.getPenerimaan().getId());
        BigDecimal totalKuantitas = kuantitasEstimasiPenerimaan.subtract(estimasiPenerimaan1.getKuantitas());
//        BigDecimal totalAmount = totalEstimasiPenerimaan.add(estimasiPenerimaan.getTotal());


        estimasiPenerimaan.setStatus(StatusRecord.HAPUS);
        penerimaan.setTotal(totalAmount);
        penerimaan.setKuantitas(totalKuantitas);
        penerimaan.setSatuan(estimasiPenerimaan.getSatuan());
        penerimaan.setAmount(estimasiPenerimaan.getJumlahSatuan());
        penerimaan.setTanggalEdit(LocalDateTime.now());
        penerimaan.setUserEdit(karyawan.getNamaKaryawan());

        estimasiPenerimaanDao.save(estimasiPenerimaan);
        penerimaanDao.save(penerimaan);

        attributes.addFlashAttribute("success", "Delete Data Berhasil");
        return "redirect:../estimasi?id="+ penerimaan.getId();

    }

    @PostMapping("/setting/penerimaan/save")
    public String savePenerimaan(Model model,
                                 @ModelAttribute @Valid Penerimaan penerimaan,
                                 BindingResult errors,
                                 RedirectAttributes attributes,
                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String date = penerimaan.getTanggalPeneriman();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
//        System.out.println("Input Date?= "+ tanggalan);
//        System.out.println("Converted Date?= " + localDate + "\n");
//        penerimaan.setTanggalPenerimaan(localDate.plusDays(1));
        penerimaan.setTanggalPenerimaan(localDate);
        penerimaan.setUserEdit(karyawan.getNamaKaryawan());
        penerimaan.setTanggalEdit(LocalDateTime.now());
        penerimaan.setRencanaPenerimaan(rencanaPenerimaanDao.findByStatusAndPeriodeAnggaranAndMataAnggaran(StatusRecord.AKTIF, penerimaan.getPeriodeAnggaran(), penerimaan.getMataAnggaran()));
        penerimaan.setTotal(penerimaan.getKuantitas().multiply(penerimaan.getAmount()));

        if(penerimaan.getId() == null){
            penerimaan.setRealisasi(BigDecimal.ZERO);
        }else{
            BigDecimal penerimaanReal = penerimaanRealDao.getTotalPenerimaanReal(penerimaan.getId());
            if(penerimaanReal == null){
                penerimaan.setRealisasi(BigDecimal.ZERO);
            }else{
                penerimaan.setRealisasi(penerimaanReal);
            }
        }

        if(errors.hasErrors()){

            model.addAttribute("selectedPeriode", periodeAnggaranDao.findById(penerimaan.getPeriodeAnggaran().getId()));
            model.addAttribute("selectedMataAnggaran", mataAnggaranDao.findById(penerimaan.getMataAnggaran().getId()));
            model.addAttribute("periode", periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));
            model.addAttribute("mataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF));
            model.addAttribute("satuan", satuanDao.findAll());

            return "setting/penerimaan/form";

        }



        penerimaanDao.save(penerimaan);
        attributes.addFlashAttribute("success", "Delete Data Berhasil");
        return "redirect:../penerimaan?periodeAnggaran="+ penerimaan.getPeriodeAnggaran().getId();

    }

    @PostMapping("/setting/penerimaan/hapus")
    public String deletePenerimaan(@RequestParam Penerimaan penerimaan,
                                   RedirectAttributes attributes,
                                   Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        Penerimaan penerimaan1 = penerimaanDao.findById(penerimaan.getId()).get();
        if(penerimaan1.getRealisasi().compareTo(BigDecimal.ZERO) == 0) {

            penerimaan1.setStatus(StatusRecord.HAPUS);
            penerimaan1.setTanggalEdit(LocalDateTime.now());
            penerimaan1.setUserEdit(karyawan.getNamaKaryawan());
            penerimaanDao.save(penerimaan1);
            attributes.addFlashAttribute("delete", "Delete Data Berhasil");
            return "redirect:../penerimaan";


        }else{

            attributes.addFlashAttribute("deletefail", "Data Tidak Bisa Dihapus karena sudah ada realisasi revanues");
            return "redirect:../penerimaan?periodeAnggaran=\"+ penerimaan.getPeriodeAnggaran().getId()";

        }

    }

}