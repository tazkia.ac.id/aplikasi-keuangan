package id.ac.tazkia.aplikasikeuangan.controller;

import id.ac.tazkia.aplikasikeuangan.dao.HistorySaldoHarianDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.*;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PenerimaanRealDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengembalianDanaProsesDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Controller
public class DashboardController {

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @Autowired
    private PenerimaanDao penerimaanDao;

    @Autowired
    private PenerimaanRealDao penerimaanRealDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PaguAnggaranDao paguAnggaranDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PencairanDanaDao pencairanDanaDao;

    @Autowired
    private PengembalianDanaProsesDao pengembalianDanaProsesDao;

    @Autowired
    private HistorySaldoHarianDao historySaldoHarianDao;



    @GetMapping("/dashboard2")
    public String dashboardUtama(Model model,
                                 @PageableDefault(size = 5) Pageable page,
                                 Authentication authentication) {

        System.out.println(authentication);
        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now().plusDays(1));

        PeriodeAnggaran periodeAnggaran1 = periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran);

        if (periodeAnggaran == null){

            return "redirect:/setting/periodeanggaran";

        }

        BigDecimal totalBudget =  anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaran);

        BigDecimal totalPengajuanDanaAktif = pengajuanDanaDao.getTotalPengajuanDana2(periodeAnggaran);


        if (totalBudget == null){
            totalBudget = BigDecimal.ZERO;
        }

        if (totalPengajuanDanaAktif == null){
            totalPengajuanDanaAktif = BigDecimal.ZERO;
        }

        BigDecimal totalBudgetBalance = totalBudget.subtract(totalPengajuanDanaAktif);

        model.addAttribute("periodeAnggaran", periodeAnggaranDao.findById(periodeAnggaran).get());

        model.addAttribute("listSisaAnggaran" , anggaranCrudDao.getSisaAnggaranPerDepartemen(periodeAnggaran));

        model.addAttribute("totalAnggaranPeriode", totalBudget);

        model.addAttribute("totalPenerimaanPeriode", penerimaanDao.getTotalEstimasiPenerimaan(periodeAnggaran));

        model.addAttribute("totalPenerimaanReale", penerimaanRealDao.getTotalRealisasiPenerimaan(periodeAnggaran));

        model.addAttribute("totalPengajuanDana", pengajuanDanaDao.getTotalPengajuanDana2(periodeAnggaran));

        model.addAttribute("totalBalance",totalBudgetBalance);

        model.addAttribute("totalPaguAnggaran", paguAnggaranDao.getTotalPaguAnggaran(periodeAnggaran));

        BigDecimal totalPencairan = pencairanDanaDao.totalPencairan(periodeAnggaran1.getTanggalMulaiAnggaran());

        if (totalPencairan == null){
            totalPencairan = BigDecimal.ZERO;
        }
        model.addAttribute("totalPencairan", totalPencairan);

        BigDecimal totalPengembalian = pengembalianDanaProsesDao.totalPengembalian(periodeAnggaran1.getTanggalMulaiAnggaran());
        if (totalPengembalian == null){
            totalPengembalian = BigDecimal.ZERO;
        }

        model.addAttribute("totalPengembalian", totalPengembalian);

        
        return "dashboard";

    }

    @GetMapping("/dashboard")
    public String dashboardUtamas(Model model,
                                 @PageableDefault(size = 5) Pageable page,
                                 Authentication authentication) {

        LocalDateTime localDateTime = historySaldoHarianDao.tanggalUpdateTerakhir();
        String periodeAnggaranAktif1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        String periodeBerjalan = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        BigDecimal totalBudget =  anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeBerjalan);
        BigDecimal totalPengajuanDanaAktif = historySaldoHarianDao.totalPengeluaran(localDateTime);
        if (totalBudget == null){
            totalBudget = BigDecimal.ZERO;
        }

        if (totalPengajuanDanaAktif == null){
            totalPengajuanDanaAktif = BigDecimal.ZERO;
        }
        BigDecimal totalBudgetBalance = totalBudget.subtract(totalPengajuanDanaAktif);

        model.addAttribute("totalAnggaranPeriode", totalBudget);
        model.addAttribute("totalPenerimaanPeriode", penerimaanDao.getTotalEstimasiPenerimaan(periodeBerjalan));
        model.addAttribute( "periodeAnggaranBerjalan", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeBerjalan));
        model.addAttribute("periodeAnggaranAktif", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaranAktif1));
        model.addAttribute("totalBalance",totalBudgetBalance);
//        model.addAttribute("totalPengajuanDana", pengajuanDanaDao.getTotalPengajuanDana2(periodeBerjalan));
        model.addAttribute("totalPengajuanDana", historySaldoHarianDao.totalPengeluaran(localDateTime));
        model.addAttribute("tanggalUpdate", localDateTime);


        return "dashboard2";
    }
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("loginError", true);
        return "login2";
    }

    @GetMapping("/")
    public String formAwal(){
        return "redirect:/dashboard";
    }

    @GetMapping("/404")
    public String form404(){

        return "error";
    }



}
