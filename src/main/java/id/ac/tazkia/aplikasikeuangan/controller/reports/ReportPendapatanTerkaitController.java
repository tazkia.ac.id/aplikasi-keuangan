package id.ac.tazkia.aplikasikeuangan.controller.reports;


import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengeluaranTerkaitDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.util.List;

@Controller
public class ReportPendapatanTerkaitController {

    @Autowired
    private PengeluaranTerkaitDao pengeluaranTerkaitDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;


    @GetMapping("/reports/saldo_pendapatan_terkait")
    public String saldoPendapatanTerkait(Model model, @PageableDefault (size = 20)Pageable pageable,
                                         Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> yourIdInstansiList = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());
        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        model.addAttribute("listSaldoPendapatanTerkait", pengeluaranTerkaitDao.saldoPendapatanTerkait(periodeAnggaran1, yourIdInstansiList, pageable));
        model.addAttribute("reports", "active");
        model.addAttribute("report_saldo_pendapatan_terkait", "active");
        return "reports/pendapatan_terkait/list";
    }

}
