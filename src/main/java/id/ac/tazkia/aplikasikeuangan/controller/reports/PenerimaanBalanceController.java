package id.ac.tazkia.aplikasikeuangan.controller.reports;


import id.ac.tazkia.aplikasikeuangan.dao.setting.PenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PenerimaanRealDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PenerimaanBalanceController {

    @Autowired
    private PenerimaanDao penerimaanDao;

    @Autowired
    private PenerimaanRealDao penerimaanRealDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @GetMapping("/reports/revenues/balance")
    public String listPenerimaanBalanceMataAnggaran(Model model,
                                                    @RequestParam(required = false)PeriodeAnggaran periodeAnggaran){

        model.addAttribute("periodeAggaranSelected", periodeAnggaranDao.findById(periodeAnggaran.getId()).get());
        model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
        model.addAttribute("listPenerimaanBalance", penerimaanRealDao.balancePenerimaan(periodeAnggaran.getId()));

        return "reports/penerimaan/balance/list";

    }

    @GetMapping("/reports/revenues/balance/detail")
    public String listPenerimaanBalanceMataAnggaranDetail(Model model,
                                                          @RequestParam(required = false) MataAnggaran mataAnggaran,
                                                          @RequestParam(required = false) PeriodeAnggaran periodeAnggaran){

        model.addAttribute("mataAnggaran", mataAnggaran);
        model.addAttribute("periodeAggaranSelected", periodeAnggaranDao.findById(periodeAnggaran.getId()).get());
        model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
        model.addAttribute("listPenerimaanBalanceDetail", penerimaanRealDao.listPenerimaanBalanaceDetail(mataAnggaran.getId(), periodeAnggaran.getId()));

        return "reports/penerimaan/balance/detaillist";

    }

}
