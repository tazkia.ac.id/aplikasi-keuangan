package id.ac.tazkia.aplikasikeuangan.controller.reports;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.LaporanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.KaryawanJabatan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

@Controller
public class BudgetRealizedController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @GetMapping("/reports/budget/realized")
    public String listDepartemen(Model model,
                                    @RequestParam(required = false) String search,
                                    @PageableDefault(size = 20) Pageable page,
                                    @RequestParam(required = false)PeriodeAnggaran periodeAnggaran,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        System.out.println("role : " + user.getRole().getName());
        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        if(periodeAnggaran == null) {
            if (user.getRole().getDescription().equals("AUDITOR") || user.getRole().getDescription().equals("SUPERUSER") || user.getRole().getDescription().equals("KEUANGAN")) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDepartemen", karyawanJabatanDao.listJabatanDepartemenSearch1(search,periodeAnggaran1, page));
                } else {
                    model.addAttribute("listDepartemen", karyawanJabatanDao.listJabatanDepartemen1(periodeAnggaran1, page));
                }
            } else {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDepartemen", karyawanJabatanDao.listJabatanAktifSearch2(karyawan.getId(), search,periodeAnggaran1, page));
                } else {
                    model.addAttribute("listDepartemen", karyawanJabatanDao.listJabatanAktif2(karyawan.getId(), periodeAnggaran1, page));
                }
            }
            model.addAttribute("priodeAnggaranSelected", periodeAnggaranDao.findById(periodeAnggaran1).get());
        }else{

            if (user.getRole().getDescription().equals("AUDITOR") || user.getRole().getDescription().equals("SUPERUSER") || user.getRole().getDescription().equals("KEUANGAN")) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDepartemen", karyawanJabatanDao.listJabatanDepartemenSearch1(search, periodeAnggaran.getId(), page));
                } else {
                    model.addAttribute("listDepartemen", karyawanJabatanDao.listJabatanDepartemen1(periodeAnggaran.getId(), page));
                }
            } else {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listDepartemen", karyawanJabatanDao.listJabatanAktifSearch2(karyawan.getId(), search, periodeAnggaran.getId(), page));
                } else {
                    model.addAttribute("listDepartemen", karyawanJabatanDao.listJabatanAktif2(karyawan.getId(), periodeAnggaran.getId(), page));
                }
            }
            model.addAttribute("priodeAnggaranSelected", periodeAnggaranDao.findById(periodeAnggaran.getId()).get());
        }

        model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
        model.addAttribute("reports", "active");
        model.addAttribute("report_budget_realized", "active");
        return "reports/budget_realized/department_list";

    }

    @GetMapping("/reports/budget/realized/budget")
    public String listAnggaran(Model model,
                               @RequestParam(required = true) Departemen departemen,
                               @RequestParam(required = false) String search,
                               @RequestParam(required = false)PeriodeAnggaran periodeAnggaran,
                               @PageableDefault(size = 20) Pageable page,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        if (periodeAnggaran == null){
            model.addAttribute("priodeAnggaranSelected", periodeAnggaranDao.findById(periodeAnggaran1).get());
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
            model.addAttribute("listRealisasiBudget", anggaranDao.realisasiBudget(departemen.getId(),periodeAnggaran1));
            model.addAttribute("listRealisasiBudgerDetail", anggaranDao.realisasiAnggaranDetail(departemen.getId(), periodeAnggaran1));
            model.addAttribute("departemen", departemen.getId());
        }else{
            model.addAttribute("priodeAnggaranSelected", periodeAnggaran);
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
            model.addAttribute("listRealisasiBudget", anggaranDao.realisasiBudget(departemen.getId(),periodeAnggaran.getId()));
            model.addAttribute("listRealisasiBudgerDetail", anggaranDao.realisasiAnggaranDetail(departemen.getId(), periodeAnggaran.getId()));
            model.addAttribute("departemen", departemen.getId());
        }

        model.addAttribute("reports", "active");
        model.addAttribute("report_budget_realized", "active");
        return "reports/budget_realized/list";

    }

}
