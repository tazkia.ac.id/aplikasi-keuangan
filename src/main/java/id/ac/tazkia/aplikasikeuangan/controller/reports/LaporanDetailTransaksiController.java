package id.ac.tazkia.aplikasikeuangan.controller.reports;


import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
public class LaporanDetailTransaksiController {

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @GetMapping("/reports/detail/transaksi")
    public String listDetailTransaksi (Model model,
                                       @RequestParam(required = false)PeriodeAnggaran periodeAnggaran){

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        if (periodeAnggaran == null){

            model.addAttribute("priodeAnggaranSelected", periodeAnggaranDao.findById(periodeAnggaran1).get());
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
            model.addAttribute("listDetailTransaksi", pengajuanDanaDao.listDetailTransaksi(periodeAnggaran1));

        }else{

            model.addAttribute("priodeAnggaranSelected", periodeAnggaran);
            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
            model.addAttribute("listDetailTransaksi", pengajuanDanaDao.listDetailTransaksi(periodeAnggaran.getId()));

        }

        return "reports/detail_transaksi/list";

    }


}
