package id.ac.tazkia.aplikasikeuangan.controller.setting;


import id.ac.tazkia.aplikasikeuangan.dao.config.UserDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailPorsiDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.LaporanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.LaporanDanaPorsiDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaPorsiDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetailPorsi;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDanaPorsi;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDanaPorsi;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Controller
public class AnggaranDetailPorsiController {

    @Autowired
    private AnggaranDetailPorsiDao anggaranDetailPorsiDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private LaporanDanaDao laporanDanaDao;

    @Autowired
    private PengajuanDanaPorsiDao pengajuanDanaPorsiDao;

    @Autowired
    private LaporanDanaPorsiDao laporanDanaPorsiDao;

    @GetMapping("/setting/anggaran/porsi")
    public String settingPorsiAnggaran(Model model,
                                       @RequestParam(required = false)PeriodeAnggaran periodeAnggaran){

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now().plusDays(1));
        if (periodeAnggaran1 == null){
            periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now().plusDays(1));
        }
        if (periodeAnggaran != null){
            periodeAnggaran1 = periodeAnggaran.getId();
        }

        model.addAttribute("periodeAggaranSelected", periodeAnggaran);
        model.addAttribute("periodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
        model.addAttribute("listDetalAnggaranPorsi", anggaranDetailPorsiDao.listPorsiAnggaranDetail(periodeAnggaran1));

        return "setting/anggaran/porsi/list";

    }

    @GetMapping("/setting/anggaran/porsi/detail")
    public String addPorsiAnggaranDetail(Model model,
                                         @RequestParam(required = true) AnggaranDetail anggaranDetail){

        model.addAttribute("anggaranDetail", anggaranDetail);
        model.addAttribute("anggaranDetailPorsi", new AnggaranDetailPorsi());
        model.addAttribute("listDepartemen", departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));

        return "setting/anggaran/porsi/form";

    }

    @PostMapping("/setting/anggaran/porsi/save")
    public String savePorsiAnggaranDetail(@ModelAttribute @Valid AnggaranDetailPorsi anggaranDetailPorsi,
                                          Model model,
                                          RedirectAttributes attributes,
                                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);

        BigDecimal totalPorsiAnggaranDetail = anggaranDetailPorsiDao.totalPorsiAnggaranDetail(anggaranDetailPorsi.getAnggaranDetail().getId());
        if (totalPorsiAnggaranDetail != null){
            totalPorsiAnggaranDetail = totalPorsiAnggaranDetail.add(anggaranDetailPorsi.getPersentasePorsi());
            if (totalPorsiAnggaranDetail.compareTo(BigDecimal.valueOf(100)) >= 1){
                model.addAttribute("anggaranDetail", anggaranDetailPorsi.getAnggaranDetail());
                model.addAttribute("anggaranDetailPorsi", anggaranDetailPorsi);
                model.addAttribute("listDepartemen", departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));
                model.addAttribute("persentaseEror", "Save Data Eror, Persentese Total sudah melebihi 100%");
                return "setting/anggaran/porsi/form";
            }
        }else{
            if (anggaranDetailPorsi.getPersentasePorsi().compareTo(BigDecimal.valueOf(100)) >= 1){
                model.addAttribute("anggaranDetail", anggaranDetailPorsi.getAnggaranDetail());
                model.addAttribute("anggaranDetailPorsi", anggaranDetailPorsi);
                model.addAttribute("listDepartemen", departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));
                model.addAttribute("persentaseEror", "Save Data Eror, Persentese Total sudah melebihi 100%");
                return "setting/anggaran/porsi/form";
            }
        }

        String date = anggaranDetailPorsi.getTanggalBerlakuString();
        String tahun = date.substring(6,10);
        String bulan = date.substring(0,2);
        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        anggaranDetailPorsi.setTanggalBerlaku(localDate);
        anggaranDetailPorsi.setUserInsert(user.getUsername());
        anggaranDetailPorsi.setTanggalInsert(LocalDateTime.now());
        if(anggaranDetailPorsi.getPersentasePorsi() == null){
            anggaranDetailPorsi.setPersentasePorsi(BigDecimal.ZERO);
        }

        anggaranDetailPorsiDao.save(anggaranDetailPorsi);

        //Cari transaksi porsi yang sudah ada
        List<String> idPengajuanDana = pengajuanDanaDao.cariPengajuanDanaLalu(anggaranDetailPorsi.getAnggaranDetail().getId(), localDate);
        List<AnggaranDetailPorsi> anggaranDetailPorsis = anggaranDetailPorsiDao.findByStatusAndAnggaranDetailOrderByDepartemen( StatusRecord.AKTIF, anggaranDetailPorsi.getAnggaranDetail());
        List<String> idLaporanDana = laporanDanaDao.cariLaporanDana(anggaranDetailPorsi.getAnggaranDetail().getId(), localDate);
        if (idPengajuanDana != null){
            for (String ipd : idPengajuanDana){
                //hapus data pengajuan dana porsi yang sudah ada
                pengajuanDanaPorsiDao.hapusDataPengajuanDanaPorsi(ipd);
                PengajuanDana pengajuanDana = pengajuanDanaDao.findByStatusAndId(StatusRecord.AKTIF, ipd);
                for(AnggaranDetailPorsi anggaranDetailPorsi1:anggaranDetailPorsis){
                    PengajuanDanaPorsi pengajuanDanaPorsi = new PengajuanDanaPorsi();
                    pengajuanDanaPorsi.setPengajuanDana(pengajuanDana);
                    pengajuanDanaPorsi.setDepartemen(anggaranDetailPorsi.getDepartemen());
                    pengajuanDanaPorsi.setStatus(StatusRecord.AKTIF);
                    pengajuanDanaPorsi.setJumlah((pengajuanDana.getJumlah().multiply(anggaranDetailPorsi1.getPersentasePorsi())).divide(BigDecimal.valueOf(100)));
                    pengajuanDanaPorsiDao.save(pengajuanDanaPorsi);
                }

            }
        }
        if (idLaporanDana != null){
            for (String ild : idLaporanDana){
                //hapus data pengajuan dana porsi yang sudah ada
                laporanDanaPorsiDao.hapusLaporanDanaPorsi(ild);
                LaporanDana laporanDana = laporanDanaDao.findByStatusAndId(StatusRecord.AKTIF, ild);
                for(AnggaranDetailPorsi anggaranDetailPorsi1:anggaranDetailPorsis){
                    LaporanDanaPorsi laporanDanaPorsi = new LaporanDanaPorsi();
                    laporanDanaPorsi.setLaporanDana(laporanDana);
                    laporanDanaPorsi.setDepartemen(anggaranDetailPorsi.getDepartemen());
                    laporanDanaPorsi.setStatus(StatusRecord.AKTIF);
                    laporanDanaPorsi.setJumlah((laporanDana.getJumlah().multiply(anggaranDetailPorsi1.getPersentasePorsi())).divide(BigDecimal.valueOf(100)));
                    laporanDanaPorsiDao.save(laporanDanaPorsi);
                }

            }
        }

        return "redirect:../porsi?periodeAnggaran="+ anggaranDetailPorsi.getAnggaranDetail().getAnggaran().getPeriodeAnggaran().getId() ;

    }

}
