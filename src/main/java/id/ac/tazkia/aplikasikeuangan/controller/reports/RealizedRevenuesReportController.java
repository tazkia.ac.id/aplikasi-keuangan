package id.ac.tazkia.aplikasikeuangan.controller.reports;


import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PenerimaanRealDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

@Controller
public class RealizedRevenuesReportController {

    @Autowired
    private PenerimaanRealDao penerimaanRealDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;



    @GetMapping("reports/realizedrevenues")
    public String listRealizedRevenues(Model model,
                                       @PageableDefault(size = 10)Pageable page,
                                       @RequestParam(required = false) String range,
                                       @RequestParam(required = false) String search){

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        String nows = sdf.format(now.getTime()).toString();

        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());


        if (StringUtils.hasText(range)) {
            String datea = range;
            String tahuna = datea.substring(6, 10);
            String bulana = datea.substring(0, 2);
            String tanggala = datea.substring(3, 5);
            String tanggalana = tahuna + '-' + bulana + '-' + tanggala;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDatea = LocalDate.parse(tanggalana, formatter);

            String dateb = range;
            String tahunb = dateb.substring(19, 23);
            String bulanb = dateb.substring(13, 15);
            String tanggalb = dateb.substring(16, 18);
            String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb;
            DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDateb = LocalDate.parse(tanggalanb, formatterb);
            System.out.println("tanggall : "+ localDatea + "-" + localDateb);
            model.addAttribute("range", range);
            model.addAttribute("listPenerimaanReal", penerimaanRealDao.findByStatusAndTanggalBetweenOrderByTanggal(StatusRecord.AKTIF, localDatea, localDateb));
            model.addAttribute("totalRealisasiPenerimaan", penerimaanRealDao.getTotalRealisasiPenerimaantgl2(localDatea, localDateb));

        }


        return "reports/realizedrevenues/list";
    }

}
