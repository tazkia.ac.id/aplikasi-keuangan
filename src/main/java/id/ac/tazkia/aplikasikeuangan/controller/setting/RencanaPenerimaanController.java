package id.ac.tazkia.aplikasikeuangan.controller.setting;


import id.ac.tazkia.aplikasikeuangan.dao.masterdata.MataAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.RencanaPenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.aspectj.apache.bcel.classfile.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RencanaPenerimaanController {

    @Autowired
    private RencanaPenerimaanDao rencanaPenerimaanDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private MataAnggaranDao mataAnggaranDao;

    @GetMapping("/setting/rencanapenerimaan")
    public String daftarDepartemen(Model model, @PageableDefault(size = 10) Pageable page, String search,
                                   @RequestParam(required = false) PeriodeAnggaran periodeAnggaran) {

        model.addAttribute("selectedPeriode", periodeAnggaran);
        if (periodeAnggaran != null) {
            model.addAttribute("periode", periodeAnggaranDao.findAll());

            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listRencanaPenerimaan", rencanaPenerimaanDao.findByStatusAndPeriodeAnggaranAndMataAnggaranNamaMataAnggaranOrderByMataAnggaran(StatusRecord.AKTIF, periodeAnggaran, search, page));
            }else{
                model.addAttribute("listRencanaPenerimaan", rencanaPenerimaanDao.findByStatusAndPeriodeAnggaranOrderByMataAnggaran(StatusRecord.AKTIF, periodeAnggaran, page));
            }

        } else {
            model.addAttribute("periode", periodeAnggaranDao.findAll());

            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listRencanaPenerimaan", rencanaPenerimaanDao.findByStatusAndPeriodeAnggaranStatusAndMataAnggaranNamaMataAnggaranOrderByMataAnggaran(StatusRecord.AKTIF, StatusRecord.AKTIF, search, page));
            }else{
                model.addAttribute("listRencanaPenerimaan", rencanaPenerimaanDao.findByStatusAndPeriodeAnggaranStatusOrderByMataAnggaran(StatusRecord.AKTIF, StatusRecord.AKTIF, page));
            }

        }

        return "setting/rencanapenerimaan/list";

    }

}
