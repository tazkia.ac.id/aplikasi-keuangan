package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import id.ac.tazkia.aplikasikeuangan.dao.BulanDao;
import id.ac.tazkia.aplikasikeuangan.dao.TahunDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.InstansiDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.SaldoBankDao;
import id.ac.tazkia.aplikasikeuangan.entity.Bulan;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.Tahun;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.SaldoBank;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Controller
public class SaldoBankController {

    @Autowired
    private SaldoBankDao saldoBankDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private KaryawanDao karyawanDao;
    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;


    @GetMapping("/transaksi/saldo_bank")
    private String listSaldoBank(Model model,
                                 @PageableDefault(size = 10) Pageable pageable,
                                 @RequestParam(required = false) Bulan bulan,
                                 @RequestParam(required = false) Tahun tahun,
                                 @RequestParam(required = false) Instansi instansi){


        model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
        model.addAttribute("listBulan", bulanDao.findByStatusOrderByBulan(StatusRecord.AKTIF));
        if (bulan != null && tahun != null) {
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, tahun.getTahun()));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndBulan(StatusRecord.AKTIF, bulan.getBulan()));

            if(instansi == null){
                model.addAttribute("listSaldoBank", saldoBankDao.listSaldoBank(bulan.getBulan(), tahun.getTahun(), pageable));
            }else{
                model.addAttribute("listSaldoBank", saldoBankDao.listSaldoBankInstansi(bulan.getBulan(), tahun.getTahun(), instansi.getId(), pageable));
                model.addAttribute("instansiSelected", instansi);
            }
        }else{
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, LocalDate.now().getYear()+""));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndBulan(StatusRecord.AKTIF, LocalDate.now().getMonthValue()+""));
            if(instansi == null){
                model.addAttribute("listSaldoBank", saldoBankDao.listSaldoBank(LocalDate.now().getMonthValue()+"", LocalDate.now().getYear()+"", pageable));
            }else{
                model.addAttribute("listSaldoBank", saldoBankDao.listSaldoBankInstansi(LocalDate.now().getMonthValue()+"", LocalDate.now().getYear()+"", instansi.getId(), pageable));
                model.addAttribute("instansiSelected", instansi);
            }
        }
        model.addAttribute("finance", "active");
        model.addAttribute("saldo_bank", "active");
        return "transaksi/saldo_bank/list";

    }

    @GetMapping("/transaksi/saldo_bank/baru")
    public String saldoBankBaru(Model model){

        model.addAttribute("saldoBank", new SaldoBank());
        model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
        model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));

        model.addAttribute("finance", "active");
        model.addAttribute("saldo_bank", "active");

        return "transaksi/saldo_bank/form";
    }

    @GetMapping("/transaksi/saldo_bank/edit")
    public String saldoBankBaru(Model model, @RequestParam(required = true) SaldoBank saldoBank){

        model.addAttribute("saldoBank", saldoBank);
        model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
        model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));

        model.addAttribute("finance", "active");
        model.addAttribute("saldo_bank", "active");

        return "transaksi/saldo_bank/form";
    }

    @PostMapping("/transaksi/saldo_bank/save")
    public String saldoBankSave(@ModelAttribute @Valid SaldoBank saldoBank,
                                Authentication authentication,
                                RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        saldoBank.setUser(user);
        saldoBank.setStatus(StatusRecord.AKTIF);
        saldoBank.setTanggalInsert(LocalDateTime.now());
        saldoBank.setUserInsert(karyawan.getNamaKaryawan());
        saldoBankDao.save(saldoBank);

        redirectAttributes.addFlashAttribute("success", "Input data sukses");

        return "redirect:../saldo_bank";
    }

    @PostMapping("/transaksi/saldo_bank/delete")
    public String saldoBankDelete(@RequestParam(required = true) SaldoBank saldoBank,
                                Authentication authentication,
                                RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        saldoBank.setUser(user);
        saldoBank.setStatus(StatusRecord.HAPUS);
        saldoBank.setTanggalInsert(LocalDateTime.now());
        saldoBank.setUserInsert(karyawan.getNamaKaryawan());
        saldoBankDao.save(saldoBank);

        redirectAttributes.addFlashAttribute("success", "Input data sukses");

        return "redirect:../saldo_bank";
    }

    @GetMapping("/transaksi/saldo_bank_history")
    public String saldoBankHistory(Model model){

        model.addAttribute("finance", "active");
        model.addAttribute("history_saldo_bank", "active");

        return "keuangan/saldo_bank/history";
    }
}
