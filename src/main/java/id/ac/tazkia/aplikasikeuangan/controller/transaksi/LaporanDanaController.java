package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.*;
import id.ac.tazkia.aplikasikeuangan.dto.PrintLaporanDanaDto;
import id.ac.tazkia.aplikasikeuangan.dto.export.LaporanDanaDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.*;
import id.ac.tazkia.aplikasikeuangan.export.ExportBuktiLaporanDana;
import id.ac.tazkia.aplikasikeuangan.export.ExportBuktiPengajuanDana;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
// import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lowagie.text.DocumentException;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class LaporanDanaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PengajuanDanaController.class);


    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PencairanDanaDao pencairanDanaDao;

    @Autowired
    private LaporanDanaDao laporanDanaDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PencairanDanaDetailDao pencairanDanaDetailDao;

    @Autowired
    private LaporanDanaFileDao laporanDanaFileDao;

    @Autowired
    @Value("${upload.laporandana}")
    private String uploadFolder;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    @Value("${lokasi.logo}")
    private String lokasiLogo;

    @GetMapping("/transaksi/laporandana/submit")
    private String formLaporanDana(Model model,
                                   @RequestParam PencairanDana pencairanDana){

        model.addAttribute("listPencairanDanaDetail", pencairanDanaDetailDao.findByPencairanDanaOrderByPencairanDanaTanggalPencairan(pencairanDana));
        model.addAttribute("pencairanDana", pencairanDana);
        model.addAttribute("laporanDana", new LaporanDana());

        model.addAttribute("transactions", "active");
        return "transaksi/laporandana/form";
    }

    @GetMapping("/transaksi/laporandana/edit")
    private String editLaporanDana(Model model,
                                   @RequestParam(required = false)LaporanDana id){

        LaporanDana laporanDana = id;
        model.addAttribute("pencairanDana", pencairanDanaDao.findById(laporanDana.getPencairanDanaDetail().getPencairanDana().getId()).get());
        model.addAttribute("pencairanDanaDetail", pencairanDanaDetailDao.findById(laporanDana.getPencairanDanaDetail().getId()).get());
        model.addAttribute("laporanDana", laporanDana);

        model.addAttribute("transactions", "active");
        return "transaksi/laporandana/edit";
    }

    @GetMapping("/transaksi/laporandana/revisi")
    private String revisiLaporanDana(Model model,
                                   @RequestParam(required = false)LaporanDana id){

        LaporanDana laporanDana = id;
        model.addAttribute("pencairanDana", pencairanDanaDao.findById(laporanDana.getPencairanDanaDetail().getPencairanDana().getId()).get());
        model.addAttribute("pencairanDanaDetail", pencairanDanaDetailDao.findById(laporanDana.getPencairanDanaDetail().getId()).get());
        model.addAttribute("laporanDana", laporanDana);

        model.addAttribute("transactions", "active");
        return "transaksi/laporandana/revisi";
    }



    @PostMapping("/transaksi/laporandana/save")
    private String saveLaporanDana(Model model,
                                   @ModelAttribute @Valid LaporanDana laporanDana,
                                   BindingResult errors,
                                   @RequestParam MultipartFile[] files,
                                   @RequestParam BigDecimal[] jumlah,
                                   @RequestParam String[] deskripsi,
                                   @RequestParam List<PencairanDanaDetail> pencairanDanaDetail,
                                   RedirectAttributes attributes,
                                   Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        System.out.println("Test File : " + files);

        //int lampir = 0;
        //int datas = 0;

//        if(files.length != jumlah.length){
//            attributes.addFlashAttribute("gagal", "Save Data Gagal");
//            return "redirect:../pengajuandana/laporan";
//        }



        if(deskripsi.length < pencairanDanaDetail.size() || jumlah.length < pencairanDanaDetail.size() || files.length < pencairanDanaDetail.size()){
            for (PencairanDanaDetail as : pencairanDanaDetail) {
                PencairanDana pencairanDana = pencairanDanaDao.findById(as.getPencairanDana().getId()).get();
                String idPencaanDana = pencairanDana.getId();

                attributes.addFlashAttribute("gagalket", "Save Data Gagal");
                return "redirect:../laporandana/submit?pencairanDana=" + idPencaanDana;
            }
        }

        int ikfile = 0;
        for (PencairanDanaDetail as : pencairanDanaDetail) {
            //int ukfile = deskripsi[ikfile].length();
            if (deskripsi[ikfile].isEmpty()){

                PencairanDana pencairanDana = pencairanDanaDao.findById(as.getPencairanDana().getId()).get();
                String idPencaanDana= pencairanDana.getId();

                attributes.addFlashAttribute("gagalket", "Save Data Gagal");
                return "redirect:../laporandana/submit?pencairanDana="+ idPencaanDana;

            }

            ikfile++;

        }


        int countFile = 0;
        for (PencairanDanaDetail as : pencairanDanaDetail){
            Long ukuran = files[countFile].getSize();
            if (ukuran == 0) {

                PencairanDana pencairanDana = pencairanDanaDao.findById(as.getPencairanDana().getId()).get();
                String idPencaanDana= pencairanDana.getId();

                model.addAttribute("laporanDana", new LaporanDana());
                attributes.addFlashAttribute("gagal", "Save Data Gagal");
                return "redirect:../laporandana/submit?pencairanDana="+ pencairanDana;

            }
            countFile++;
        }


        int ixFile = 0;
        for (PencairanDanaDetail s : pencairanDanaDetail){
            LaporanDana laporanDana1 = new LaporanDana();

            laporanDana1.setTanggalLaporan(LocalDateTime.now().plusHours(7));
            laporanDana1.setPencairanDanaDetail(s);
            laporanDana1.setAmount(jumlah[ixFile].divide(s.getPengajuanDana().getKuantitas(), 2, RoundingMode.HALF_EVEN));
            laporanDana1.setJumlah(jumlah[ixFile]);
            laporanDana1.setDeskripsi(deskripsi[ixFile]);
            laporanDana1.setUserPelapor(karyawan);
            laporanDana1.setPeriodeAnggaran(laporanDana1.getPencairanDanaDetail().getPengajuanDana().getAnggaranDetail().getAnggaran().getPeriodeAnggaran());
//            laporanDana1.setAmount(laporanDana.getJumlah().divide(s.getPengajuanDana().getKuantitas()));
            laporanDanaDao.save(laporanDana1);

            PencairanDana pencairanDana = pencairanDanaDao.findById(s.getPencairanDana().getId()).get();
            pencairanDana.setStatusCair(StatusRecord.LAPOR);
            pencairanDanaDao.save(pencairanDana);

            for (int j = 0; j < files.length; j++) {
                String namaFile =  files[j].getName();
                String jenisFile = files[j].getContentType();
                String namaAsli = files[j].getOriginalFilename();
                Long ukuran = files[j].getSize();

                LaporanDanaFile laporanDanaFile = new LaporanDanaFile();
                String extension = "";
                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }
                String idFile = UUID.randomUUID().toString();
                if (ukuran == 0){
                    laporanDanaFile.setFile("default.jpg");
                }else{
                    laporanDanaFile.setFile(idFile + "." + extension);
                    new File(uploadFolder).mkdirs();
                    File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
                    files[j].transferTo(tujuan);
                }
                laporanDanaFile.setLaporanDana(laporanDana1);
                laporanDanaFileDao.save(laporanDanaFile);
                System.out.println("Test : " + laporanDanaFile);
            }

            ixFile++;
        }

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../pengajuandana/laporan";

    }

    @PostMapping("/transaksi/laporandana/update")
    private String updateLaporanDana(Model model,
                                   @ModelAttribute @Valid LaporanDana laporanDana,
                                   BindingResult errors,
                                   @RequestParam MultipartFile files,
                                   RedirectAttributes attributes,
                                   Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        LaporanDana laporanDana1 = laporanDanaDao.findById(laporanDana.getId()).get();

            String namaFile =  files.getName();
            String jenisFile = files.getContentType();
            String namaAsli = files.getOriginalFilename();
            Long ukuran = files.getSize();

            String extension = "";
            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            if (ukuran == 0){
                laporanDana1.setFile("default.jpg");
            }else{
                laporanDana1.setFile(idFile + "." + extension);
                new File(uploadFolder).mkdirs();
                File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
                files.transferTo(tujuan);
            }

        laporanDana1.setTanggalLaporan(LocalDateTime.now().plusHours(7));
        laporanDana1.setAmount(laporanDana.getJumlah().divide(laporanDana.getPencairanDanaDetail().getPengajuanDana().getKuantitas()));
        laporanDana1.setJumlah(laporanDana.getJumlah());
        laporanDana1.setDeskripsi(laporanDana.getDeskripsi());
        laporanDana1.setUserPelapor(karyawan);
        laporanDana1.setStatusLapor(StatusRecord.WAITING);
        laporanDana1.setPeriodeAnggaran(laporanDana1.getPencairanDanaDetail().getPengajuanDana().getAnggaranDetail().getAnggaran().getPeriodeAnggaran());
        laporanDanaDao.save(laporanDana1);

        return "redirect:../pengajuandana/laporanwaiting";

    }

    @PostMapping("/transaksi/laporandana/rev")
    private String revisiLaporanDana(Model model,
                                     @ModelAttribute @Valid LaporanDana laporanDana,
                                     BindingResult errors,
                                     @RequestParam MultipartFile files,
                                     RedirectAttributes attributes,
                                     Authentication authentication) throws IOException {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        LaporanDana laporanDana1 = laporanDanaDao.findById(laporanDana.getId()).get();

        String namaFile =  files.getName();
        String jenisFile = files.getContentType();
        String namaAsli = files.getOriginalFilename();
        Long ukuran = files.getSize();

        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran == 0){
            laporanDana1.setFile("default.jpg");
        }else{
            laporanDana1.setFile(idFile + "." + extension);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            files.transferTo(tujuan);
        }

        laporanDana1.setTanggalLaporan(LocalDateTime.now().plusHours(7));
        laporanDana1.setAmount(laporanDana.getJumlah().divide(laporanDana.getPencairanDanaDetail().getPengajuanDana().getKuantitas()));
        laporanDana1.setJumlah(laporanDana.getJumlah());
        laporanDana1.setDeskripsi(laporanDana.getDeskripsi());
        laporanDana1.setUserPelapor(karyawan);
        laporanDana1.setStatusLapor(StatusRecord.WAITING);
        laporanDana1.setPeriodeAnggaran(laporanDana1.getPencairanDanaDetail().getPengajuanDana().getAnggaranDetail().getAnggaran().getPeriodeAnggaran());
        laporanDanaDao.save(laporanDana1);

        return "redirect:../pengajuandana/laporanrejected";

    }

    @GetMapping("/transaksi/laporandana/kembalian")
    public String inputPengembalianDana(Model model,
                                        @RequestParam LaporanDana laporanDana){

        model.addAttribute("laporanDana", laporanDana);

        return "";
    }


    @GetMapping("/laporandana/{laporanDana}/bukti/")
    public ResponseEntity<byte[]> tampilkanBuktiLaporan(@PathVariable LaporanDana laporanDana) throws Exception {
        String lokasiFile = uploadFolder + File.separator + laporanDana.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (laporanDana.getFile().toLowerCase().endsWith("jpeg") || laporanDana.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (laporanDana.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (laporanDana.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }



    @GetMapping("/laporan_dana/bukti")
    public void generatePdfFile(HttpServletResponse response,
                                @RequestParam(required = true) LaporanDana laporanDana) throws DocumentException, IOException {

        List<LaporanDana> laporanDanaList = laporanDanaDao.findByStatusAndPencairanDanaDetailPencairanDana(StatusRecord.AKTIF, laporanDana.getPencairanDanaDetail().getPencairanDana());


        List<LaporanDanaFile> laporanDanaFiles = laporanDanaFileDao.findByLaporanDanaAndStatus(laporanDana, StatusRecord.AKTIF);
        PrintLaporanDanaDto printLaporanDanaDto = laporanDanaDao.printLaporanDana(laporanDana.getPencairanDanaDetail().getPencairanDana().getId());

        response.setContentType("application/pdf");

        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD:HH:MM:SS");

        String headerkey = "Content-Disposition";

        String str = laporanDana.getUserPelapor().getNamaKaryawan().replaceAll("[^a-zA-Z0-9]", "-");
        System.out.println("nama sebelum : "+ laporanDana.getUserPelapor().getNamaKaryawan());
        System.out.println("nama sesuadah : "+ str);
        String headervalue = "attachment; filename=Lampiran_laporan_dana_" + str +"_" + laporanDana.getTanggalLaporan() + ".pdf";

        response.setHeader(headerkey, headervalue);

        ExportBuktiLaporanDana generator = new ExportBuktiLaporanDana();

//        generator.generateLaporanDana(printLaporanDanaDto,laporanDanaList,  uploadFolder, lokasiLogo , response);
        generator.generateLaporanDanaFile(printLaporanDanaDto,laporanDanaFiles,  uploadFolder, lokasiLogo , response);

    }

}
