package id.ac.tazkia.aplikasikeuangan.controller.masterdata;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDetailDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Standard;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.StandardDetail;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

@Controller
public class StandardController {

    @Autowired
    private StandardDao standardDao;

    @Autowired
    private StandardDetailDao standardDetailDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @GetMapping("/masterdata/standard")
    public String listStandard(Model model,
                               @PageableDefault(size = 10) Pageable page,
                               @RequestParam(required = false) String search,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listStandard", standardDao.findByStatusNotLikeAndKodeStandardOrStatusNotLikeAndNamaStandardOrderByKodeStandard(StatusRecord.HAPUS, search, StatusRecord.HAPUS, search, page));
            model.addAttribute("listStandardDetail", standardDetailDao.findByStatusOrderByDeskripsi(StatusRecord.AKTIF));
            model.addAttribute("standardDetail", new StandardDetail());
        }else{
            model.addAttribute("listStandard" , standardDao.findByStatusNotLikeOrderByKodeStandard(StatusRecord.HAPUS, page));
            model.addAttribute("listStandardDetail", standardDetailDao.findByStatusOrderByDeskripsi(StatusRecord.AKTIF));
            model.addAttribute("standardDetail", new StandardDetail());
        }


        model.addAttribute("setting", "active");
        model.addAttribute("standardss", "active");
        return "masterdata/standard/list";

    }

    @GetMapping("/masterdata/standard/baru")
    public String baruStandard(Model model,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        model.addAttribute("standard", new Standard());

        model.addAttribute("setting", "active");
        model.addAttribute("standardss", "active");
        return "masterdata/standard/form";

    }

    @GetMapping("/masterdata/standard/edit")
    public String editStandard(Model model,
                               @RequestParam(required = false) Standard standard,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        model.addAttribute("standard", standard);

        model.addAttribute("setting", "active");
        model.addAttribute("standardss", "active");
        return "masterdata/standard/form";

    }

    @PostMapping("/masterdata/standard/save")
    public String saveStandard(@ModelAttribute @Valid Standard standard,
                               Authentication authentication,
                               BindingResult errors,
                               RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        standard.setUserUpdate(karyawan.getNamaKaryawan());

        standardDao.save(standard);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../standard";

    }

}
