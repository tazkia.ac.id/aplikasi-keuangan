package id.ac.tazkia.aplikasikeuangan.controller.reports;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import id.ac.tazkia.aplikasikeuangan.controller.transaksi.PengajuanDanaController;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.*;
import id.ac.tazkia.aplikasikeuangan.services.JournalTemplateService;
import jakarta.servlet.http.HttpServletResponse;

// import org.apache.tomcat.jni.Local;
import id.ac.tazkia.aplikasikeuangan.dto.report.KasHarianDto;
import id.ac.tazkia.aplikasikeuangan.export.ExportDailyCash;
import lombok.extern.flogger.Flogger;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import id.ac.tazkia.aplikasikeuangan.dao.BulanDao;
import id.ac.tazkia.aplikasikeuangan.dao.HistoryDetailTransaksiDao;
import id.ac.tazkia.aplikasikeuangan.dao.HistorySaldoHarianDao;
import id.ac.tazkia.aplikasikeuangan.dao.TahunDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.LaporanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dto.report.DetailTransaksiDepartemenDto;
import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardKodeDto;
import id.ac.tazkia.aplikasikeuangan.entity.Bulan;
import id.ac.tazkia.aplikasikeuangan.entity.HistorySaldoHarian;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.Tahun;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.export.ExportSaldoBudgetHarian;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.util.logging.Logger;


@Controller
public class KasHarianReportController {
//    private static final Logger LOGGER = LoggerFactory.getLogger(KasHarianReportController.class);
    private static final Logger logger = Logger.getLogger(KasHarianReportController.class.getName());

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PencairanDanaDetailDao pencairanDanaDetailDao;

    @Autowired
    private LaporanDanaDao laporanDanaDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private TahunDao tahunDao;

    @Autowired
    private BulanDao bulanDao;

    @Autowired
    private HistorySaldoHarianDao historySaldoHarianDao;

    @Autowired
    private HistoryDetailTransaksiDao historyDetailTransaksiDao;


    @Autowired
    @Value("${api.akunting}")
    private String lokasiAkunting;
    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("/reports/dailycash")
    public String kasHarian(Model model,
                            @RequestParam(required = false) Bulan bulan,
                            @RequestParam(required = false) Tahun tahun,
                            @RequestParam(required = false) String journalTemplate){

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        if (bulan == null && tahun == null) {
            int year = LocalDate.now().getYear();
            String yearInString = String.valueOf(year);

            int month = LocalDate.now().getMonthValue();
            String monthString = String.valueOf(month);

            System.out.println("Masuk null");
            model.addAttribute("tahunSelected", tahunDao.findByStatusAndTahun(StatusRecord.AKTIF, yearInString));
            model.addAttribute("bulanSelected", bulanDao.findByStatusAndBulan(StatusRecord.AKTIF, monthString));
            model.addAttribute("listPengajuanDanaDetail", pencairanDanaDetailDao.cariPencairanDanaDetail(monthString, yearInString));
            model.addAttribute("listPengajuanDana", pengajuanDanaDao.dapatPengajuanDana(monthString, yearInString));
            model.addAttribute("listKasHarian", pengajuanDanaDao.pageKasHarian());
            model.addAttribute("listLaporanDana", laporanDanaDao.attachmentLaporanAll3());
            model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
            model.addAttribute("listBulan", bulanDao.findByStatusOrderByBulan(StatusRecord.AKTIF));

        }else if (bulan != null && tahun != null){
            System.out.println("Masuk not null");
            model.addAttribute("tahunSelected", tahun);
            model.addAttribute("bulanSelected", bulan);
            model.addAttribute("listKasHarian", pengajuanDanaDao.pageKasHarianDef(bulan.getBulan(), tahun.getTahun()));
//
            model.addAttribute("listPengajuanDanaDetail", pencairanDanaDetailDao.cariPencairanDanaDetail(bulan.getBulan(), tahun.getTahun()));
            model.addAttribute("listPengajuanDana", pengajuanDanaDao.dapatPengajuanDana(bulan.getBulan(), tahun.getTahun()));
            model.addAttribute("listLaporanDana", laporanDanaDao.attachmentLaporanAll2(bulan.getBulan(), tahun.getTahun()));
            model.addAttribute("listTahun", tahunDao.findByStatusOrderByTahunDesc(StatusRecord.AKTIF));
            model.addAttribute("listBulan", bulanDao.findByStatusOrderByBulan(StatusRecord.AKTIF));

        }

        model.addAttribute("reports", "active");
        model.addAttribute("report_daily_cash", "active");

        String apiUrl = lokasiAkunting + "/public/api/journal-template?type=FINANCE";

        try {
            // Panggil API untuk mendapatkan semua journal templates
            JournalTemplateResponsDto response = restTemplate.getForObject(apiUrl, JournalTemplateResponsDto.class);
            List<JournalTemplateDto> journalTemplatesDto = response != null ? response.getData() : Collections.emptyList();
            model.addAttribute("journalTemplates", journalTemplatesDto);
        } catch (RestClientException e) {
            // Menangani kesalahan ketika API tidak tersedia atau tidak dapat dijangkau
            logger.severe("Error while connecting to API for journal templates: " + e.getMessage());
            model.addAttribute("journalTemplates", Collections.emptyList());
            model.addAttribute("errorMessage", "Tidak dapat terhubung ke layanan journal templates. Silakan coba lagi nanti.");
        }

        if (StringUtils.hasText(journalTemplate)) {
            try {
                // Panggil API untuk mendapatkan satu journal template berdasarkan ID
                String apiUrlSatu = lokasiAkunting + "/public/api/journal-template?id=" + journalTemplate;
                JournalTemplateResponSatuDto responseSatu = restTemplate.getForObject(apiUrlSatu, JournalTemplateResponSatuDto.class);
                JournalTemplateDto journalTemplateDto = responseSatu != null ? responseSatu.getData() : null;
                model.addAttribute("journalTemplateSelected", journalTemplateDto);
            } catch (RestClientException e) {
                // Menangani kesalahan ketika API tidak tersedia atau tidak dapat dijangkau
                logger.severe("Error while connecting to API for a single journal template: " + e.getMessage());
                model.addAttribute("journalTemplateSelected", null);
                model.addAttribute("errorMessageSatu", "Tidak dapat terhubung ke layanan untuk journal template terpilih. Silakan coba lagi nanti.");
            }

            try {
                // Panggil API untuk mendapatkan detail journal template berdasarkan ID
                String apiDetailUrl = lokasiAkunting + "/public/api/journal-template?detail=" + journalTemplate;
                JournalTemplateDetailResponsDto responseDetail = restTemplate.getForObject(apiDetailUrl, JournalTemplateDetailResponsDto.class);
                List<JournalTemplateDetailDto> journalTemplateDetailDtos = responseDetail != null ? responseDetail.getData() : Collections.emptyList();
                model.addAttribute("journalTemplateDetailList", journalTemplateDetailDtos);
            } catch (RestClientException e) {
                // Menangani kesalahan ketika API tidak tersedia atau tidak dapat dijangkau
                logger.severe("Error while connecting to API for journal template details: " + e.getMessage());
                model.addAttribute("journalTemplateDetailList", Collections.emptyList());
                model.addAttribute("errorMessageDetail", "Tidak dapat terhubung ke layanan detail journal template. Silakan coba lagi nanti.");
            }
        }


        return "reports/dailycash/list";

    }

//    @GetMapping("/public/api/journal-template/detail/{id}")
//    public ResponseEntity<List<JournalTemplateDetailDto>> getJournalTemplateDetails(@PathVariable("id") String templateId) {
//        try {
//            List<JournalTemplateDetailDto> journalTemplateDetails = journalTemplateService.getDetailsByTemplateId(templateId);
//            return ResponseEntity.ok(journalTemplateDetails);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Collections.emptyList());
//        }
//    }


    @GetMapping("/reports/dailycash/toexcel")
    public void exportToExcel2(HttpServletResponse response,
                               @RequestParam(required = false) Bulan bulan,
                               @RequestParam(required = false) Tahun tahun) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Daily_cash-bulan-" + bulan.getNama() + "-tahun-"+ tahun.getTahun() +".xlsx";
        response.setHeader(headerKey, headerValue);


        List<KasHarianDto> kasHarianDtos = pengajuanDanaDao.pageKasHarianDef(bulan.getBulan(), tahun.getTahun());
        ExportDailyCash excelExporter = new ExportDailyCash(kasHarianDtos);
        excelExporter.export(response);
    }

    @GetMapping("/reports/dailycash/home")
    public String dailyCashHome(Model model, @RequestParam(required = false) String search,
                               @RequestParam(required = false)PeriodeAnggaran periodeAnggaran,
                                Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        if(periodeAnggaran != null){
            periodeAnggaran1 = periodeAnggaran.getId();
            // model.addAttribute("periodeAnggaranSelected", periodeAnggaran);
        }
        
        LocalDateTime localDateTime = historySaldoHarianDao.tanggalUpdateTerakhir();

        model.addAttribute("priodeAnggaranSelected", periodeAnggaranDao.findById(periodeAnggaran1).get());
        model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
        
        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listSisaAnggaran", historySaldoHarianDao.listSaldoHarianSearch(localDateTime, search));
            model.addAttribute("listSisaAnggaranTotal", historySaldoHarianDao.listSaldoHarianTotalSearch(localDateTime, search));
        }else{
            model.addAttribute("listSisaAnggaran", historySaldoHarianDao.listSaldoHarian(localDateTime));
            model.addAttribute("listSisaAnggaranTotal", historySaldoHarianDao.listSaldoHarianTotal(localDateTime));
        }


        model.addAttribute("tanggalUpdate", localDateTime);
        model.addAttribute("reports", "active");
        model.addAttribute("report_departement_transaction", "active");
        return "reports/dailycashdepartemen/dashboard";

    }

    @GetMapping("/reports/dailycash/departemen")
    public String kasHarianDepartemen(Model model,
                                      @RequestParam(required = false) String id){

        HistorySaldoHarian historySaldoHarian = historySaldoHarianDao.findByStatusAndId(StatusRecord.AKTIF, id);
        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        model.addAttribute("periodeAnggaran", periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran));
        model.addAttribute("departemen", departemenDao.findByStatusAndId(StatusRecord.AKTIF, historySaldoHarian.getDepartemen().getId()));
//        model.addAttribute("listKasHarian",historyDetailTransaksiDao.findByStatusAndDepartemenAndTanggalUpdateOrderByTanggalPengajuan(StatusRecord.AKTIF,historySaldoHarian.getDepartemen(),historySaldoHarian.getTanggalUpdate()));

        model.addAttribute("listKasHarian",historyDetailTransaksiDao.detailTransaksiDepartemen(historySaldoHarian.getDepartemen().getId(),historySaldoHarian.getTanggalUpdate()));

        model.addAttribute("tanggalUpdate", historySaldoHarian.getTanggalUpdate());
        model.addAttribute("reports", "active");
        model.addAttribute("report_departement_transaction", "active");
        return "reports/dailycashdepartemen/list";

    }

    @GetMapping("/reports/dailycash/export")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        LocalDateTime localDateTime = historySaldoHarianDao.tanggalUpdateTerakhir();

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Departement_transaction_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<SisaAnggaranDashboardKodeDto> sisaAnggaranDashboardKodeDtos = historySaldoHarianDao.listSaldoHarianKode(localDateTime);
        List<DetailTransaksiDepartemenDto> detailTransaksiDepartemenDtos = historyDetailTransaksiDao.detailTransaksi(localDateTime);
        ExportSaldoBudgetHarian excelExporter = new ExportSaldoBudgetHarian(sisaAnggaranDashboardKodeDtos, detailTransaksiDepartemenDtos);
        excelExporter.export(response);
    }

}
