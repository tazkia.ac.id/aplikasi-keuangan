package id.ac.tazkia.aplikasikeuangan.controller.setting;

import id.ac.tazkia.aplikasikeuangan.dao.config.UserDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.*;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.setting.*;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Controller
public class AnggaranProkerAdminController {

    @Autowired
    private AnggaranProkerDao anggaranProkerDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private StandardDetailDao standardDetailDao;

    @Autowired
    private AkunLevelTigaDao akunLevelTigaDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PicAnggaranDao picAnggaranDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @GetMapping("/setting/admin/anggaran/proker/baru")
    public String anggaranProkerBaru(Model model,
                                     @RequestParam(required = true) Anggaran anggaran,
                                     @RequestParam(required = true) Departemen departemen,
                                     @RequestParam(required = true) PeriodeAnggaran periode) {

        model.addAttribute("anggaran", anggaran);
        model.addAttribute("anggaranProker", new AnggaranProker());
        model.addAttribute("listAkunLevelTiga", akunLevelTigaDao.findByStatusOrderByAkunLevelDuaAkunLevelSatuAkunNamaAkun(StatusRecord.AKTIF));
        model.addAttribute("listStandardDetail", standardDetailDao.findByStatusOrderByStandard(StatusRecord.AKTIF));

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return "setting/anggaran/admin/anggaranprokerform";
    }

    @GetMapping("/setting/admin/anggaran/proker/edit")
    public String anggaranProkerBaru(Model model,
                                     @RequestParam(required = true) AnggaranProker anggaranProker,
                                     @RequestParam(required = true) Anggaran anggaran,
                                     @RequestParam(required = true) Departemen departemen,
                                     @RequestParam(required = true) PeriodeAnggaran periode) {

        model.addAttribute("anggaran", anggaran);
        model.addAttribute("anggaranProker", anggaranProker);
        model.addAttribute("listAkunLevelTiga", akunLevelTigaDao.findByStatusOrderByAkunLevelDuaAkunLevelSatuAkunNamaAkun(StatusRecord.AKTIF));
        model.addAttribute("listStandardDetail", standardDetailDao.findByStatusOrderByStandard(StatusRecord.AKTIF));

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return "setting/anggaran/admin/anggaranprokerform";
    }

    @Transactional
    @PostMapping("/setting/admin/anggaran/proker/save")
    public String saveProkerAnggaran(@RequestParam(required = false) Anggaran anggaran,
                                       @ModelAttribute @Valid AnggaranProker anggaranProker,
                                       BindingResult errors,
                                       Authentication authentication,
                                       RedirectAttributes attribute){

        User user = currentUserService.currentUser(authentication);

        anggaranProker.setStatus(StatusRecord.AKTIF);
        anggaranProker.setUserUpdate(user.getUsername());
        anggaranProker.setDateUpdate(LocalDateTime.now());

        anggaranProkerDao.save(anggaranProker);
        return "redirect:../setting?departemen=" + anggaran.getDepartemen().getId() + "&periode=" + anggaran.getPeriodeAnggaran().getId();

    }

    @Transactional
    @PostMapping("/setting/admin/anggaran/proker/hapus")
    public String deleteAnggaranProker(@RequestParam(required = true)AnggaranProker anggaranProker,
                                        Authentication authentication,
                                        RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);

        BigDecimal totalPengajuanDanaProker = pengajuanDanaDao.getTotalPengajuanDanaProker(anggaranProker.getId());

        if (totalPengajuanDanaProker != null){

            attributes.addFlashAttribute("gagalProker", "Save Data Gagal");
        }else{

            anggaranDetailDao.deleteAnggaranDetailByProker(anggaranProker.getId());
            anggaranProker.setStatus(StatusRecord.HAPUS);
            anggaranProker.setUserUpdate(user.getUsername());
            anggaranProker.setDateUpdate(LocalDateTime.now());
            anggaranProkerDao.save(anggaranProker);
            attributes.addFlashAttribute("hapusProker", "Save Data Berhasil");

            PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, anggaranProker.getAnggaran().getPeriodeAnggaran().getId(), anggaranProker.getAnggaran().getDepartemen().getId());
            BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(anggaranProker.getAnggaran().getDepartemen().getId(), anggaranProker.getAnggaran().getPeriodeAnggaran().getId());
            if(totalAmountAnggaran == null){
                picAnggaran.setJumlah(BigDecimal.ZERO);
            }else{
                picAnggaran.setJumlah(totalAmountAnggaran);
            }

        }

        return "redirect:../setting?departemen="+anggaranProker.getAnggaran().getDepartemen().getId()+"&periode="+anggaranProker.getAnggaran().getPeriodeAnggaran().getId();
    }


}
