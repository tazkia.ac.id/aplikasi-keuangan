package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import id.ac.tazkia.aplikasikeuangan.dao.setting.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.lowagie.text.DocumentException;

import id.ac.tazkia.aplikasikeuangan.dao.BlockPengajuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.config.RoleDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.LaporanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PencairanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaApproveDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengembalianDanaDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengembalianDanaProsesDao;
import id.ac.tazkia.aplikasikeuangan.dto.TransaksiSetahunDuaDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.ChartTransaksiSetahunDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.TransaksiSetahunDto;
import id.ac.tazkia.aplikasikeuangan.entity.BlockPengajuan;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.KaryawanJabatan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import id.ac.tazkia.aplikasikeuangan.entity.setting.SettingApprovalPengajuanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDanaApprove;
import id.ac.tazkia.aplikasikeuangan.export.ExportBuktiPengajuanDana;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import id.ac.tazkia.aplikasikeuangan.services.GmailApiService;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

@Controller
public class PengajuanDanaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PengajuanDanaController.class);

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private AnggaranDetailEstimasiDao anggaranDetailEstimasiDao;

    @Autowired
    private SettingApprovalPengajuanDanaDao settingApprovalPengajuanDanaDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private PengajuanDanaApproveDao pengajuanDanaApproveDao;

    @Autowired
    private SatuanDao satuanDao;

    @Autowired
    private PencairanDanaDao pencairanDanaDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private LaporanDanaDao laporanDanaDao;

    @Autowired
    private PengembalianDanaDao pengembalianDanaDao;

    @Autowired
    private PengembalianDanaProsesDao pengembalianDanaProsesDao;

    @Autowired
    private AnggaranDetailCrudDao anggaranDetailCrudDao;

    @Autowired private GmailApiService gmailApiService;
    @Autowired private MustacheFactory mustacheFactory;

    @Autowired
    private BlockPengajuanDao blockPengajuanDao;

    @Autowired
    @Value("${upload.pengajuandana}")
    private String uploadFolder;

    @Autowired
    @Value("${lokasi.logo}")
    private String lokasiLogo;


    @GetMapping("/pengajuan_dana/bukti")
    public void generatePdfFile(HttpServletResponse response,
                                @RequestParam(required = true) PengajuanDana pengajuanDana) throws DocumentException, IOException {

        response.setContentType("application/pdf");

        // DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD:HH:MM:SS");

        String headerkey = "Content-Disposition";

        String str = pengajuanDana.getKaryawanPengaju().getNamaKaryawan().replaceAll("[^a-zA-Z0-9]", "-");
        System.out.println("nama sebelum : "+ pengajuanDana.getKaryawanPengaju().getNamaKaryawan());
        System.out.println("nama sesuadah : "+ str);
        String headervalue = "attachment; filename=Lampiran_pengajuan_dana_" + str +"_" + pengajuanDana.getTanggalPengajuan() + ".pdf";

        response.setHeader(headerkey, headervalue);

        ExportBuktiPengajuanDana generator = new ExportBuktiPengajuanDana();

        generator.generate(pengajuanDana, uploadFolder, lokasiLogo , response);

    }

    @GetMapping("/transaksi/pengajuandana")
    public String listPengajuanDana(Model model,
                                    @RequestParam(required = false)String search,
                                    @RequestParam(required = false)AnggaranDetail anggaranDetail,
                                    @PageableDefault(size = 10)Pageable page,
                                    Authentication authentication){




        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
        model.addAttribute("selectedAnggaranDetail", anggaranDetail);
        if (anggaranDetail != null){
            model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranDepartemenIdIn(StatusRecord.AKTIF, karyawanDepartemen));
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
                model.addAttribute("listPengajuanDanaWaiting", pengajuanDanaDao.pengajuanDanaWaitingSearchAnggaranDetail(StatusRecord.AKTIF, karyawan,anggaranDetail, search, page));
            }else{
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
                model.addAttribute("listPengajuanDanaWaiting", pengajuanDanaDao.pengajuanDanaWaitingAnggaranDetail(StatusRecord.AKTIF, karyawan, anggaranDetail , page));
            }
        }else{
            model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranDepartemenIdIn(StatusRecord.AKTIF, karyawanDepartemen));
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
                model.addAttribute("listPengajuanDanaWaiting", pengajuanDanaDao.pengajuanDanaWaitingSearch(StatusRecord.AKTIF, karyawan, search, page));
            }else{
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
                model.addAttribute("listPengajuanDanaWaiting", pengajuanDanaDao.pengajuanDanaWaiting(StatusRecord.AKTIF, karyawan, page));
            }
        }

        model.addAttribute("pengajuandana", "active");
        model.addAttribute("transactions", "active");
        return "transaksi/pengajuandana/list";

    }

    @GetMapping("/transaksi/pengajuandana/rejectedlist")
    public String listPengajuanDanaRejected(Model model,
                                    @PageableDefault(size = 10)Pageable page,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));

        model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.REJECTED));
        model.addAttribute("listPengajuanDanaRejected", pengajuanDanaDao.pengajuanDanaRejected(StatusRecord.REJECTED,karyawan, page));

        model.addAttribute("rejected", "active");
        model.addAttribute("transactions", "active");
        return "transaksi/pengajuandana/rejectedlist";
    }

    @GetMapping("/transaksi/pengajuandana/approved")
    public String listPengajuanDanaApprove(Model model,
                                            @PageableDefault(size = 10)Pageable page,
                                            @RequestParam(required = false) String search,
                                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            model.addAttribute("listPengajuanDanaApproved", pengajuanDanaDao.pengajuanDanaApprovedSearch(StatusRecord.AKTIF,karyawan, search, page));
        }else{
            model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            model.addAttribute("listPengajuanDanaApproved", pengajuanDanaDao.pengajuanDanaApproved(StatusRecord.AKTIF,karyawan, page));
        }

        model.addAttribute("approved", "active");
        model.addAttribute("transactions", "active");
        return "transaksi/pengajuandana/listapproved";
    }

    @GetMapping("/transaksi/pengajuandana/disbursment")
    public String listPengajuanDanaDisbursment(Model model,
                                           @RequestParam(required = false) String search,
                                           @PageableDefault(size = 10)Pageable page,
                                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));

        if(StringUtils.hasText(search)){
            model.addAttribute("search", search);
            model.addAttribute("listPengajuanDanaCair", pencairanDanaDao.findByStatusAndStatusPencairanAndUserMintaPencairanAndKeteranganContainingIgnoreCaseOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan, search, page));

        }else{
            model.addAttribute("listPengajuanDanaCair", pencairanDanaDao.findByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan, page));

        }

        model.addAttribute("siapdisburse", "active");
        model.addAttribute("transactions", "active");
        return "transaksi/pengajuandana/listdisbursment";
    }

    @GetMapping("/transaksi/pengajuandana/already/disbursment")
    public String listPengajuanDanaAlreadyDisbursment(Model model,
                                               @RequestParam(required = false)String search,
                                               @PageableDefault(size = 10)Pageable page,
                                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));

        if(StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listPengajuanDanaCair", pencairanDanaDao.findByStatusPencairanAndStatusAndUserMintaPencairanAndKeteranganPencairanContainingIgnoreCaseOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan,search, page));
        }else{
            model.addAttribute("listPengajuanDanaCair", pencairanDanaDao.findByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan, page));

        }

        model.addAttribute("disbursment", "active");
        model.addAttribute("transactions", "active");
        return "transaksi/pengajuandana/cairdisbursment";
    }

    @GetMapping("/transaksi/pengajuandana/closed")
    public String listPengajuanDanaClosed(Model model,
                                           @PageableDefault(size = 10)Pageable page,
                                           @RequestParam(required = false)String search,
                                           Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listPengajuanDanaClosed", pengajuanDanaDao.findByStatusAndDepartemenIdInAndAnggaranKodeAnggaranContainingIgnoreCaseOrStatusAndDepartemenIdInAndAnggaranNamaAnggaranContainingIgnoreCaseOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen, search, StatusRecord.CLOSED, karyawanDepartemen, search, page));
        }else {
            model.addAttribute("listPengajuanDanaClosed", pengajuanDanaDao.findByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen, page));
        }

        model.addAttribute("transactions", "active");
        model.addAttribute("closed", "active");
        return "transaksi/pengajuandana/listclosed";
    }



    @GetMapping("/transaksi/pengajuandana/all")
    public String listPengajuanDanaAll(Model model,
                                          @PageableDefault(size = 10)Pageable page,
                                          @RequestParam(required = false)String search,
                                          Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));

        model.addAttribute("listPengajuanDanaAll", pengajuanDanaDao.findByStatusOrderByTanggalPengajuan(StatusRecord.HAPUS, page));

        model.addAttribute("transactions", "active");
        model.addAttribute("all", "active");
        return "transaksi/pengajuandana/listclosed";
    }

    @GetMapping("/transaksi/pengajuandana/laporan")
    public String listPengajuanDanaReport(Model model,
                                       @PageableDefault(size = 10)Pageable page,
                                       @RequestParam(required = false)String search,
                                       Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
//        model.addAttribute("totalPengajuanDana", pencairanDanaDetailDao.totalPengajuanDana(pencairanDana.getId()));
        model.addAttribute("listPengajuanDanaReport", pencairanDanaDao.findByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan, page));


        model.addAttribute("transactions", "active");
        model.addAttribute("laporan", "active");
        return "transaksi/pengajuandana/listlaporan";
    }

    @GetMapping("/transaksi/pengajuandana/laporanwaiting")
    public String listPengajuanDanaReportWaiting(Model model,
                                                 @PageableDefault(size = 10) Pageable page,
                                                 @RequestParam(required = false)String search,
                                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
//        model.addAttribute("listPengajuanDanaReport", pencairanDanaDao.findByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.LAPOR, StatusRecord.LAPOR, karyawan, page));
        //      model.addAttribute("listPengajuanDanaReport", laporanDanaDao.findByStatusAndStatusLaporOrderByTanggalLaporan(StatusRecord.AKTIF,StatusRecord.WAITING, page));

        model.addAttribute("listPelaporanDanaWaiting", laporanDanaDao.findByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan, page));

        model.addAttribute("transactions", "active");
        model.addAttribute("laporanwaiting", "active");
        return "transaksi/pengajuandana/listlaporanwaiting";
    }

    @GetMapping("/transaksi/pengajuandana/laporanrejected")
    public String listPengajuanDanaReportRejected(Model model,
                                                 @PageableDefault(size = 10) Pageable page,
                                                 @RequestParam(required = false)String search,
                                                 Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
//        model.addAttribute("listPengajuanDanaReport", pencairanDanaDao.findByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.LAPOR, StatusRecord.LAPOR, karyawan, page));
        //      model.addAttribute("listPengajuanDanaReport", laporanDanaDao.findByStatusAndStatusLaporOrderByTanggalLaporan(StatusRecord.AKTIF,StatusRecord.WAITING, page));

        model.addAttribute("listPelaporanDanaRejected", laporanDanaDao.findByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan, page));

        model.addAttribute("transactions", "active");
        model.addAttribute("laporanrejected", "active");
        return "transaksi/pengajuandana/listlaporanrejected";
    }

    @GetMapping("/transaksi/pengajuandana/waitingrefund")
    public String listWaitingRefund(Model model,
                                    @PageableDefault(size = 10) Pageable page,
                                    @RequestParam(required = false)String search,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
//      model.addAttribute("listPengajuanDanaReport", pencairanDanaDao.findByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.LAPOR, StatusRecord.LAPOR, karyawan, page));
//      model.addAttribute("listPengajuanDanaReport", laporanDanaDao.findByStatusAndStatusLaporOrderByTanggalLaporan(StatusRecord.AKTIF,StatusRecord.WAITING, page));

        model.addAttribute("listPengembalianDana" , pengembalianDanaDao.dataPengembalian(karyawan.getId(), "AKTIF", "WAITING", page));

//      model.addAttribute("listPengajuanDana", pengajuanDanaDao.findByStatusAAndKaryawanPengaju(StatusRecord.AKTIF, karyawan));


        model.addAttribute("transactions", "active");
        model.addAttribute("waitingrefund", "active");
        return "transaksi/pengajuandana/waitingrefund";
    }


    @GetMapping("/transaksi/pengajuandana/refund")
    public String listRefund(Model model,
                                    @PageableDefault(size = 10) Pageable page,
                                    @RequestParam(required = false)String search,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
//        model.addAttribute("listPengajuanDanaReport", pencairanDanaDao.findByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.LAPOR, StatusRecord.LAPOR, karyawan, page));
        //      model.addAttribute("listPengajuanDanaReport", laporanDanaDao.findByStatusAndStatusLaporOrderByTanggalLaporan(StatusRecord.AKTIF,StatusRecord.WAITING, page));

        //model.addAttribute("listRefund" , pengembalianDanaProsesDao.findByStatusAndStatusPengembalianAndUserPengembaliOrderByTanggalPengembalian(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan, page));
//        model.addAttribute("listRefund" , pengembalianDanaProsesDao.dataPengembalianProses(karyawan.getId(), "AKTIF", "OPEN", page));
//        model.addAttribute("listRefund", pengembalianDanaProsesDao.findByStatusAndUserPengembaliOrderByTanggalPengembalianDesc(StatusRecord.AKTIF, karyawan, page));
        model.addAttribute("listRefund", pengembalianDanaProsesDao.pageRefundKaryawan(karyawan.getId(), "WAITING", page));
        model.addAttribute("listRefundDetail", pengembalianDanaProsesDao.listRefundKaryawanDetail(karyawan.getId(),"WAITING"));

        model.addAttribute("transactions", "active");
        model.addAttribute("refund", "active");
        return "transaksi/pengajuandana/refund";
    }

    @GetMapping("/transaksi/pengajuandana/refundclose")
    public String listRefundClosed(Model model,
                             @PageableDefault(size = 10) Pageable page,
                             @RequestParam(required = false)String search,
                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
        model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
        model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
        model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
        model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
        model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
        model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
        model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
        model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
        model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
//        model.addAttribute("listPengajuanDanaReport", pencairanDanaDao.findByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.LAPOR, StatusRecord.LAPOR, karyawan, page));
        //      model.addAttribute("listPengajuanDanaReport", laporanDanaDao.findByStatusAndStatusLaporOrderByTanggalLaporan(StatusRecord.AKTIF,StatusRecord.WAITING, page));

        //model.addAttribute("listRefund" , pengembalianDanaProsesDao.findByStatusAndStatusPengembalianAndUserPengembaliOrderByTanggalPengembalian(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan, page));
//        model.addAttribute("listRefund" , pengembalianDanaProsesDao.dataPengembalianProses(karyawan.getId(), "AKTIF", "CLOSED", page));

        model.addAttribute("listRefund", pengembalianDanaProsesDao.pageRefundKaryawanClose(karyawan.getId(), "CLOSED", page));
        model.addAttribute("listRefundDetail", pengembalianDanaProsesDao.listRefundKaryawanDetail(karyawan.getId(),"CLOSED"));


        model.addAttribute("transactions", "active");
        model.addAttribute("refundclosd", "active");
        return "transaksi/pengajuandana/refundclosed";
    }



    @GetMapping("/transaksi/pengajuandana/baru")
    public String baruPengajuanDana(Model model,  Authentication authentication, RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());

        Integer jmlDepartemen = departemenDao.jmlBlockingPengajuan(karyawanDepartemen);
        Integer jumlahBelumConfirm = pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan);
        Integer jumlahBelumLapor =  pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan);
        Integer JumlahLaporanRejected = laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan);
        Integer jumlahDepartemen = karyawanJabatanDao.jmlDepartemen(karyawan.getId());

//        Integer jmlPencairanMangkrak = pencairanDanaDao.cariLaporanMankrak(karyawan.getId());
//        if (jmlPencairanMangkrak != null) {
//            if (jmlPencairanMangkrak > 0) {
//                redirectAttributes.addFlashAttribute("pencairanMangkrak", "Maaf untuk sementara anda tidak diizinkan melakukan pengajuan dana dikarenakan ada "+ jmlPencairanMangkrak +" transaksi yang belum anda laporkan yang sudah melewati batas waktu 2 pekan");
//                return "redirect:../pengajuandana";
//            }
//        }


        BlockPengajuan blockPengajuan = blockPengajuanDao.findByStatus(StatusRecord.AKTIF);

        if (blockPengajuan == null){

            model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
            model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
            model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
            model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
            model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
            model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
            model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
            model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
            model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
            model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
            model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
            model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
            model.addAttribute("jmlins", jmlDepartemen);

            model.addAttribute("transactions", "active");
            return "transaksi/pengajuandana/blocktutup";

        }else {

            //   if (jumlahInstansi > 1){
            if (jumlahBelumConfirm > jmlDepartemen) {

                model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
                model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
                model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
                model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
                model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
                model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
                model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
                model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
                model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
                model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
                model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
                model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
                model.addAttribute("jmlins", jmlDepartemen);

                model.addAttribute("transactions", "active");
                return "transaksi/pengajuandana/blockkonfirmasi";
            }

            if (jumlahBelumLapor > jmlDepartemen) {

                model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
                model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
                model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
                model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
                model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
                model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
                model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
                model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
                model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
                model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
                model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
                model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
                model.addAttribute("jmlins", jmlDepartemen);

                model.addAttribute("transactions", "active");
                return "transaksi/pengajuandana/blocklaporan";
            }

            if (JumlahLaporanRejected > jmlDepartemen) {

                model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
                model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
                model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
                model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
                model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
                model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
                model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
                model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
                model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
                model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
                model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
                model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
                model.addAttribute("jmlins", jmlDepartemen);

                model.addAttribute("transactions", "active");
                return "transaksi/pengajuandana/blocklaporanrejected";

            }

//        }else{
//            if (jumlahBelumConfirm > 3){
//
//                model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
//                model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
//                model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
//                model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
//                model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
//                model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
//                model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
//                model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
//                model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
//                model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
//                model.addAttribute("jumlahRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.OPEN, karyawan.getId()));
//                model.addAttribute("jumlahRefundClosed", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.CLOSED, karyawan.getId()));
//                return "transaksi/pengajuandana/blockkonfirmasi";
//            }
//
//            if (jumlahBelumLapor > 3){
//
//                model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
//                model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
//                model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
//                model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
//                model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
//                model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
//                model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
//                model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
//                model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
//                model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
//                model.addAttribute("jumlahRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.OPEN, karyawan.getId()));
//                model.addAttribute("jumlahRefundClosed", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.CLOSED, karyawan.getId()));
//                return "transaksi/pengajuandana/blocklaporan";
//            }
//
//            if (JumlahLaporanRejected > 0){
//
//                model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
//                model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
//                model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
//                model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
//                model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
//                model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
//                model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
//                model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
//                model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
//                model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
//                model.addAttribute("jumlahRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.OPEN, karyawan.getId()));
//                model.addAttribute("jumlahRefundClosed", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.CLOSED, karyawan.getId()));
//                return "transaksi/pengajuandana/blocklaporanrejected";
//
//            }
            // }

            if (user.getRole() == roleDao.findById("superuser").get() || user.getRole().equals(roleDao.findById("superuser")) || user.getRole() == roleDao.findById("anggaran").get() || user.getRole().equals(roleDao.findById("anggaran"))) {
                System.out.println("test");
                model.addAttribute("listAnggaran", anggaranDao.getAnggaranDepartemens(periodeAnggaran));
            } else {
                //model.addAttribute("listAnggaran", anggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenIdInOrderByKodeAnggaran(StatusRecord.AKTIF, periodeAnggaran, karyawanDepartemen));
                model.addAttribute("listAnggaran", anggaranDao.getAnggaranDepartemen(periodeAnggaran, karyawanDepartemen));
            }

            model.addAttribute("satuan", satuanDao.findAll());
            model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF));
            model.addAttribute("pengajuanDana", new PengajuanDana());

//            model.addAttribute("jumlahPengajuanDanaWaiting", pengajuanDanaDao.jumlahPengajuanDanaWaiting(karyawan.getId()));
//            model.addAttribute("jumlahPengajuanDanaApproved", pengajuanDanaDao.jumlahPengajuanDanaApproved(karyawan.getId()));
//            model.addAttribute("jumlahPengajuanDanaRejected", pengajuanDanaDao.jumlahPengajuanDanaRejected(karyawan.getId()));
//            model.addAttribute("jumlahDisbursement", pencairanDanaDao.countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
//            model.addAttribute("jumlahAlreadyDisbursement", pencairanDanaDao.countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord.CAIR, StatusRecord.AKTIF, karyawan));
//            model.addAttribute("jumlahUnreported", pencairanDanaDao.countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord.AKTIF, StatusRecord.CAIR, StatusRecord.LAPOR, karyawan));
//            model.addAttribute("jumlahReportedWaiting", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan));
//            model.addAttribute("jumlahReportedRejected", laporanDanaDao.countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord.AKTIF, StatusRecord.REJECTED, karyawan));
//            model.addAttribute("jumlahClosed", pengajuanDanaDao.countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord.CLOSED, karyawanDepartemen));
//            model.addAttribute("jumlahWaitingRefund", pengembalianDanaDao.countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord.AKTIF, StatusRecord.WAITING, karyawan.getId()));
//            model.addAttribute("jumlahRefund", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.WAITING));
//            model.addAttribute("jumlahRefundClosed", pengembalianDanaProsesDao.countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord.AKTIF, karyawan, StatusRecord.CLOSED));
//            model.addAttribute("jmlins", jmlDepartemen);

            model.addAttribute("transactions", "active");
            return "transaksi/pengajuandana/form";

        }

    }

    @GetMapping("/transaksi/pengajuandana/edit")
    public String editPengajuanDana(Model model,
                                    @RequestParam(required = false)String id,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        model.addAttribute("listAnggaran", anggaranDao.getAnggaranDepartemen(periodeAnggaran, karyawanDepartemen));
//        model.addAttribute("listAnggaran", anggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenIdInOrderByKodeAnggaran(StatusRecord.AKTIF, periodeAnggaran, karyawanDepartemen));
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF));
        model.addAttribute("pengajuanDana", pengajuanDanaDao.findById(id).get());

        model.addAttribute("transactions", "active");
        return "transaksi/pengajuandana/form";

    }



    @PostMapping("/transaksi/pengajuandana/save")
    @Transactional
    public String savePengajuanDana(Model model,
                                    @ModelAttribute @Valid PengajuanDana pengajuanDana,
                                    BindingResult errors,
                                    @RequestParam("lampiran") MultipartFile file,
                                    Authentication authentication,
                                    RedirectAttributes attribute) throws Exception {


        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        BigDecimal getSisa = anggaranDetailDao.getSisaDetailAnggaran(pengajuanDana.getAnggaranDetail().getId());
        BigDecimal sisaAngggaran = anggaranDetailCrudDao.getSisaAnggaranBudget(pengajuanDana.getAnggaran().getDepartemen().getId(), periodeAnggaran);
        BigDecimal sisaPaguBudget = anggaranDetailCrudDao.getSisaPaguBudget(pengajuanDana.getAnggaran().getDepartemen().getId(), periodeAnggaran);

        if (errors.hasErrors()) {
            model.addAttribute("listAnggaran", anggaranDao.getAnggaranDepartemen(periodeAnggaran, karyawanDepartemen));
            model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF));
            return "transaksi/pengajuandana/form";
        }

        Integer jmlPencairanMangkrak = pencairanDanaDao.cariLaporanMankrak(karyawan.getId(), pengajuanDana.getAnggaranDetail().getAnggaran().getDepartemen().getId());
        if (jmlPencairanMangkrak != null) {
            if (jmlPencairanMangkrak > 0) {
                attribute.addFlashAttribute("pencairanMangkrak", "Maaf untuk sementara anda tidak diizinkan melakukan pengajuan dana dikarenakan ada "+ jmlPencairanMangkrak +" transaksi yang belum anda laporkan yang sudah melewati batas waktu 2 pekan");
                return "redirect:../pengajuandana";
            }
        }

        Departemen departemen = pengajuanDana.getAnggaran().getDepartemen();
        KaryawanJabatan karyawanJabatan = karyawanJabatanDao.findByStatusAndStatusAktifAndKaryawanAndJabatanDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF, karyawan, departemen);
        BigDecimal jumlah =  pengajuanDana.getKuantitas().multiply(pengajuanDana.getAmount());
        LOGGER.debug("Jumlah : {}",jumlah);

//        if (sisaPagu.longValue() < 0){
//
//            attribute.addFlashAttribute("gagalpagu", "Save Data Gagal");
//            return "redirect:../pengajuandana";
//
//        }

        AnggaranDetail anggaranDetail = pengajuanDana.getAnggaranDetail();

        BigDecimal totalEstimasi = anggaranDetailEstimasiDao.getAmountAnggaranDetailEstimasiBulan(anggaranDetail.getId());
        BigDecimal totalAnggaranKeluar = pengajuanDanaDao.getTotalPengeluaranDetailAnggaran(pengajuanDana.getAnggaranDetail().getId());
        if (totalAnggaranKeluar == null){
            totalAnggaranKeluar = BigDecimal.ZERO;
        }
        BigDecimal keluaran = totalAnggaranKeluar.add(pengajuanDana.getKuantitas().multiply(pengajuanDana.getAmount()));
        if(totalEstimasi == null){
            model.addAttribute("gagalBlocking", "Pengajuan dana tidak diizinkan karena anda tidak membuat estimasi pengeluaran untuk bulan ini");
            model.addAttribute("listAnggaran", anggaranDao.getAnggaranDepartemen(periodeAnggaran, karyawanDepartemen));
            model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF));
            return "transaksi/pengajuandana/form";
        }else{
            if(totalEstimasi.compareTo(BigDecimal.ZERO) == 0){
                model.addAttribute("gagalBlocking", "Pengajuan dana tidak diizinkan karena anda tidak membuat estimasi pengeluaran untuk bulan ini");
                model.addAttribute("listAnggaran", anggaranDao.getAnggaranDepartemen(periodeAnggaran, karyawanDepartemen));
                model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF));
                return "transaksi/pengajuandana/form";
            }else{
                if(totalEstimasi.compareTo(keluaran) < 0){
                    model.addAttribute("gagalBlocking", "Pengajuan dana tidak diizinkan karena melebihi total estimasi yang anda buat untuk bulan ini");
                    model.addAttribute("listAnggaran", anggaranDao.getAnggaranDepartemen(periodeAnggaran, karyawanDepartemen));
                    model.addAttribute("listKaryawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF));
                    return "transaksi/pengajuandana/form";
                }
            }
        }



        if (pengajuanDana.getRequestFor() == null){
            attribute.addFlashAttribute("gagalkaryawan", "Save Data Gagal");
            return "redirect:../pengajuandana";
        }

        if (sisaPaguBudget.subtract(jumlah).longValue()<0){

            attribute.addFlashAttribute("gagalpagubudget", "Save Data Gagal");
            return "redirect:../pengajuandana";

        }

        if (sisaAngggaran.subtract(jumlah).longValue()<0){

            attribute.addFlashAttribute("gagalAnggaran", "Save Data Gagal");
            return "redirect:../pengajuandana";

        }

        if (getSisa.longValue() < 0){

            attribute.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:../pengajuandana";

        }

        if (getSisa.subtract(jumlah).compareTo(BigDecimal.ZERO) < 0 ){

            attribute.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:../pengajuandana";

        }

        if(user.getRole() == roleDao.findById("warek2").get() || user.getRole().equals(roleDao.findById("warek2"))) {
            Integer jumlahApproval = 0;

            String namaAsli = file.getOriginalFilename();
            Long ukuran = file.getSize();
//
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
//
            String idFile = UUID.randomUUID().toString();
            pengajuanDana.setTanggalPengajuan(LocalDateTime.now().plusHours(7));
            pengajuanDana.setKaryawanPengaju(karyawan);
            pengajuanDana.setDepartemen(departemen);
            pengajuanDana.setJumlah(jumlah);
            pengajuanDana.setNomorUrut(1);
            pengajuanDana.setTotalNomorUrut(jumlahApproval);
            pengajuanDana.setPeriodeAnggaran(pengajuanDana.getAnggaranDetail().getAnggaran().getPeriodeAnggaran());
            if (ukuran == 0){
                pengajuanDana.setFile("default.jpg");
            }else{
                pengajuanDana.setFile(idFile + "." + extension);
            }
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);

            LOGGER.debug("Lokasi upload : {}", uploadFolder);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);


            LOGGER.debug("Pengajuan dana sebelum save : {}", pengajuanDana);
            pengajuanDanaDao.save(pengajuanDana);

            return "redirect:../pengajuandana";
        }else {


                    Integer nomorApproval=settingApprovalPengajuanDanaDao.nomorApproval(karyawanJabatan.getJabatan().getId(), jumlah);
                    List<SettingApprovalPengajuanDana> settingApprovalPengajuanDana = settingApprovalPengajuanDanaDao.findByStatusAndJabatanIdAndBatasPengajuanLessThanOrderByNomorUrut(StatusRecord.AKTIF, karyawanJabatan.getJabatan().getId(), jumlah);
                    Integer jumlahApproval = settingApprovalPengajuanDanaDao.getJumlahApproval(karyawanJabatan.getJabatan().getId(), jumlah);

            String namaAsli = file.getOriginalFilename();
            Long ukuran = file.getSize();
//
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
//
            String idFile = UUID.randomUUID().toString();
            pengajuanDana.setTanggalPengajuan(LocalDateTime.now().plusHours(7));
            pengajuanDana.setKaryawanPengaju(karyawan);
            pengajuanDana.setDepartemen(departemen);
            pengajuanDana.setJumlah(jumlah);
            pengajuanDana.setNomorUrut(nomorApproval);
            pengajuanDana.setPeriodeAnggaran(pengajuanDana.getAnggaranDetail().getAnggaran().getPeriodeAnggaran());
            pengajuanDana.setTotalNomorUrut(jumlahApproval);
            if (ukuran == 0){
                pengajuanDana.setFile("default.jpg");
            }else{
                pengajuanDana.setFile(idFile + "." + extension);
                System.out.println("file :"+ namaAsli);
                System.out.println("Ukuran :"+ ukuran);

                LOGGER.debug("Lokasi upload : {}", uploadFolder);
                new File(uploadFolder).mkdirs();
                File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
                file.transferTo(tujuan);
                LOGGER.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());
                LOGGER.debug("Jumlah setting approval : {}", settingApprovalPengajuanDana.size());
            }


            Integer adas = 1;
            for(SettingApprovalPengajuanDana s : settingApprovalPengajuanDana){

//                KaryawanJabatan karyawanJabatan1=karyawanJabatanDao.findByStatusAndStatusAktifAndJabatanIdAndMulaiBerlakuBefore(StatusRecord.AKTIF, StatusRecord.AKTIF, s.getJabatanApprove().getId(), LocalDate.now());
                List<KaryawanJabatan> karyawanJabatans = karyawanJabatanDao.findByStatusAndStatusAktifAndJabatanIdAndMulaiBerlakuBeforeOrderByKaryawanNamaKaryawan(StatusRecord.AKTIF, StatusRecord.AKTIF, s.getJabatanApprove().getId(), LocalDate.now());
                for(KaryawanJabatan kj : karyawanJabatans) {
                    PengajuanDanaApprove pengajuanDanaApprove = new PengajuanDanaApprove();
                    pengajuanDanaApprove.setPengajuanDana(pengajuanDana);
                    pengajuanDanaApprove.setNomorUrut(s.getNomorUrut());
                    pengajuanDanaApprove.setJabatanApprove(s.getJabatanApprove());
                    pengajuanDanaApprove.setStatusApprove(StatusRecord.WAITING);
                    pengajuanDanaApprove.setStatus(StatusRecord.AKTIF);
                    pengajuanDanaApprove.setUserApprove(kj.getKaryawan());
                    pengajuanDana.getApprovalPengajuanDana().add(pengajuanDanaApprove);
                    pengajuanDanaApproveDao.save(pengajuanDanaApprove);
                    if (adas == 1) {

                        //EmailDto pengajuanDanaApproves = pengajuanDanaApproveDao.pengajuanDanaNotifikasi(pengajuanDana.getId());

                        Mustache templateEmail = mustacheFactory.compile("invoice.html");
                        Map<String, String> data = new HashMap<>();
                        data.put("nama", kj.getKaryawan().getNamaKaryawan());
                        data.put("item", pengajuanDana.getDeskripsiBiaya());
                        data.put("kuantitas", pengajuanDana.getKuantitas().toString());
                        data.put("amount", pengajuanDana.getAmount().toString());
                        data.put("satuan", pengajuanDana.getSatuan());
                        data.put("jumlah", pengajuanDana.getJumlah().toString());
                        data.put("jabatan", kj.getJabatan().getNamaJabatan());
                        data.put("tanggal", pengajuanDana.getTanggalPengajuan().toString());
                        data.put("anggaran", pengajuanDana.getAnggaranDetail().getDeskripsi());
                        data.put("dari", pengajuanDana.getKaryawanPengaju().getNamaKaryawan());
                        data.put("departemendari", pengajuanDana.getAnggaranDetail().getAnggaran().getDepartemen().getNamaDepartemen());
                        data.put("id", pengajuanDana.getId());


                        StringWriter output = new StringWriter();
                        templateEmail.execute(output, data);

                        gmailApiService.kirimEmail(
                                "Notifikasi Pengajuan Dana",
                                kj.getKaryawan().getEmail(),
                                "Approval Pengajuan dana",
                                output.toString());

                    }
                }
                adas=adas+1;
            }

            LOGGER.debug("Pengajuan dana sebelum save : {}", pengajuanDana);
            pengajuanDanaDao.save(pengajuanDana);

            attribute.addFlashAttribute("success", "Save Data Berhasil");
            return "redirect:../pengajuandana";
        }

    }

    @PostMapping("/transaksi/pengajuandana/update")
    @Transactional
    public String updatePengajuanDana(Model model,
                                    @ModelAttribute @Valid PengajuanDana pengajuanDana,
                                    BindingResult errors,
                                    @RequestParam("lampiran") MultipartFile file,
                                    Authentication authentication,
                                      RedirectAttributes attribute) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        BigDecimal getSisa = anggaranDetailDao.getSisaDetailAnggaran(pengajuanDana.getAnggaranDetail().getId());
        BigDecimal sisaPaguBudget = anggaranDetailCrudDao.getSisaPaguBudget(pengajuanDana.getAnggaran().getDepartemen().getId(), periodeAnggaran);
        PengajuanDana pengajuanDana1=pengajuanDanaDao.findById(pengajuanDana.getId()).get();

        LOGGER.debug("Pengajuan dana dari inputan : {}", pengajuanDana);

        if (errors.hasErrors()) {
            LOGGER.debug("Error input pengajuan : {}"+errors);
            model.addAttribute("listAnggaran", anggaranDao.getAnggaranDepartemen(periodeAnggaran, karyawanDepartemen));
            return "transaksi/pengajuandana/edit";
        }


        Departemen departemen = pengajuanDana.getAnggaran().getDepartemen();
        KaryawanJabatan karyawanJabatan = karyawanJabatanDao.findByStatusAndStatusAktifAndKaryawanAndJabatanDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF, karyawan, departemen);
        BigDecimal jumlah =  pengajuanDana.getKuantitas().multiply(pengajuanDana.getAmount());
        LOGGER.debug("Jumlah : {}",jumlah);

//        if (getSisa.longValue() < 0){
//
//            attribute.addFlashAttribute("gagal", "Save Data Gagal");
//            return "redirect:../pengajuandana";
//
//        }

        BigDecimal amount2 = pengajuanDana.getAmount().multiply(pengajuanDana.getKuantitas());
        BigDecimal jumlah2 = amount2.subtract(pengajuanDana1.getJumlah());
        BigDecimal getSisa2 = getSisa.add(pengajuanDana1.getJumlah());
        BigDecimal getSisa3 = getSisa2.subtract(amount2);

        if (pengajuanDana.getRequestFor() == null){
            attribute.addFlashAttribute("gagalkaryawan", "Save Data Gagal");
            return "redirect:../pengajuandana";
        }

        if (sisaPaguBudget.subtract(jumlah2).longValue() < 0){

            attribute.addFlashAttribute("gagalpagubudget", "Save Data Gagal");
            return "redirect:../pengajuandana";


        }

        if (getSisa3.longValue() < 0){

            attribute.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:../pengajuandana";


        }

        List<SettingApprovalPengajuanDana> settingApprovalPengajuanDana = settingApprovalPengajuanDanaDao.findByStatusAndJabatanIdAndBatasPengajuanLessThanOrderByNomorUrut(StatusRecord.AKTIF, karyawanJabatan.getJabatan().getId(), jumlah);
        Integer jumlahApproval = settingApprovalPengajuanDanaDao.getJumlahApproval(karyawanJabatan.getJabatan().getId(), jumlah);
        Integer nomorApproval=settingApprovalPengajuanDanaDao.nomorApproval(karyawanJabatan.getJabatan().getId(), jumlah);
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
//
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
//
        String idFile = UUID.randomUUID().toString();
        pengajuanDana.setTanggalPengajuan(LocalDateTime.now().plusHours(7));
        pengajuanDana.setKaryawanPengaju(karyawan);
        pengajuanDana.setDepartemen(departemen);
        pengajuanDana.setJumlah(jumlah);
        pengajuanDana.setNomorUrut(nomorApproval);
        pengajuanDana.setTotalNomorUrut(jumlahApproval);
        pengajuanDana.setPeriodeAnggaran(pengajuanDana.getAnggaranDetail().getAnggaran().getPeriodeAnggaran());
        if (ukuran == 0){
            pengajuanDana.setFile("default.jpg");
        }else{
            pengajuanDana.setFile(idFile + "." + extension);
        }
        System.out.println("file :"+ namaAsli);
        System.out.println("Ukuran :"+ ukuran);

        LOGGER.debug("Lokasi upload : {}", uploadFolder);
        new File(uploadFolder).mkdirs();
        File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);
        LOGGER.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());
        LOGGER.debug("Jumlah setting approval : {}", settingApprovalPengajuanDana.size());

        List<PengajuanDanaApprove> pengajuanDanaApprove1 = pengajuanDanaApproveDao.findByPengajuanDanaId(pengajuanDana.getId());
        for(PengajuanDanaApprove pa : pengajuanDanaApprove1){
            PengajuanDanaApprove pengajuanDanaApprove2 = pengajuanDanaApproveDao.findById(pa.getId()).get();

            pengajuanDanaApprove2.setStatus(StatusRecord.HAPUS);
            pengajuanDanaApproveDao.save(pengajuanDanaApprove2);
        }

        Integer adas= 1;
        for(SettingApprovalPengajuanDana s : settingApprovalPengajuanDana){

//            KaryawanJabatan karyawanJabatan1=karyawanJabatanDao.findByStatusAndStatusAktifAndJabatanIdAndMulaiBerlakuBefore(StatusRecord.AKTIF, StatusRecord.AKTIF, s.getJabatanApprove().getId(), LocalDate.now());
            List<KaryawanJabatan> karyawanJabatans = karyawanJabatanDao.findByStatusAndStatusAktifAndJabatanIdAndMulaiBerlakuBeforeOrderByKaryawanNamaKaryawan(StatusRecord.AKTIF, StatusRecord.AKTIF, s.getJabatanApprove().getId(), LocalDate.now());
            for(KaryawanJabatan kj : karyawanJabatans) {
                PengajuanDanaApprove pengajuanDanaApprove = new PengajuanDanaApprove();
                pengajuanDanaApprove.setPengajuanDana(pengajuanDana);
                pengajuanDanaApprove.setNomorUrut(s.getNomorUrut());
                pengajuanDanaApprove.setJabatanApprove(s.getJabatanApprove());
                pengajuanDanaApprove.setStatusApprove(StatusRecord.WAITING);
                pengajuanDanaApprove.setStatus(StatusRecord.AKTIF);
                pengajuanDanaApprove.setUserApprove(kj.getKaryawan());
                pengajuanDana.getApprovalPengajuanDana().add(pengajuanDanaApprove);
                pengajuanDanaApproveDao.save(pengajuanDanaApprove);
                if (adas == 1) {

                    //EmailDto pengajuanDanaApproves = pengajuanDanaApproveDao.pengajuanDanaNotifikasi(pengajuanDana.getId());

                    Mustache templateEmail = mustacheFactory.compile("invoice.html");
                    Map<String, String> data = new HashMap<>();
                    data.put("nama", kj.getKaryawan().getNamaKaryawan());
                    data.put("item", pengajuanDana.getDeskripsiBiaya());
                    data.put("kuantitas", pengajuanDana.getKuantitas().toString());
                    data.put("amount", pengajuanDana.getAmount().toString());
                    data.put("satuan", pengajuanDana.getSatuan());
                    data.put("jumlah", pengajuanDana.getJumlah().toString());
                    data.put("jabatan", kj.getJabatan().getNamaJabatan());
                    data.put("tanggal", pengajuanDana.getTanggalPengajuan().toString());
                    data.put("anggaran", pengajuanDana.getAnggaranDetail().getDeskripsi());
                    data.put("id", pengajuanDana.getId());
                    data.put("dari", pengajuanDana.getKaryawanPengaju().getNamaKaryawan());
                    data.put("departemendari", pengajuanDana.getAnggaranDetail().getAnggaran().getDepartemen().getNamaDepartemen());


                    StringWriter output = new StringWriter();
                    templateEmail.execute(output, data);

                    gmailApiService.kirimEmail(
                            "Notifikasi Pengajuan Dana",
                            kj.getKaryawan().getEmail(),
                            "Approval Pengajuan dana",
                            output.toString());

                }
            }
            adas=adas+1;
        }

        LOGGER.debug("Pengajuan dana sebelum save : {}", pengajuanDana);
        pengajuanDanaDao.save(pengajuanDana);

        return "redirect:../pengajuandana";

    }

    @PostMapping("/transaksi/pengajuandana/batal")
    public String batalPengajuanDana(@RequestParam PengajuanDana pengajuandana,
                                     RedirectAttributes attribute){

        pengajuandana.setStatus(StatusRecord.CANCELED);
        pengajuanDanaDao.save(pengajuandana);

        attribute.addFlashAttribute("batal", "Pengajuan talah dibatalkan");

        return "redirect:../pengajuandana";
    }

    @GetMapping("transaksi/pengajuandana/batal")
    public String batalPengajuan(@RequestParam PengajuanDana pengajuandana,
                                 RedirectAttributes attribute){


        pengajuandana.setStatus(StatusRecord.CANCELED);
        pengajuanDanaDao.save(pengajuandana);

        attribute.addFlashAttribute("batal", "Pengajuan talah dibatalkan");

        return "redirect:../pengajuandana/approved";

    }



    @GetMapping("/pengajuandana/{pengajuanDana}/bukti/")
    public ResponseEntity<byte[]> tampilkanBuktiPembayaran(@PathVariable PengajuanDana pengajuanDana) throws Exception {
        String lokasiFile = uploadFolder + File.separator + pengajuanDana.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pengajuanDana.getFile().toLowerCase().endsWith("jpeg") || pengajuanDana.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pengajuanDana.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pengajuanDana.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/pengajuandana/tumnail")
    public ResponseEntity<byte[]> tampilkanPengajuanDanaTumnail() throws Exception {
        String lokasiFile = uploadFolder + File.separator + "default.jpg";
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);
        String nama = "default.jpg";

        try {
            HttpHeaders headers = new HttpHeaders();
            if (nama.toLowerCase().endsWith("jpeg") || nama.toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (nama.toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (nama.toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/api/pengajuandana/setahun")
    @ResponseBody
    public ChartTransaksiSetahunDto totalPengajuanDanaSetahun(){

        ChartTransaksiSetahunDto chartTransaksiSetahunDto = new ChartTransaksiSetahunDto();
        List<TransaksiSetahunDto> transaksiSetahunDtos = pengajuanDanaDao.jumlahTransaksiSetahun();
        for (TransaksiSetahunDto api : transaksiSetahunDtos){
            chartTransaksiSetahunDto.getDaftarBulan().add(api.getBulan());
            chartTransaksiSetahunDto.getDaftarTotal().add(api.getTotal());
        }

        return chartTransaksiSetahunDto;
    }

    @GetMapping("/api/transaksi/setahun")
    @ResponseBody
    public ResponseEntity<List<TransaksiSetahunDuaDto>> cariTransaksiSetahun(){

        List<TransaksiSetahunDto> transaksi = pengajuanDanaDao.jumlahTransaksiSetahun();
        List<TransaksiSetahunDuaDto> transaksiSetahunDuaDtos = new ArrayList<>();
        for(TransaksiSetahunDto transaksiSetahunDto : transaksi){
            TransaksiSetahunDuaDto transaksiSetahunDuaDto = new TransaksiSetahunDuaDto();
            transaksiSetahunDuaDto.setTotal(transaksiSetahunDto.getTotal());
            transaksiSetahunDuaDto.setBulan(transaksiSetahunDto.getBulan());
            transaksiSetahunDuaDtos.add(transaksiSetahunDuaDto);
        }

        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(transaksiSetahunDuaDtos, httpHeaders, HttpStatus.OK);
    }


//    @GetMapping("/api/detail/anggaran")
//    @ResponseBody
//    public List<AnggaranDetail> cariDetailAnggaran(@RequestParam(required = false) String idAnggaran){
//
//        List<AnggaranDetail> anggaranDetails = anggaranDetailDao.findByStatusAndAnggaranOrderByDeskripsi(StatusRecord.AKTIF, anggaranDao.findById(idAnggaran).get());
//
//        return anggaranDetails;
//
//    }
}
