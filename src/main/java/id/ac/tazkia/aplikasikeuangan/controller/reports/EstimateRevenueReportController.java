package id.ac.tazkia.aplikasikeuangan.controller.reports;

import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.dto.report.EstimasiPengeluaranDto;
import id.ac.tazkia.aplikasikeuangan.dto.report.LaporanEstimasiPenerimaanDto;
import id.ac.tazkia.aplikasikeuangan.export.ExportEstimasiRevenue;
import id.ac.tazkia.aplikasikeuangan.export.ExportEstimasiTransaksi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class EstimateRevenueReportController {

    @Autowired
    private PenerimaanDao penerimaanDao;

    @GetMapping("/reports/estimasi_revenue")
    public String estimasiPengeluaran(Model model,
                                      @RequestParam(required = false) String tanggalMulai,
                                      @RequestParam(required = false) String tanggalSelesai){


        System.out.println("tanggalMulai : " + tanggalMulai);
        System.out.println("tanggalSelesai : " + tanggalSelesai);

        if (StringUtils.hasText(tanggalMulai) && StringUtils.hasText(tanggalSelesai)){

            String tahun = tanggalMulai.substring(6, 10);
            String bulan = tanggalMulai.substring(0, 2);
            String tanggal = tanggalMulai.substring(3, 5);
            String tanggalan = tahun + '-' + bulan + '-' + tanggal;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalan, formatter);

            System.out.println("tanggal_mulai : " + tanggalan);

            String tahun1 = tanggalSelesai.substring(6, 10);
            String bulan1 = tanggalSelesai.substring(0, 2);
            String tanggal1 = tanggalSelesai.substring(3, 5);
            String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);

            System.out.println("tanggal_selesai : " + tanggalan1);

            model.addAttribute("tanggalMulai", tanggalMulai);
            model.addAttribute("tanggalSelesai", tanggalSelesai);
            model.addAttribute("listEstimasiPenerimaan", penerimaanDao.laporanPenerimaanEstimasi(localDate, localDate1));
            model.addAttribute("totalEstimasiPenerimaan", penerimaanDao.totalPenerimaanEstimasi(localDate, localDate1));

        }

        return "reports/estimate_revenue/list";

    }

    @PostMapping("/reports/estimasi_revenue/export")
    public String estimasiPenerimaanExport(Model model,
                                            @RequestParam(required = false) String tanggalMulaiExport,
                                            @RequestParam(required = false) String tanggalSelesaiExport,
                                            HttpServletResponse response) throws IOException {


        if (StringUtils.hasText(tanggalMulaiExport) && StringUtils.hasText(tanggalSelesaiExport)) {

            String tahun = tanggalMulaiExport.substring(6, 10);
            String bulan = tanggalMulaiExport.substring(0, 2);
            String tanggal = tanggalMulaiExport.substring(3, 5);
            String tanggalan = tahun + '-' + bulan + '-' + tanggal;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalan, formatter);

            System.out.println("tanggal_mulai : " + tanggalan);

            String tahun1 = tanggalSelesaiExport.substring(6, 10);
            String bulan1 = tanggalSelesaiExport.substring(0, 2);
            String tanggal1 = tanggalSelesaiExport.substring(3, 5);
            String tanggalan1 = tahun1 + '-' + bulan1 + '-' + tanggal1;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate1 = LocalDate.parse(tanggalan1, formatter1);

            System.out.println("tanggal_selesai : " + tanggalan1);
            response.setContentType("application/octet-stream");
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=Estimated_revenue_from_" + tanggalMulaiExport + "_to_" + tanggalSelesaiExport + ".xlsx";
            response.setHeader(headerKey, headerValue);

            List<LaporanEstimasiPenerimaanDto> estimasiPenerimaanDtos = penerimaanDao.laporanPenerimaanEstimasi(localDate, localDate1);
            BigDecimal totalEstimasi = penerimaanDao.totalPenerimaanEstimasi(localDate, localDate1);
            ExportEstimasiRevenue excelExporter = new ExportEstimasiRevenue(estimasiPenerimaanDtos, totalEstimasi);
            excelExporter.export(response);

        }

        return "redirect:../estimasi_revenue";}

}
