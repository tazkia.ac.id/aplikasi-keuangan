package id.ac.tazkia.aplikasikeuangan.controller;


import id.ac.tazkia.aplikasikeuangan.dto.akunting.JournalTemplateDetailDto;
import id.ac.tazkia.aplikasikeuangan.services.JournalTemplateService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class JournalTemplateController {

    private final JournalTemplateService journalTemplateService;

    public JournalTemplateController(JournalTemplateService journalTemplateService) {
        this.journalTemplateService = journalTemplateService;
    }

    @GetMapping("/public/api/journal-template/detail/{id}")
    public ResponseEntity<List<JournalTemplateDetailDto>> getJournalTemplateDetails(@PathVariable("id") String templateId) {
        List<JournalTemplateDetailDto> journalTemplateDetails = journalTemplateService.getDetailsByTemplateId(templateId);
        return ResponseEntity.ok(journalTemplateDetails);
    }

}
