package id.ac.tazkia.aplikasikeuangan.controller.keuangan;


import id.ac.tazkia.aplikasikeuangan.controller.transaksi.PengajuanDanaController;
import id.ac.tazkia.aplikasikeuangan.dao.config.UserDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.InstansiDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPendapatanTerkaitDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPendapatanTerkaitDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PendapatanTerkaitDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Controller
public class RealisasiPendapatanTerkaitController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PengajuanDanaController.class);

    @Autowired
    private EstimasiPendapatanTerkaitDao estimasiPendapatanTerkaitDao;

    @Autowired
    private EstimasiPendapatanTerkaitDetailDao estimasiPendapatanTerkaitDetailDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private KaryawanDao karyawanDao;
    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;
    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private SatuanDao satuanDao;

    @Autowired
    private PendapatanTerkaitDao pendapatanTerkaitDao;

    @Autowired
    @Value("${upload.pendapatanTerkait}")
    private String uploadFolder;

    @GetMapping("/transaksi/pendapatan_terkait")
    public String transaksiPendapatanTerkait(Model model,
                                             @RequestParam(name = "search", required = false) String search,
                                             @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                             @PageableDefault(size = 10, sort = "keterangan") Pageable pageable,
                                             Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> yourIdInstansiList = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        Page<EstimasiPendapatanTerkait> estimasiList;

        if(periodeAnggaran != null){
            if (search != null && !search.isEmpty()) {
                estimasiList = estimasiPendapatanTerkaitDao.findByStatusAndPeriodeAnggaranAndInstansiIdInAndKeteranganContainingIgnoreCaseOrderByKeterangan(
                        StatusRecord.AKTIF,periodeAnggaran, yourIdInstansiList, search, pageable
                );
                model.addAttribute("search", search);
            } else {
                estimasiList = estimasiPendapatanTerkaitDao.findByStatusAndPeriodeAnggaranAndInstansiIdInOrderByKeterangan(
                        StatusRecord.AKTIF, periodeAnggaran, yourIdInstansiList, pageable
                );
            }
            model.addAttribute("periodeAnggaranSelected", periodeAnggaran);
        }else{
            String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
            PeriodeAnggaran periodeAnggaranSelected = periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran1);
            if (search != null && !search.isEmpty()) {
                estimasiList = estimasiPendapatanTerkaitDao.findByStatusAndPeriodeAnggaranAndInstansiIdInAndKeteranganContainingIgnoreCaseOrderByKeterangan(
                        StatusRecord.AKTIF,periodeAnggaranSelected,  yourIdInstansiList, search, pageable
                );
                model.addAttribute("search", search);
            } else {
                estimasiList = estimasiPendapatanTerkaitDao.findByStatusAndPeriodeAnggaranAndInstansiIdInOrderByKeterangan(
                        StatusRecord.AKTIF,periodeAnggaranSelected, yourIdInstansiList, pageable
                );
            }
            model.addAttribute("periodeAnggaranSelected", periodeAnggaranSelected);
        }


        model.addAttribute("estimasiList", estimasiList);
        model.addAttribute("periodeAnggaranList", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));


        model.addAttribute("finance","active");
        model.addAttribute("pendapatan_terkait","active");
        return "keuangan/pendapatan_terkait/realisasi/list";
    }


    @GetMapping("/transaksi/pendapatan_terkait/add")
    public String pendapatanTerkaitList(Model model,
                                        @RequestParam(required = true) EstimasiPendapatanTerkait id,
                                        @PageableDefault(size = 10) Pageable pageable){

        model.addAttribute("listPendapatanTerkait", pendapatanTerkaitDao.findByStatusAndEstimasiPendapatanTerkaitOrderByTanggalDesc(StatusRecord.AKTIF, id, pageable));
        model.addAttribute("estimasiPendapatanTerkait", id);
        model.addAttribute("finance","active");
        model.addAttribute("pendapatan_terkait","active");
        return "keuangan/pendapatan_terkait/realisasi/list_add";
    }

    @GetMapping("/transaksi/pendapatan_terkait/new")
    public String pendapatanTerkaitNew(Model model,
                                        @RequestParam(required = true) EstimasiPendapatanTerkait estimasiPendapatanTerkait){

        model.addAttribute("pendapatanTerkait", new PendapatanTerkait());
        model.addAttribute("estimasiPendapatanTerkait", estimasiPendapatanTerkait);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("finance","active");
        model.addAttribute("pendapatan_terkait","active");
        return "keuangan/pendapatan_terkait/realisasi/form";
    }


    @GetMapping("/transaksi/pendapatan_terkait/edit")
    public String pendapatanTerkaitEdit(Model model,
                                        @RequestParam(required = true) PendapatanTerkait id){

        model.addAttribute("pendapatanTerkait", id);
        model.addAttribute("estimasiPendapatanTerkait", id.getEstimasiPendapatanTerkait());
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("finance","active");
        model.addAttribute("pendapatan_terkait","active");
        return "keuangan/pendapatan_terkait/realisasi/form";
    }

    @PostMapping("/transaksi/pendapatan_terkait/save")
    public String pendapatanTerkaitSave(@ModelAttribute @Valid PendapatanTerkait pendapatanTerkait,
                                        @RequestParam EstimasiPendapatanTerkait estimasiPendapatanTerkait,
                                        @RequestParam("lampiran") MultipartFile file,
                                        Authentication authentication,
                                        RedirectAttributes attribute) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        String date = pendapatanTerkait.getTanggalString();
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        pendapatanTerkait.setEstimasiPendapatanTerkait(estimasiPendapatanTerkait);
        pendapatanTerkait.setSatuan(estimasiPendapatanTerkait.getSatuan());
        pendapatanTerkait.setTotal(pendapatanTerkait.getJumlah().multiply(pendapatanTerkait.getNominalSatuan()));
        pendapatanTerkait.setStatus(StatusRecord.AKTIF);
        pendapatanTerkait.setTanggalInsert(LocalDateTime.now());
        pendapatanTerkait.setUserInsert(user.getUsername());
        pendapatanTerkait.setKaryawan(karyawan);
        pendapatanTerkait.setTanggal(localDate);

        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
//
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
//
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
//
        String idFile = UUID.randomUUID().toString();
        if (ukuran == 0){
            pendapatanTerkait.setFile("default.jpg");
        }else{
            pendapatanTerkait.setFile(idFile + "." + extension);
        }
        System.out.println("file :"+ namaAsli);
        System.out.println("Ukuran :"+ ukuran);

        LOGGER.debug("Lokasi upload : {}", uploadFolder);
        new File(uploadFolder).mkdirs();
        File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);


        LOGGER.debug("Pengajuan dana sebelum save : {}", pendapatanTerkait);
        pendapatanTerkaitDao.save(pendapatanTerkait);

        attribute.addFlashAttribute("success", "Data berhasil disimpan");
        return "redirect:../pendapatan_terkait/add?id="+ estimasiPendapatanTerkait.getId();
    }


    @GetMapping("/pendapatan_terkait/{pendapatanTerkait}/attachement/")
    public ResponseEntity<byte[]> tampilkanBuktiPembayaran(@PathVariable PendapatanTerkait pendapatanTerkait) throws Exception {
        String lokasiFile = uploadFolder + File.separator + pendapatanTerkait.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pendapatanTerkait.getFile().toLowerCase().endsWith("jpeg") || pendapatanTerkait.getFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pendapatanTerkait.getFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pendapatanTerkait.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
