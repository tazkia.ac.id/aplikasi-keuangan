package id.ac.tazkia.aplikasikeuangan.controller.reports;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.InstansiDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailEstimasiDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dto.report.EstimasiPengeluaranDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.export.ExportEstimasiTransaksi;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class EstimasiPengeluaranController {

    @Autowired
    private AnggaranDetailEstimasiDao anggaranDetailEstimasiDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @GetMapping("/reports/estimasi")
    public String estimasiPengeluaran(Model model,
                                      @RequestParam(required = false) Instansi instansi,
                                      @RequestParam(required = false) String tanggalMulai,
                                      @RequestParam(required = false) Departemen departemen,
                                      @RequestParam(required = false) String tanggalSelesai,
                                      Authentication authentication){

        System.out.println("tanggalMulai : " + tanggalMulai);
        System.out.println("tanggalSelesai : " + tanggalSelesai);

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now().plusDays(1));

        model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
//        Model listDepartemen = model.addAttribute("listDepartemen", departemenDao.findByStatusAndStatusAktifAndInstansi(StatusRecord.AKTIF, StatusRecord.AKTIF, karyawan.));
        model.addAttribute("listDepartemen", departemenDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, StatusRecord.AKTIF));


        if (StringUtils.hasText(tanggalMulai) && StringUtils.hasText(tanggalSelesai) && instansi != null){
            if(departemen == null) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);
                System.out.println("tanggal_mulai : " + localDate);
                DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate localDate1 = LocalDate.parse(tanggalSelesai, formatter1);
                System.out.println("tanggal_selesai : " + localDate1);
                model.addAttribute("instansiSelected", instansi);
                model.addAttribute("tanggalMulai", localDate);
                model.addAttribute("tanggalSelesai", localDate1);
                model.addAttribute("listEstimasiPengeluaran", anggaranDetailEstimasiDao.listEstimasiPengeluaranInstansi(localDate, localDate1, instansi.getId(),periodeAnggaran1));
                model.addAttribute("totalEstimasiPengeluaran", anggaranDetailEstimasiDao.totalEstimasiPengeluaranInstansi(localDate, localDate1, instansi.getId(),periodeAnggaran1));
            }else{
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate localDate = LocalDate.parse(tanggalMulai, formatter);
                System.out.println("tanggal_mulai : " + localDate);
                DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate localDate1 = LocalDate.parse(tanggalSelesai, formatter1);
                System.out.println("tanggal_selesai : " + localDate1);
                model.addAttribute("instansiSelected", instansi);
                model.addAttribute("departemenSelected", departemen);
                model.addAttribute("tanggalMulai", localDate);
                model.addAttribute("tanggalSelesai", localDate1);
                model.addAttribute("listEstimasiPengeluaran", anggaranDetailEstimasiDao.listEstimasiPengeluaranInstansiDepartemen(localDate, localDate1, instansi.getId(), departemen.getId(), periodeAnggaran1));
                model.addAttribute("totalEstimasiPengeluaran", anggaranDetailEstimasiDao.totalEstimasiPengeluaranInstansiDepartemen(localDate, localDate1, instansi.getId(), departemen.getId(), periodeAnggaran1));
            }
        }else{
            model.addAttribute("tanggalMulai", LocalDate.now());
            model.addAttribute("tanggalSelesai", LocalDate.now());
        }

        model.addAttribute("reports", "active");
        model.addAttribute("report_estimasi_transaksi", "active");

        return "reports/estimasi_pengeluaran/list2";
    }


    @PostMapping("/reports/estimasi_keluar_excel/export")
    public String estimasiPengeluaranExport(Model model,
                                            @RequestParam(required = false) String tanggalMulaiExport,
                                            @RequestParam(required = false) String tanggalSelesaiExport,
                                            @RequestParam(required = true) Instansi instansi,
                                            @RequestParam(required = false) Departemen departemen,
                                            HttpServletResponse response) throws IOException{

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now().plusDays(1));
        if (StringUtils.hasText(tanggalMulaiExport) && StringUtils.hasText(tanggalSelesaiExport) && instansi != null){

            System.out.println("tanggal mulai 1 : " + tanggalMulaiExport);
            System.out.println("tanggal selesai 1 : " + tanggalSelesaiExport);
            System.out.println("instansi 1 : "+ instansi.getNama());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(tanggalMulaiExport, formatter);

//            System.out.println("tanggal_mulai : " + localDate);

            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate1 = LocalDate.parse(tanggalSelesaiExport, formatter1);


            response.setContentType("application/octet-stream");
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=Estimated_transaction_from_" + tanggalMulaiExport + "_to_" + tanggalSelesaiExport + ".xlsx";
            response.setHeader(headerKey, headerValue);




//                System.out.println("tanggal_selesai : " + localDate1);

                model.addAttribute("instansiSelected", instansi);
            model.addAttribute("departemenSelected", departemen);
                model.addAttribute("tanggalMulai", localDate);
                model.addAttribute("tanggalSelesai", localDate1);
                List<EstimasiPengeluaranDto> estimasiPengeluaranDtos = anggaranDetailEstimasiDao.listEstimasiPengeluaranInstansi(localDate, localDate1, instansi.getId(), periodeAnggaran1);
                BigDecimal totalEstimasi = anggaranDetailEstimasiDao.totalEstimasiPengeluaranInstansi(localDate, localDate1, instansi.getId(), periodeAnggaran1);

                if(departemen != null){
                    estimasiPengeluaranDtos = anggaranDetailEstimasiDao.listEstimasiPengeluaranInstansiDepartemen(localDate, localDate1, instansi.getId(), departemen.getId(), periodeAnggaran1);
                    totalEstimasi = anggaranDetailEstimasiDao.totalEstimasiPengeluaranInstansiDepartemen(localDate, localDate1, instansi.getId(), departemen.getId(),periodeAnggaran1);

                }

                ExportEstimasiTransaksi excelExporter = new ExportEstimasiTransaksi(estimasiPengeluaranDtos, totalEstimasi);
                excelExporter.export(response);

        }

        return "redirect:../estimasi";}

}
