package id.ac.tazkia.aplikasikeuangan.controller.reports;

import id.ac.tazkia.aplikasikeuangan.dao.setting.*;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PenerimaanRealDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.time.LocalDate;

@Controller
public class ReportDashboardController {

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @Autowired
    private PenerimaanCrudDao penerimaanCrudDao;

    @Autowired
    private PenerimaanDao penerimaanDao;

    @Autowired
    private PenerimaanRealDao penerimaanRealDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PaguAnggaranDao paguAnggaranDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @GetMapping("/reports/dashboard")
    public String reportDashboard(Model model,
                                  @RequestParam(required = false)PeriodeAnggaran periodeAnggaran,
                                  Authentication authentication){

        if (periodeAnggaran == null){

            String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now().plusDays(1));

            BigDecimal totalBudget =  anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaran1);

            BigDecimal totalPengajuanDanaAktif = pengajuanDanaDao.getTotalPengajuanDana2(periodeAnggaran1);

            if (totalBudget == null){
                totalBudget = BigDecimal.ZERO;
            }

            if (totalPengajuanDanaAktif == null){
                totalPengajuanDanaAktif = BigDecimal.ZERO;
            }

            BigDecimal totalBudgetBalance = totalBudget.subtract(totalPengajuanDanaAktif);

            model.addAttribute("periodeAnggaran", periodeAnggaranDao.findById(periodeAnggaran1).get());

//            model.addAttribute("listSisaAnggaran" , anggaranCrudDao.getSisaAnggaranPerDepartemen(periodeAnggaran1));

            model.addAttribute("totalAnggaranPeriode", totalBudget);

            model.addAttribute("totalPenerimaanPeriode", penerimaanDao.getTotalEstimasiPenerimaan(periodeAnggaran1));

            model.addAttribute("totalPenerimaanReale", penerimaanRealDao.getTotalRealisasiPenerimaan(periodeAnggaran1));

            model.addAttribute("totalPengajuanDana", pengajuanDanaDao.getTotalPengajuanDana2(periodeAnggaran1));

            model.addAttribute("totalBalance",totalBudgetBalance);

            model.addAttribute("totalPaguAnggaran", paguAnggaranDao.getTotalPaguAnggaran(periodeAnggaran1));

            model.addAttribute("priodeAnggaranSelected", periodeAnggaranDao.findById(periodeAnggaran1).get());

            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));

        }else{

            BigDecimal totalBudget =  anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaran.getId());

            BigDecimal totalPengajuanDanaAktif = pengajuanDanaDao.getTotalPengajuanDana2(periodeAnggaran.getId());

            if (totalBudget == null){
                totalBudget = BigDecimal.ZERO;
            }

            if (totalPengajuanDanaAktif == null){
                totalPengajuanDanaAktif = BigDecimal.ZERO;
            }

            BigDecimal totalBudgetBalance = totalBudget.subtract(totalPengajuanDanaAktif);

            model.addAttribute("periodeAnggaran", periodeAnggaranDao.findById(periodeAnggaran.getId()).get());

//            model.addAttribute("listSisaAnggaran" , anggaranCrudDao.getSisaAnggaranPerDepartemen(periodeAnggaran.getId()));

            model.addAttribute("totalAnggaranPeriode", totalBudget);

            model.addAttribute("totalPenerimaanPeriode", penerimaanDao.getTotalEstimasiPenerimaan(periodeAnggaran.getId()));

            model.addAttribute("totalPenerimaanReale", penerimaanRealDao.getTotalRealisasiPenerimaan(periodeAnggaran.getId()));

            model.addAttribute("totalPengajuanDana", pengajuanDanaDao.getTotalPengajuanDana2(periodeAnggaran.getId()));

            model.addAttribute("totalBalance",totalBudgetBalance);

            model.addAttribute("totalPaguAnggaran", paguAnggaranDao.getTotalPaguAnggaran(periodeAnggaran.getId()));

            model.addAttribute("priodeAnggaranSelected", periodeAnggaran);

            model.addAttribute("listPeriodeAnggaran", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));

        }




        return "reports/dashboard/list";
    }

}
