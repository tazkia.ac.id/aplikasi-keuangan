package id.ac.tazkia.aplikasikeuangan.controller.setting;

import id.ac.tazkia.aplikasikeuangan.dao.config.RoleDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PicAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PicAnggaran;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Controller
public class PicAnggaranController {

    @Autowired
    private PicAnggaranDao picAnggaranDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/setting/picanggaran")
    public String daftarPicAnggaran(Model model,
                                    @PageableDefault(size = 10) Pageable page,
                                    @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                    Authentication authentication,
                                    String search) {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> idInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("selectedPeriode", periodeAnggaran);
        model.addAttribute("periode", periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));

        if(roleDao.findById("superuser").get() == user.getRole()) {
            if (periodeAnggaran != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranAndKaryawanNamaKaryawanContainingOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaran, search, page));
                } else {
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaran, page));
                }
            } else {
                String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndKaryawanNamaKaryawanContainingOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaranAktif, search, page));
                } else {
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaranAktif, page));
                }
            }
        }else{
            if (periodeAnggaran != null) {
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranAndKaryawanNamaKaryawanContainingOrderByDepartemen(StatusRecord.AKTIF, idInstansi, periodeAnggaran, search, page));
                } else {
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranOrderByDepartemen(StatusRecord.AKTIF, idInstansi, periodeAnggaran, page));
                }
            } else {
                String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdAndKaryawanNamaKaryawanContainingOrderByDepartemen(StatusRecord.AKTIF, idInstansi, periodeAnggaranAktif, search, page));
                } else {
                    model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord.AKTIF, idInstansi, periodeAnggaranAktif, page));
                }
            }
        }

        model.addAttribute("setting", "active");
        model.addAttribute("picss", "active");
        return "setting/picanggaran/list";
    }

    @GetMapping("/setting/picanggaran/edit")
    public String editPicAnggaran(Model model, @RequestParam(required = false)String id){

        PicAnggaran picAnggaran = picAnggaranDao.findById(id).get();

        if (picAnggaran != null) {

            model.addAttribute("picAnggaran", picAnggaran);
            model.addAttribute("karyawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF));
            model.addAttribute("departemen",departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));
        }

        model.addAttribute("setting", "active");
        model.addAttribute("picss", "active");
        return "setting/picanggaran/form";
    }

    @PostMapping("/setting/picanggaran/save")
    public String savePicAnggaran(Model model, @ModelAttribute @Valid PicAnggaran picAnggaran, BindingResult errors) {


        if (errors.hasErrors()) {

            model.addAttribute("selectedKaryawan", karyawanDao.findById(picAnggaran.getKaryawan().getId()));
            model.addAttribute("selectedDepartemen", departemenDao.findById(picAnggaran.getDepartemen().getId()));

            model.addAttribute("karyawan", karyawanDao.findByStatusOrderByNamaKaryawanAsc(StatusRecord.AKTIF));
            model.addAttribute("departemen",departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));

            model.addAttribute("setting", "active");
            model.addAttribute("picss", "active");
            return "transaksi/penerimaan/form";

        }



        picAnggaranDao.save(picAnggaran);
        return "redirect:../picanggaran?periodeAnggaran="+picAnggaran.getPeriodeAnggaran().getId();

    }

}
