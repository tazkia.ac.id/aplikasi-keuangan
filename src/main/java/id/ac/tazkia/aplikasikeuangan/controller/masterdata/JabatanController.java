package id.ac.tazkia.aplikasikeuangan.controller.masterdata;

import id.ac.tazkia.aplikasikeuangan.dao.config.RoleDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.JabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.SettingApprovalPengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Jabatan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.SettingApprovalPengajuanDana;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

import java.time.LocalDate;
import java.util.List;

@Controller
public class JabatanController {

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private SettingApprovalPengajuanDanaDao settingApprovalPengajuanDanaDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/masterdata/jabatan")
    private String listJabatan(Model model, @PageableDefault(size = 10) Pageable page, Authentication authentication, String search){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> idInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        if(roleDao.findById("superuser").get() == user.getRole()) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listJabatan", jabatanDao.pageDataJabatanWithSearch(search, page));
            } else {
                model.addAttribute("listJabatan", jabatanDao.pageDataJabatanAll(page));
            }
        }else{
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listJabatan", jabatanDao.pageDataJabatanInstansiWithSearch(search, idInstansi, page));
            } else {
                model.addAttribute("listJabatan", jabatanDao.pageDataJabatanInstansiAll(idInstansi, page));
            }
        }

        model.addAttribute("setting", "active");
        model.addAttribute("positionss", "active");
        return "masterdata/jabatan/list";
    }

    @GetMapping("/masterdata/jabatan/detail")
    private String detailJabatan(Model model, @RequestParam(required = false)String detail){

        Jabatan jabatan = jabatanDao.findById(detail).get();
        if (jabatan != null){
            model.addAttribute("jabatan", jabatan);
            model.addAttribute("listSettingApprovalPengajuanDana", settingApprovalPengajuanDanaDao.findByStatusAndJabatanOrderByNomorUrut(StatusRecord.AKTIF, jabatan));
        }

        model.addAttribute("setting", "active");
        model.addAttribute("positionss", "active");
        return "masterdata/jabatan/detail";

    }


    @GetMapping("/masterdata/jabatan/baru")
    public String baruJabatan(Model model) {

        model.addAttribute("listDepartemen", departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));
        model.addAttribute("jabatan", new Jabatan());

        model.addAttribute("setting", "active");
        model.addAttribute("positionss", "active");
        return "masterdata/jabatan/form";

    }

    @GetMapping("/masterdata/jabatan/edit")
    public String editJabatan(Model model, @RequestParam(required = false)String id) {

        Jabatan jabatan = jabatanDao.findById(id).get();

        if (jabatan != null){

            model.addAttribute("jabatan", jabatan);
            model.addAttribute("listDepartemen", departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));

        }

        model.addAttribute("setting", "active");
        model.addAttribute("positionss", "active");
        return "masterdata/jabatan/form";

    }

    @PostMapping("/masterdata/jabatan/save")
    public String saveJabatan(@ModelAttribute @Valid Jabatan jabatan, BindingResult errors, RedirectAttributes attributes, Model model){

        if(errors.hasErrors()){
            model.addAttribute("listDepartemen", departemenDao.findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));
            model.addAttribute("setting", "active");
            model.addAttribute("positionss", "active");
            return "/masterdata/jabatan/form";
        }

        if (jabatan.getStatus()==null){
            jabatan.setStatus(StatusRecord.HAPUS);
        }
        if (jabatan.getStatusAktif() == null){
            jabatan.setStatusAktif(StatusRecord.NONAKTIF);
        }

        jabatanDao.save(jabatan);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../jabatan";
    }

    @PostMapping("/masterdata/jabatan/hapus")
    public String hapusJabatan(@RequestParam(required = false)Jabatan jabatan,
                               RedirectAttributes attributes){

        Integer jumlahDiaprove = settingApprovalPengajuanDanaDao.countByStatusAndJabatan(StatusRecord.AKTIF, jabatan);
        Integer jumlahApprove = settingApprovalPengajuanDanaDao.countByStatusAndJabatanApprove(StatusRecord.AKTIF, jabatan);

        Integer karyawanJabatan = karyawanJabatanDao.countByStatusAndJabatan(StatusRecord.AKTIF, jabatan);

        if(jumlahApprove > 0 || jumlahDiaprove > 0){

            attributes.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:../jabatan";

        }

        if(karyawanJabatan > 0){

            attributes.addFlashAttribute("gagalkaryawan", "Save Data Gagal");
            return "redirect:../jabatan";

        }

        return "redirect:../jabatan";
    }

}
