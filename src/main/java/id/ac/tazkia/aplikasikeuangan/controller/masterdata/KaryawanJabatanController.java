package id.ac.tazkia.aplikasikeuangan.controller.masterdata;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.JabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.KaryawanJabatan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class KaryawanJabatanController {

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private JabatanDao jabatanDao;

    @GetMapping("/masterdata/karyawan/jabatan/baru")
    private String baruKaryawanJabatan(Model model,
                                       @RequestParam(required = false)String detail){

        Karyawan karyawan = karyawanDao.findById(detail).get();

        model.addAttribute("karyawan", karyawan);
        model.addAttribute("listJabatan", jabatanDao.findByStatusAndStatusAktifOrderByKodeJabatan(StatusRecord.AKTIF, StatusRecord.AKTIF));
        model.addAttribute("karyawanJabatan", new KaryawanJabatan());

        return "masterdata/karyawan/jabatan/form";
    }

    @GetMapping("/masterdata/karyawan/jabatan/edit")
    private String editKaryawanJabatan(Model model,
                                       @RequestParam(required = false)String detail,
                                       @RequestParam(required = false)String id){

        Karyawan karyawan = karyawanDao.findById(detail).get();
        KaryawanJabatan karyawanJabatan = karyawanJabatanDao.findById(id).get();

        if (karyawanJabatan != null){

            model.addAttribute("karyawan", karyawan);
            model.addAttribute("karyawanJabatan", karyawanJabatan);
            model.addAttribute("listJabatan", jabatanDao.findByStatusAndStatusAktifOrderByKodeJabatan(StatusRecord.AKTIF,StatusRecord.AKTIF));

        }

        return "masterdata/karyawan/jabatan/form";

    }

    @PostMapping("/masterdata/karyawan/jabatan/save")
    private String saveKaryawanJabatan(Model model,
                                       @RequestParam(required = false)String detail,
                                       @ModelAttribute @Valid KaryawanJabatan karyawanJabatan,
                                       BindingResult errors,
                                       RedirectAttributes attributes){

        Karyawan karyawan = karyawanDao.findById(detail).get();


        String date =  karyawanJabatan.getTanggalBerlaku();
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;

//        String date = karyawanJabatan.getTanggalBerlaku();
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        karyawanJabatan.setMulaiBerlaku(localDate.plusDays(1));

        if(errors.hasErrors()){
            model.addAttribute("karyawan", karyawan);
            return "/masterdata/karyawan/jabatan/form";
        }

        if (karyawanJabatan.getStatus()==null){
            karyawanJabatan.setStatus(StatusRecord.HAPUS);
        }
        if (karyawanJabatan.getStatusAktif() == null){
            karyawanJabatan.setStatusAktif(StatusRecord.NONAKTIF);
        }

        karyawanJabatan.setKaryawan(karyawan);
        karyawanJabatanDao.save(karyawanJabatan);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../detail?detail="+ karyawan.getId();

    }

    @PostMapping("/masterdata/karyawan/jabatan/hapus")
    private String hapusKaryawanJabatan(Model model,
                                        @RequestParam(required = false)String detail,
                                        @RequestParam KaryawanJabatan karyawanJabatan){

        String karyawan = detail;
        karyawanJabatan.setStatus(StatusRecord.HAPUS);
        karyawanJabatanDao.save(karyawanJabatan);

        return "redirect:../detail?detail="+ karyawan;

    }

}
