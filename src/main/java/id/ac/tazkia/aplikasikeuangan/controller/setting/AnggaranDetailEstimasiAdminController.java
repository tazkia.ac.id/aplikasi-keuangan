package id.ac.tazkia.aplikasikeuangan.controller.setting;


import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailEstimasiDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PicAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetailEstimasi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class AnggaranDetailEstimasiAdminController {

    @Autowired
    private PicAnggaranDao picAnggaranDao;

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private AnggaranDetailEstimasiDao anggaranDetailEstimasiDao;

    @Autowired
    private SatuanDao satuanDao;

    @GetMapping("/setting/admin/anggarandetail/estimasi")
    public String anggaranDetailEstimasi(Model model,
                                         @RequestParam(required = false) AnggaranDetail anggaranDetail){

        model.addAttribute("anggaranDetail", anggaranDetail);
        model.addAttribute("anggaranDetailEstimasiList", anggaranDetailEstimasiDao.findByStatusAndAnggaranDetailOrderByTanggalEstimasi(StatusRecord.AKTIF, anggaranDetail));

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return "setting/anggaran/admin/estimasi/list";

    }

    @GetMapping("/setting/admin/anggarandetail/estimasi/baru")
    public String anggaranDetailEstimasiBaru(Model model,
                                             @RequestParam(required = false)AnggaranDetail anggaranDetail){

        model.addAttribute("anggaranDetail", anggaranDetail);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("anggaranDetailEstimasi", new AnggaranDetailEstimasi());

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return "setting/anggaran/admin/estimasi/form";

    }

    @GetMapping("/setting/admin/anggarandetail/estimasi/edit")
    public String anggaranDetailEstimasiEdit(Model model,
                                             @RequestParam(required = false) AnggaranDetailEstimasi anggaranDetailEstimasi,
                                             @RequestParam(required = false)AnggaranDetail anggaranDetail){


        model.addAttribute("anggaranDetail", anggaranDetail);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("anggaranDetailEstimasi", anggaranDetailEstimasi);

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return "setting/anggaran/admin/estimasi/edit";

    }

    @PostMapping("/setting/admin/anggarandetail/estimasi/save")
    public String anggaranDetailEstimasiSave(Model model,
                                             @RequestParam(required = false) AnggaranDetail anggaranDetail,
                                             @ModelAttribute @Valid AnggaranDetailEstimasi anggaranDetailEstimasi,
                                             BindingResult errors,
                                             RedirectAttributes attributes){

        String date = anggaranDetailEstimasi.getTanggalEstimasiString();
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;

//        String tanggalan = tanggalActive;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

//        String date = anggaranDetailEstimasi.getTanggalEstimasiString();
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        anggaranDetailEstimasi.setTanggalEstimasi(localDate);
        anggaranDetailEstimasi.setAnggaranDetail(anggaranDetail);
        anggaranDetailEstimasiDao.save(anggaranDetailEstimasi);

        BigDecimal totalAmount = anggaranDetailEstimasiDao.getAmountAnggaranDetailEstimasi(anggaranDetail.getId());
        BigDecimal totalKuantitas = anggaranDetailEstimasiDao.getKuantitasAnggaranDetailEstiamasi(anggaranDetail.getId());

        if(totalKuantitas.longValue() > anggaranDetail.getKuantitas().longValue() ){
            anggaranDetail.setKuantitas(totalKuantitas);
        }
        if(anggaranDetailEstimasi.getAmount().longValue() > anggaranDetail.getAmount().longValue()){
            anggaranDetail.setAmount(anggaranDetailEstimasi.getAmount());
        }
        anggaranDetail.setSatuan(anggaranDetailEstimasi.getSatuan());
        anggaranDetailDao.save(anggaranDetail);

        BigDecimal totalAmountAnggaraDetail = anggaranDetailDao.getTotalAnggaranDetail(anggaranDetail.getAnggaran().getId());
        Anggaran anggaran = anggaranDao.findById(anggaranDetail.getAnggaran().getId()).get();
        anggaran.setAmount(totalAmountAnggaraDetail);
        anggaranDao.save(anggaran);

        attributes.addFlashAttribute("success", "Delete Data Berhasil");
        return "redirect:../estimasi?anggaranDetail="+anggaranDetail.getId();

    }


    @PostMapping("/setting/admin/anggarandetail/estimasi/update")
    public String anggaranDetailEstimasiUpdate(Model model,
                                               @RequestParam(required = false)AnggaranDetail anggaranDetail,
                                               @ModelAttribute @Valid AnggaranDetailEstimasi anggaranDetailEstimasi,
                                               BindingResult errors,
                                               RedirectAttributes attributes){

        String date = anggaranDetailEstimasi.getTanggalEstimasiString();
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;

//        String tanggalan = tanggalActive;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

//        String date = anggaranDetailEstimasi.getTanggalEstimasiString();
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        anggaranDetailEstimasi.setTanggalEstimasi(localDate);
        anggaranDetailEstimasi.setAnggaranDetail(anggaranDetail);
        anggaranDetailEstimasiDao.save(anggaranDetailEstimasi);

        BigDecimal totalAmount = anggaranDetailEstimasiDao.getAmountAnggaranDetailEstimasi(anggaranDetail.getId());
        BigDecimal totalKuantitas = anggaranDetailEstimasiDao.getKuantitasAnggaranDetailEstiamasi(anggaranDetail.getId());

        if(totalKuantitas.longValue() > anggaranDetail.getKuantitas().longValue() ){
            anggaranDetail.setKuantitas(totalKuantitas);
        }
        if(anggaranDetailEstimasi.getAmount().longValue() > anggaranDetail.getAmount().longValue()){
            anggaranDetail.setAmount(anggaranDetailEstimasi.getAmount());
        }
        anggaranDetail.setSatuan(anggaranDetailEstimasi.getSatuan());
        anggaranDetailDao.save(anggaranDetail);

        BigDecimal totalAmountAnggaraDetail = anggaranDetailDao.getTotalAnggaranDetail(anggaranDetail.getAnggaran().getId());
        Anggaran anggaran = anggaranDao.findById(anggaranDetail.getAnggaran().getId()).get();
        anggaran.setAmount(totalAmountAnggaraDetail);
        anggaranDao.save(anggaran);

        attributes.addFlashAttribute("success", "Delete Data Berhasil");
        return "redirect:../estimasi?anggaranDetail="+anggaranDetail.getId();

    }


    @PostMapping("/setting/admin/anggarandetail/estimasi/delete")
    public String anggaranDetailEstimasiDelete(Model model,
                                               @RequestParam(required = false)AnggaranDetail anggaranDetail,
                                               @RequestParam(required = false) AnggaranDetailEstimasi anggaranDetailEstimasi,
                                               RedirectAttributes attributes){

        String date = anggaranDetailEstimasi.getTanggalEstimasiString();
        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;

//        String tanggalan = tanggalActive;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

//        String date = anggaranDetailEstimasi.getTanggalEstimasiString();
//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
//        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        anggaranDetailEstimasi.setStatus(StatusRecord.HAPUS);
        anggaranDetailEstimasiDao.save(anggaranDetailEstimasi);

        BigDecimal totalAmount = anggaranDetailEstimasiDao.getAmountAnggaranDetailEstimasi(anggaranDetail.getId());
        BigDecimal totalKuantitas = anggaranDetailEstimasiDao.getKuantitasAnggaranDetailEstiamasi(anggaranDetail.getId());

//        anggaranDetail.setKuantitas(totalKuantitas);
//        anggaranDetail.setAmount(anggaranDetailEstimasi.getAmount());
//        anggaranDetail.setSatuan(anggaranDetailEstimasi.getSatuan());
//        anggaranDetailDao.save(anggaranDetail);

        BigDecimal totalAmountAnggaraDetail = anggaranDetailDao.getTotalAnggaranDetail(anggaranDetail.getAnggaran().getId());
        Anggaran anggaran = anggaranDao.findById(anggaranDetail.getAnggaran().getId()).get();
        anggaran.setAmount(totalAmountAnggaraDetail);
        anggaranDao.save(anggaran);

        attributes.addFlashAttribute("success", "Delete Data Berhasil");
        return "redirect:../estimasi?anggaranDetail="+anggaranDetail.getId();

    }

}
