package id.ac.tazkia.aplikasikeuangan.controller.setting;

import id.ac.tazkia.aplikasikeuangan.dao.config.RoleDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PaguAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PaguAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Controller
public class PaguAnggaranController {

    @Autowired
    private PaguAnggaranDao paguAnggaranDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;


    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/setting/paguanggaran")
    private String daftarPaguAnggaran(Model model, @PageableDefault(size = 10) Pageable page, String search,
                                      @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                      Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> idInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        if(roleDao.findById("superuser").get() == user.getRole()){
            if (periodeAnggaran != null) {
                model.addAttribute("selectedPeriode", periodeAnggaran);
                model.addAttribute("periode",periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));

                if (StringUtils.hasText(search)) {

                    model.addAttribute("search", search);
                    model.addAttribute("totalPagu", paguAnggaranDao.getTotalPaguAnggaran(periodeAnggaran.getId()));
                    model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndPeriodeAnggaranAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, periodeAnggaran, search, page));

                }else{

                    model.addAttribute("totalPagu", paguAnggaranDao.getTotalPaguAnggaran(periodeAnggaran.getId()));
                    model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndPeriodeAnggaranOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF,periodeAnggaran,page));

                }

            }else{

                String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
                if (periodeAnggaran1 != null){
                    PeriodeAnggaran periodeAnggaran2 = periodeAnggaranDao.findById(periodeAnggaran1).get();
                    model.addAttribute("selectedPeriode", periodeAnggaran2);
                }
                model.addAttribute("periode",periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));


                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("totalPagu", paguAnggaranDao.getTotalPaguAnggaran(periodeAnggaran1));
//                model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndPeriodeAnggaranStatusAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF, search, page));
                    model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, periodeAnggaran1, search, page));
                }else{
                    model.addAttribute("totalPagu", paguAnggaranDao.getTotalPaguAnggaran(periodeAnggaran1));
//                model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndPeriodeAnggaranStatusOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF, page));
                    model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndPeriodeAnggaranIdOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF,periodeAnggaran1, page));
                }


            }

        }else{
            if (periodeAnggaran != null) {
                model.addAttribute("selectedPeriode", periodeAnggaran);
                model.addAttribute("periode",periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));

                if (StringUtils.hasText(search)) {

                    model.addAttribute("search", search);
                    model.addAttribute("totalPagu", paguAnggaranDao.getTotalPaguAnggaranInstansi(periodeAnggaran.getId(), idInstansi));
                    model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, idInstansi, periodeAnggaran, search, page));

                }else{

                    model.addAttribute("totalPagu", paguAnggaranDao.getTotalPaguAnggaranInstansi(periodeAnggaran.getId(), idInstansi));
                    model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndDepartemenIdInAndPeriodeAnggaranOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, idInstansi,periodeAnggaran,page));

                }

            }else{

                String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
                if (periodeAnggaran1 != null){
                    PeriodeAnggaran periodeAnggaran2 = periodeAnggaranDao.findById(periodeAnggaran1).get();
                    model.addAttribute("selectedPeriode", periodeAnggaran2);
                }
                model.addAttribute("periode",periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));


                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("totalPagu", paguAnggaranDao.getTotalPaguAnggaranInstansi(periodeAnggaran1, idInstansi));
//                model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndPeriodeAnggaranStatusAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF, search, page));
                    model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, idInstansi, periodeAnggaran1, search, page));
                }else{
                    model.addAttribute("totalPagu", paguAnggaranDao.getTotalPaguAnggaranInstansi(periodeAnggaran1, idInstansi));
//                model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndPeriodeAnggaranStatusOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF, page));
                    model.addAttribute("listPaguAnggaran", paguAnggaranDao.findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdOrderByDepartemenNamaDepartemen(StatusRecord.AKTIF,idInstansi,periodeAnggaran1, page));
                }
            }
        }

        model.addAttribute("setting", "active");
        model.addAttribute("paguss", "active");
        return "setting/pagu/list";

    }

    @GetMapping("/setting/paguanggaran/edit")
    public String editPaguAnggaran(Model model, @RequestParam(required = false)String id){

        PaguAnggaran paguAnggaran = paguAnggaranDao.findById(id).get();
        if (paguAnggaran != null) {
            model.addAttribute("paguAnggaran", paguAnggaran);
        }

        model.addAttribute("setting", "active");
        model.addAttribute("paguss", "active");
        return "setting/pagu/form";

    }

    @PostMapping("/setting/paguanggaran/save")
    public String savePaguAnggaran(Model model,
                                   @ModelAttribute @Valid PaguAnggaran paguAnggaran,
                                   BindingResult errors,
                                   RedirectAttributes attributes,
                                   Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        PaguAnggaran paguAnggaran1 = paguAnggaranDao.findById(paguAnggaran.getId()).get();

        if(paguAnggaran.getAmbil().equals("NETT")){
            paguAnggaran1.setAmbil("NETT");
            paguAnggaran1.setPersentasePaguAnggaran(BigDecimal.ZERO);
        }else{
            paguAnggaran1.setAmbil("DEVIASI");
            paguAnggaran1.setPersentasePaguAnggaran(paguAnggaran.getPersentasePaguAnggaran());
        }
        paguAnggaran1.setKeterangan(paguAnggaran.getKeterangan());
        paguAnggaran1.setBlocking(paguAnggaran.getBlocking());
        paguAnggaran1.setPaguAnggaranAmount(paguAnggaran.getPaguAnggaranAmount());

        paguAnggaranDao.save(paguAnggaran1);

        return "redirect:../paguanggaran?periodeAnggaran="+ paguAnggaran1.getPeriodeAnggaran().getId();
    }

}
