package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaApproveDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDanaApprove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class PengajuanDanaDetailController {

    @Autowired
    private PengajuanDanaApproveDao pengajuanDanaApproveDao;

    @RequestMapping("/transaksi/pengajuandana/detailwaiting")
    public @ResponseBody
    String getReservationDetails(@RequestParam(required = false) String id, Model model) {


        model.addAttribute("listApproval", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaIdOrderByNomorUrut(StatusRecord.AKTIF, id));

        return "/detail/waiting :: pengajuanDanaDetailWaiting";

    }

    @RequestMapping(path = "/transaksi/pengajuandana/detailwaiting/{reservationId}", method = RequestMethod.GET)
    public @ResponseBody
    String getReservationDetails(@PathVariable("reservationId") String reservationId,
                                 Model model,
                                 Principal principal,
                                 HttpServletRequest request) {

        model.addAttribute("listApproval", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaIdOrderByNomorUrut(StatusRecord.AKTIF, reservationId));


        return "detail/waiting :: pengajuanDanaDetailWaiting";
    }

}