package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.InstansiDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaApproveDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.SaldoBankDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDanaApprove;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;

@Controller
public class ApprovalController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;



    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;


    @Autowired
    private PengajuanDanaApproveDao pengajuanDanaApproveDao;
    @Autowired
    private SaldoBankDao saldoBankDao;
    @Autowired
    private InstansiDao instansiDao;

    @GetMapping("/transaksi/approval/pengajuandana")
    private String homeApproval(Model model,
                                @PageableDefault(size = 10) Pageable page,
                                @RequestParam(required = false) String search,
                                @RequestParam(required = false) String range,
                                Authentication authentication){

        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        String nows = sdf.format(now.getTime()).toString();

//        Integer halaman = 0;
//        if (page != null){
//            halaman = page.getPageNumber();
//        }

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdJabatan = karyawanJabatanDao.cariIdJabatan(StatusRecord.AKTIF, karyawan, LocalDate.now());
        List<String> cariIdPengajuanDana = pengajuanDanaApproveDao.getIdPengajuanDana(cariIdJabatan);
        List<Integer> cariNomorUrut = pengajuanDanaApproveDao.getIdNomorUrut(cariIdJabatan);
        List<String> cariIdPengajuanDana1 = pengajuanDanaApproveDao.getIdPengajuanDanaApprove(cariIdJabatan);
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());

        List<String> idInstansi = instansiDao.cariIdInstansi(karyawanDepartemen);


        model.addAttribute("jumlahRequestApproval", pengajuanDanaApproveDao.countApprovalPengajuanDana("WAITING", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahApproved", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahOpen", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahCair", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));
        model.addAttribute("pageable", page);

        model.addAttribute("sisaSaldoBank" , saldoBankDao.sisaSaldoBankList(periodeAnggaran, idInstansi));

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            if (StringUtils.hasText(range)) {
                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
                model.addAttribute("range", range);
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanBetweenAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, localDatea, localDateb, search, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithDateSearch("WAITING", karyawan.getId(), localDatea, localDateb, search,cariIdJabatan, page));
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            }else{

                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, search, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithSearch("WAITING", karyawan.getId(), search,cariIdJabatan, page));
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            }
        }else{
            if (StringUtils.hasText(range)) {

                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + " 00:00:00" ;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + " 00:00:00";
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);

                model.addAttribute("range", range);
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanAfterAndPengajuanDanaTanggalPengajuanBeforeAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF,StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, localDatea, localDateb, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithDate("WAITING", karyawan.getId(), localDatea, localDateb,cariIdJabatan, page));
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            }else{
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
               // model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDana("WAITING", karyawan.getId(),cariIdJabatan, page));
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            }
        }

        model.addAttribute("approvalf", "active");
        model.addAttribute("approvalRequest", "active");
        return "transaksi/approval/list";
    }

    @GetMapping("/transaksi/approval/pengajuandana_check")
    private String homeApprovalCheck(Model model,
                                @PageableDefault(size = 10) Pageable page,
                                @RequestParam(required = false) String search,
                                @RequestParam(required = false) String range,
                                Authentication authentication){

        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        String nows = sdf.format(now.getTime()).toString();

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdJabatan = karyawanJabatanDao.cariIdJabatan(StatusRecord.AKTIF, karyawan, LocalDate.now());
        List<String> cariIdPengajuanDana = pengajuanDanaApproveDao.getIdPengajuanDana(cariIdJabatan);
        List<Integer> cariNomorUrut = pengajuanDanaApproveDao.getIdNomorUrut(cariIdJabatan);
        List<String> cariIdPengajuanDana1 = pengajuanDanaApproveDao.getIdPengajuanDanaApprove(cariIdJabatan);
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        List<String> idInstansi = instansiDao.cariIdInstansi(karyawanDepartemen);


        model.addAttribute("jumlahRequestApproval", pengajuanDanaApproveDao.countApprovalPengajuanDana("WAITING", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahApproved", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahOpen", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahCair", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));
        model.addAttribute("pageable", page);

        model.addAttribute("sisaSaldoBank" , saldoBankDao.sisaSaldoBankList(periodeAnggaran, idInstansi));


        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            if (StringUtils.hasText(range)) {
                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
                model.addAttribute("range", range);
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanBetweenAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, localDatea, localDateb, search, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithDateSearch("WAITING", karyawan.getId(), localDatea, localDateb, search,cariIdJabatan, page));

            }else{

                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, search, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithSearch("WAITING", karyawan.getId(), search,cariIdJabatan, page));

            }
        }else{
            if (StringUtils.hasText(range)) {

                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + " 00:00:00" ;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + " 00:00:00";
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);

                model.addAttribute("range", range);
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanAfterAndPengajuanDanaTanggalPengajuanBeforeAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF,StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, localDatea, localDateb, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithDate("WAITING", karyawan.getId(), localDatea, localDateb,cariIdJabatan, page));
            }else{
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                // model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDana("WAITING", karyawan.getId(),cariIdJabatan, page));
            }
        }

        model.addAttribute("approvalc", "active");
        model.addAttribute("approvalf", "active");
        return "transaksi/approval/list2";
    }


    @GetMapping("/transaksi/approval/pengajuandana/approvallist")
    private String homeApprovallist(Model model,
                                @PageableDefault(size = 50) Pageable page,
                                @RequestParam(required = false) String search,
                                @RequestParam(required = false) String range,
                                Authentication authentication){

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        String nows = sdf.format(now.getTime()).toString();

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdJabatan = karyawanJabatanDao.cariIdJabatan(StatusRecord.AKTIF, karyawan, LocalDate.now());
        List<String> cariIdPengajuanDana = pengajuanDanaApproveDao.getIdPengajuanDanaApprove(cariIdJabatan);
        List<Integer> cariNomorUrut = pengajuanDanaApproveDao.getIdNomorUrutApprove(cariIdJabatan);
        List<String> cariIdPengajuanDana1 = pengajuanDanaApproveDao.getIdPengajuanDanaApprove(cariIdJabatan);
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());


        model.addAttribute("jumlahRequestApproval", pengajuanDanaApproveDao.countApprovalPengajuanDana("WAITING", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahApproved", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahOpen", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));
        model.addAttribute("jumlahCair", pengajuanDanaApproveDao.countApprovalPengajuanDanaApprove("APPROVED", karyawan.getId(), cariIdJabatan));

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            if (StringUtils.hasText(range)) {
                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
                model.addAttribute("range", range);
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanBetweenAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, localDatea, localDateb, search, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithDateSearchApproved("APPROVED", karyawan.getId(), localDatea, localDateb, search,cariIdJabatan, page));
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            }else{

                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, search, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithSearchApproved("APPROVED", karyawan.getId(), search,cariIdJabatan, page));
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            }
        }else{
            if (StringUtils.hasText(range)) {

                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + " 00:00:00" ;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + " 00:00:00";
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);

                model.addAttribute("range", range);
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                //model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanAfterAndPengajuanDanaTanggalPengajuanBeforeAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF,StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, localDatea, localDateb, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaWithDateApproved("APPROVED", karyawan.getId(), localDatea, localDateb,cariIdJabatan, page));
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            }else{
                model.addAttribute("pengajuanDana", new PengajuanDana());
                model.addAttribute("pengajuanDanaApprove", new PengajuanDanaApprove());
                // model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, karyawan, karyawanDepartemen, page));
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.pageApprovalPengajuanDanaApproved("APPROVED", karyawan.getId(),cariIdJabatan, page));
                model.addAttribute("listPengajuanDanaApprovalWaiting", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord.AKTIF, StatusRecord.AKTIF));
            }
        }

        System.out.println("Jabatan Id :"+ cariIdJabatan);
        System.out.println("Pengajuan Dana id :"+ cariIdPengajuanDana);
        System.out.println("Nomor Urut :"+ cariNomorUrut);

        model.addAttribute("approvalf", "active");
        model.addAttribute("approvedRequest", "active");
        return "transaksi/approval/listapproved";
    }

    @GetMapping("/transaksi/approval/pengajuandana/alllist")
    private String homeApprovalalllist(Model model,
                                @PageableDefault(size = 10) Pageable page,
                                @RequestParam(required = false) String search,
                                @RequestParam(required = false) String range,
                                Authentication authentication){

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        String nows = sdf.format(now.getTime()).toString();

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> cariIdJabatan = karyawanJabatanDao.cariIdJabatan(StatusRecord.AKTIF, karyawan, LocalDate.now());
        List<String> cariIdPengajuanDana = pengajuanDanaApproveDao.getIdPengajuanDana(cariIdJabatan);
        List<Integer> cariNomorUrut = pengajuanDanaApproveDao.getIdNomorUrutApprove(cariIdJabatan);
        List<String> cariIdPengajuanDana1 = pengajuanDanaApproveDao.getIdPengajuanDanaAll(cariIdJabatan);
        List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());

        model.addAttribute("jumlahRequestApproval", pengajuanDanaApproveDao.countByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdIn(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.WAITING, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, karyawan, karyawanDepartemen));
        model.addAttribute("jumlahApproved", pengajuanDanaApproveDao.countByStatusAndPengajuanDanaIdInAndStatusApproveAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdIn(StatusRecord.AKTIF, cariIdPengajuanDana1, StatusRecord.APPROVED,  karyawan, karyawanDepartemen));

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            if (StringUtils.hasText(range)) {
                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);
                model.addAttribute("range", range);
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanBetweenAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.APPROVED, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, localDatea, localDateb, search, karyawan, karyawanDepartemen, page));

            }else{


                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.APPROVED, StatusRecord.AKTIF, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, search, karyawan, karyawanDepartemen, page));

            }
        }else{
            if (StringUtils.hasText(range)) {

                String datea = range;
                String tahuna = datea.substring(6, 10);
                String bulana = datea.substring(0, 2);
                String tanggala = datea.substring(3, 5);
                String tanggalana = tahuna + '-' + bulana + '-' + tanggala + ' ' + nows;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDatea = LocalDateTime.parse(tanggalana, formatter);

                String dateb = range;
                String tahunb = dateb.substring(19, 23);
                String bulanb = dateb.substring(13, 15);
                String tanggalb = dateb.substring(16, 18);
                String tanggalanb = tahunb + '-' + bulanb + '-' + tanggalb + ' ' + nows;
                DateTimeFormatter formatterb = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime localDateb = LocalDateTime.parse(tanggalanb, formatterb);

                model.addAttribute("range", range);
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanAfterAndPengajuanDanaTanggalPengajuanBeforeAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.APPROVED, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, localDatea, localDateb, karyawan, karyawanDepartemen, page));
            }else{
                model.addAttribute("listPengajuanDanaApprove", pengajuanDanaApproveDao.findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord.AKTIF, StatusRecord.AKTIF, StatusRecord.APPROVED, cariIdPengajuanDana, cariNomorUrut, cariNomorUrut, karyawan, karyawanDepartemen, page));
            }
        }

        System.out.println("Jabatan Id :"+ cariIdJabatan);
        System.out.println("Pengajuan Dana id :"+ cariIdPengajuanDana);
        System.out.println("Nomor Urut :"+ cariNomorUrut);

        model.addAttribute("approvalf", "active");
        model.addAttribute("approvalRequest", "active");
        return "transaksi/approval/listall";
    }

}
