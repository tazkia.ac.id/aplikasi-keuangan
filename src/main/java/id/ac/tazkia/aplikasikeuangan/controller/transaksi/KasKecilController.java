package id.ac.tazkia.aplikasikeuangan.controller.transaksi;


import id.ac.tazkia.aplikasikeuangan.dao.masterdata.InstansiDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.KasKecilDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.KasKecil;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class KasKecilController {

    @Autowired
    private KasKecilDao kasKecilDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @GetMapping("/transaksi/kaskecil")
    public String kasKecilList(Model model,
                               @RequestParam(required = false) String range,
                               @RequestParam(required = false) String search,
                               @PageableDefault(size = 10)Pageable page,
                               Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<String> idInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

//        model.addAttribute("listKasKecil", kasKecilDao.findByStatusOrderByTanggalDesc(StatusRecord.AKTIF, page));
        model.addAttribute("listKasKecil", kasKecilDao.findByStatusAndInstansiIdInOrderByTanggalDesc(StatusRecord.AKTIF, idInstansi, page));

        model.addAttribute("finance", "active");
        model.addAttribute("saldobank", "active");
        return "transaksi/kaskecil/list";

    }

    @GetMapping("/transaksi/kaskecil/baru")
    public String kasKecil(Model model){

        model.addAttribute("kasKecil", new KasKecil());

        model.addAttribute("finance", "active");
        model.addAttribute("saldobank", "active");
        return "transaksi/kaskecil/form";

    }

    @GetMapping("/transaksi/kaskecil/edit")
    public String editKasKecil(Model model,
                               @RequestParam(required = false)String kaskecil){

        model.addAttribute("kasKecil", kasKecilDao.findById(kaskecil));

        model.addAttribute("finance", "active");
        model.addAttribute("saldobank", "active");
        return "transaksi/kaskecil/edit";

    }

    @PostMapping("/transaksi/kaskecil/save")
    public String saveKasKecil(Model model,
                               @ModelAttribute  @Valid KasKecil kasKecil,
                               BindingResult errors,
                               Authentication authentication,
                               RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String cariIdInstansi = karyawanJabatanDao.cariIdInstansi3(karyawan.getId(), LocalDate.now());
        Instansi instansi = instansiDao.findByStatusAndId(StatusRecord.AKTIF, cariIdInstansi);


        String date = kasKecil.getTanggalString();

        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);

//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);

        String periodeAnggaran = periodeAnggaranCrudDao.getAnggaranAktif(localDate);
        PeriodeAnggaran periodeAnggaran1 = periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran);

        kasKecil.setTanggal(localDate);
        kasKecil.setInstansi(instansi);
        kasKecil.setStatus(StatusRecord.AKTIF);
        kasKecil.setPeriodeAnggaran(periodeAnggaran1);
        kasKecil.setUserInsert(karyawan.getNamaKaryawan());
        kasKecil.setTanggalInsert(LocalDateTime.now());

        kasKecilDao.save(kasKecil);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../kaskecil";

    }


    @PostMapping("/transaksi/kaskecil/update")
    public String updateKasKecil(Model model,
                               @ModelAttribute  @Valid KasKecil kasKecil,
                               BindingResult errors,
                               Authentication authentication,
                               RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        String cariIdInstansi = karyawanJabatanDao.cariIdInstansi3(karyawan.getId(), LocalDate.now());
        Instansi instansi = instansiDao.findByStatusAndId(StatusRecord.AKTIF, cariIdInstansi);

        String date = kasKecil.getTanggalString();

        String tahun = date.substring(0,4);
        String bulan = date.substring(5,7);
        String tanggal = date.substring(8,10);


//        String tahun = date.substring(6,10);
//        String bulan = date.substring(0,2);
//        String tanggal = date.substring(3,5);
        String tanggalan = tahun + '-' + bulan + '-' + tanggal;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(tanggalan, formatter);
        kasKecil.setTanggal(localDate);
        kasKecil.setInstansi(instansi);
        kasKecil.setStatus(StatusRecord.AKTIF);
        kasKecil.setUserEdit(karyawan.getNamaKaryawan());
        kasKecil.setTanggalEdit(LocalDateTime.now());

        kasKecilDao.save(kasKecil);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../kaskecil";

    }

    @PostMapping("/transaksi/kaskecil/hapus")
    public String hapusKasKecil(@RequestParam(required = false) String kaskecil,
                                 Authentication authentication,
                                 RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        KasKecil kasKecil1 = kasKecilDao.findById(kaskecil).get();
        kasKecil1.setStatus(StatusRecord.HAPUS);
        kasKecil1.setTanggalEdit(LocalDateTime.now());
        kasKecil1.setUserEdit(karyawan.getNamaKaryawan());

        kasKecilDao.save(kasKecil1);
        attributes.addFlashAttribute("delete", "Save Data Berhasil");
        return "redirect:../kaskecil";

    }

}
