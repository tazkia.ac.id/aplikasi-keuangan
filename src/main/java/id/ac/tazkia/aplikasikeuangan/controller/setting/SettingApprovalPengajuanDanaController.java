package id.ac.tazkia.aplikasikeuangan.controller.setting;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.JabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.SettingApprovalPengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Jabatan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.SettingApprovalPengajuanDana;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

@Controller
public class SettingApprovalPengajuanDanaController {

    @Autowired
    private SettingApprovalPengajuanDanaDao settingApprovalPengajuanDanaDao;

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private DepartemenDao departemenDao;


    @GetMapping("/setting/approvalpengajuandana")
    private String listApprovalPengajuanDana(Model model, @PageableDefault(size = 10) Pageable page, String search){


        model.addAttribute("setting", "active");
        model.addAttribute("positionss", "active");
        return "setting/settingapprovalpengajuandana/list";
    }


    @GetMapping("/masterdata/approvaldana/baru")
    private String baruApprovalPengajuanDana(Model model,
                                             @RequestParam(required = false)String detail) {

        Jabatan jabatan = jabatanDao.findById(detail).get();

        if (jabatan != null){

            model.addAttribute("jabatan", jabatan);
            model.addAttribute("listJabatan", jabatanDao.findByStatusAndStatusAktifOrderByKodeJabatan(StatusRecord.AKTIF, StatusRecord.AKTIF));
            model.addAttribute("settingApprovalPengajuanDana", new SettingApprovalPengajuanDana());

        }

        model.addAttribute("setting", "active");
        model.addAttribute("positionss", "active");
        return "masterdata/jabatan/approvaldana/form";

    }

    @GetMapping("/masterdata/approvaldana/edit")
    private String editApprovalPengajuanDana(Model model,
                                             @RequestParam(required = false)String detail,
                                             @RequestParam(required = false)String id){

        Jabatan jabatan = jabatanDao.findById(detail).get();

        SettingApprovalPengajuanDana settingApprovalPengajuanDana = settingApprovalPengajuanDanaDao.findById(id).get();

        if (settingApprovalPengajuanDana != null){

            model.addAttribute("jabatan", jabatan);
            model.addAttribute("listJabatan", jabatanDao.findByStatusAndStatusAktifOrderByKodeJabatan(StatusRecord.AKTIF, StatusRecord.AKTIF));
            model.addAttribute("settingApprovalPengajuanDana", settingApprovalPengajuanDana);


        }

        model.addAttribute("setting", "active");
        model.addAttribute("positionss", "active");
        return "masterdata/jabatan/approvaldana/form";

    }

    @PostMapping("/masterdata/approvaldana/save")
    private String saceApprovalPengajuanDana(Model model,
                                             @RequestParam(required = false)String detail,
                                             @ModelAttribute @Valid SettingApprovalPengajuanDana settingApprovalPengajuanDana, BindingResult errors, RedirectAttributes attributes){

        Jabatan jabatan = jabatanDao.findById(detail).get();

        if(errors.hasErrors()){
            model.addAttribute("listJabatan", jabatanDao.findByStatusAndStatusAktifOrderByKodeJabatan(StatusRecord.AKTIF, StatusRecord.AKTIF));
            model.addAttribute("setting", "active");
            model.addAttribute("positionss", "active");
            return "/masterdata/jabatan/approvaldana/form";
        }

        if(settingApprovalPengajuanDana.getStatus()==null){
            settingApprovalPengajuanDana.setStatus(StatusRecord.HAPUS);
        }

        settingApprovalPengajuanDana.setJabatan(jabatan);
        settingApprovalPengajuanDanaDao.save(settingApprovalPengajuanDana);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../jabatan/detail?detail="+ jabatan.getId() +"#home";

    }

    @PostMapping("/masterdata/approvaldana/hapus")
    public String deleteSettingApprovalPengajuanDana(@RequestParam String id){
        SettingApprovalPengajuanDana settingApprovalPengajuanDana = settingApprovalPengajuanDanaDao.findById(id).get();
        settingApprovalPengajuanDana.setStatus(StatusRecord.HAPUS);
        settingApprovalPengajuanDanaDao.save(settingApprovalPengajuanDana);
        return "redirect:../jabatan/detail?detail="+ settingApprovalPengajuanDana.getJabatan().getId() +"#home";
    }

}
