package id.ac.tazkia.aplikasikeuangan.controller.setting;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import id.ac.tazkia.aplikasikeuangan.dto.akunting.JournalTemplateDto;
import id.ac.tazkia.aplikasikeuangan.dto.akunting.JournalTemplateResponsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDetailEstimasiDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PaguAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PicAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dto.setting.AnggaranDetailDto;
import id.ac.tazkia.aplikasikeuangan.dto.setting.BelumAdaEstimasiDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetailEstimasi;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranProker;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PaguAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PicAnggaran;
import jakarta.validation.Valid;

@Controller
public class AnggaranDetailController {

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private AnggaranDetailCrudDao anggaranDetailCrudDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @Autowired
    private PicAnggaranDao picAnggaranDao;

    @Autowired
    private SatuanDao satuanDao;

    @Autowired
    private StandardDetailDao standardDetailDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PaguAnggaranDao paguAnggaranDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private AnggaranDetailEstimasiDao anggaranDetailEstimasiDao;

    @Autowired
    private StandardDao standardDao;

    @Autowired
    @Value("${api.akunting}")
    private String lokasiAkunting;

    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("/setting/anggaran/detail")
    private String listSettingAnggaranDetail(Model model, @PageableDefault(size = 10)Pageable page,
                                      @RequestParam(required = false)Anggaran anggaran,
                                      @RequestParam(required = false)String departemen,
                                      @RequestParam(required = false)String periode,
                                      String search){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        BigDecimal totalPngajuanDanaDetail = pengajuanDanaDao.getSisaPaguAnggaranDetail(periodeAnggaranAktif, anggaran.getId());

        if (totalPngajuanDanaDetail == null){
            BigDecimal sisaAnggaranDetail= BigDecimal.ZERO;
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId()));
                model.addAttribute("departemen",departemen);
                model.addAttribute("departemens", departemenDao.findById(departemen).get());
                model.addAttribute("periode", periodeAnggaranAktif);
                model.addAttribute("totalPengajuanDana", totalPngajuanDanaDetail);
                model.addAttribute("sisaAnggaranDetail", sisaAnggaranDetail);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranAndDeskripsiContainingIgnoreCaseOrderByDeskripsi(StatusRecord.AKTIF, anggaran, search, page));

            }else{

                model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId()));
                model.addAttribute("departemen",departemen);
                model.addAttribute("departemens", departemenDao.findById(departemen).get());
                model.addAttribute("periode", periodeAnggaranAktif);
                model.addAttribute("totalPengajuanDana", totalPngajuanDanaDetail);
                model.addAttribute("sisaAnggaranDetail", sisaAnggaranDetail);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranOrderByDeskripsi(StatusRecord.AKTIF, anggaran, page));

            }

            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_budget","active");
            return"setting/anggaran/detailanggaranlist";
        }else{
            BigDecimal sisaAnggaranDetail= anggaran.getAmount().subtract(totalPngajuanDanaDetail);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId()));
                model.addAttribute("departemen",departemen);
                model.addAttribute("departemens", departemenDao.findById(departemen).get());
                model.addAttribute("periode", periodeAnggaranAktif);
                model.addAttribute("totalPengajuanDana", totalPngajuanDanaDetail);
                model.addAttribute("sisaAnggaranDetail", sisaAnggaranDetail);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranAndDeskripsiContainingIgnoreCaseOrderByDeskripsi(StatusRecord.AKTIF, anggaran, search, page));

            }else{

                model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId()));
                model.addAttribute("departemen",departemen);
                model.addAttribute("departemens", departemenDao.findById(departemen).get());
                model.addAttribute("periode", periodeAnggaranAktif);
                model.addAttribute("totalPengajuanDana", totalPngajuanDanaDetail);
                model.addAttribute("sisaAnggaranDetail", sisaAnggaranDetail);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranOrderByDeskripsi(StatusRecord.AKTIF, anggaran, page));

            }

            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_budget","active");
            return"setting/anggaran/detailanggaranlist";
        }

    }

    @GetMapping("/setting/anggaran/detail/baru")
    private String baruDetailAnggaran(Model model, @RequestParam(required = false)String anggaran,
                                      @RequestParam(required = true)AnggaranProker anggaranProker,
                                      @RequestParam(required = false)String departemen,
                                      @RequestParam(required = false)String periode){

        BelumAdaEstimasiDto belumAdaEstimasiDto = anggaranDetailDao.cariEstimasiBelumSelesai(anggaranProker.getId()); 
        if(belumAdaEstimasiDto != null){
            model.addAttribute("error", "Dibawah ini detail anggaran yang belum selesai dalam setting estmasi pengeluaran nya, tolong lengkapi dulu estimasi pengeluaran pada detail budget di bawah sebelum menambah detail budget baru");
            model.addAttribute("anggaranDetailAda", anggaranDetailDao.findById(belumAdaEstimasiDto.getId()).get());
        }                     

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        model.addAttribute("totalPengeluaran", BigDecimal.ZERO);
        model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran));
        model.addAttribute("anggaranProker", anggaranProker);
        model.addAttribute("departemen",departemen);
        model.addAttribute("departemens", departemenDao.findById(departemen).get());
        model.addAttribute("periode", periodeAnggaranAktif);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("listStandardDetail",standardDetailDao.findByStatusOrderByStandard(StatusRecord.AKTIF));
        model.addAttribute("anggaranDetail", new AnggaranDetail());

        String apiUrl = lokasiAkunting + "/public/api/journal-template?type=FINANCE";


        try {
            // Panggil API untuk mendapatkan semua journal templates
            JournalTemplateResponsDto response = restTemplate.getForObject(apiUrl, JournalTemplateResponsDto.class);
            List<JournalTemplateDto> journalTemplatesDto = response != null ? response.getData() : Collections.emptyList();
            model.addAttribute("journalTemplates", journalTemplatesDto);
        } catch (RestClientException e) {
            // Menangani kesalahan ketika API tidak tersedia atau tidak dapat dijangkau
            model.addAttribute("journalTemplates", Collections.emptyList());
            model.addAttribute("errorMessage", "Tidak dapat terhubung ke layanan journal templates. Silakan coba lagi nanti.");
        }


        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_budget","active");
        return"setting/anggaran/detailanggaranbaru";
    }

    @GetMapping("/setting/anggaran/detail/edit")
    private String editDetailAnggaran(Model model,
                                      @RequestParam(required = false)String data,
                                      @RequestParam(required = false)String anggaran,
                                      @RequestParam(required = false)String departemen,
                                      @RequestParam(required = false)String periode){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        if (data != null && !data.isEmpty()) {
            AnggaranDetail anggaranDetail = anggaranDetailDao.findById(data).get();
            if (anggaranDetail != null) {
                model.addAttribute("anggaranDetail", anggaranDetail);
                model.addAttribute("anggaranProker", anggaranDetail.getAnggaranProker());
                if (anggaranDetail.getStatus() == null){
                    anggaranDetail.setStatus(StatusRecord.HAPUS);
                }
            }
        }

        model.addAttribute("totalPengeluaran", pengajuanDanaDao.getTotalPengajuanDanaDetail(periodeAnggaranAktif, data));
        model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran));
        model.addAttribute("departemen",departemen);
        model.addAttribute("departemens", departemenDao.findById(departemen).get());
        model.addAttribute("periode", periodeAnggaranAktif);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("listStandardDetail",standardDetailDao.findByStatusOrderByStandard(StatusRecord.AKTIF));

        String apiUrl = lokasiAkunting + "/public/api/journal-template?type=FINANCE";

        try {
            // Panggil API untuk mendapatkan semua journal templates
            JournalTemplateResponsDto response = restTemplate.getForObject(apiUrl, JournalTemplateResponsDto.class);
            List<JournalTemplateDto> journalTemplatesDto = response != null ? response.getData() : Collections.emptyList();
            model.addAttribute("journalTemplates", journalTemplatesDto);
        } catch (RestClientException e) {
            // Menangani kesalahan ketika API tidak tersedia atau tidak dapat dijangkau
            model.addAttribute("journalTemplates", Collections.emptyList());
            model.addAttribute("errorMessage", "Tidak dapat terhubung ke layanan journal templates. Silakan coba lagi nanti.");
        }

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_budget","active");
        return"setting/anggaran/detailanggaranform";
    }


    @PostMapping("/setting/anggaran/detail/save")
    private String saveDetailAnggarand(Model model,
                                      @RequestParam(required = false)Anggaran anggaran,
                                      @RequestParam(required = true)AnggaranProker anggaranProker,
                                      @RequestParam(required = false)String departemen,
                                      @RequestParam(required = false)String periode,
                                      @ModelAttribute @Valid AnggaranDetail anggaranDetail, BindingResult errors, RedirectAttributes attributes){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        model.addAttribute("anggaran", anggaran);
        String idAnggaran=anggaran.getId();
        model.addAttribute("departemen",departemen);
        model.addAttribute("departemens", departemenDao.findById(departemen).get());
        model.addAttribute("periode", periode);


            BigDecimal nominalLama = BigDecimal.ZERO;
      //      AnggaranDetail anggaranDetail1 = anggaranDetailDao.findById(anggaranDetail.getId()).get();
            BigDecimal nominalBaru = anggaranDetail.getKuantitas().multiply(anggaranDetail.getAmount());
            PaguAnggaran paguAnggaran = paguAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
            BigDecimal totalAnggaran1 = paguAnggaran.getPaguAnggaranAmount();
            BigDecimal totalAnggaran2 = anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
            BigDecimal anggaranLama = totalAnggaran2.subtract(nominalLama);
            BigDecimal anggaranBaru = anggaranLama.add(nominalBaru);
            BigDecimal totalPengajuanDana = BigDecimal.ZERO;
//           BigDecimal selisihKeluar = nominalBaru.subtract(totalPengajuanDana);

            if (totalPengajuanDana == null){
                BigDecimal totalPengajuanDana1 = BigDecimal.ZERO;
                if (nominalBaru.doubleValue() < totalPengajuanDana.doubleValue()){

                    attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                    return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;

                    return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                }

//                if (nominalBaru.doubleValue() > anggaranDetail.getAmount().multiply(anggaranDetail.getKuantitas()).doubleValue()) {
                    if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                        attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                        return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;
                        return "redirect:../setting?departemen=" + departemen + "&periode=" + periodeAnggaranAktif;

                    }
//                }

                anggaranDetail.setStandardDetail(anggaranProker.getStandardDetail());
                anggaranDetail.setAnggaranProker(anggaranProker);
                anggaranDetail.setAnggaran(anggaran);
                anggaranDetail.setIdPeriodeAnggaran(anggaran.getPeriodeAnggaran().getId());
                anggaranDetailDao.save(anggaranDetail);

                Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if(totalAnggaran == null){
                    anggaranData.setAmount(BigDecimal.ZERO);
                }else{
                    anggaranData.setAmount(totalAnggaran);
                }
                anggaranDao.save(anggaranData);

                PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
                BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode);
                if(totalAmountAnggaran == null){
                    picAnggaran.setJumlah(BigDecimal.ZERO);
                }else{
                    picAnggaran.setJumlah(totalAmountAnggaran);
                }
                picAnggaranDao.save(picAnggaran);

                attributes.addFlashAttribute("success", "Save Data Berhasil");
//                return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

            }else {

                BigDecimal totalPengajuanDana1 = totalPengajuanDana;

                if (nominalBaru.doubleValue() < totalPengajuanDana.doubleValue()){

                    attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                    return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                    return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                }


//                if (nominalBaru.doubleValue() > anggaranDetail.getAmount().multiply(anggaranDetail.getKuantitas()).doubleValue()) {
                    if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                        attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                        return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;

                        return "redirect:../setting?departemen=" + departemen + "&periode=" + periodeAnggaranAktif;

                    }
//                }

                anggaranDetail.setStandardDetail(anggaranProker.getStandardDetail());
                anggaranDetail.setAnggaranProker(anggaranProker);
                anggaranDetail.setAnggaran(anggaran);
                anggaranDetail.setIdPeriodeAnggaran(anggaran.getPeriodeAnggaran().getId());
                anggaranDetailDao.save(anggaranDetail);

                Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if(totalAnggaran == null){
                    anggaranData.setAmount(BigDecimal.ZERO);
                }else{
                    anggaranData.setAmount(totalAnggaran);
                }
                anggaranDao.save(anggaranData);

                PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
                BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode);
                if(totalAmountAnggaran == null){
                    picAnggaran.setJumlah(BigDecimal.ZERO);
                }else{
                    picAnggaran.setJumlah(totalAmountAnggaran);
                }
                picAnggaranDao.save(picAnggaran);

                attributes.addFlashAttribute("success", "Save Data Berhasil");
//                return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;

                return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

            }

    }

    @PostMapping("/setting/anggaran/detail/update")
    private String saveDetailAnggaran(Model model,
                                      @RequestParam(required = false)Anggaran anggaran,
                                      @RequestParam(required = true)AnggaranProker anggaranProker,
                                      @RequestParam(required = false)String departemen,
                                      @RequestParam(required = false)String periode,
                                      @ModelAttribute @Valid AnggaranDetail anggaranDetail, BindingResult errors, RedirectAttributes attributes){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        model.addAttribute("anggaran", anggaran);
        String idAnggaran=anggaran.getId();
        model.addAttribute("departemen",departemen);
        model.addAttribute("departemens", departemenDao.findById(departemen).get());
        model.addAttribute("periode", periodeAnggaranAktif);

       if (StringUtils.hasText(anggaranDetail.getId())){

           AnggaranDetail anggaranDetail1 = anggaranDetailDao.findById(anggaranDetail.getId()).get();
           BigDecimal nominalLama = anggaranDetail1.getKuantitas().multiply(anggaranDetail1.getAmount());
           BigDecimal nominalBaru = anggaranDetail.getKuantitas().multiply(anggaranDetail.getAmount());
           PaguAnggaran paguAnggaran = paguAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
           BigDecimal totalAnggaran1 = paguAnggaran.getPaguAnggaranAmount();
           BigDecimal totalAnggaran2 = anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
           BigDecimal anggaranLama = totalAnggaran2.subtract(nominalLama);
           BigDecimal anggaranBaru = anggaranLama.add(nominalBaru);
           BigDecimal totalPengajuanDana = pengajuanDanaDao.getTotalPengajuanDanaDetail(periode, anggaranDetail.getId());
//           BigDecimal selisihKeluar = nominalBaru.subtract(totalPengajuanDana);

            if (totalPengajuanDana == null){
                BigDecimal totalPengajuanDana1 = BigDecimal.ZERO;

                if (nominalBaru.doubleValue() > anggaranDetail1.getAmount().multiply(anggaranDetail1.getKuantitas()).doubleValue()){
                    if (nominalBaru.doubleValue() < totalPengajuanDana1.doubleValue()){

                        attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                        return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                        return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                    }

                    if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()){

                        attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                        return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                        return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                    }

                }

                anggaranDetail.setAnggaranProker(anggaranProker);
                anggaranDetail.setAnggaran(anggaran);
                anggaranDetail.setIdPeriodeAnggaran(anggaran.getPeriodeAnggaran().getId());
                anggaranDetailDao.save(anggaranDetail);

                Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if(totalAnggaran == null){
                    anggaranData.setAmount(BigDecimal.ZERO);
                }else{
                    anggaranData.setAmount(totalAnggaran);
                }
                anggaranDao.save(anggaranData);

                PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
                BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
                if(totalAmountAnggaran == null){
                    picAnggaran.setJumlah(BigDecimal.ZERO);
                }else{
                    picAnggaran.setJumlah(totalAmountAnggaran);
                }
                picAnggaranDao.save(picAnggaran);

                attributes.addFlashAttribute("success", "Save Data Berhasil");
//                return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

            }else{

                BigDecimal totalPengajuanDana1 = totalPengajuanDana;
                if (nominalBaru.doubleValue() < totalPengajuanDana1.doubleValue()){

                    attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                    return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                    return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                }

                if (nominalBaru.doubleValue() > anggaranDetail1.getAmount().multiply(anggaranDetail1.getKuantitas()).doubleValue()) {
                    if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                        attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                        return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;
                        return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                    }
                }

                anggaranDetail.setAnggaranProker(anggaranProker);
                anggaranDetail.setAnggaran(anggaran);
                anggaranDetail.setIdPeriodeAnggaran(anggaran.getPeriodeAnggaran().getId());
                anggaranDetailDao.save(anggaranDetail);

                Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if(totalAnggaran == null){
                    anggaranData.setAmount(BigDecimal.ZERO);
                }else{
                    anggaranData.setAmount(totalAnggaran);
                }
                anggaranDao.save(anggaranData);

                PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
                BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
                if(totalAmountAnggaran == null){
                    picAnggaran.setJumlah(BigDecimal.ZERO);
                }else{
                    picAnggaran.setJumlah(totalAmountAnggaran);
                }
                picAnggaranDao.save(picAnggaran);

                attributes.addFlashAttribute("success", "Save Data Berhasil");
//                return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;

                return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;
            }



       }else{

           BigDecimal nominalLama = BigDecimal.ZERO;
           AnggaranDetail anggaranDetail1 = anggaranDetailDao.findById(anggaranDetail.getId()).get();
           BigDecimal nominalBaru = anggaranDetail.getKuantitas().multiply(anggaranDetail.getAmount());
           PaguAnggaran paguAnggaran = paguAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
           BigDecimal totalAnggaran1 = paguAnggaran.getPaguAnggaranAmount();
          BigDecimal totalAnggaran2 = anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
           BigDecimal anggaranLama = totalAnggaran2.subtract(nominalLama);
           BigDecimal anggaranBaru = anggaranLama.add(nominalBaru);
           BigDecimal totalPengajuanDana = BigDecimal.ZERO;
//           BigDecimal selisihKeluar = nominalBaru.subtract(totalPengajuanDana);

           if (totalPengajuanDana == null){
               BigDecimal totalPengajuanDana1 = BigDecimal.ZERO;
                   if (nominalBaru.doubleValue() < totalPengajuanDana.doubleValue()){

                       attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                       return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                       return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                   }

               if (nominalBaru.doubleValue() > anggaranDetail1.getAmount().multiply(anggaranDetail1.getKuantitas()).doubleValue()) {
                   if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                       attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                       return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;
                       return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                   }
               }

               anggaranDetail.setAnggaranProker(anggaranProker);
               anggaranDetail.setAnggaran(anggaran);
               anggaranDetailDao.save(anggaranDetail);

               Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
               BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
               if(totalAnggaran == null){
                   anggaranData.setAmount(BigDecimal.ZERO);
               }else{
                   anggaranData.setAmount(totalAnggaran);
               }
               anggaranDao.save(anggaranData);

               PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
               BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
               if(totalAmountAnggaran == null){
                   picAnggaran.setJumlah(BigDecimal.ZERO);
               }else{
                   picAnggaran.setJumlah(totalAmountAnggaran);
               }
               picAnggaranDao.save(picAnggaran);

               attributes.addFlashAttribute("success", "Save Data Berhasil");
//               return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
               return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

           }else {

               BigDecimal totalPengajuanDana1 = totalPengajuanDana;

               if (nominalBaru.doubleValue() < totalPengajuanDana.doubleValue()){

                   attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                   return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                   return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

               }


               if (nominalBaru.doubleValue() > anggaranDetail1.getAmount().multiply(anggaranDetail1.getKuantitas()).doubleValue()) {
                   if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                       attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                       return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;
                       return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;

                   }
               }

               anggaranDetail.setAnggaranProker(anggaranProker);
               anggaranDetail.setAnggaran(anggaran);
               anggaranDetailDao.save(anggaranDetail);

               Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
               BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
               if(totalAnggaran == null){
                   anggaranData.setAmount(BigDecimal.ZERO);
               }else{
                   anggaranData.setAmount(totalAnggaran);
               }
               anggaranDao.save(anggaranData);

               PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
               BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
               if(totalAmountAnggaran == null){
                   picAnggaran.setJumlah(BigDecimal.ZERO);
               }else{
                   picAnggaran.setJumlah(totalAmountAnggaran);
               }
               picAnggaranDao.save(picAnggaran);

               attributes.addFlashAttribute("success", "Save Data Berhasil");
//               return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
               return "redirect:../setting?departemen="+departemen+"&periode="+periodeAnggaranAktif;


           }

       }



    }

    @PostMapping("/setting/anggaran/detail/hapus")
    public String deleteAnggaran(@RequestParam AnggaranDetail anggaranDetail,
                                 @RequestParam(required = false)String anggaran,
                                 @RequestParam(required = false)String departemen,
                                 @RequestParam(required = false)String periode,
                                 RedirectAttributes attributes){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        String idAnggaran = anggaran;
        String idDepartemen = departemen;
        String idPeriode = periodeAnggaranAktif;

        BigDecimal totalPengajuanDana = pengajuanDanaDao.getTotalPengajuanDanaDetail(periodeAnggaranAktif, anggaranDetail.getId());

        List<AnggaranDetailEstimasi> anggaranDetailEstimasis = anggaranDetailEstimasiDao.findByStatusAndAnggaranDetailOrderByTanggalEstimasi(StatusRecord.AKTIF, anggaranDetail);
        for(AnggaranDetailEstimasi g : anggaranDetailEstimasis){
            g.setStatus(StatusRecord.HAPUS);
            anggaranDetailEstimasiDao.save(g);
        }
        if (totalPengajuanDana == null){
            Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, idAnggaran);

            anggaranDetail.setStatus(StatusRecord.HAPUS);
            anggaranDetailDao.save(anggaranDetail);
            BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(idAnggaran);
            if(totalAnggaran == null){
                anggaranData.setAmount(BigDecimal.ZERO);
            }else{
                anggaranData.setAmount(totalAnggaran);
            }
            anggaranDao.save(anggaranData);

            PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periodeAnggaranAktif, departemen);
            BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periodeAnggaranAktif);
            if(totalAmountAnggaran == null){
                picAnggaran.setJumlah(BigDecimal.ZERO);
            }else{
                picAnggaran.setJumlah(totalAmountAnggaran);
            }

            attributes.addFlashAttribute("hapusdet", "Save Data Berhasil");
//            return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+idDepartemen+"&periode="+periodeAnggaranAktif;
            return "redirect:../setting?departemen="+idDepartemen+"&periode="+periodeAnggaranAktif;
        }else{

            attributes.addFlashAttribute("gagaldethapus", "Save Data Gagal");
//            return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+idDepartemen+"&periode="+periodeAnggaranAktif;

            return "redirect:../setting?departemen="+idDepartemen+"&periode="+periodeAnggaranAktif;
        }

    }


    @GetMapping("/api/detail/anggaran")
    @ResponseBody
    public List<AnggaranDetail> cariDetailAnggaran(@RequestParam(required = false) String idAnggaran){

        List<AnggaranDetail> anggaranDetails = anggaranDetailDao.findByStatusAndAnggaranOrderByDeskripsi(StatusRecord.AKTIF, anggaranDao.findById(idAnggaran).get());

        return anggaranDetails;

    }


    @GetMapping("/api/detail/anggarandto")
    @ResponseBody
    public List<AnggaranDetailDto> cariDetailAnggaranDto(@RequestParam(required = false) String idAnggaran){

        List<AnggaranDetailDto> anggaranDetailDtos = anggaranDetailDao.getDetailAnggaran(idAnggaran);

        return anggaranDetailDtos;
    }



    @GetMapping("/api/anggaran/hitung")
    @ResponseBody
    public AnggaranDetail cariNominalAnggaran(@RequestParam(required = false) String id){

        AnggaranDetail anggaranDetail= anggaranDetailDao.findById(id).get();

        return anggaranDetail;
    }

}
