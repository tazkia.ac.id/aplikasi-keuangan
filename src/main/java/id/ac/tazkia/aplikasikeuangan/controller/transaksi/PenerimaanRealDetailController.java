package id.ac.tazkia.aplikasikeuangan.controller.transaksi;

import id.ac.tazkia.aplikasikeuangan.dao.setting.PenerimaanDao;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PenerimaanRealDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PenerimaanReal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;

@Controller
public class PenerimaanRealDetailController {

    @Autowired
    private PenerimaanRealDao penerimaanRealDao;

    @Autowired
    private PenerimaanDao penerimaanDao;

    @GetMapping("/transaksi/penerimaanreal/detail")
    public String detailPenerimaanReal(Model model,
                                       @PageableDefault(size = 10) Pageable page,
                                       @RequestParam(required = false)String estimasi){

        Penerimaan penerimaan = penerimaanDao.findById(estimasi).get();
        model.addAttribute("penerimaan", penerimaan);
        model.addAttribute("listPenerimaanReal", penerimaanRealDao.findByStatusAndPenerimaanOrderByTanggal(StatusRecord.AKTIF, penerimaan, page));

        return "transaksi/penerimaanreal/listreal";

    }



    @PostMapping("/transaksi/penerimaanreal/delete")
    public String deletePenerimaanReal(@RequestParam PenerimaanReal penerimaanReal,
                                        RedirectAttributes attributes) {


        penerimaanReal.setStatus(StatusRecord.HAPUS);
        penerimaanRealDao.save(penerimaanReal);
        Penerimaan penerimaan = penerimaanDao.findById(penerimaanReal.getPenerimaan().getId()).get();
        BigDecimal totalRealisasi = penerimaanRealDao.getTotalPenerimaanReal(penerimaan.getId());
        if (totalRealisasi == null){
            penerimaan.setRealisasi(BigDecimal.ZERO);
        }else{
            penerimaan.setRealisasi(penerimaanRealDao.getTotalPenerimaanReal(penerimaan.getId()));
        }
        penerimaanDao.save(penerimaan);

        attributes.addFlashAttribute("success", "Delete Data Berhasil");
        return "redirect:../penerimaanreal/detail?estimasi="+penerimaan.getId();
    }

}
