package id.ac.tazkia.aplikasikeuangan.controller.masterdata;


import id.ac.tazkia.aplikasikeuangan.dao.config.RoleDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.InstansiDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.AnggaranDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Controller
public class DepartemenController {

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/masterdata/departemen")
    public String daftarDepartemen(Model model, @PageableDefault(size = 10) Pageable page,
                                   Authentication authentication, String search) {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> idInstansi = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        if(roleDao.findById("superuser").get() == user.getRole()) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
                model.addAttribute("listDepartemen", departemenDao.findByStatusAndKodeDepartemenContainingOrNamaDepartemenContainingOrderByKodeDepartemen(StatusRecord.AKTIF, search, search, page));
            } else {
                model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
                model.addAttribute("listDepartemen", departemenDao.findByStatusOrderByKodeDepartemen(StatusRecord.AKTIF, page));
            }
        }else{
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
                model.addAttribute("listDepartemen", departemenDao.findByStatusAndInstansiIdInAndKodeDepartemenContainingOrNamaDepartemenContainingOrderByKodeDepartemen(StatusRecord.AKTIF, idInstansi, search, search, page));
            } else {
                model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
                model.addAttribute("listDepartemen", departemenDao.findByStatusAndInstansiIdInOrderByKodeDepartemen(StatusRecord.AKTIF, idInstansi, page));
            }
        }

        model.addAttribute("setting", "active");
        model.addAttribute("departmentss", "active");
        return "masterdata/departemen/list";
    }

    @GetMapping("/masterdata/departemenbaru")
    public String departemenBaru(Model model) {


        model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
        model.addAttribute("departemen", new Departemen());

        model.addAttribute("setting", "active");
        model.addAttribute("departmentss", "active");
        return "masterdata/departemen/form";

    }

    @GetMapping("/masterdata/departemen/edit")
    public String  departemenEdit(Model model,
                                  @RequestParam(required = false) Departemen departemen){

        model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
        model.addAttribute("departemen", departemen);

        model.addAttribute("setting", "active");
        model.addAttribute("departmentss", "active");
        return "masterdata/departemen/form";
    }


    @PostMapping("/masterdata/departemen/save")
    public String departemenSave(Model model,
                                 @ModelAttribute @Valid Departemen departemen,
                                 BindingResult errors,
                                 RedirectAttributes attributes){

        if(errors.hasErrors()){
            model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
            model.addAttribute("karyawan", departemen);

            model.addAttribute("setting", "active");
            model.addAttribute("departmentss", "active");
            return "/masterdata/departemen/form";
        }


            if (departemen.getStatus()==null){
                departemen.setStatus(StatusRecord.HAPUS);
            }
            if (departemen.getStatusAktif() == null){
                departemen.setStatusAktif(StatusRecord.NONAKTIF);
            }
//            departemen.setBudget("CLOSED");
            departemen.setBlockingPengajuan(3);
            departemenDao.save(departemen);
            attributes.addFlashAttribute("success", "Save Data Berhasil");
            return "redirect:../departemen";


    }

    @PostMapping("/masterdata/departemen/hapus")
    public String departemenHapus(@RequestParam(required = false) Departemen departemen,
                                  RedirectAttributes attributes){

        Departemen departemen1 = departemenDao.findById(departemen.getId()).get();

        BigDecimal jumlaAnggaran = anggaranDao.jumlahAnggaranAktif(departemen1.getId());

        if (jumlaAnggaran.intValue() > 0){

            attributes.addFlashAttribute("gagal", "Save Data Berhasil");
            return "redirect:../departemen";

        }
        departemen1.setStatus(StatusRecord.HAPUS);
        departemenDao.save(departemen1);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../departemen";

    }
}
