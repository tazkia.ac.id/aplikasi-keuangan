package id.ac.tazkia.aplikasikeuangan.controller.masterdata;


import id.ac.tazkia.aplikasikeuangan.dao.masterdata.MataAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;

@Controller
public class MataAnggaranController {

    @Autowired
    private MataAnggaranDao mataAnggaranDao;

    @GetMapping("/masterdata/mataanggaran")
    public String daftarMataAnggaran(Model model, @PageableDefault(size = 10)Pageable page, String search) {

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listMataAnggaran", mataAnggaranDao.findByStatusAndKodeMataAnggaranContainingOrderByKodeMataAnggaran(StatusRecord.AKTIF, search, page));
        } else {
            model.addAttribute("listMataAnggaran", mataAnggaranDao.findByStatusOrderByKodeMataAnggaran(StatusRecord.AKTIF, page));
        }

        model.addAttribute("setting", "active");
        model.addAttribute("budget_line", "active");
        return "masterdata/mataanggaran/list";
    }

    @GetMapping("/masterdata/mataanggaranedit")
    public String editMataAnggaran(Model model,
                                   @RequestParam(required = false)String id) {

        model.addAttribute("mataAnggaran", new MataAnggaran());
        if (id != null && !id.isEmpty()) {
            MataAnggaran mataAnggaran = mataAnggaranDao.findById(id).get();
            if (mataAnggaran != null) {
                model.addAttribute("mataAnggaran", mataAnggaran);
                if (mataAnggaran.getStatus() == null){
                    mataAnggaran.setStatus(StatusRecord.HAPUS);
                }
                if (mataAnggaran.getStatusAktif() == null){
                    mataAnggaran.setStatusAktif(StatusRecord.NONAKTIF);
                }
            }
        }

        model.addAttribute("setting", "active");
        model.addAttribute("budget_line", "active");
        return "masterdata/mataanggaran/form";
    }

    @GetMapping("/masterdata/mataanggaranbaru")
    public String baruMataAnggaran(Model model, @RequestParam(required = false)String id) {

        model.addAttribute("mataAnggaran", new MataAnggaran());

        model.addAttribute("setting", "active");
        model.addAttribute("budget_line", "active");
        return "masterdata/mataanggaran/form";

    }

    @PostMapping("/masterdata/mataanggaransave")
    public String prosesMataAnggaran(@ModelAttribute @Valid MataAnggaran mataAnggaran,
                                     BindingResult errors,
                                     RedirectAttributes attributes){

        if(errors.hasErrors()){
            return "/masterdata/mataanggaran/form";
        }

        if (mataAnggaran.getStatus()==null){
            mataAnggaran.setStatus(StatusRecord.HAPUS);
        }
        if (mataAnggaran.getStatusAktif() == null){
            mataAnggaran.setStatusAktif(StatusRecord.NONAKTIF);
        }

        mataAnggaranDao.save(mataAnggaran);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:mataanggaran";
    }

    @PostMapping("/masterdata/mataanggaranhapus")
    public String deleteMataAnggaran(@RequestParam MataAnggaran mataAnggaran,
                                     RedirectAttributes attributes){
        mataAnggaran.setStatus(StatusRecord.HAPUS);
        mataAnggaranDao.save(mataAnggaran);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:mataanggaran";
    }

}
