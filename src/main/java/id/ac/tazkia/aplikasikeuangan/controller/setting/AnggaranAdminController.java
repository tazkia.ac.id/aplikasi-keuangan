package id.ac.tazkia.aplikasikeuangan.controller.setting;

import id.ac.tazkia.aplikasikeuangan.dao.config.RoleDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.*;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PicAnggaran;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Controller
public class AnggaranAdminController {

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PicAnggaranDao picAnggaranDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PaguAnggaranDao paguAnggaranDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private AnggaranDetailCrudDao anggaranDetailCrudDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private AnggaranProkerDao anggaranProkerDao;

    @GetMapping("/setting/admin/anggaran")
    private String listAnggaran(Model model,
                                @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                @PageableDefault(size = 10) Pageable page,
                                String search,
                                Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        //  List<String> karyawanDepartemen = karyawanJabatanDao.cariId(StatusRecord.AKTIF, karyawan, LocalDate.now());
        List<String> karyawanDepartemen = karyawanJabatanDao.cariIdDepartemen(karyawan.getId());

//        String periodeAnggaranAktif = periodeAnggaran.getId();
        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        System.out.println("user_role : "+ user.getRole());

        if (periodeAnggaran == null){
            model.addAttribute("periode",periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaranAktif));
                model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaranAktif, search, page));
            } else {
                model.addAttribute("periode",periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));
                model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaranAktif));
                model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaranAktif, page));
            }


        }else{
            model.addAttribute("selectedPeriode", periodeAnggaran);
            model.addAttribute("periode",periodeAnggaranDao.findByStatusOrderByStatusAktif(StatusRecord.AKTIF));
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaran.getId()));
                model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaran.getId(), search, page));
            } else {
                model.addAttribute("totalAnggaranPeriode", anggaranCrudDao.getTotalAnggaranPeriodeAktif(periodeAnggaran.getId()));
                model.addAttribute("listPicAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord.AKTIF, periodeAnggaran.getId(), page));
            }


        }


        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return "setting/anggaran/admin/list";

    }

    @GetMapping("/setting/admin/anggaran/setting")
    private String listSettingAnggaran(Model model, @PageableDefault(size = 10) Pageable page, @RequestParam(required = false)String departemen
            , @RequestParam(required = false)PeriodeAnggaran periode, String search){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        BigDecimal paguAnggaran = paguAnggaranDao.getTotalPaguAnggaranDepartemen(periode.getId(), departemen);
        //BigDecimal pengajuanDanaTotal = pengajuanDanaDao.getSisaPaguAnggaran(periode, departemen);
        BigDecimal pengajuanDanaTotal = anggaranCrudDao.getSisaAnggaranPerDepartemenIn(periode.getId(), departemen);

        if (pengajuanDanaTotal == null) {
            BigDecimal sisaAnggaran = paguAnggaran.subtract(BigDecimal.ZERO);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("departemen", departemen);
                model.addAttribute("periode", periode);
                model.addAttribute("paguAnggaran", paguAnggaran);
                model.addAttribute("pengajuanDana", pengajuanDanaTotal);
                model.addAttribute("sisaAnggaran", sisaAnggaran);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("totalAnggaran", anggaranCrudDao.getTotalAnggaran(departemen,periode.getId()));
                model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
                model.addAttribute("listAnggaran", anggaranDao.findByStatusAndDepartemenIdAndPeriodeAnggaranIdAndKodeAnggaranOrNamaAnggaranOrderByKodeAnggaran(StatusRecord.AKTIF, departemen, periode.getId(), search, search, page));
                model.addAttribute("listAnggaranProker", anggaranProkerDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
            }else{
                model.addAttribute("departemen", departemen);
                model.addAttribute("periode", periode);
                model.addAttribute("paguAnggaran", paguAnggaran);
                model.addAttribute("pengajuanDana", pengajuanDanaTotal);
                model.addAttribute("sisaAnggaran", sisaAnggaran);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("totalAnggaran", anggaranCrudDao.getTotalAnggaran(departemen,periode.getId()));
                model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
                model.addAttribute("listAnggaran", anggaranDao.findByStatusAndDepartemenIdAndPeriodeAnggaranIdOrderByKodeAnggaran(StatusRecord.AKTIF, departemen, periode.getId()));
                model.addAttribute("listAnggaranProker", anggaranProkerDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
            }

            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_administrator","active");
            return "setting/anggaran/admin/anggaranlist";

        }else{

            BigDecimal sisaAnggaran = paguAnggaran.subtract(pengajuanDanaTotal);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("departemen", departemen);
                model.addAttribute("periode", periode);
                model.addAttribute("paguAnggaran", paguAnggaran);
                model.addAttribute("pengajuanDana", pengajuanDanaTotal);
                model.addAttribute("sisaAnggaran", sisaAnggaran);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("totalAnggaran", anggaranCrudDao.getTotalAnggaran(departemen,periode.getId()));
                model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
                model.addAttribute("listAnggaran", anggaranDao.findByStatusAndDepartemenIdAndPeriodeAnggaranIdAndKodeAnggaranOrNamaAnggaranOrderByKodeAnggaran(StatusRecord.AKTIF, departemen, periode.getId(), search, search, page));
                model.addAttribute("listAnggaranProker", anggaranProkerDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
            }else{
                model.addAttribute("departemen", departemen);
                model.addAttribute("periode", periode);
                model.addAttribute("paguAnggaran", paguAnggaran);
                model.addAttribute("pengajuanDana", pengajuanDanaTotal);
                model.addAttribute("sisaAnggaran", sisaAnggaran);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("totalAnggaran", anggaranCrudDao.getTotalAnggaran(departemen,periode.getId()));
                model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
                model.addAttribute("listAnggaran", anggaranDao.findByStatusAndDepartemenIdAndPeriodeAnggaranIdOrderByKodeAnggaran(StatusRecord.AKTIF, departemen, periode.getId()));
                model.addAttribute("listAnggaranProker", anggaranProkerDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));
            }

            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_administrator","active");
            return "setting/anggaran/admin/anggaranlist";
        }


    }


    @GetMapping("/setting/admin/anggaran/baru")
    private String baruSettingAnggaran(Model model,@RequestParam(required = false)String id, @RequestParam(required = false)String departemen
            , @RequestParam(required = false)PeriodeAnggaran periode) {

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        model.addAttribute("departemen",departemen);
        model.addAttribute("periode", periode.getId());
        model.addAttribute("anggaran", new Anggaran());
        model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return "setting/anggaran/admin/anggaranform";

    }

    @GetMapping("/setting/admin/anggaran/edit")
    private String editSettingAnggaran(Model model,
                                       @RequestParam(required = false)String data,
                                       @RequestParam(required = false)String departemen,
                                       @RequestParam(required = false)PeriodeAnggaran periode) {

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        if (data != null && !data.isEmpty()) {
            Anggaran anggaran = anggaranDao.findById(data).get();
            if (anggaran != null) {
                model.addAttribute("anggaran", anggaran);
                if (anggaran.getStatus() == null){
                    anggaran.setStatus(StatusRecord.HAPUS);
                }
            }
        }

        model.addAttribute("departemen",departemen);
        model.addAttribute("periode", periode.getId());
        model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return "setting/anggaran/admin/anggaranform";

    }


    @PostMapping("/setting/admin/anggaran/save")
    public String prosesSaveAnggaran(Model model,
                                     @RequestParam(required = false)String departemen,
                                     @RequestParam(required = false)PeriodeAnggaran periode,
                                     @ModelAttribute @Valid Anggaran anggaran,
                                     BindingResult errors,
                                     RedirectAttributes attributes){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        String idDepartemen = departemen;
        String idPeriode = periodeAnggaranAktif;

        model.addAttribute("departemen",departemen);
        model.addAttribute("periode", periode.getId());
        model.addAttribute("picAnggaran", picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen));

        String idAnggaran=anggaran.getId();

        if(idAnggaran==null){
            anggaran.setAmount(BigDecimal.ZERO);
        }else{
            BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
            if (totalAnggaran==null){
                anggaran.setAmount(BigDecimal.ZERO);
            }else{
                anggaran.setAmount(totalAnggaran);
            }
        }


        anggaran.setDepartemen(departemenDao.findById(departemen).get());
        anggaran.setPeriodeAnggaran(periodeAnggaranDao.findById(periode.getId()).get());
        if (anggaran.getStatus()==null){
            anggaran.setStatus(StatusRecord.HAPUS);
        }

        if(errors.hasErrors()){
            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_administrator","active");
            return "setting/anggaran/admin/anggaranform";
        }
        anggaranDao.save(anggaran);


        PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
        BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
        picAnggaran.setJumlah(totalAmountAnggaran);
        picAnggaranDao.save(picAnggaran);


        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:setting?departemen="+departemen+"&periode="+periode.getId();
    }

    @PostMapping("/setting/admin/anggaran/hapus")
    public String deleteAnggaran(@RequestParam Anggaran anggaran, @RequestParam(required = false)String departemen
            , @RequestParam(required = false)PeriodeAnggaran periode,
                                 RedirectAttributes attributes){
        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        String idDepartemen = departemen;
        String idPeriode = periode.getId();

        BigDecimal totalPengjuanDana = pengajuanDanaDao.getTotalPengajuanDanaAnggaran(anggaran.getId());

        if ( totalPengjuanDana == null ) {

            String idAnggaran=anggaran.getId();
            if(idAnggaran==null){
                anggaran.setAmount(BigDecimal.ZERO);
            }else{
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if (totalAnggaran == null){
                    anggaran.setAmount(BigDecimal.ZERO);
                }else{
                    anggaran.setAmount(totalAnggaran);
                }
            }
            anggaran.setStatus(StatusRecord.HAPUS);
            anggaranDao.save(anggaran);

            PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
            BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
            if(totalAmountAnggaran == null){
                picAnggaran.setJumlah(BigDecimal.ZERO);
            }else{
                picAnggaran.setJumlah(totalAmountAnggaran);
            }

            picAnggaranDao.save(picAnggaran);

            attributes.addFlashAttribute("hapus", "Save Data Berhasil");
            return "redirect:setting?departemen="+idDepartemen+"&periode="+periode.getId();

        }else{

            attributes.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:setting?departemen="+idDepartemen+"&periode="+periode.getId();

        }


    }

}
