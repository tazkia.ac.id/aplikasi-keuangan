package id.ac.tazkia.aplikasikeuangan.controller.setting;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.*;
import id.ac.tazkia.aplikasikeuangan.dao.transaksi.PengajuanDanaDao;
import id.ac.tazkia.aplikasikeuangan.dto.setting.AnggaranDetailDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Controller
public class AnggaranDetailAdminController {

    @Autowired
    private AnggaranDao anggaranDao;

    @Autowired
    private AnggaranDetailDao anggaranDetailDao;

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private AnggaranDetailCrudDao anggaranDetailCrudDao;

    @Autowired
    private AnggaranCrudDao anggaranCrudDao;

    @Autowired
    private PicAnggaranDao picAnggaranDao;

    @Autowired
    private SatuanDao satuanDao;

    @Autowired
    private PengajuanDanaDao pengajuanDanaDao;

    @Autowired
    private PaguAnggaranDao paguAnggaranDao;

    @Autowired
    private StandardDetailDao standardDetailDao;

    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;

    @Autowired
    private AnggaranDetailEstimasiDao anggaranDetailEstimasiDao;

    @Autowired
    private StandardDao standardDao;


    @GetMapping("/setting/admin/anggaran/detail")
    private String listSettingAnggaranDetail(Model model, @PageableDefault(size = 10) Pageable page,
                                             @RequestParam(required = false) Anggaran anggaran,
                                             @RequestParam(required = false)String departemen,
                                             @RequestParam(required = false)PeriodeAnggaran periode,
                                             String search){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        BigDecimal totalPngajuanDanaDetail = pengajuanDanaDao.getSisaPaguAnggaranDetail(periode.getId(), anggaran.getId());

        if (totalPngajuanDanaDetail == null){
            BigDecimal sisaAnggaranDetail= BigDecimal.ZERO;
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId()));
                model.addAttribute("departemen",departemen);
                model.addAttribute("periode", periode.getId());
                model.addAttribute("totalPengajuanDana", totalPngajuanDanaDetail);
                model.addAttribute("sisaAnggaranDetail", sisaAnggaranDetail);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranAndDeskripsiContainingIgnoreCaseOrderByDeskripsi(StatusRecord.AKTIF, anggaran, search, page));

            }else{

                model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId()));
                model.addAttribute("departemen",departemen);
                model.addAttribute("periode", periode.getId());
                model.addAttribute("totalPengajuanDana", totalPngajuanDanaDetail);
                model.addAttribute("sisaAnggaranDetail", sisaAnggaranDetail);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranOrderByDeskripsi(StatusRecord.AKTIF, anggaran, page));

            }

            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_administrator","active");
            return"setting/anggaran/admin/detailanggaranlist";
        }else{
            BigDecimal sisaAnggaranDetail= anggaran.getAmount().subtract(totalPngajuanDanaDetail);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId()));
                model.addAttribute("departemen",departemen);
                model.addAttribute("periode", periode.getId());
                model.addAttribute("totalPengajuanDana", totalPngajuanDanaDetail);
                model.addAttribute("sisaAnggaranDetail", sisaAnggaranDetail);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranAndDeskripsiContainingIgnoreCaseOrderByDeskripsi(StatusRecord.AKTIF, anggaran, search, page));

            }else{

                model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId()));
                model.addAttribute("departemen",departemen);
                model.addAttribute("periode", periode.getId());
                model.addAttribute("totalPengajuanDana", totalPngajuanDanaDetail);
                model.addAttribute("sisaAnggaranDetail", sisaAnggaranDetail);
                model.addAttribute("getPeriode", periodeAnggaranDao.getPeriodeAnggaran("AKTIF"));
                model.addAttribute("listAnggaranDetail", anggaranDetailDao.findByStatusAndAnggaranOrderByDeskripsi(StatusRecord.AKTIF, anggaran, page));

            }

            model.addAttribute("setting", "active");
            model.addAttribute("anggaran_administrator","active");
            return"setting/anggaran/anggaran/detailanggaranlist";
        }

    }

    @GetMapping("/setting/admin/anggaran/detail/baru")
    private String baruDetailAnggaran(Model model, @RequestParam(required = false)String anggaran,
                                      @RequestParam(required = true)AnggaranProker anggaranProker,
                                      @RequestParam(required = false)String departemen,
                                      @RequestParam(required = false)PeriodeAnggaran periode){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());

        model.addAttribute("totalPengeluaran", BigDecimal.ZERO);
        model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran));
        model.addAttribute("anggaranProker", anggaranProker);
        model.addAttribute("departemen",departemen);
        model.addAttribute("periode", periode.getId());
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("listStandardDetail",standardDetailDao.findByStatusOrderByStandard(StatusRecord.AKTIF));
        model.addAttribute("anggaranDetail", new AnggaranDetail());

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return"setting/anggaran/admin/detailanggaranbaru";
    }

    @GetMapping("/setting/admin/anggaran/detail/edit")
    private String editDetailAnggaran(Model model,
                                      @RequestParam(required = false)String data,
                                      @RequestParam(required = false)String anggaran,
                                      @RequestParam(required = false)String departemen,
                                      @RequestParam(required = false)PeriodeAnggaran periode){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        if (data != null && !data.isEmpty()) {
            AnggaranDetail anggaranDetail = anggaranDetailDao.findById(data).get();
            if (anggaranDetail != null) {
                model.addAttribute("anggaranDetail", anggaranDetail);
                model.addAttribute("anggaranProker", anggaranDetail.getAnggaranProker());
                if (anggaranDetail.getStatus() == null){
                    anggaranDetail.setStatus(StatusRecord.HAPUS);
                }
            }
        }

        model.addAttribute("totalPengeluaran", pengajuanDanaDao.getTotalPengajuanDanaDetail(periode.getId(), data));
        model.addAttribute("anggaran", anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran));
        model.addAttribute("departemen",departemen);
        model.addAttribute("periode", periode.getId());
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("listStandardDetail",standardDetailDao.findByStatusOrderByStandard(StatusRecord.AKTIF));

        model.addAttribute("setting", "active");
        model.addAttribute("anggaran_administrator","active");
        return"setting/anggaran/admin/detailanggaranform";
    }


    @PostMapping("/setting/admin/anggaran/detail/save")
    private String saveDetailAnggarand(Model model,
                                       @RequestParam(required = false)Anggaran anggaran,
                                       @RequestParam(required = true)AnggaranProker anggaranProker,
                                       @RequestParam(required = false)String departemen,
                                       @RequestParam(required = false)PeriodeAnggaran periode,
                                       @ModelAttribute @Valid AnggaranDetail anggaranDetail, BindingResult errors, RedirectAttributes attributes){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        model.addAttribute("anggaran", anggaran);
        String idAnggaran=anggaran.getId();
        model.addAttribute("departemen",departemen);
        model.addAttribute("periode", periode.getId());


        BigDecimal nominalLama = BigDecimal.ZERO;
        //      AnggaranDetail anggaranDetail1 = anggaranDetailDao.findById(anggaranDetail.getId()).get();
        BigDecimal nominalBaru = anggaranDetail.getKuantitas().multiply(anggaranDetail.getAmount());
        PaguAnggaran paguAnggaran = paguAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
        BigDecimal totalAnggaran1 = paguAnggaran.getPaguAnggaranAmount();
        BigDecimal totalAnggaran2 = anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
        BigDecimal anggaranLama = totalAnggaran2.subtract(nominalLama);
        BigDecimal anggaranBaru = anggaranLama.add(nominalBaru);
        BigDecimal totalPengajuanDana = BigDecimal.ZERO;

//           BigDecimal selisihKeluar = nominalBaru.subtract(totalPengajuanDana);

        if (totalPengajuanDana == null){
            BigDecimal totalPengajuanDana1 = BigDecimal.ZERO;
            if (nominalBaru.doubleValue() < totalPengajuanDana.doubleValue()){

                attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                    return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;

                return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

            }

//                if (nominalBaru.doubleValue() > anggaranDetail.getAmount().multiply(anggaranDetail.getKuantitas()).doubleValue()) {
            if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                        return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;
                return "redirect:../setting?departemen=" + departemen + "&periode=" + periode.getId();

            }
//                }

            anggaranDetail.setAnggaranProker(anggaranProker);
            anggaranDetail.setAnggaran(anggaran);
            anggaranDetailDao.save(anggaranDetail);

            Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
            BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
            if(totalAnggaran == null){
                anggaranData.setAmount(BigDecimal.ZERO);
            }else{
                anggaranData.setAmount(totalAnggaran);
            }
            anggaranDao.save(anggaranData);

            PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
            BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
            if(totalAmountAnggaran == null){
                picAnggaran.setJumlah(BigDecimal.ZERO);
            }else{
                picAnggaran.setJumlah(totalAmountAnggaran);
            }
            picAnggaranDao.save(picAnggaran);

            attributes.addFlashAttribute("success", "Save Data Berhasil");
//                return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
            return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

        }else {

            BigDecimal totalPengajuanDana1 = totalPengajuanDana;

            if (nominalBaru.doubleValue() < totalPengajuanDana.doubleValue()){

                attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                    return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

            }


//                if (nominalBaru.doubleValue() > anggaranDetail.getAmount().multiply(anggaranDetail.getKuantitas()).doubleValue()) {
            if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                        return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;

                return "redirect:../setting?departemen=" + departemen + "&periode=" + periode.getId();

            }
//                }

            anggaranDetail.setAnggaranProker(anggaranProker);
            anggaranDetail.setAnggaran(anggaran);
            anggaranDetailDao.save(anggaranDetail);

            Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
            BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
            if(totalAnggaran == null){
                anggaranData.setAmount(BigDecimal.ZERO);
            }else{
                anggaranData.setAmount(totalAnggaran);
            }
            anggaranDao.save(anggaranData);

            PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
            BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
            if(totalAmountAnggaran == null){
                picAnggaran.setJumlah(BigDecimal.ZERO);
            }else{
                picAnggaran.setJumlah(totalAmountAnggaran);
            }
            picAnggaranDao.save(picAnggaran);

            attributes.addFlashAttribute("success", "Save Data Berhasil");
//                return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;

            return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

        }

    }

    @PostMapping("/setting/admin/anggaran/detail/update")
    private String saveDetailAnggaran(Model model,
                                      @RequestParam(required = false)Anggaran anggaran,
                                      @RequestParam(required = true)AnggaranProker anggaranProker,
                                      @RequestParam(required = false)String departemen,
                                      @RequestParam(required = false)PeriodeAnggaran periode,
                                      @ModelAttribute @Valid AnggaranDetail anggaranDetail, BindingResult errors, RedirectAttributes attributes){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        model.addAttribute("anggaran", anggaran);
        String idAnggaran=anggaran.getId();
        model.addAttribute("departemen",departemen);
        model.addAttribute("periode", periode.getId());

        if (StringUtils.hasText(anggaranDetail.getId())){

            AnggaranDetail anggaranDetail1 = anggaranDetailDao.findById(anggaranDetail.getId()).get();
            BigDecimal nominalLama = anggaranDetail1.getKuantitas().multiply(anggaranDetail1.getAmount());
            BigDecimal nominalBaru = anggaranDetail.getKuantitas().multiply(anggaranDetail.getAmount());
            PaguAnggaran paguAnggaran = paguAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
            BigDecimal totalAnggaran1 = paguAnggaran.getPaguAnggaranAmount();
            BigDecimal totalAnggaran2 = anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
            BigDecimal anggaranLama = totalAnggaran2.subtract(nominalLama);
            BigDecimal anggaranBaru = anggaranLama.add(nominalBaru);
            BigDecimal totalPengajuanDana = pengajuanDanaDao.getTotalPengajuanDanaDetail(periode.getId(), anggaranDetail.getId());
//           BigDecimal selisihKeluar = nominalBaru.subtract(totalPengajuanDana);

            if (totalPengajuanDana == null){
                BigDecimal totalPengajuanDana1 = BigDecimal.ZERO;

                if (nominalBaru.doubleValue() > anggaranDetail1.getAmount().multiply(anggaranDetail1.getKuantitas()).doubleValue()){
                    if (nominalBaru.doubleValue() < totalPengajuanDana1.doubleValue()){

                        attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                        return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                        return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

                    }

                    if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()){

                        attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                        return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                        return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

                    }

                }

                anggaranDetail.setStandardDetail(anggaranProker.getStandardDetail());
                anggaranDetail.setAnggaranProker(anggaranProker);
                anggaranDetail.setAnggaran(anggaran);
                anggaranDetailDao.save(anggaranDetail);

                Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if(totalAnggaran == null){
                    anggaranData.setAmount(BigDecimal.ZERO);
                }else{
                    anggaranData.setAmount(totalAnggaran);
                }
                anggaranDao.save(anggaranData);

                PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
                BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
                if(totalAmountAnggaran == null){
                    picAnggaran.setJumlah(BigDecimal.ZERO);
                }else{
                    picAnggaran.setJumlah(totalAmountAnggaran);
                }
                picAnggaranDao.save(picAnggaran);

                attributes.addFlashAttribute("success", "Save Data Berhasil");
//                return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

            }else{

                BigDecimal totalPengajuanDana1 = totalPengajuanDana;
                if (nominalBaru.doubleValue() < totalPengajuanDana1.doubleValue()){

                    attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                    return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                    return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

                }

                if (nominalBaru.doubleValue() > anggaranDetail1.getAmount().multiply(anggaranDetail1.getKuantitas()).doubleValue()) {
                    if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                        attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                        return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;
                        return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

                    }
                }

                anggaranDetail.setStandardDetail(anggaranProker.getStandardDetail());
                anggaranDetail.setAnggaranProker(anggaranProker);
                anggaranDetail.setAnggaran(anggaran);
                anggaranDetailDao.save(anggaranDetail);

                Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if(totalAnggaran == null){
                    anggaranData.setAmount(BigDecimal.ZERO);
                }else{
                    anggaranData.setAmount(totalAnggaran);
                }
                anggaranDao.save(anggaranData);

                PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
                BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
                if(totalAmountAnggaran == null){
                    picAnggaran.setJumlah(BigDecimal.ZERO);
                }else{
                    picAnggaran.setJumlah(totalAmountAnggaran);
                }
                picAnggaranDao.save(picAnggaran);

                attributes.addFlashAttribute("success", "Save Data Berhasil");
//                return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;

                return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();
            }



        }else{

            BigDecimal nominalLama = BigDecimal.ZERO;
            AnggaranDetail anggaranDetail1 = anggaranDetailDao.findById(anggaranDetail.getId()).get();
            BigDecimal nominalBaru = anggaranDetail.getKuantitas().multiply(anggaranDetail.getAmount());
            PaguAnggaran paguAnggaran = paguAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
            BigDecimal totalAnggaran1 = paguAnggaran.getPaguAnggaranAmount();
            BigDecimal totalAnggaran2 = anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
            BigDecimal anggaranLama = totalAnggaran2.subtract(nominalLama);
            BigDecimal anggaranBaru = anggaranLama.add(nominalBaru);
            BigDecimal totalPengajuanDana = BigDecimal.ZERO;
//           BigDecimal selisihKeluar = nominalBaru.subtract(totalPengajuanDana);

            if (totalPengajuanDana == null){
                BigDecimal totalPengajuanDana1 = BigDecimal.ZERO;
                if (nominalBaru.doubleValue() < totalPengajuanDana.doubleValue()){

                    attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                       return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                    return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

                }

                if (nominalBaru.doubleValue() > anggaranDetail1.getAmount().multiply(anggaranDetail1.getKuantitas()).doubleValue()) {
                    if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                        attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                       return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;
                        return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

                    }
                }

                anggaranDetail.setStandardDetail(anggaranProker.getStandardDetail());
                anggaranDetail.setAnggaranProker(anggaranProker);
                anggaranDetail.setAnggaran(anggaran);
                anggaranDetailDao.save(anggaranDetail);

                Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if(totalAnggaran == null){
                    anggaranData.setAmount(BigDecimal.ZERO);
                }else{
                    anggaranData.setAmount(totalAnggaran);
                }
                anggaranDao.save(anggaranData);

                PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
                BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
                if(totalAmountAnggaran == null){
                    picAnggaran.setJumlah(BigDecimal.ZERO);
                }else{
                    picAnggaran.setJumlah(totalAmountAnggaran);
                }
                picAnggaranDao.save(picAnggaran);

                attributes.addFlashAttribute("success", "Save Data Berhasil");
//               return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

            }else {

                BigDecimal totalPengajuanDana1 = totalPengajuanDana;

                if (nominalBaru.doubleValue() < totalPengajuanDana.doubleValue()){

                    attributes.addFlashAttribute("gagaldet", "Save Data Gagal, Nominal lebih kecil dati total anggaran yang sudah terpakai");
//                   return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                    return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

                }


                if (nominalBaru.doubleValue() > anggaranDetail1.getAmount().multiply(anggaranDetail1.getKuantitas()).doubleValue()) {
                    if (anggaranBaru.doubleValue() > totalAnggaran1.doubleValue()) {

                        attributes.addFlashAttribute("gagil", "Save Data Gagal, Total anggaran tidak boleh melebihi Pagu");
//                       return "redirect:../detail?anggaran=" + idAnggaran + "&departemen=" + departemen + "&periode=" + periodeAnggaranAktif;
                        return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();

                    }
                }

                anggaranDetail.setStandardDetail(anggaranProker.getStandardDetail());
                anggaranDetail.setAnggaranProker(anggaranProker);
                anggaranDetail.setAnggaran(anggaran);
                anggaranDetailDao.save(anggaranDetail);

                Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, anggaran.getId());
                BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(anggaran.getId());
                if(totalAnggaran == null){
                    anggaranData.setAmount(BigDecimal.ZERO);
                }else{
                    anggaranData.setAmount(totalAnggaran);
                }
                anggaranDao.save(anggaranData);

                PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
                BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
                if(totalAmountAnggaran == null){
                    picAnggaran.setJumlah(BigDecimal.ZERO);
                }else{
                    picAnggaran.setJumlah(totalAmountAnggaran);
                }
                picAnggaranDao.save(picAnggaran);

                attributes.addFlashAttribute("success", "Save Data Berhasil");
//               return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+departemen+"&periode="+periodeAnggaranAktif;
                return "redirect:../setting?departemen="+departemen+"&periode="+periode.getId();


            }

        }



    }

    @PostMapping("/setting/admin/anggaran/detail/hapus")
    public String deleteAnggaran(@RequestParam AnggaranDetail anggaranDetail,
                                 @RequestParam(required = false)String anggaran,
                                 @RequestParam(required = false)String departemen,
                                 @RequestParam(required = false)PeriodeAnggaran periode,
                                 RedirectAttributes attributes){

        String periodeAnggaranAktif = periodeAnggaranCrudDao.getAnggaranPerencanaanAktif(LocalDate.now());
        String idAnggaran = anggaran;
        String idDepartemen = departemen;
        String idPeriode = periode.getId();

        BigDecimal totalPengajuanDana = pengajuanDanaDao.getTotalPengajuanDanaDetail(periode.getId(), anggaranDetail.getId());

        List<AnggaranDetailEstimasi> anggaranDetailEstimasis = anggaranDetailEstimasiDao.findByStatusAndAnggaranDetailOrderByTanggalEstimasi(StatusRecord.AKTIF, anggaranDetail);
        for(AnggaranDetailEstimasi g : anggaranDetailEstimasis){
            g.setStatus(StatusRecord.HAPUS);
            anggaranDetailEstimasiDao.save(g);
        }
        if (totalPengajuanDana == null){
            Anggaran anggaranData = anggaranDao.findByStatusAndId(StatusRecord.AKTIF, idAnggaran);

            anggaranDetail.setStatus(StatusRecord.HAPUS);
            anggaranDetailDao.save(anggaranDetail);
            BigDecimal totalAnggaran=anggaranDetailCrudDao.getTotalAnggaran(idAnggaran);
            if(totalAnggaran == null){
                anggaranData.setAmount(BigDecimal.ZERO);
            }else{
                anggaranData.setAmount(totalAnggaran);
            }
            anggaranDao.save(anggaranData);

            PicAnggaran picAnggaran = picAnggaranDao.findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord.AKTIF, periode.getId(), departemen);
            BigDecimal totalAmountAnggaran=anggaranCrudDao.getTotalAnggaran(departemen, periode.getId());
            if(totalAmountAnggaran == null){
                picAnggaran.setJumlah(BigDecimal.ZERO);
            }else{
                picAnggaran.setJumlah(totalAmountAnggaran);
            }

            attributes.addFlashAttribute("hapusdet", "Save Data Berhasil");
//            return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+idDepartemen+"&periode="+periodeAnggaranAktif;
            return "redirect:../setting?departemen="+idDepartemen+"&periode="+periode.getId();
        }else{

            attributes.addFlashAttribute("gagaldethapus", "Save Data Gagal");
//            return "redirect:../detail?anggaran="+idAnggaran+"&departemen="+idDepartemen+"&periode="+periodeAnggaranAktif;

            return "redirect:../setting?departemen="+idDepartemen+"&periode="+periode.getId();
        }

    }


    @GetMapping("/api/admin/detail/anggaran")
    @ResponseBody
    public List<AnggaranDetail> cariDetailAnggaran(@RequestParam(required = false) String idAnggaran){

        List<AnggaranDetail> anggaranDetails = anggaranDetailDao.findByStatusAndAnggaranOrderByDeskripsi(StatusRecord.AKTIF, anggaranDao.findById(idAnggaran).get());

        return anggaranDetails;

    }


    @GetMapping("/api/admin/detail/anggarandto")
    @ResponseBody
    public List<AnggaranDetailDto> cariDetailAnggaranDto(@RequestParam(required = false) String idAnggaran){

        List<AnggaranDetailDto> anggaranDetailDtos = anggaranDetailDao.getDetailAnggaran(idAnggaran);

        return anggaranDetailDtos;
    }



    @GetMapping("/api/admin/anggaran/hitung")
    @ResponseBody
    public AnggaranDetail cariNominalAnggaran(@RequestParam(required = false) String id){

        AnggaranDetail anggaranDetail= anggaranDetailDao.findById(id).get();

        return anggaranDetail;
    }
}
