package id.ac.tazkia.aplikasikeuangan.controller.keuangan;

import id.ac.tazkia.aplikasikeuangan.dao.config.UserDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.InstansiDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.SatuanDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPendapatanTerkaitDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.EstimasiPendapatanTerkaitDetailDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranCrudDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkaitDetail;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class EstimasiPendapatanTerkaitController {

    @Autowired
    private EstimasiPendapatanTerkaitDao estimasiPendapatanTerkaitDao;

    @Autowired
    private EstimasiPendapatanTerkaitDetailDao estimasiPendapatanTerkaitDetailDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private KaryawanDao karyawanDao;
    @Autowired
    private PeriodeAnggaranCrudDao periodeAnggaranCrudDao;
    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private SatuanDao satuanDao;


    @GetMapping("/finance/estimasi_pendapatan_terkait")
    public String pendapatanTerkait(Model model,
                                    @RequestParam(name = "search", required = false) String search,
                                    @RequestParam(required = false) PeriodeAnggaran periodeAnggaran,
                                    @PageableDefault(size = 10, sort = "keterangan") Pageable pageable,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> yourIdInstansiList = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        Page<EstimasiPendapatanTerkait> estimasiList;

        if(periodeAnggaran != null){
            if (search != null && !search.isEmpty()) {
                estimasiList = estimasiPendapatanTerkaitDao.findByStatusAndPeriodeAnggaranAndInstansiIdInAndKeteranganContainingIgnoreCaseOrderByKeterangan(
                        StatusRecord.AKTIF,periodeAnggaran, yourIdInstansiList, search, pageable
                );
                model.addAttribute("search", search);
            } else {
                estimasiList = estimasiPendapatanTerkaitDao.findByStatusAndPeriodeAnggaranAndInstansiIdInOrderByKeterangan(
                        StatusRecord.AKTIF, periodeAnggaran, yourIdInstansiList, pageable
                );
            }
            model.addAttribute("periodeAnggaranSelected", periodeAnggaran);
        }else{
            String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());
            PeriodeAnggaran periodeAnggaranSelected = periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran1);
            if (search != null && !search.isEmpty()) {
                estimasiList = estimasiPendapatanTerkaitDao.findByStatusAndPeriodeAnggaranAndInstansiIdInAndKeteranganContainingIgnoreCaseOrderByKeterangan(
                        StatusRecord.AKTIF,periodeAnggaranSelected,  yourIdInstansiList, search, pageable
                );
                model.addAttribute("search", search);
            } else {
                estimasiList = estimasiPendapatanTerkaitDao.findByStatusAndPeriodeAnggaranAndInstansiIdInOrderByKeterangan(
                        StatusRecord.AKTIF,periodeAnggaranSelected, yourIdInstansiList, pageable
                );
            }
            model.addAttribute("periodeAnggaranSelected", periodeAnggaranSelected);
        }


        model.addAttribute("estimasiList", estimasiList);
        model.addAttribute("periodeAnggaranList", periodeAnggaranDao.findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
        model.addAttribute("pendapatanTerkait","active");

        return "keuangan/pendapatan_terkait/estimasi/list";
    }


    @GetMapping("/finance/estimasi_pendapatan_terkait/baru")
    public String tambahEstimasiPendapatanTerkait(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        EstimasiPendapatanTerkait estimasiPendapatanTerkait = new EstimasiPendapatanTerkait();

        List<String> yourIdInstansiList = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        List<Instansi> listInstansi = instansiDao.findByStatusAndIdIn(StatusRecord.AKTIF, yourIdInstansiList); // Ganti dengan metode yang sesuai.

        model.addAttribute("estimasiPendapatanTerkait", estimasiPendapatanTerkait);
        model.addAttribute("listInstansi", listInstansi);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("status", "baru");
        model.addAttribute("pendapatanTerkait","active");
        return "keuangan/pendapatan_terkait/estimasi/form"; // Gantilah dengan nama tampilan yang sesuai.
    }

    @PostMapping("/finance/estimasi_pendapatan_terkait/baru")
    public String simpanEstimasiPendapatanTerkait(@ModelAttribute EstimasiPendapatanTerkait estimasiPendapatanTerkait, Authentication authentication){

        User user = currentUserService.currentUser(authentication);

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        if(estimasiPendapatanTerkait.getPeriodeAnggaran() == null){
            estimasiPendapatanTerkait.setPeriodeAnggaran(periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran1));
        }
        estimasiPendapatanTerkait.setNominalTotal(estimasiPendapatanTerkait.getNominalSatuan());
        estimasiPendapatanTerkait.setUserInsert(user.getUsername());
        estimasiPendapatanTerkait.setTanggalInsert(LocalDateTime.now());
        estimasiPendapatanTerkaitDao.save(estimasiPendapatanTerkait);

        return "redirect:/finance/estimasi_pendapatan_terkait";
    }

    @GetMapping("/finance/estimasi_pendapatan_terkait/edit")
    public String editEstimasiPendapatanTerkait(Model model,@RequestParam EstimasiPendapatanTerkait id, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        List<String> yourIdInstansiList = karyawanJabatanDao.cariIdInstansi(StatusRecord.AKTIF, karyawan, LocalDate.now());

        List<Instansi> listInstansi = instansiDao.findByStatusAndIdIn(StatusRecord.AKTIF, yourIdInstansiList); // Ganti dengan metode yang sesuai.

        model.addAttribute("estimasiPendapatanTerkait", id);
        model.addAttribute("listInstansi", listInstansi);
        model.addAttribute("satuan", satuanDao.findAll());
        model.addAttribute("status", "edit");
        model.addAttribute("pendapatanTerkait","active");
        return "keuangan/pendapatan_terkait/estimasi/form"; // Gantilah dengan nama tampilan yang sesuai.
    }

    @PostMapping("/finance/estimasi_pendapatan_terkait/edit")
    public String updateEstimasiPendapatanTerkait(@ModelAttribute EstimasiPendapatanTerkait estimasiPendapatanTerkait, Authentication authentication){

        User user = currentUserService.currentUser(authentication);

        String periodeAnggaran1 = periodeAnggaranCrudDao.getAnggaranAktif(LocalDate.now());

        if(estimasiPendapatanTerkait.getPeriodeAnggaran() == null){
            estimasiPendapatanTerkait.setPeriodeAnggaran(periodeAnggaranDao.findByStatusAndId(StatusRecord.AKTIF, periodeAnggaran1));
        }

        BigDecimal totalDetail = estimasiPendapatanTerkaitDetailDao.totalEstimasiPendapatanTerkait(estimasiPendapatanTerkait.getId());
        if(totalDetail == null){
         totalDetail = BigDecimal.ZERO;
        }
        if(totalDetail.compareTo(BigDecimal.ZERO) > 0){
            estimasiPendapatanTerkait.setNominalTotal(totalDetail);
        }else {
            estimasiPendapatanTerkait.setNominalTotal(estimasiPendapatanTerkait.getNominalSatuan());
        }
        estimasiPendapatanTerkait.setUserUpdate(user.getUsername());
        estimasiPendapatanTerkait.setTanggalUpdate(LocalDateTime.now());
        estimasiPendapatanTerkaitDao.save(estimasiPendapatanTerkait);

        return "redirect:/finance/estimasi_pendapatan_terkait";
    }

}
