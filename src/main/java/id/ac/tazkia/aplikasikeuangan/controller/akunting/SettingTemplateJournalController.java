package id.ac.tazkia.aplikasikeuangan.controller.akunting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.DepartemenDao;
import id.ac.tazkia.aplikasikeuangan.dao.setting.PeriodeAnggaranDao;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;


@Controller
public class SettingTemplateJournalController {
    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    @Value("${api.akunting}")
    private String lokasiAkunting;

    @Autowired
    private PeriodeAnggaranDao periodeAnggaranDao;

    @GetMapping("/settingtemplate")
    public String getSettingTemplate(Model model) {
        model.addAttribute("settingtemplate", "active");
        model.addAttribute("departemen", departemenDao
            .findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord.AKTIF, StatusRecord.AKTIF));
        model.addAttribute("periode", periodeAnggaranDao
            .findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord.AKTIF));
        return "akunting/settingTemplate";
    }
    
}
