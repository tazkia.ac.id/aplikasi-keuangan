package id.ac.tazkia.aplikasikeuangan.controller.masterdata;

import id.ac.tazkia.aplikasikeuangan.dao.masterdata.KaryawanDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDao;
import id.ac.tazkia.aplikasikeuangan.dao.masterdata.StandardDetailDao;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Standard;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.StandardDetail;
import id.ac.tazkia.aplikasikeuangan.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class StandardDetailController {

    @Autowired
    private StandardDao standardDao;

    @Autowired
    private StandardDetailDao standardDetailDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @GetMapping("/masterdata/standard_detail/baru")
    public String standardDetailBaru(Model model,
                                     @RequestParam(required = false)Standard standard){

        model.addAttribute("standard", standard);
        model.addAttribute("standardDetail", new StandardDetail());

        model.addAttribute("setting", "active");
        model.addAttribute("standardss", "active");
        return "masterdata/standard_detail/form";

    }

    @GetMapping("/masterdata/standard_detail/edit")
    public String standardDetailEdit(Model model,
                                     @RequestParam(required = false)StandardDetail standardDetail,
                                     @RequestParam(required = false)Standard standard){

        model.addAttribute("standard", standard);
        model.addAttribute("standardDetail", standardDetail);

        model.addAttribute("setting", "active");
        model.addAttribute("standardss", "active");
        return "masterdata/standard_detail/form";

    }

    @PostMapping("/masterdata/standard_detail/save")
    public String saveStandardDetail(@ModelAttribute @Valid StandardDetail standardDetail,
                                     @RequestParam(required = false)Standard standard,
                                     Authentication authentication,
                                     BindingResult errors,
                                     RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        standardDetail.setStandard(standard);
        standardDetail.setUserUpdate(karyawan.getNamaKaryawan());
        standardDetail.setTanggalUpdate(LocalDateTime.now());

        standardDetailDao.save(standardDetail);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../standard";

    }


    @PostMapping("/masterdata/standard_detail/update")
    public String updateStandardDetail(@ModelAttribute @Valid StandardDetail dataStandardDetail,
                                     @RequestParam(required = false)Standard standard,
                                     Authentication authentication,
                                     BindingResult errors,
                                     RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

//        dataStandardDetail.setStandard(standard);
        dataStandardDetail.setUserUpdate(karyawan.getNamaKaryawan());
        dataStandardDetail.setTanggalUpdate(LocalDateTime.now());

        standardDetailDao.save(dataStandardDetail);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../standard";

    }

}
