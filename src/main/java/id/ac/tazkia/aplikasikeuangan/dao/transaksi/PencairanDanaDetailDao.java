package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDanaDetail;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface PencairanDanaDetailDao extends PagingAndSortingRepository<PencairanDanaDetail, String>, CrudRepository<PencairanDanaDetail, String> {

    List<PencairanDanaDetail> findByPencairanDana(PencairanDana pencairanDana);

    List<PencairanDanaDetail> findByPengajuanDanaAnggaranDetailAnggaranDepartemenIdAndPengajuanDanaAnggaranDetailAnggaranPeriodeAnggaranIdOrderByPengajuanDanaTanggalPengajuan(String Departemen, String periode);

    @Query(value = "select b.*,a.id_pencairan_dana from pencairan_dana_detail as a inner join pengajuan_dana as b on a.id_pengajuan_dana = b.id where b.status not in ('HAPUS','DELETE','REJECTED','CANCELED') and month(b.tanggal_pengajuan) = ?1 and year(b.tanggal_pengajuan) = ?2", nativeQuery = true)
    List<Object> cariPencairanDanaDetail(String bulan, String tahun);

    PencairanDanaDetail findByPencairanDanaId(String pencairanDana);

    @Query(value ="select coalesce(sum(b.jumlah),0)as totalPengajuanDana from pencairan_dana_detail as a inner join pengajuan_dana as b on a.id_pengajuan_dana = b.id where a.id_pencairan_dana = ?1 group by a.id_pencairan_dana", nativeQuery = true)
    BigDecimal totalPengajuanDana(String pencairanDana);

    @Query("select pd from PencairanDanaDetail pd group by pd.pencairanDana")
    Page<PengajuanDana> pengajuanDanaCair(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, Pageable page);


    @Query(value ="select id_pengajuan_dana from pencairan_dana_detail where id_pencairan_dana = ?1", nativeQuery = true)
    List<String> listDetailPencairanDana(String pencairanDana);

    List<PencairanDanaDetail> findByIdIn(List<String> idPencairanDanaDetail);

    List<PencairanDanaDetail> findByPencairanDanaOrderByPencairanDanaTanggalPencairan(PencairanDana pencairanDana);



}
