package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkaitDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface EstimasiPendapatanTerkaitDetailDao extends PagingAndSortingRepository<EstimasiPendapatanTerkaitDetail, String>, CrudRepository<EstimasiPendapatanTerkaitDetail, String> {

    List<EstimasiPendapatanTerkaitDetail> findByStatusAndEstimasiPendapatanTerkaitOrderByTanggalEstimasi(StatusRecord statusRecord, EstimasiPendapatanTerkait estimasiPendapatanTerkait);

    @Query(value = "select sum(nominal_total) as total \n" +
            "from estimasi_pendapatan_terkait_detail\n" +
            "where status = 'AKTIF' and id_estimasi_pendapatan_terkait = ?1", nativeQuery = true)
    BigDecimal totalEstimasiPendapatanTerkait(String idPendapatanTerkait);

}
