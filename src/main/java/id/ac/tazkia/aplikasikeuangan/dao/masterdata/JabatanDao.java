package id.ac.tazkia.aplikasikeuangan.dao.masterdata;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Jabatan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JabatanDao extends PagingAndSortingRepository<Jabatan, String>, CrudRepository<Jabatan, String> {

    Page<Jabatan> findByStatusOrderByKodeJabatan(StatusRecord statusRecord, Pageable page);

    @Query(value = "select a.id,a.kode_jabatan, a.nama_jabatan, g.nama_departemen , a.status_aktif,group_concat(b.nomor_urut,'. ',c.nama_jabatan,'-',b.batas_pengajuan order by b.nomor_urut SEPARATOR '\n') as approval from jabatan as a inner join departemen as g on a.id_departemen = g.id \n" +
            "left join (select * from setting_approval_pengajuan_dana where status = 'AKTIF') as b on a.id = b.id_jabatan\n" +
            "left join jabatan as c on b.id_jabatan_approve = c.id\n" +
            "where a.status = 'AKTIF' group by a.id order by a.kode_jabatan",countQuery = "select count(id) as ada from jabatan where status = 'AKTIF'", nativeQuery = true)
    Page<Object> pageDataJabatanAll(Pageable pageable);

    @Query(value = "select a.id,a.kode_jabatan, a.nama_jabatan, g.nama_departemen , a.status_aktif,group_concat(b.nomor_urut,'. ',c.nama_jabatan,'-',b.batas_pengajuan order by b.nomor_urut SEPARATOR '\n') as approval from jabatan as a inner join departemen as g on a.id_departemen = g.id \n" +
            "left join (select * from setting_approval_pengajuan_dana where status = 'AKTIF') as b on a.id = b.id_jabatan\n" +
            "left join jabatan as c on b.id_jabatan_approve = c.id inner join departemen as d on c.id_departemen = d.id \n" +
            "where a.status = 'AKTIF' and d.id_instansi in ?1 group by a.id order by a.kode_jabatan",countQuery = "select count(id) as ada from jabatan where status = 'AKTIF'", nativeQuery = true)
    Page<Object> pageDataJabatanInstansiAll(List<String> id_instansi, Pageable pageable);

    @Query(value = "select a.id,a.kode_jabatan, a.nama_jabatan, g.nama_departemen, a.status_aktif,group_concat(b.nomor_urut,'. ',c.nama_jabatan,'-',b.batas_pengajuan order by b.nomor_urut SEPARATOR '\n') as approval from jabatan as a inner join departemen as g on a.id_departemen = g.id\n" +
            "left join (select * from setting_approval_pengajuan_dana where status = 'AKTIF') as b on a.id = b.id_jabatan\n" +
            "left join jabatan as c on b.id_jabatan_approve = c.id\n" +
            "where a.status = 'AKTIF' and (a.kode_jabatan like %?1% or a.nama_jabatan like %?1%) group by a.id order by a.kode_jabatan",countQuery = "select count(id) as ada from jabatan where status = 'AKTIF' and (kode_jabatan like %?1% or nama_jabatan like %?1%)", nativeQuery = true)
    Page<Object> pageDataJabatanWithSearch(String search, Pageable pageable);

    @Query(value = "select a.id,a.kode_jabatan, a.nama_jabatan, g.nama_departemen, a.status_aktif,group_concat(b.nomor_urut,'. ',c.nama_jabatan,'-',b.batas_pengajuan order by b.nomor_urut SEPARATOR '\n') as approval from jabatan as a inner join departemen as g on a.id_departemen = g.id\n" +
            "left join (select * from setting_approval_pengajuan_dana where status = 'AKTIF') as b on a.id = b.id_jabatan\n" +
            "left join jabatan as c on b.id_jabatan_approve = c.id inner join departemen as d on c.id_departemen = d.id \n" +
            "where a.status = 'AKTIF' and d.id_instansi in ?2 and (a.kode_jabatan like %?1% or a.nama_jabatan like %?1%) group by a.id order by a.kode_jabatan",countQuery = "select count(id) as ada from jabatan where status = 'AKTIF' and (kode_jabatan like %?1% or nama_jabatan like %?1%)", nativeQuery = true)
    Page<Object> pageDataJabatanInstansiWithSearch(String search, List<String> idInstansi, Pageable pageable);

    Page<Jabatan> findByStatusAndKodeJabatanContainingIgnoreCaseOrNamaJabatanContainingIgnoreCaseOrderByKodeJabatan(StatusRecord statusRecord, String kodeJabatan, String namaJabatan, Pageable page);

    List<Jabatan> findByStatusOrderByKodeJabatan(StatusRecord statusRecord);

    List<Jabatan> findByStatusAndStatusAktifOrderByKodeJabatan(StatusRecord statusRecord, StatusRecord statusAktif);


}
