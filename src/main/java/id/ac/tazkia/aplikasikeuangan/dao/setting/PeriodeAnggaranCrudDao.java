package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface PeriodeAnggaranCrudDao extends CrudRepository<PeriodeAnggaran, String>, PagingAndSortingRepository<PeriodeAnggaran, String> {


    @Query(value = "select id from periode_anggaran where tanggal_mulai_anggaran <= ?1 and tanggal_selesai_anggaran >= ?1 and status='AKTIF' and status_aktif='AKTIF'", nativeQuery = true)
    String getAnggaranAktif(LocalDate tanggal);

    @Query(value = "select id from periode_anggaran where tanggal_mulai_perencanaan <= ?1 and tanggal_selesai_anggaran >= ?1 and status='AKTIF' and status_aktif='AKTIF'", nativeQuery = true)
    String getAnggaranAktifA(LocalDate tanggal);

    @Query(value = "select id from periode_anggaran where tanggal_mulai_perencanaan <= ?1 and tanggal_selesai_perencanaan >= ?1 and status='AKTIF' and status_aktif='AKTIF'", nativeQuery = true)
    String getAnggaranPerencanaanAktif(LocalDate tanggal);

    @Query(value = "select id from periode_anggaran where tanggal_mulai_anggaran <= ?1 and tanggal_selesai_anggaran >= ?1 and status='AKTIF' and status_aktif='AKTIF' order by kode_periode_anggaran", nativeQuery = true)
    List<String> getAnggaranAktif1(LocalDate tanggal);

    @Query(value = "select id from periode_anggaran where tanggal_mulai_perencanaan <= ?1 and tanggal_selesai_anggaran >= ?1 and status='AKTIF' and status_aktif='AKTIF' order by kode_periode_anggaran", nativeQuery = true)
    List<String> getAnggaranAktif2(LocalDate tanggal);


}
