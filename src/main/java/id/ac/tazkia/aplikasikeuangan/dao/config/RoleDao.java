package id.ac.tazkia.aplikasikeuangan.dao.config;

import id.ac.tazkia.aplikasikeuangan.entity.config.Role;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RoleDao extends PagingAndSortingRepository<Role, String>, CrudRepository<Role, String> {


}
