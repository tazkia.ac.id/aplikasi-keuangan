package id.ac.tazkia.aplikasikeuangan.dao.setting;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.ac.tazkia.aplikasikeuangan.dto.setting.AnggaranDetailDto;
import id.ac.tazkia.aplikasikeuangan.dto.setting.BelumAdaEstimasiDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;

public interface AnggaranDetailDao extends PagingAndSortingRepository<AnggaranDetail, String>, CrudRepository<AnggaranDetail, String> {

    Page<AnggaranDetail> findByStatusAndAnggaranOrderByDeskripsi(StatusRecord statusRecord, Anggaran anggaran, Pageable page);

    Page<AnggaranDetail> findByStatusAndAnggaranAndDeskripsiContainingIgnoreCaseOrderByDeskripsi(StatusRecord statusRecord, Anggaran anggaran, String deskripsi, Pageable page);

    List<AnggaranDetail> findByStatusAndAnggaranOrderByDeskripsi(StatusRecord statusRecord, Anggaran anggaran);

    List<AnggaranDetail> findByStatusAndAnggaranDepartemenIdIn(StatusRecord statusRecord, List<String> departemen);

    List<AnggaranDetail> findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord statusRecord, String periodeAnggaran, String departemen);


    @Query(value = "SELECT a.id,a.deskripsi,(a.amount * a.kuantitas)AS anggaran,COALESCE(b.totalPengajuanDana,0)AS totalPengajuanDana, COALESCE((a.amount * a.kuantitas),0)-COALESCE(b.totalPengajuanDana,0) AS sisa  FROM" +
            "(SELECT * FROM anggaran_detail WHERE id_anggaran = ?1 and status='AKTIF')a LEFT JOIN " +
            "(SELECT id_anggaran_detail,SUM(jumlah) AS totalPengajuanDana FROM pengajuan_dana WHERE STATUS not in ('HAPUS','REJECTED','CANCELED') AND id_anggaran = ?1 " +
            "GROUP BY id_anggaran_detail)b ON a.id=b.id_anggaran_detail", nativeQuery = true)
    List<AnggaranDetailDto> getDetailAnggaran1(String anggaran);

    @Query(value = "SELECT a.id,program_kerja as programKerja ,a.deskripsi,(a.amount * a.kuantitas)AS anggaran,COALESCE(b.totalPengajuanDana,0)AS totalPengajuanDana, \n" +
            "COALESCE((a.amount * a.kuantitas),0)-COALESCE(b.totalPengajuanDana,0) AS sisa  FROM\n" +
            "(SELECT a.*,b.program_kerja FROM anggaran_detail as a inner join anggaran_proker as b on a.id_anggaran_proker = b.id \n" +
            "WHERE b.id_anggaran = ?1 and a.status='AKTIF')a LEFT JOIN \n" +
            "(select id_anggaran_detail,sum(totalPengajuanDana)as totalPengajuanDana from \n" +
            "(SELECT aa.id as pengajuan_dana,aa.id_anggaran_detail,coalesce(sum(dd.jumlah),sum(aa.jumlah),0) AS totalPengajuanDana \n" +
            "FROM pengajuan_dana as aa left join pencairan_dana_detail as bb on aa.id = bb.id_pengajuan_dana left join\n" +
            "pencairan_dana as cc on bb.id_pencairan_dana = cc.id left join laporan_dana as dd on bb.id=dd.id_pencairan_dana_detail \n" +
            "WHERE aa.STATUS not in ('HAPUS','REJECTED','CANCELED') AND aa.id_anggaran = ?1\n" +
            "GROUP BY coalesce(aa.id))bbb group by id_anggaran_detail)b ON a.id=b.id_anggaran_detail\n" +
            "order by program_kerja, deskripsi", nativeQuery = true)
    List<AnggaranDetailDto> getDetailAnggaran(String anggaran);

    @Query(value = "SELECT COALESCE((a.amount * a.kuantitas),0)-COALESCE(b.totalPengajuanDana,0) AS sisa  FROM\n" +
            "(SELECT * FROM anggaran_detail WHERE id = ?1 and status='AKTIF')a LEFT JOIN \n" +
            "(select id_anggaran_detail,sum(totalPengajuanDana)as totalPengajuanDana from \n" +
            "(SELECT aa.id as pengajuan_dana,aa.id_anggaran_detail,coalesce(sum(dd.jumlah),sum(aa.jumlah),0) AS totalPengajuanDana FROM pengajuan_dana as aa left join pencairan_dana_detail as bb on aa.id = bb.id_pengajuan_dana left join\n" +
            "pencairan_dana as cc on bb.id_pencairan_dana = cc.id left join laporan_dana as dd on bb.id=dd.id_pencairan_dana_detail \n" +
            "WHERE aa.STATUS not in ('HAPUS','REJECTED','CANCELED') AND aa.id_anggaran_detail = ?1\n" +
            "GROUP BY coalesce(aa.id))bbb group by id_anggaran_detail)b ON a.id=b.id_anggaran_detail",nativeQuery = true)
    BigDecimal getSisaDetailAnggaran(String detailAnggaran);

    @Query(value = "select coalesce(sum(kuantitas * amount),0.00) as total from anggaran_detail where status='AKTIF' and id_anggaran = ?1", nativeQuery = true)
    BigDecimal getTotalAnggaranDetail(String idAnggaran);

    @Modifying
    @Query(value = "update anggaran_detail set status = 'HAPUS' where id_anggaran_proker = ?1", nativeQuery = true)
    void deleteAnggaranDetailByProker(String idAnggaranProker);



    @Query(value = "select id, idAnggaran,idAnggaranProker, totalAnggaranDetail, totalEstimasi from \n" +
    "(select a.id, id_anggaran as idAnggaran, id_anggaran_proker as idAnggaranProker, a.kuantitas * a.amount as totalAnggaranDetail,  \n" +
    "sum(b.kuantitas * b.amount) as totalEstimasi from anggaran_detail as a  \n" +
    "left join anggaran_detail_estimasi as b on a.id = b.id_anggaran_detail  \n" +
    "where a.id_anggaran_proker = ?1 and a.status = 'AKTIF' and coalesce(b.status,'AKTIF')= 'AKTIF' \n" +
    "group by a.id) as a  \n" +
    "where totalEstimasi is null or totalEstimasi < totalAnggaranDetail limit 1", nativeQuery = true)
    BelumAdaEstimasiDto cariEstimasiBelumSelesai(String anggaranProker);

}
