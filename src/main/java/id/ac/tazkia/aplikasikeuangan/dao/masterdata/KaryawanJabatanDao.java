package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Jabatan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.KaryawanJabatan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface KaryawanJabatanDao extends PagingAndSortingRepository<KaryawanJabatan, String>, CrudRepository<KaryawanJabatan, String> {

    Page<KaryawanJabatan> findByStatusAndKaryawanIdOrderByMulaiBerlaku(StatusRecord statusRecord, String karyawan, Pageable page);

    Page<KaryawanJabatan> findByStatusAndJabatanStatusAndJabatanStatusAktifAndKaryawanIdOrderByMulaiBerlaku(StatusRecord statusRecord, StatusRecord statusRecord2, StatusRecord statusRecord3, String karyawan, Pageable page);


    List<KaryawanJabatan> findByStatusAndStatusAktifAndKaryawanAndMulaiBerlakuBefore(StatusRecord statusRecord, StatusRecord statusAktif, Karyawan karyawan, LocalDate tanggal);

    KaryawanJabatan findByStatusAndStatusAktifAndKaryawanAndJabatanDepartemen(StatusRecord statusRecord, StatusRecord statusAktif, Karyawan karyawan, Departemen departemen);

    @Query("select kd.jabatan.departemen.id from KaryawanJabatan kd where kd.status = :status and statusAktif='AKTIF' and kd.karyawan = :karyawan and kd.mulaiBerlaku <= :tanggal")
    List<String> cariId(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, @Param("tanggal")LocalDate tanggal);

    @Query(value = "\n" +
            "SELECT * FROM\n" +
            "(SELECT dp.id AS id_departemen FROM karyawan_jabatan AS kj INNER JOIN jabatan AS ja ON kj.id_jabatan=ja.id INNER JOIN \n" +
            "departemen AS dp ON ja.id_departemen=dp.id WHERE kj.status='AKTIF' AND kj.status_aktif='AKTIF' AND kj.id_karyawan=?1 AND kj.mulai_berlaku <= DATE(NOW())\n" +
            "UNION\n" +
            "SELECT da.id AS id_departemen FROM karyawan_jabatan AS kj INNER JOIN setting_approval_pengajuan_dana AS sa ON kj.id_jabatan = sa.id_jabatan_approve\n" +
            "INNER JOIN jabatan AS ja ON sa.id_jabatan=ja.id\n" +
            "INNER JOIN departemen AS da ON ja.id_departemen = da.id\n" +
            "WHERE kj.status='AKTIF' AND kj.status_aktif='AKTIF' AND kj.id_karyawan= ?1 AND kj.mulai_berlaku <= DATE(NOW()) GROUP BY da.id) a GROUP BY id_departemen\n", nativeQuery = true)
    List<String> cariIdDepartemen(String idKaryawan);

    @Query("select kd.jabatan.id from KaryawanJabatan kd where kd.status = :status and statusAktif='AKTIF' and kd.karyawan = :karyawan and kd.mulaiBerlaku <= :tanggal")
    List<String> cariIdJabatan(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, @Param("tanggal")LocalDate tanggal);

    KaryawanJabatan findByStatusAndStatusAktifAndJabatanIdAndMulaiBerlakuBefore(StatusRecord statusRecord, StatusRecord statusAktif, String jabatan, LocalDate mulaiBerlaku);

    List<KaryawanJabatan> findByStatusAndStatusAktifAndJabatanIdAndMulaiBerlakuBeforeOrderByKaryawanNamaKaryawan(StatusRecord statusRecord, StatusRecord statusAktif, String jabatan, LocalDate mulaiBerlaku);

    @Query("select kd.jabatan.departemen.instansi.id from KaryawanJabatan kd where kd.status = :status and statusAktif='AKTIF' and kd.karyawan = :karyawan and kd.mulaiBerlaku <= :tanggal group by kd.jabatan.departemen.instansi.id")
    List<String> cariIdInstansi(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, @Param("tanggal")LocalDate tanggal);


    @Query(value = "select d.id as idInstansi \n" +
            "from karyawan_jabatan as kd inner join jabatan as b on kd.id_jabatan = b.id \n" +
            "inner join departemen as c on b.id_departemen = c.id \n" +
            "inner join instansi as d on c.id_instansi = d.id  \n" +
            "where kd.status = ?1 and kd.status_aktif='AKTIF' and kd.id_karyawan = ?2 and kd.mulai_berlaku <= ?3 group by id_instansi \n", nativeQuery = true)
    List<String> cariIdInstansi21(StatusRecord statusRecord, String idKaryawan,LocalDate tanggal);

    @Query("select  kd.jabatan.departemen.instansi.id  from KaryawanJabatan kd where kd.status = :status and statusAktif='AKTIF' and kd.jabatan.departemen.status='AKTIF' and kd.karyawan = :karyawan and kd.mulaiBerlaku <= :tanggal")
    String cariIdInstansi2(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, @Param("tanggal")LocalDate tanggal);

    @Query(value = "select d.id as instansi from karyawan_jabatan as a inner join jabatan as b on a.id_jabatan=b.id inner join departemen as c on b.id_departemen=c.id inner join " +
            "instansi as d on c.id_instansi=d.id where a.status='AKTIF' and b.status='AKTIF' and c.status='AKTIF' and d.status='AKTIF' and b.keuangan='AKTIF' and a.id_karyawan=?1 and a.mulai_berlaku <= ?2 order by b.nama_jabatan limit 1", nativeQuery = true)
    String cariIdInstansi3 (String karyawan, LocalDate tanggal);

    @Query(value = "select count(id) as jml from\n" +
            "(select a.id from karyawan_jabatan as a inner join jabatan as b on a.id_jabatan = b.id inner join departemen as c on b.id_departemen=c.id\n" +
            "inner join instansi as d on c.id_instansi=d.id where id_karyawan=?1 group by id_instansi)aa", nativeQuery = true)
    Integer jmlInstansi(String idKaryawan);

    @Query(value = "select count(id) as jml from\n" +
            "(select a.id from karyawan_jabatan as a inner join jabatan as b on a.id_jabatan = b.id inner join departemen as c on b.id_departemen=c.id where id_karyawan=?1 and a.status='AKTIF' AND a.status_aktif='AKTIF' group by id_departemen)aa", nativeQuery = true)
    Integer jmlDepartemen(String idKaryawan);

    Integer countByStatusAndJabatan(StatusRecord statusRecord, Jabatan jabatan);

    @Query(value = "select a.id, kode_departemen, nama_departemen as namaDepartemen from departemen as a\n" +
            "inner join anggaran as b on a.id = b.id_departemen\n" +
            "inner join periode_anggaran as c on b.id_periode_anggaran = c.id\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status='AKTIF' and c.status_aktif = 'AKTIF'\n" +
            "and tanggal_mulai_anggaran <= date(now()) and tanggal_selesai_anggaran >= date(now()) group by nama_departemen",
    countQuery = "select count(id) as jumlah from\n" +
            "(select a.id, kode_departemen, nama_departemen as namaDepartemen from departemen as a\n" +
            "inner join anggaran as b on a.id = b.id_departemen\n" +
            "inner join periode_anggaran as c on b.id_periode_anggaran = c.id\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status='AKTIF' and c.status_aktif = 'AKTIF'\n" +
            "and tanggal_mulai_anggaran <= date(now()) and tanggal_selesai_anggaran >= date(now()) group by nama_departemen) as a" , nativeQuery = true)
    Page<Object> listJabatanDepartemen(Pageable pageable);

    @Query(value = "select a.id, kode_departemen, nama_departemen as namaDepartemen from departemen as a\n" +
            "inner join anggaran as b on a.id = b.id_departemen\n" +
            "inner join periode_anggaran as c on b.id_periode_anggaran = c.id\n" +
            "where a.status in ('AKTIF','HAPUSS') and b.status = 'AKTIF' and c.id = ?1 group by nama_departemen",
            countQuery = "select count(id) as jumlah from\n" +
                    "(select a.id, kode_departemen, nama_departemen as namaDepartemen from departemen as a\n" +
                    "inner join anggaran as b on a.id = b.id_departemen\n" +
                    "inner join periode_anggaran as c on b.id_periode_anggaran = c.id\n" +
                    "where a.status in ('AKTIF','HAPUSS') and b.status = 'AKTIF' and c.status='AKTIF' and c.id = ?1 group by nama_departemen) as a" , nativeQuery = true)
    Page<Object> listJabatanDepartemen1(String idPeriodeAnggaran,Pageable pageable);

    @Query(value = "select a.id, kode_departemen, nama_departemen as namaDepartemen from departemen as a\n" +
            "inner join anggaran as b on a.id = b.id_departemen\n" +
            "inner join periode_anggaran as c on b.id_periode_anggaran = c.id\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status='AKTIF' and c.status_aktif = 'AKTIF'\n" +
            "and tanggal_mulai_anggaran <= date(now()) and tanggal_selesai_anggaran >= date(now()) and (nama_departemen like %?1% or kode_departemen like %?1%) group by nama_departemen",
            countQuery = "select count(id) as jumlah from\n" +
                    "(select a.id, kode_departemen, nama_departemen as namaDepartemen from departemen as a\n" +
                    "inner join anggaran as b on a.id = b.id_departemen\n" +
                    "inner join periode_anggaran as c on b.id_periode_anggaran = c.id\n" +
                    "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status='AKTIF' and c.status_aktif = 'AKTIF' and (nama_departemen like %?1% or kode_departemen like %?1%) \n" +
                    "and tanggal_mulai_anggaran <= date(now()) and tanggal_selesai_anggaran >= date(now()) group by nama_departemen) as a" , nativeQuery = true)
    Page<Object> listJabatanDepartemenSearch(String search, Pageable pageable);


    @Query(value = "select a.id, kode_departemen, nama_departemen as namaDepartemen from departemen as a\n" +
            "inner join anggaran as b on a.id = b.id_departemen\n" +
            "inner join periode_anggaran as c on b.id_periode_anggaran = c.id\n" +
            "where a.status in ('AKTIF','HAPUSS') and b.status = 'AKTIF' and c.status = 'AKTIF' and c.id = ?2 and (nama_departemen like %?1% or kode_departemen like %?1%) group by nama_departemen",
            countQuery = "select count(id) as jumlah from\n" +
                    "(select a.id, kode_departemen, nama_departemen as namaDepartemen from departemen as a\n" +
                    "inner join anggaran as b on a.id = b.id_departemen\n" +
                    "inner join periode_anggaran as c on b.id_periode_anggaran = c.id\n" +
                    "where a.status in ('AKTIF','HAPUSS') and b.status = 'AKTIF' and c.status = 'AKTIF' and (nama_departemen like %?1% or kode_departemen like %?1%) \n" +
                    "and c.id = ?2 group by nama_departemen) as a" , nativeQuery = true)
    Page<Object> listJabatanDepartemenSearch1(String search, String idPeriodeAnggaran, Pageable pageable);

    @Query(value ="select * from\n" +
            "(SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
            "FROM karyawan_jabatan AS a \n" +
            "INNER JOIN jabatan AS b ON a.id_jabatan = b.id\n" +
            "INNER JOIN departemen AS c ON b.id_departemen = c.id\n" +
            "WHERE a.id_karyawan = ?1 AND a.status = 'AKTIF' AND b.status='AKTIF' AND c.status = 'AKTIF'\n" +
            "union\n" +
            "SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
            "FROM karyawan_jabatan AS a \n" +
            "inner join setting_approval_pengajuan_dana as g on g.id_jabatan_approve = a.id_jabatan \n" +
            "INNER JOIN jabatan AS b ON g.id_jabatan = b.id\n" +
            "INNER JOIN departemen AS c ON b.id_departemen = c.id\n" +
            "WHERE a.id_karyawan = ?1 AND a.status = 'AKTIF' AND b.status='AKTIF' AND c.status = 'AKTIF')a\n" +
            "group by id \n" +
            "order by kode_departemen",
            countQuery = "SELECT COUNT(a.id) AS jumlah\n" +
                    "FROM (select * from\n" +
                    "(SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
                    "FROM karyawan_jabatan AS a \n" +
                    "INNER JOIN jabatan AS b ON a.id_jabatan = b.id\n" +
                    "INNER JOIN departemen AS c ON b.id_departemen = c.id\n" +
                    "WHERE a.id_karyawan = ?1 AND a.status = 'AKTIF' AND b.status='AKTIF' AND c.status = 'AKTIF'\n" +
                    "union\n" +
                    "SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
                    "FROM karyawan_jabatan AS a \n" +
                    "inner join setting_approval_pengajuan_dana as g on g.id_jabatan_approve = a.id_jabatan \n" +
                    "INNER JOIN jabatan AS b ON g.id_jabatan = b.id\n" +
                    "INNER JOIN departemen AS c ON b.id_departemen = c.id\n" +
                    "WHERE a.id_karyawan = ?1 AND a.status = 'AKTIF' AND b.status='AKTIF' AND c.status = 'AKTIF')a\n" +
                    "group by id) as  a", nativeQuery = true)
    Page<Object> listJabatanAktif(String idKaryawan, Pageable pageable);


    @Query(value ="select * from\n" +
            "(SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
            "FROM karyawan_jabatan AS a \n" +
            "INNER JOIN jabatan AS b ON a.id_jabatan = b.id\n" +
            "INNER JOIN departemen AS c ON b.id_departemen = c.id inner join anggaran as d on c.id = d.id_departemen\n" +
            "WHERE a.id_karyawan = ?1 and d.id_periode_anggaran = ?2  AND a.status = 'AKTIF' AND b.status in ('AKTIF','HAPUSS') AND c.status in ('AKTIF','HAPUSS')\n" +
            "union\n" +
            "SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
            "FROM karyawan_jabatan AS a \n" +
            "inner join setting_approval_pengajuan_dana as g on g.id_jabatan_approve = a.id_jabatan \n" +
            "INNER JOIN jabatan AS b ON g.id_jabatan = b.id\n" +
            "INNER JOIN departemen AS c ON b.id_departemen = c.id inner join anggaran as d on c.id = d.id_departemen\n" +
            "WHERE a.id_karyawan = ?1 and d.id_periode_anggaran = ?2  AND a.status = 'AKTIF' AND b.status in ('AKTIF','HAPUSS') AND c.status in ('AKTIF','HAPUSS'))a\n" +
            "group by id \n" +
            "order by kode_departemen",
            countQuery = "SELECT COUNT(a.id) AS jumlah\n" +
                    "FROM (select * from\n" +
                    "(SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
                    "FROM karyawan_jabatan AS a \n" +
                    "INNER JOIN jabatan AS b ON a.id_jabatan = b.id\n" +
                    "INNER JOIN departemen AS c ON b.id_departemen = c.id inner join anggaran as d on c.id = d.id_departemen\n" +
                    "WHERE a.id_karyawan = ?1 and d.id_periode_anggaran = ?2  AND a.status = 'AKTIF' AND b.status in ('AKTIF','HAPUSS') AND c.status in ('AKTIF','HAPUSS')\n" +
                    "union\n" +
                    "SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
                    "FROM karyawan_jabatan AS a \n" +
                    "inner join setting_approval_pengajuan_dana as g on g.id_jabatan_approve = a.id_jabatan \n" +
                    "INNER JOIN jabatan AS b ON g.id_jabatan = b.id\n" +
                    "INNER JOIN departemen AS c ON b.id_departemen = c.id inner join anggaran as d on c.id = d.id_departemen\n" +
                    "WHERE a.id_karyawan = ?1 and d.id_periode_anggaran = ?2  AND a.status = 'AKTIF' AND b.status in ('AKTIF','HAPUSS') AND c.status in ('AKTIF','HAPUSS'))a\n" +
                    "group by id) as  a", nativeQuery = true)
    Page<Object> listJabatanAktif2(String idKaryawan, String periodeAnggaran, Pageable pageable);

    @Query(value ="select * from\n" +
            "(SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
            "FROM karyawan_jabatan AS a \n" +
            "INNER JOIN jabatan AS b ON a.id_jabatan = b.id\n" +
            "INNER JOIN departemen AS c ON b.id_departemen = c.id inner join anggaran as d on c.id = d.id_departemen\n" +
            "WHERE a.id_karyawan = ?1 and d.id_periode_anggaran = ?3 AND a.status = 'AKTIF' AND b.status in ('AKTIF','HAPUSS') AND c.status in ('AKTIF','HAPUSS') AND (c.nama_departemen like %?2% or c.kode_departemen like %?2%)\n" +
            "union\n" +
            "SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
            "FROM karyawan_jabatan AS a \n" +
            "inner join setting_approval_pengajuan_dana as g on g.id_jabatan_approve = a.id_jabatan \n" +
            "INNER JOIN jabatan AS b ON g.id_jabatan = b.id\n" +
            "INNER JOIN departemen AS c ON b.id_departemen = c.id inner join anggaran as d on c.id = d.id_departemen\n" +
            "WHERE a.id_karyawan = ?1 and d.id_periode_anggaran = ?3  AND a.status = 'AKTIF' AND b.status in ('AKTIF','HAPUSS') AND c.status in ('AKTIF','HAPUSS') AND (c.nama_departemen like %?2% or c.kode_departemen like %?2%))a\n" +
            "group by id order by kode_departemen",
            countQuery = "SELECT COUNT(a.id) AS jumlah\n" +
                    "FROM (select * from\n" +
                    "(SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
                    "FROM karyawan_jabatan AS a \n" +
                    "INNER JOIN jabatan AS b ON a.id_jabatan = b.id\n" +
                    "INNER JOIN departemen AS c ON b.id_departemen = c.id inner join anggaran as d on c.id = d.id_departemen\n" +
                    "WHERE a.id_karyawan = ?1 and d.id_periode_anggaran = ?3  AND a.status = 'AKTIF' AND b.status in ('AKTIF','HAPUSS') AND c.status in ('AKTIF','HAPUSS') AND (c.nama_departemen like %?2% or c.kode_departemen like %?2%)\n" +
                    "union\n" +
                    "SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
                    "FROM karyawan_jabatan AS a \n" +
                    "inner join setting_approval_pengajuan_dana as g on g.id_jabatan_approve = a.id_jabatan \n" +
                    "INNER JOIN jabatan AS b ON g.id_jabatan = b.id\n" +
                    "INNER JOIN departemen AS c ON b.id_departemen = c.id inner join anggaran as d on c.id = d.id_departemen\n" +
                    "WHERE a.id_karyawan = ?1 and d.id_periode_anggaran = ?3  AND a.status = 'AKTIF' AND b.status in ('AKTIF','HAPUSS') AND c.status in ('AKTIF','HAPUSS') AND (c.nama_departemen like %?2% or c.kode_departemen like %?2%))a\n" +
                    "group by id) as a", nativeQuery = true)
    Page<Object> listJabatanAktifSearch2(String idKaryawan, String search, String periodeAnggaran, Pageable pageable);


    @Query(value ="select * from\n" +
            "(SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
            "FROM karyawan_jabatan AS a \n" +
            "INNER JOIN jabatan AS b ON a.id_jabatan = b.id\n" +
            "INNER JOIN departemen AS c ON b.id_departemen = c.id \n" +
            "WHERE a.id_karyawan = ?1 AND a.status = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF' AND (c.nama_departemen like %?2% or c.kode_departemen like %?2%)\n" +
            "union\n" +
            "SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
            "FROM karyawan_jabatan AS a \n" +
            "inner join setting_approval_pengajuan_dana as g on g.id_jabatan_approve = a.id_jabatan \n" +
            "INNER JOIN jabatan AS b ON g.id_jabatan = b.id\n" +
            "INNER JOIN departemen AS c ON b.id_departemen = c.id\n" +
            "WHERE a.id_karyawan = ?1 AND a.status = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF' AND (c.nama_departemen like %?2% or c.kode_departemen like %?2%))a\n" +
            "group by id order by kode_departemen",
            countQuery = "SELECT COUNT(a.id) AS jumlah\n" +
                    "FROM (select * from\n" +
                    "(SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
                    "FROM karyawan_jabatan AS a \n" +
                    "INNER JOIN jabatan AS b ON a.id_jabatan = b.id\n" +
                    "INNER JOIN departemen AS c ON b.id_departemen = c.id\n" +
                    "WHERE a.id_karyawan = ?1 and a.status = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF' AND (c.nama_departemen like %?2% or c.kode_departemen like %?2%)\n" +
                    "union\n" +
                    "SELECT c.id,c.kode_departemen, c.nama_departemen AS namaDepartemen\n" +
                    "FROM karyawan_jabatan AS a \n" +
                    "inner join setting_approval_pengajuan_dana as g on g.id_jabatan_approve = a.id_jabatan \n" +
                    "INNER JOIN jabatan AS b ON g.id_jabatan = b.id\n" +
                    "INNER JOIN departemen AS c ON b.id_departemen = c.id\n" +
                    "WHERE a.id_karyawan = ?1 and a.status = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF' AND (c.nama_departemen like %?2% or c.kode_departemen like %?2%))a\n" +
                    "group by id) as a", nativeQuery = true)
    Page<Object> listJabatanAktifSearch(String idKaryawan, String search,String idPeriode, Pageable pageable);

}
