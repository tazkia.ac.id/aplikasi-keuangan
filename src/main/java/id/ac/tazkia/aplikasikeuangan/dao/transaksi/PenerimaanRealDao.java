package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.dto.transaksi.PenerimaanBalanceDetailDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.PenerimaanBalanceDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PenerimaanReal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface PenerimaanRealDao extends PagingAndSortingRepository<PenerimaanReal, String>, CrudRepository<PenerimaanReal, String> {

    Page<PenerimaanReal> findByStatusAndPenerimaanPeriodeAnggaranIdOrderByTanggal(StatusRecord statusRecord, String periodeAnggaran, Pageable page);



    List<PenerimaanReal> findByStatusAndPenerimaanPeriodeAnggaranIdOrderByTanggal(StatusRecord statusRecord, String periodeAnggaran);

    List<PenerimaanReal> findByStatusAndPenerimaanPeriodeAnggaranIdAndTanggalBetweenOrderByTanggal(StatusRecord statusRecord, String periodeAnggaran, LocalDate tanggar1, LocalDate tanggal2);

    List<PenerimaanReal> findByStatusAndTanggalBetweenOrderByTanggal(StatusRecord statusRecord, LocalDate tanggar1, LocalDate tanggal2);

    Page<PenerimaanReal> findByStatusAndPenerimaanPeriodeAnggaranIdAndKeteranganContainingIgnoreCaseOrderByTanggal(StatusRecord statusRecord, String periodeAnggaran, String cari, Pageable page);

    Page<PenerimaanReal> findByStatusAndPenerimaanPeriodeAnggaranAndPenerimaanMataAnggaranOrderByTanggal(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, MataAnggaran mataAnggaran, Pageable page);

    Page<PenerimaanReal> findByStatusAndPenerimaanPeriodeAnggaranAndPenerimaanMataAnggaranAndKeteranganContainingIgnoreCaseOrderByTanggal(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, MataAnggaran mataAnggaran, String cari, Pageable page);

    @Query(value = "SELECT coalesce(sum(total),0)as total_penerimaan from penerimaan_real where id_penerimaan_estimasi= ?1 and status='AKTIF' group by id_penerimaan_estimasi", nativeQuery = true)
    BigDecimal getTotalPenerimaanReal(String penerimaan);

    @Query(value = "SELECT coalesce(sum(a.total),0) as total_penerimaan from penerimaan_real as a inner join penerimaan as b on a.id_penerimaan_estimasi = b.id where  b.id_periode = ?1 and a.status='AKTIF' and b.status='AKTIF' group by b.id_periode", nativeQuery = true)
    BigDecimal getTotalRealisasiPenerimaan(String idPeriode);

    @Query(value = "SELECT coalesce(sum(a.total),0) as total_penerimaan from penerimaan_real as a inner join penerimaan as b on a.id_penerimaan_estimasi = b.id where  b.id_periode = ?1 and a.status='AKTIF' and b.status='AKTIF' and a.tanggal between ?2 and ?3 group by b.id_periode", nativeQuery = true)
    BigDecimal getTotalRealisasiPenerimaantgl(String idPeriode, LocalDate tanggal1, LocalDate tanggal2);


    @Query(value = "SELECT coalesce(sum(a.total),0) as total_penerimaan from penerimaan_real as a inner join penerimaan as b on a.id_penerimaan_estimasi = b.id where a.status='AKTIF' and b.status='AKTIF' and a.tanggal between ?1 and ?2 group by b.id_periode", nativeQuery = true)
    BigDecimal getTotalRealisasiPenerimaantgl2(LocalDate tanggal1, LocalDate tanggal2);

    Page<PenerimaanReal> findByStatusAndPenerimaanOrderByTanggal(StatusRecord statusRecord, Penerimaan penerimaan, Pageable page);


    @Query(value = "SELECT a.id_mata_anggaran as id,kode_mata_anggaran as kodeMataAnggaran, nama_mata_anggaran as namaMataAnggaran,estimasi,COALESCE(penerimaan_real,0)AS penerimaan, (COALESCE(penerimaan_real,0) - estimasi) AS margin  FROM\n" +
            "(SELECT b.id AS id_mata_anggaran, kode_mata_anggaran, nama_mata_anggaran, SUM(total)AS estimasi FROM penerimaan AS a \n" +
            "\tINNER JOIN mata_anggaran AS b ON a.id_mata_anggaran = b.id \n" +
            "\tWHERE a.id_periode = ?1 AND a.status = 'AKTIF' AND b.status = 'AKTIF'\n" +
            "\tGROUP BY id_mata_anggaran) a LEFT JOIN\n" +
            "(SELECT b.id_mata_anggaran,SUM(a.total) AS penerimaan_real FROM penerimaan_real AS a \n" +
            "\tINNER JOIN penerimaan AS b ON a.id_penerimaan_estimasi = b.id \n" +
            "\tINNER JOIN mata_anggaran AS c ON b.id_mata_anggaran = c.id\n" +
            "\tWHERE b.id_periode = ?1 AND a.STATUS = 'AKTIF' AND b.status = 'AKTIF' AND c.status = 'AKTIF'\n" +
            "\tGROUP BY b.id_mata_anggaran)b ON a.id_mata_anggaran = b.id_mata_anggaran" , nativeQuery = true)
    List<PenerimaanBalanceDto> balancePenerimaan(String idPeriodeAnggaran);


    @Query(value="SELECT a.id, kode_mata_anggaran AS kodeMataAnggaran, a.keterangan AS keterangan, total AS estimasi, penerimaan, COALESCE(penerimaan,0) - total AS margin FROM\n" +
            "(SELECT a.id,kode_mata_anggaran,a.keterangan,total FROM penerimaan AS a INNER JOIN mata_anggaran AS b ON a.id_mata_anggaran = b.id\n" +
            " WHERE a.id_periode = ?2 AND a.status = 'AKTIF' AND a.id_mata_anggaran = ?1)a\n" +
            "LEFT JOIN\n" +
            "(SELECT b.id AS id_penerimaan_estimasi,SUM(a.total)AS penerimaan FROM penerimaan_real AS a\n" +
            "\t\tINNER JOIN penerimaan AS b ON a.id_penerimaan_estimasi = b.id \n" +
            "\t\tWHERE a.status = 'AKTIF' AND b.status = 'AKTIF' AND b.id_mata_anggaran = ?1 AND b.id_periode = ?2\n" +
            "\t\tGROUP BY id_penerimaan_estimasi)b\n" +
            "ON a.id = b.id_penerimaan_estimasi", nativeQuery = true)
    List<PenerimaanBalanceDetailDto> listPenerimaanBalanaceDetail(String idMataAnggaran, String idPeriodeAnggaran);

}
