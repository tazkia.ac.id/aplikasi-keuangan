package id.ac.tazkia.aplikasikeuangan.dao.masterdata;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface InstansiDao extends PagingAndSortingRepository<Instansi, String>, CrudRepository<Instansi, String> {

    List<Instansi> findByStatusOrderByNama(StatusRecord statusRecord);

    Instansi findByStatusAndId(StatusRecord statusRecord, String id);

    List<Instansi> findByStatusAndIdIn(StatusRecord statusRecord, List<String> id);

    @Query(value = "select id_instansi from departemen where id in ?1 and status = 'AKTIF' group by id_instansi", nativeQuery = true)
    List<String> cariIdInstansi(List<String> idDepartemen);

}
