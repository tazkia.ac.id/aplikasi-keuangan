package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface AnggaranDao extends PagingAndSortingRepository<Anggaran, String>, CrudRepository<Anggaran, String> {

    List<Anggaran> findByStatusAndDepartemenIdAndPeriodeAnggaranIdOrderByKodeAnggaran(StatusRecord statusRecord, String departemen, String periodeAnggaran);

    Page<Anggaran> findByStatusAndDepartemenIdAndPeriodeAnggaranIdAndKodeAnggaranOrNamaAnggaranOrderByKodeAnggaran(StatusRecord statusRecord, String departemen, String periodeAnggaran, String kodeAnggaran, String namaAnggaran, Pageable page);

    Anggaran findByStatusAndId(StatusRecord statusRecord, String idAnggatan);

    List<Anggaran> findByStatusAndPeriodeAnggaranIdAndDepartemenIdInOrderByKodeAnggaran(StatusRecord statusRecord, String periodeAnggaran, List<String> departemen);

    List<Anggaran> findByStatusAndPeriodeAnggaranIdOrderByKodeAnggaran(StatusRecord statusRecord, String periodeAnggaran);

    Integer countByStatusAndPeriodeAnggaran(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran);


    @Query(value = "SELECT a.id as id, a.kode_anggaran as kodeAnggaran, a.nama_anggaran as namaAnggaran ,COALESCE(a.amount,0) AS totalAnggaran,COALESCE(SUM(b.jumlah),0) AS totalPengaluaran, COALESCE(a.amount,0) - COALESCE(SUM(b.jumlah),0) AS sisa FROM " +
            "anggaran AS a LEFT JOIN pengajuan_dana AS b ON a.id=b.id_anggaran WHERE a.id_periode_anggaran= ?1 and a.id_departemen in ?2 and a.status='AKTIF' and b.status not in ('HAPUS','REJECTED')  GROUP BY a.id", nativeQuery = true)
    List<Object[]> getAnggaranDepartemens(String periodeAnggaran, List<String> departemen);

    @Query(value = "select a.id, a.kode_anggaran as kodeAnggaran, a.nama_anggaran as namaAnggaran,COALESCE(a.amount,0) AS totalAnggaran, \n" +
            "sum(coalesce(b.jumlah,a.jumlah,0)) as totalPengeluaran, COALESCE(a.amount,0) - sum(coalesce(b.jumlah,a.jumlah,0)) AS sisa from\n" +
            "(select a.*,c.id as id_pengajuan_dana, c.jumlah from anggaran as a\n" +
            "inner join anggaran_detail as b on a.id = b.id_anggaran\n" +
            "left join (select * from pengajuan_dana where status not in ('HAPUS','CANCELED','REJECTED')) as c on b.id = c.id_anggaran_detail\n" +
            "where a.id_departemen in ?2 and a.status ='AKTIF' and a.id_periode_anggaran = ?1 \n" +
            "and b.status = 'AKTIF')a\n" +
            "left join\n" +
            "(select a.id_pengajuan_dana,b.* from pencairan_dana_detail as a\n" +
            "inner join laporan_dana as b on a.id = b.id_pencairan_dana_detail and b.status not in ('HAPUS','REJECTED','CANCELED')) b on a.id_pengajuan_dana = b.id_pengajuan_dana\n" +
            "group by a.id", nativeQuery = true)
    List<Object[]> getAnggaranDepartemen3(String periodeAnggaran, List<String> departemen);

    @Query(value = "select id, kode_anggaran as kodeAnggaran, nama_anggaran as namaAnggaran, coalesce(amount,0)as totalAnggaran,'-' as totalPengeluaran, '-' as sisa from anggaran\n" +
            "where id_periode_anggaran = ?1 and status = 'AKTIF' and id_departemen in ?2 \n" +
            "order by kode_anggaran", nativeQuery = true)
    List<Object[]> getAnggaranDepartemen(String periodeAnggaran, List<String> departemen);


    @Query(value = "select id, kode_anggaran as kodeAnggaran, nama_anggaran as namaAnggaran, coalesce(amount,0)as totalAnggaran,'-' as totalPengeluaran, '-' as sisa from anggaran\n" +
            "where id_periode_anggaran = ?1 and status = 'AKTIF' \n" +
            "order by kode_anggaran", nativeQuery = true)
    List<Object[]> getAnggaranDepartemens(String periodeAnggaran);


    @Query(value = "select a.id, kodeAnggaran, namaAnggaran, totalAnggaran,sum(coalesce(a.jumlah,b.jumlah,0))as totalPengeluaran, totalAnggaran - sum(coalesce(a.jumlah,b.jumlah,0)) as sisa from\n" +
            "(select a.*,b.jumlah, c.id as idPencairanDanaDetail from\n" +
            "(select a.id,a.kode_anggaran as kodeAnggaran, a.nama_anggaran as namaAnggaran,COALESCE(a.amount,0) AS totalAnggaran, c.id as idAnggaranDetail from anggaran as a \n" +
            "inner join anggaran_proker as b on a.id = b.id_anggaran\n" +
            "inner join anggaran_detail as c on b.id = c.id_anggaran_proker \n" +
            "where a.id_periode_anggaran = ?1 and a.id_departemen in ?2 and a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' group by c.id)as a\n" +
            "left join pengajuan_dana as b on a.idAnggaranDetail = b.id_anggaran_detail\n" +
            "left join pencairan_dana_detail as c on b.id = c.id_pengajuan_dana\n" +
            "where b.status not in ('HAPUS','CANCELED','REJECTED')) as a\n" +
            "left join laporan_dana as b on a.idPencairanDanaDetail = b.id_pencairan_dana_detail\n" +
            "group by a.id\n", nativeQuery = true)
    List<Object[]> getAnggaranDepartemen2(String periodeAnggaran, List<String> departemen);


    @Query(value = "select a.id, a.kode_anggaran as kodeAnggaran, a.nama_anggaran as namaAnggaran,COALESCE(a.amount,0) AS totalAnggaran, \n" +
            "sum(coalesce(b.jumlah,a.jumlah,0)) as totalPengeluaran, COALESCE(a.amount,0) - sum(coalesce(b.jumlah,a.jumlah,0)) AS sisa from\n" +
            "(select a.*,c.id as id_pengajuan_dana, c.jumlah from anggaran as a\n" +
            "inner join anggaran_detail as b on a.id = b.id_anggaran\n" +
            "left join (select * from pengajuan_dana where status not in ('HAPUS','CANCELED','REJECTED')) as c on b.id = c.id_anggaran_detail\n" +
            "where a.status ='AKTIF' and a.id_periode_anggaran = ?1 \n" +
            "and b.status = 'AKTIF')a\n" +
            "left join\n" +
            "(select a.id_pengajuan_dana,b.* from pencairan_dana_detail as a\n" +
            "inner join laporan_dana as b on a.id = b.id_pencairan_dana_detail \n" +
            "inner join pengajuan_dana as c on a.id_pengajuan_dana = c.id\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id\n" +
            "inner join anggaran as e on d.id_anggaran = e.id\n" +
            "where e.id_periode_anggaran = ?1 and b.status not in ('HAPUS','REJECTED','CANCELED')\n" +
            "group by c.id) b on a.id_pengajuan_dana = b.id_pengajuan_dana\n" +
            "group by a.id", nativeQuery = true)
    List<Object[]> getAnggaranDepartemens3(String periodeAnggaran);

    @Query(value = "select a.id, kodeAnggaran, namaAnggaran, totalAnggaran,sum(coalesce(a.jumlah,b.jumlah,0))as totalPengeluaran, totalAnggaran - sum(coalesce(a.jumlah,b.jumlah,0)) as sisa from\n" +
            "(select a.*,b.jumlah, c.id as idPencairanDanaDetail from\n" +
            "(select a.id,a.kode_anggaran as kodeAnggaran, a.nama_anggaran as namaAnggaran,COALESCE(a.amount,0) AS totalAnggaran, c.id as idAnggaranDetail from anggaran as a \n" +
            "inner join anggaran_proker as b on a.id = b.id_anggaran\n" +
            "inner join anggaran_detail as c on b.id = c.id_anggaran_proker \n" +
            "where a.id_periode_anggaran = ?1 and a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' group by c.id)as a\n" +
            "left join pengajuan_dana as b on a.idAnggaranDetail = b.id_anggaran_detail\n" +
            "left join pencairan_dana_detail as c on b.id = c.id_pengajuan_dana\n" +
            "where b.status not in ('HAPUS','CANCELED','REJECTED')) as a\n" +
            "left join laporan_dana as b on a.idPencairanDanaDetail = b.id_pencairan_dana_detail\n" +
            "group by a.id", nativeQuery = true)
    List<Object[]> getAnggaranDepartemens1(String periodeAnggaran);

    @Query(value="SELECT coalesce(sum(amount),0) as total from anggaran where id_departemen= ?1 and status='AKTIF'", nativeQuery = true)
    BigDecimal jumlahAnggaranAktif(String departemen);

    @Query(value = "select a.id, a.kode_anggaran as kodeAnggaran, a.nama_anggaran as namaAnggaran,COALESCE(a.amount,0) AS totalAnggaran, \n" +
            "sum(coalesce(b.jumlah,a.jumlah,0)) as totalPengeluaran, COALESCE(a.amount,0) - sum(coalesce(b.jumlah,a.jumlah,0)) AS sisa from\n" +
            "(select a.*,c.id as id_pengajuan_dana, c.jumlah from anggaran as a\n" +
            "inner join anggaran_detail as b on a.id = b.id_anggaran\n" +
            "left join (select * from pengajuan_dana where status not in ('HAPUS','CANCELED','REJECTED')) as c on b.id = c.id_anggaran_detail\n" +
            "where a.id_departemen = ?1 and a.status ='AKTIF' and a.id_periode_anggaran = ?2 \n" +
            "and b.status = 'AKTIF')a\n" +
            "left join\n" +
            "(select a.id_pengajuan_dana,b.* from pencairan_dana_detail as a\n" +
            "inner join laporan_dana as b on a.id = b.id_pencairan_dana_detail) b on a.id_pengajuan_dana = b.id_pengajuan_dana\n" +
            "group by a.id",nativeQuery = true)
    List<Object> realisasiBudget(String idDepartemen, String idPeriodeAnggaran);

    @Query(value = "select a.id_anggaran as id, a.id as id_anggaran_detail, a.kode_anggaran as kodeAnggaran, a.deskripsi as namaAnggaran,COALESCE(a.amount * a.kuantitas ,0) AS totalAnggaran, \n" +
            "sum(coalesce(b.jumlah,a.jumlah,0)) as totalPengeluaran, COALESCE(a.amount * a.kuantitas,0) - sum(coalesce(b.jumlah,a.jumlah,0)) AS sisa from\n" +
            "(select b.*,c.id as id_pengajuan_dana, c.jumlah, a.kode_anggaran from anggaran as a\n" +
            "inner join anggaran_detail as b on a.id = b.id_anggaran\n" +
            "left join (select * from pengajuan_dana where status not in ('HAPUS','CANCELED','REJECTED')) as c on b.id = c.id_anggaran_detail\n" +
            "where a.id_departemen = ?1 and a.status ='AKTIF' and a.id_periode_anggaran = ?2 \n" +
            "and b.status = 'AKTIF' )a\n" +
            "left join\n" +
            "(select a.id_pengajuan_dana,b.* from pencairan_dana_detail as a\n" +
            "inner join laporan_dana as b on a.id = b.id_pencairan_dana_detail where b.status not in ('REJECTED','CANCELED','HAPUS')) b on a.id_pengajuan_dana = b.id_pengajuan_dana\n" +
            "group by a.id", nativeQuery = true)
    List<Object> realisasiAnggaranDetail(String idDepartemen, String idPeriodeAnggaran);

}
