package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Satuan;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SatuanDao extends PagingAndSortingRepository<Satuan, String>, CrudRepository<Satuan, String> {


}
