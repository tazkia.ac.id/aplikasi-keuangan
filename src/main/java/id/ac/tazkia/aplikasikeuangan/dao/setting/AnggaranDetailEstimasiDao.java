package id.ac.tazkia.aplikasikeuangan.dao.setting;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import id.ac.tazkia.aplikasikeuangan.dto.report.RealisasiEstimasiBulananDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.ac.tazkia.aplikasikeuangan.dto.export.ExportAnggaranDetailEstimasiDepartemenDto;
import id.ac.tazkia.aplikasikeuangan.dto.report.EstimasiPengeluaranDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetailEstimasi;

public interface AnggaranDetailEstimasiDao extends PagingAndSortingRepository<AnggaranDetailEstimasi, String>, CrudRepository<AnggaranDetailEstimasi, String> {

    Page<AnggaranDetailEstimasi> findByStatusAndAnggaranDetailOrderByTanggalEstimasi(StatusRecord statusRecord, AnggaranDetail anggaranDetail, Pageable page);

    List<AnggaranDetailEstimasi> findByStatusAndAnggaranDetailOrderByTanggalEstimasi(StatusRecord statusRecord, AnggaranDetail anggaranDetail);

    @Query(value = "select coalesce(sum(kuantitas),0.00) as kuantitas from anggaran_detail_estimasi where status='AKTIF' and id_anggaran_detail = ?1", nativeQuery = true)
    BigDecimal getKuantitasAnggaranDetailEstiamasi(String idRencanaPenerimaan);

    @Query(value = "select coalesce(sum(kuantitas * amount),0.00) as total from anggaran_detail_estimasi where status='AKTIF' and id_anggaran_detail = ?1", nativeQuery = true)
    BigDecimal getAmountAnggaranDetailEstimasi(String idAnggaranDetail);

    @Query(value = "select coalesce(sum(kuantitas * amount),0.00) as total from anggaran_detail_estimasi where status='AKTIF' and id_anggaran_detail = ?1 and month(tanggal_estimasi) = month(now()) and year(tanggal_estimasi) = year(now())", nativeQuery = true)
    BigDecimal getAmountAnggaranDetailEstimasiBulan(String idAnggaranDetail);


    @Query(value = "select c.kode_anggaran as kodeAnggaran, c.nama_anggaran as namaAnggaran, b.deskripsi as  desDetail, " +
            "a.deskripsi as  desEstimasi, a.tanggal_estimasi as tanggalEstimasi, a.kuantitas as  kuantitas, a.satuan as satuan, a.amount as amount,\n" +
            "a.kuantitas * a.amount as jumlah from anggaran_detail_estimasi as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' \n" +
            "and a.tanggal_estimasi between ?1 and ?2 \n" +
            "order by a.tanggal_estimasi\n", nativeQuery = true)
    List<EstimasiPengeluaranDto> listEstimasiPengeluaran(LocalDate dariTanggal, LocalDate sampaiTanggal);

    @Query(value = "select c.kode_anggaran as kodeAnggaran, c.nama_anggaran as namaAnggaran, b.deskripsi as  desDetail, \n" +
            "a.deskripsi as  desEstimasi, a.tanggal_estimasi as tanggalEstimasi, a.kuantitas as  kuantitas, a.satuan as satuan, a.amount as amount,\n" +
            "a.kuantitas * a.amount as jumlah from anggaran_detail_estimasi as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id \n" +
            "inner join departemen as d on c.id_departemen = d.id \n" +
            "inner join periode_anggaran as e on c.id_periode_anggaran = e.id and e.id = ?4 \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' \n" +
            "and d.id_instansi = ?3 and a.tanggal_estimasi between ?1 and ?2 \n" +
            "order by a.tanggal_estimasi", nativeQuery = true)
    List<EstimasiPengeluaranDto> listEstimasiPengeluaranInstansi(LocalDate dariTanggal, LocalDate sampaiTanggal, String idInstansi, String idPeriode);

    @Query(value = "select c.kode_anggaran as kodeAnggaran, c.nama_anggaran as namaAnggaran, b.deskripsi as  desDetail, \n" +
            "a.deskripsi as  desEstimasi, a.tanggal_estimasi as tanggalEstimasi, a.kuantitas as  kuantitas, a.satuan as satuan, a.amount as amount,\n" +
            "a.kuantitas * a.amount as jumlah from anggaran_detail_estimasi as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id \n" +
            "inner join departemen as d on c.id_departemen = d.id \n" +
            "inner join periode_anggaran as e on c.id_periode_anggaran = e.id and e.id = ?5 \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' \n" +
            "and d.id_instansi = ?3  and d.id = ?4 and a.tanggal_estimasi between ?1 and ?2 \n" +
            "order by a.tanggal_estimasi", nativeQuery = true)
    List<EstimasiPengeluaranDto> listEstimasiPengeluaranInstansiDepartemen(LocalDate dariTanggal, LocalDate sampaiTanggal, String idInstansi, String idDepartemen, String idPeriode);

    @Query(value = "select sum(a.kuantitas * a.amount) as total from anggaran_detail_estimasi as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' \n" +
            "and a.tanggal_estimasi between ?1 and ?2", nativeQuery = true)
    BigDecimal totalEstimasiPengeluaran(LocalDate dariTanggal, LocalDate sampaiTanggal);

    @Query(value = "select sum(a.kuantitas * a.amount) as total from anggaran_detail_estimasi as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id \n" +
            "inner join departemen as d on c.id_departemen = d.id \n" +
            "inner join periode_anggaran as e on c.id_periode_anggaran = e.id and e.id = ?4 \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' \n" +
            "and d.id_instansi = ?3 and a.tanggal_estimasi between ?1 and ?2", nativeQuery = true)
    BigDecimal totalEstimasiPengeluaranInstansi(LocalDate dariTanggal, LocalDate sampaiTanggal, String instansi, String idPeriode);

    @Query(value = "select sum(a.kuantitas * a.amount) as total from anggaran_detail_estimasi as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id \n" +
            "inner join departemen as d on c.id_departemen = d.id \n" +
            "inner join periode_anggaran as e on c.id_periode_anggaran = e.id and e.id = ?5 \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' \n" +
            "and d.id_instansi = ?3 and d.id = ?4 and a.tanggal_estimasi between ?1 and ?2", nativeQuery = true)
    BigDecimal totalEstimasiPengeluaranInstansiDepartemen(LocalDate dariTanggal, LocalDate sampaiTanggal, String instansi, String departemen, String idPeropde);



    @Query(value = "select d.kode_anggaran as kodeAnggaran,d.nama_anggaran as namaAnggaran,c.program_kerja as programKerja, b.deskripsi as  deskripsiDetail,\n" +
            "a.deskripsi as deskripsiEstimasi, a.tanggal_estimasi as tanggalEstimasi, a.kuantitas as kuantitas, a.satuan as satuan, a.amount as amount,\n" +
            "a.kuantitas * a.amount as jumlah \n" +
            "from anggaran_detail_estimasi as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran_proker as c on b.id_anggaran_proker = c.id\n" +
            "inner join anggaran as d on c.id_anggaran = d.id\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' and d.status = 'AKTIF'\n" +
            "and d.id_departemen = ?1 and d.id_periode_anggaran = ?2\n" +
            "order by d.kode_anggaran", nativeQuery = true)
    List<ExportAnggaranDetailEstimasiDepartemenDto> exportAnggaranDetailEstimasiDepartemen(String departemen, String periode);


        @Query(value = "select coalesce(estimasi_penerimaan,0) - coalesce(estimasi_pengeluaran,0) as sisa_estimasi from \n" +
        "(select year(tanggal) as tahun, month(tanggal) as bulan, sum(total) as estimasi_penerimaan from estimasi_penerimaan \n" +
        "where status = 'AKTIF' and year(tanggal) = ?1 and month(tanggal) = ?2) as a \n" +
        "left join \n" +
        "(select year(tanggal_estimasi) as tahun, month(tanggal_estimasi) as bulan, sum(kuantitas * amount) as estimasi_pengeluaran from anggaran_detail_estimasi \n" +
        "where status = 'AKTIF' and year(tanggal_estimasi) = ?1 and month(tanggal_estimasi)=?2) as b \n" +
        "on a.tahun = b.tahun and a.bulan = b.bulan", nativeQuery = true)
        BigDecimal cariSisaEstimasi(String tahun, String bulan);


        @Query(value = "select coalesce(sum(coalesce(kuantitas,0) * coalesce(amount,0)),0) as total_estimasi from anggaran_detail_estimasi where id_anggaran_detail = ?1 and status = 'AKTIF'", nativeQuery = true)
        BigDecimal cariSisaEstimasiPengeluaran(String idAnggaranDetail);


        @Query(value = "select id, tanggal_estimasi as tanggalEstimasi, nama_departemen as namaDepartemen, kode_anggaran as kodeAnggaran, nama_anggaran as namaAnggaran,\n" +
                "deskripsi, estimasi, total_estimasi as totalEstimasi, coalesce(pengajuan,0) as realisasi, if(tanggal_estimasi <= date(now()), 'DUE DATE EXCEEDED','ACTIVE') as status from\n" +
                "(select b.id,a.tanggal_estimasi, d.nama_departemen, c.kode_anggaran,c.nama_anggaran, b.deskripsi, a.deskripsi as estimasi, sum(a.kuantitas * a.amount) as total_estimasi \n" +
                "from anggaran_detail_estimasi as a\n" +
                "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
                "inner join anggaran as c on b.id_anggaran = c.id\n" +
                "inner join departemen as d on c.id_departemen = d.id\n" +
                "where c.id_periode_anggaran = ?1 and d.id_instansi in (?2) \n" +
                "and a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF'\n" +
                "and month(a.tanggal_estimasi)=month(now()) and year(a.tanggal_estimasi) = year(now()) group by a.id_anggaran_detail) as a\n" +
                "left join (select id_anggaran_detail, sum(jumlah) as pengajuan from pengajuan_dana  where month(tanggal_pengajuan) = month(now()) \n" +
                "and year(tanggal_pengajuan) = year(now()) and status not in ('HAPUS','CANCELED','REJECTED') group by id_anggaran_detail) as b on a.id = b.id_anggaran_detail", nativeQuery = true)
        List<RealisasiEstimasiBulananDto> listRealisasiEstimasiBulanan(String idPeriodeAnggaran, List<String> idInstansi);

}
