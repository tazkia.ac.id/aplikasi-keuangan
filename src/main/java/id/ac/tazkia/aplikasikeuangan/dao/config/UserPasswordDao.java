package id.ac.tazkia.aplikasikeuangan.dao.config;

import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.config.UserPassword;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserPasswordDao extends PagingAndSortingRepository<UserPassword, String>, CrudRepository<UserPassword, String> {
    UserPassword findByUser(User user);
}
