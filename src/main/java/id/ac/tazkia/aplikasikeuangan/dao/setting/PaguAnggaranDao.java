package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PaguAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface PaguAnggaranDao extends PagingAndSortingRepository<PaguAnggaran, String>, CrudRepository<PaguAnggaran, String> {

    Page<PaguAnggaran> findByStatusAndPeriodeAnggaranOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, Pageable page);

    Page<PaguAnggaran> findByStatusAndDepartemenIdInAndPeriodeAnggaranOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, List<String> idInstansi, PeriodeAnggaran periodeAnggaran, Pageable page);

    Page<PaguAnggaran> findByStatusAndPeriodeAnggaranAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, String namaDepartemen, Pageable page);

    Page<PaguAnggaran> findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, List<String> idInstansi, PeriodeAnggaran periodeAnggaran, String namaDepartemen, Pageable page);

    Page<PaguAnggaran> findByStatusAndPeriodeAnggaranStatusOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, StatusRecord statusPeriode, Pageable page);

    Page<PaguAnggaran> findByStatusAndPeriodeAnggaranIdOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, String idPeriodeAnggaran, Pageable page);

    Page<PaguAnggaran> findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, List<String> idInstansi, String idPeriodeAnggaran, Pageable page);

    Page<PaguAnggaran> findByStatusAndPeriodeAnggaranStatusAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, StatusRecord statusPeriode, String namaDepartemen, Pageable page);

    Page<PaguAnggaran> findByStatusAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, String idPeriodeAnggaran, String namaDepartemen, Pageable page);

    Page<PaguAnggaran> findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemenNamaDepartemen(StatusRecord statusRecord, List<String> idInstansi, String idPeriodeAnggaran, String namaDepartemen, Pageable page);

    @Query(value = "SELECT coalesce(sum(jumlah),0)as total_anggaran from pic_anggaran where  id_periode_anggaran= ?1 and status='AKTIF' group by id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalAnggaranPeriodeAktif(String periode);

    @Query(value = "SELECT coalesce(sum(pagu_anggaran_amount),0)as total_pagu_anggaran from pagu_anggaran where id_periode_anggaran= ?1 and status='AKTIF' and ambil = 'NETT' group by id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalPaguAnggaran(String periode);

    @Query(value = "SELECT coalesce(sum(pagu_anggaran_amount),0)as total_pagu_anggaran from pagu_anggaran as a inner join departemen as b on a.id_departemen = b.id where a.id_periode_anggaran= ?1 and b.id_instansi in ?2 and a.status='AKTIF' and a.ambil = 'NETT' group by id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalPaguAnggaranInstansi(String periode, List<String> idInstansi);

    @Query(value = "SELECT coalesce(sum(pagu_anggaran_amount),0)as total_pagu_anggaran from pagu_anggaran where id_periode_anggaran= ?1 and id_departemen = ?2 and status='AKTIF' group by id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalPaguAnggaranDepartemen(String periode, String departemen);


    PaguAnggaran findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord statusRecord, String periodeAnggaran, String departemen);



}
