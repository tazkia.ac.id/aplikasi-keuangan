package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.dto.transaksi.SaldoPendapatanTerkaitDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengeluaranTerkait;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PengeluaranTerkaitDao extends PagingAndSortingRepository<PengeluaranTerkait, String>, CrudRepository<PengeluaranTerkait, String> {

    Page<PengeluaranTerkait> findByStatusAndEstimasiPendapatanTerkaitOrderByTanggalDesc(StatusRecord statusRecord, EstimasiPendapatanTerkait estimasiPendapatanTerkait, Pageable pageable);


    @Query(value = "SELECT * FROM \n" +
            "(SELECT a.id,tanggal AS tgl,a.tanggal_insert as tanggalInsert,deskripsi, file,jumlah,a.satuan,total AS masuk,0 AS keluar,\n" +
            "(SELECT COALESCE(SUM(total), 0) FROM pendapatan_terkait AS aa INNER JOIN estimasi_pendapatan_terkait AS bb ON aa.id_estimasi_pendapatan_terkait = bb.id\n" +
            "WHERE aa.status = 'AKTIF' AND bb.id_periode_anggaran = ?1 and bb.id_instansi in (?2) AND \n" +
            "UNIX_TIMESTAMP(CONCAT(aa.tanggal, ' ', TIME(aa.tanggal_insert))) <= UNIX_TIMESTAMP(CONCAT(a.tanggal, ' ', TIME(a.tanggal_insert)))) - (SELECT COALESCE(SUM(total), 0)\n" +
            "FROM pengeluaran_terkait AS aa INNER JOIN estimasi_pendapatan_terkait AS bb ON aa.id_estimasi_pendapatan_terkait = bb.id \n" +
            "WHERE aa.status = 'AKTIF' AND bb.id_periode_anggaran = ?1 and bb.id_instansi in (?2) AND UNIX_TIMESTAMP(concat(aa.tanggal, ' ', TIME(aa.tanggal_insert))) <= UNIX_TIMESTAMP(CONCAT(a.tanggal, ' ', TIME(a.tanggal_insert)))\n" +
            ") AS saldo, 'MASUK' as status FROM pendapatan_terkait AS a INNER JOIN estimasi_pendapatan_terkait AS b ON a.id_estimasi_pendapatan_terkait = b.id\n" +
            "WHERE a.status = 'AKTIF' AND b.id_periode_anggaran = ?1 and b.id_instansi in (?2) \n" +
            "UNION\n" +
            "SELECT a.id,tanggal AS tgl,a.tanggal_insert as tanggalInsert,deskripsi, file,jumlah,a.satuan,0 AS masuk,total AS keluar,\n" +
            "(SELECT COALESCE(SUM(total), 0) FROM pendapatan_terkait AS aa\n" +
            "INNER JOIN estimasi_pendapatan_terkait AS bb ON aa.id_estimasi_pendapatan_terkait = bb.id\n" +
            "WHERE aa.status = 'AKTIF' AND bb.id_periode_anggaran = ?1 and bb.id_instansi in (?2) AND UNIX_TIMESTAMP(CONCAT(aa.tanggal, ' ', TIME(aa.tanggal_insert))) <= UNIX_TIMESTAMP(CONCAT(a.tanggal, ' ', TIME(a.tanggal_insert)))\n" +
            ") - (SELECT COALESCE(SUM(total), 0) FROM pengeluaran_terkait AS aa\n" +
            "INNER JOIN estimasi_pendapatan_terkait AS bb ON aa.id_estimasi_pendapatan_terkait = bb.id\n" +
            "WHERE aa.status = 'AKTIF' AND bb.id_periode_anggaran = ?1 and bb.id_instansi in (?2) AND UNIX_TIMESTAMP(CONCAT(aa.tanggal, ' ', TIME(aa.tanggal_insert))) <= UNIX_TIMESTAMP(CONCAT(a.tanggal, ' ', TIME(a.tanggal_insert)))\n" +
            ") AS saldo,'KELUAR' as status FROM pengeluaran_terkait AS a INNER JOIN estimasi_pendapatan_terkait AS b ON a.id_estimasi_pendapatan_terkait = b.id\n" +
            "WHERE a.status = 'AKTIF' AND b.id_periode_anggaran = ?1 and b.id_instansi in (?2) \n" +
            ") AS a ORDER BY UNIX_TIMESTAMP(CONCAT(tgl, ' ', TIME(tanggalInsert))) \n", nativeQuery = true)
    Page<SaldoPendapatanTerkaitDto> saldoPendapatanTerkait(String idPeriodeAnggaran, List<String> idInstansi, Pageable pageable);

}
