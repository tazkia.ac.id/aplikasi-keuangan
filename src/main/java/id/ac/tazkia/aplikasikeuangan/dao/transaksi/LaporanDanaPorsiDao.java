package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDanaPorsi;
import org.hibernate.sql.Update;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LaporanDanaPorsiDao extends PagingAndSortingRepository<LaporanDanaPorsi, String>, CrudRepository<LaporanDanaPorsi, String> {

    @Query(value = "update laporan_dana_porsi set status = 'NONAKTIF' where id_laporan_dana = ?1 and status = 'AKTIF'", nativeQuery = true)
    Update hapusLaporanDanaPorsi(String idLaporanDana);

}
