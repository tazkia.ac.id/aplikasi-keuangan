package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.RencanaPenerimaan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RencanaPenerimaanDao extends PagingAndSortingRepository<RencanaPenerimaan, String>, CrudRepository<RencanaPenerimaan, String> {

    Page<RencanaPenerimaan> findByStatusAndPeriodeAnggaranOrderByMataAnggaran(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, Pageable page);

    Page<RencanaPenerimaan> findByStatusAndPeriodeAnggaranAndMataAnggaranNamaMataAnggaranOrderByMataAnggaran(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, String namaMataAnggaran, Pageable page);

    Page<RencanaPenerimaan> findByStatusAndPeriodeAnggaranStatusOrderByMataAnggaran(StatusRecord statusRecord,StatusRecord statusPeriodeAnggaran, Pageable page);

    Page<RencanaPenerimaan> findByStatusAndPeriodeAnggaranStatusAndMataAnggaranNamaMataAnggaranOrderByMataAnggaran(StatusRecord statusRecord, StatusRecord statusPeriodeAnggaran, String namaMataAnggaran, Pageable page);

    RencanaPenerimaan findByStatusAndPeriodeAnggaranAndMataAnggaran(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, MataAnggaran mataAnggaran);
}
