package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.ImporTransaksiAllDto;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface AnggaranCrudDao extends CrudRepository<Anggaran, String>, PagingAndSortingRepository<Anggaran, String> {

    @Query(value = "SELECT coalesce(sum(amount),0)as total_anggaran from anggaran where  id_departemen= ?1 and id_periode_anggaran= ?2 and status='AKTIF' group by id_departemen,id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalAnggaran(String departemen, String periode);


    @Query(value = "SELECT coalesce(sum(jumlah),0)as total_anggaran from pic_anggaran where  id_periode_anggaran= ?1 and status='AKTIF' group by id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalAnggaranPeriodeAktif(String periode);

    @Query(value = "SELECT coalesce(sum(jumlah),0)as total_anggaran from pic_anggaran as a inner join departemen as b on a.id_departemen = b.id where  a.id_periode_anggaran= ?1 and b.id_instansi in ?2 and a.status='AKTIF' group by a.id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalAnggaranPeriodeAktifInstansi(String periode, List<String> idInstansi);

    @Query(value = "select a.id_departemen as id,c.nama_departemen as namaDepartemen,a.total_anggaran as totalAnggaran,coalesce(b.pengeluaran,0.00) as pengeluaran, round(((100/total_anggaran)* coalesce(pengeluaran,0.00)),2) as persentase from " +
            "(select sum(amount) as total_anggaran, id_periode_anggaran, id_departemen from anggaran where id_periode_anggaran= ?1 group by id_departemen) a Left join " +
            "(select sum(jumlah) as pengeluaran, bb.id_departemen from pengajuan_dana as aa inner join anggaran as bb on aa.id_anggaran=bb.id where bb.id_periode_anggaran= ?1 and aa.status not in('HAPUS','REJECTED','CANCEL') group by bb.id_departemen) b " +
            "on a.id_departemen = b.id_departemen inner join " +
            "departemen as c on a.id_departemen=c.id order by round(((100/total_anggaran)* coalesce(pengeluaran,0.00)),2) desc", nativeQuery = true)
    List<SisaAnggaranDashboardDto> getSisaAnggaranPerDepartemen2(String periodeAnggaran);

    @Query(value = "select a.id_departemen as id,c.nama_departemen as namaDepartemen, a.total_anggaran as totalAnggaran, coalesce(b.pengeluaran,0.00) as pengeluaran, coalesce (round(((100/total_anggaran)* coalesce(pengeluaran,0.00)),2),0) as persentase from\n" +
            "(select sum(amount) as total_anggaran, id_periode_anggaran, id_departemen from anggaran where id_periode_anggaran= ?1 group by id_departemen)a left join\n" +
            "(select id_departemen,sum(coalesce(realisasi,pencairan,minta_pencairan,pengajuan,0))as pengeluaran from\n" +
            "(select aa.id as id_pengajuan_dana,aa.id_departemen,sum(aa.jumlah)as pengajuan,cc.nominal_minta_pencairan as minta_pencairan, cc.nominal_pencairan as pencairan,sum(dd.jumlah)as realisasi\n" +
            "from pengajuan_dana as aa left join pencairan_dana_detail as bb on aa.id = bb.id_pengajuan_dana left join pencairan_dana as cc\n" +
            "on bb.id_pencairan_dana = cc.id left join laporan_dana as dd on bb.id=dd.id_pencairan_dana_detail inner join anggaran as ee on aa.id_anggaran=ee.id inner join departemen as ff on aa.id_departemen=ff.id where aa.status not in('HAPUS','REJECTED','CANCELED') and ff.status='AKTIF' and ee.status='AKTIF' and ee.id_periode_anggaran= ?1 group by coalesce(bb.id_pencairan_dana,aa.id))xx group by id_departemen)b\n" +
            "on a.id_departemen=b.id_departemen inner join\n" +
            "departemen as c on a.id_departemen=c.id where c.status='AKTIF' order by round(((100/total_anggaran)* coalesce(pengeluaran,0.00)),2) desc\n", nativeQuery = true)
    List<SisaAnggaranDashboardDto> getSisaAnggaranPerDepartemen1(String periodeAnggaran);

//    @Query(value = "   select a.id_departemen as id,c.nama_departemen as namaDepartemen, a.total_anggaran as totalAnggaran, coalesce(b.pengeluaran,0.00) as pengeluaran, coalesce (round(((100/total_anggaran)* coalesce(pengeluaran,0.00)),2),0) as persentase from\n" +
//            "\t\t\t(select pagu_anggaran_amount as total_anggaran, id_periode_anggaran, id_departemen from pagu_anggaran where id_periode_anggaran= ?1 and status='AKTIF')a left join\n" +
//            "\t\t\t(select id_departemen,sum(isi) as pengeluaran from \n" +
//            "\t\t\t(select aa.id as id_pengajuan_dana,ff.id as id_departemen,sum(coalesce(dd.jumlah,cc.nominal_pencairan,aa.jumlah,0))as isi\n" +
//            "\t\t\tfrom pengajuan_dana as aa left join pencairan_dana_detail as bb on aa.id = bb.id_pengajuan_dana left join pencairan_dana as cc \n" +
//            "\t\t\ton bb.id_pencairan_dana = cc.id left join laporan_dana as dd on bb.id=dd.id_pencairan_dana_detail inner join anggaran_detail as ee on aa.id_anggaran_detail=ee.id \n" +
//            "\t\t\tinner join anggaran as gg on ee.id_anggaran=gg.id inner join departemen as ff on gg.id_departemen=ff.id where aa.status not in('HAPUS','REJECTED','CANCELED') and ff.status='AKTIF' and ee.status='AKTIF' \n" +
//            "\t\t\tand gg.id_periode_anggaran= ?1 group by coalesce(ff.id))xx group by id_departemen)b\n" +
//            "\t\t\ton a.id_departemen=b.id_departemen \n" +
//            "\t\t\tleft join departemen as c on a.id_departemen=c.id where c.status='AKTIF' order by round(((100/total_anggaran)* coalesce(pengeluaran,0.00)),2) desc\n" +
//            "            ", nativeQuery = true)
//    List<SisaAnggaranDashboardDto> getSisaAnggaranPerDepartemen2(String periodeAnggaran);
//

    @Query(value = "select aa.id as id, namaDepartemen, totalAnggaran, pengeluaran, persentase,coalesce(bb.pagu_anggaran_amount,0) as paguAnggaran,coalesce(bb.pagu_anggaran_amount,0) - totalAnggaran as sisaPagu from (select bbb.id_departemen as id, nama_departemen as namaDepartemen, total_anggaran as totalAnggaran,coalesce(pengeluaran, 0)as pengeluaran, \n" +
            "COALESCE(ROUND(((COALESCE(pengeluaran,0.00) * 100) / COALESCE(total_anggaran,0.00)),2),0.00) AS persentase from\n" +
            "(select id_departemen,sum(jumlah)as pengeluaran from\n" +
            "(select id_departemen, coalesce(jumlah_laporan,nominal_pencairan,nominal_minta_pencairan,jumlah_pengajuan)as jumlah from\n" +
            "(select c.id_departemen ,a.id as id_pengajuan,a.jumlah as jumlah_pengajuan,d.id,e.id as id_pencairan, e.nominal_minta_pencairan,e.nominal_pencairan,f.id as id_laoran,f.jumlah as jumlah_laporan from pengajuan_dana as a \n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id\n" +
            "left join pencairan_dana_detail as d on a.id = d.id_pengajuan_dana\n" +
            "left join pencairan_dana as e on d.id_pencairan_dana = e.id\n" +
            "left join (select * from laporan_dana where status not in ('HAPUS','REJECTED','CANCELED')) as f on d.id = f.id_pencairan_dana_detail\n" +
            "where c.id_periode_anggaran = ?1 and a.status not in  ('HAPUS','REJECTED','CANCELED') \n" +
            "group by coalesce(f.id,e.id,a.id))aa order by id_departemen)bb\n" +
            "group by id_departemen\n" +
            "order by pengeluaran desc)aaa\n" +
            "right join\n" +
            "(select id_departemen,nama_Departemen,sum(amount)as total_anggaran from anggaran as a\n" +
            "inner join departemen as b on a.id_departemen = b.id where id_periode_anggaran = ?1 and a.status='AKTIF' and b.status='AKTIF' group by id_departemen)bbb\n" +
            "on aaa.id_departemen=bbb.id_departemen) aa left join " +
            "(select * from pagu_anggaran where status = 'AKTIF' and id_periode_anggaran = ?1) bb on aa.id = bb.id_departemen order by persentase desc", nativeQuery = true)
    List<SisaAnggaranDashboardDto> getSisaAnggaranPerDepartemen(String periodeAnggaran);

    @Query(value = "select aaa.id_departemen as id, nama_departemen as namaDepartemen, total_anggaran as totalAnggaran,coalesce(pengeluaran, 0)as pengeluaran, \n" +
            "COALESCE(ROUND(((COALESCE(pengeluaran,0.00) * 100) / COALESCE(total_anggaran,0.00)),2),0.00) AS persentase from\n" +
            "(select id_departemen,sum(jumlah)as pengeluaran from\n" +
            "(select id_departemen, coalesce(jumlah_laporan,nominal_pencairan,nominal_minta_pencairan,jumlah_pengajuan)as jumlah from\n" +
            "(select c.id_departemen ,a.id as id_pengajuan,a.jumlah as jumlah_pengajuan,d.id,e.id as id_pencairan, e.nominal_minta_pencairan,e.nominal_pencairan,f.id as id_laoran,f.jumlah as jumlah_laporan from pengajuan_dana as a \n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id\n" +
            "left join pencairan_dana_detail as d on a.id = d.id_pengajuan_dana\n" +
            "left join pencairan_dana as e on d.id_pencairan_dana = e.id\n" +
            "left join (select * from laporan_dana where status not in ('HAPUS','REJECTED','CANCELED')) as f on d.id = f.id_pencairan_dana_detail\n" +
            "where c.id_periode_anggaran = ?1 and a.status not in  ('HAPUS','REJECTED','CANCELED') \n" +
            "group by coalesce(f.id,e.id,a.id))aa order by id_departemen)bb\n" +
            "group by id_departemen\n" +
            "order by pengeluaran desc)aaa\n" +
            "right join\n" +
            "(select id_departemen,nama_Departemen,sum(amount)as total_anggaran from anggaran as a\n" +
            "inner join departemen as b on a.id_departemen = b.id where id_periode_anggaran = ?1 and a.status='AKTIF' and b.status='AKTIF' group by id_departemen)bbb\n" +
            "on aaa.id_departemen=bbb.id_departemen order by persentase desc",
            countQuery = "SELECT COUNT(id_departemen) AS nomor FROM\n" +
                    "(SELECT id_departemen FROM anggaran WHERE id_periode_anggaran = ?1 AND STATUS ='AKTif' GROUP BY id_departemen)aa", nativeQuery = true)
    Page<SisaAnggaranDashboardDto> getSisaAnggaranPerDepartemenPage(String periodeAnggaran, Pageable pageable);


//    @Query(value = "select aaa.id_departemen as id, nama_departemen as namaDepartemen, total_anggaran as totalAnggaran,coalesce(pengeluaran, 0)as pengeluaran, \n" +
//            "COALESCE(ROUND(((COALESCE(pengeluaran,0.00) * 100) / COALESCE(total_anggaran,0.00)),2),0.00) AS persentase from\n" +
//            "(select id_departemen,sum(jumlah)as pengeluaran from\n" +
//            "(select id_departemen, coalesce(jumlah_laporan,nominal_pencairan,nominal_minta_pencairan,jumlah_pengajuan)as jumlah from\n" +
//            "(select c.id_departemen ,a.id as id_pengajuan,a.jumlah as jumlah_pengajuan,d.id,e.id as id_pencairan, e.nominal_minta_pencairan,e.nominal_pencairan,f.id as id_laoran,f.jumlah as jumlah_laporan from pengajuan_dana as a \n" +
//            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
//            "inner join anggaran as c on b.id_anggaran = c.id\n" +
//            "left join pencairan_dana_detail as d on a.id = d.id_pengajuan_dana\n" +
//            "left join pencairan_dana as e on d.id_pencairan_dana = e.id\n" +
//            "left join (select * from laporan_dana where status not in ('HAPUS','REJECTED','CANCELED')) as f on d.id = f.id_pencairan_dana_detail\n" +
//            "where c.id_periode_anggaran = ?1 and a.status not in  ('HAPUS','REJECTED','CANCELED') \n" +
//            "group by coalesce(f.id,e.id,a.id))aa order by id_departemen)bb\n" +
//            "group by id_departemen\n" +
//            "order by pengeluaran desc)aaa\n" +
//            "right join\n" +
//            "(select id_departemen,nama_Departemen,sum(amount)as total_anggaran from anggaran as a\n" +
//            "inner join departemen as b on a.id_departemen = b.id where id_periode_anggaran = ?1 and a.status='AKTIF' and b.status='AKTIF' group by id_departemen)bbb\n" +
//            "on aaa.id_departemen=bbb.id_departemen order by persentase desc", nativeQuery = true)
//    Page<SisaAnggaranDashboardDto> getSisaAnggaranPerDepartemenl1(String periodeAnggaran);


    @Query(value = "select sum(coalesce(b.jumlah,a.jumlah)) as pengeluaran from\n" +
            "(select c.* from anggaran as a\n" +
            "inner join anggaran_detail as b on a.id = b.id_anggaran\n" +
            "left join (select * from pengajuan_dana where status not in ('HAPUS','CANCELED','REJECTED')) as c on b.id = c.id_anggaran_detail\n" +
            "where a.id_departemen = ?2 and a.status ='AKTIF' and a.id_periode_anggaran = ?1 \n" +
            "and b.status = 'AKTIF' and c.status not in ('HAPUS','CANCELED','REJECTED'))a\n" +
            "left join\n" +
            "(select a.id_pengajuan_dana,b.* from pencairan_dana_detail as a\n" +
            "inner join laporan_dana as b on a.id = b.id_pencairan_dana_detail where b.status not in ('REJECTED','CANCELED','HAPUS')) b on a.id = b.id_pengajuan_dana", nativeQuery = true)
    BigDecimal getSisaAnggaranPerDepartemenIn(String periodeAnggaran, String departemen);


    @Query(value = "select a.id,d.id_departemen as idDepartemen, a.tanggal_pengajuan as tanggalPengajuan, a.deskripsi_biaya as deskripsiBiaya, a.jumlah as pengajuan, \n" +
            "f.tanggal_laporan as tanggalLaporan, f.jumlah as realisasi \n" +
            "from pengajuan_dana as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran_proker as c on b.id_anggaran_proker = c.id\n" +
            "inner join anggaran as d on c.id_anggaran = d.id\n" +
            "left join pencairan_dana_detail as e on a.id = e.id_pengajuan_dana\n" +
            "left join (select id_pencairan_dana_detail,tanggal_laporan, jumlah from laporan_dana where status not in ('CANCELED','HAPUS','REJECTED')) as f on e.id = f.id_pencairan_dana_detail\n" +
            "where d.id_periode_anggaran = ?1 and coalesce(tanggal_laporan,tanggal_pengajuan) <= now() and a.status not in ('CANCELED','HAPUS','REJECTED')", nativeQuery = true)
    List<ImporTransaksiAllDto> detailTransaksiAll(String idPeriodeAnggaran, LocalDateTime tanggal);

    
}
