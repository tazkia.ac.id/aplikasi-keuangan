package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Standard;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.StandardDetail;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.awt.print.Pageable;
import java.util.List;

public interface StandardDetailDao extends PagingAndSortingRepository<StandardDetail, String>, CrudRepository<StandardDetail, String> {

    List<StandardDetail> findByStatusAndStandardOrderByStandard(StatusRecord statusRecord, Standard standard);

    List<StandardDetail> findByStatusOrderByDeskripsi(StatusRecord statusRecord);

    List<StandardDetail> findByStatusOrderByStandard(StatusRecord statusRecord);

}
