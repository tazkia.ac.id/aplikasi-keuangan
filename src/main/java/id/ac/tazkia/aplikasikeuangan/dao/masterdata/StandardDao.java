package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Standard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StandardDao extends PagingAndSortingRepository<Standard, String>, CrudRepository<Standard, String> {

    List<Standard> findByStatusOrderByKodeStandard(StatusRecord statusRecord);

    Page<Standard> findByStatusNotLikeOrderByKodeStandard(StatusRecord statusRecord, Pageable page);

    Page<Standard> findByStatusNotLikeAndKodeStandardOrStatusNotLikeAndNamaStandardOrderByKodeStandard(StatusRecord statusRecord, String kode, StatusRecord statusRecord2, String nama, Pageable pageable);

}
