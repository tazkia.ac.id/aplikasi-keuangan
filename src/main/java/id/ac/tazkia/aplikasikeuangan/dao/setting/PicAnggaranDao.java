package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PicAnggaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface PicAnggaranDao extends PagingAndSortingRepository<PicAnggaran, String>, CrudRepository<PicAnggaran, String> {

    Page<PicAnggaran> findByStatusAndPeriodeAnggaranOrderByDepartemen(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, Pageable pageable);

    Page<PicAnggaran> findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranOrderByDepartemen(StatusRecord statusRecord, List<String> idInstansi, PeriodeAnggaran periodeAnggaran, Pageable pageable);

    Page<PicAnggaran> findByStatusAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord statusRecord, String periodeAnggaran, Pageable pageable);

    Page<PicAnggaran> findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord statusRecord, List<String> idInstansi, String periodeAnggaran, Pageable pageable);

    Page<PicAnggaran> findByStatusAndPeriodeAnggaranAndKaryawanNamaKaryawanContainingOrderByDepartemen(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, String nama, Pageable page);

    Page<PicAnggaran> findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranAndKaryawanNamaKaryawanContainingOrderByDepartemen(StatusRecord statusRecord, List<String> idInstansi, PeriodeAnggaran periodeAnggaran, String nama, Pageable page);

    Page<PicAnggaran> findByStatusAndPeriodeAnggaranIdAndKaryawanNamaKaryawanContainingOrderByDepartemen(StatusRecord statusRecord, String periodeAnggaran, String nama, Pageable page);

    Page<PicAnggaran> findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdAndKaryawanNamaKaryawanContainingOrderByDepartemen(StatusRecord statusRecord, List<String> idInstansi, String periodeAnggaran, String nama, Pageable page);


//    Page<PicAnggaran> findByStatusAndPeriodeAnggaranIdOrderByDepartemen(StatusRecord statusRecord, String periodeAnggaran, Pageable page);

    Page<PicAnggaran> findByStatusAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord statusRecord, String periodeAnggaran, String namaDepartemen, Pageable page);

    Page<PicAnggaran> findByStatusAndDepartemenInstansiIdInAndPeriodeAnggaranIdAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord statusRecord, List<String> idInstansi ,String periodeAnggaran, String namaDepartemen, Pageable page);

    Page<PicAnggaran> findByStatusAndPeriodeAnggaranIdAndPeriodeAnggaranTanggalMulaiPerencanaanBeforeAndPeriodeAnggaranTanggalSelesaiAnggaranAfterAndDepartemenIdInOrderByDepartemen(StatusRecord statusRecord, String periodeAnggaran, LocalDate tanggalMulai, LocalDate tanggalSelesai, List<String> departemen, Pageable page);

    Page<PicAnggaran> findByStatusAndPeriodeAnggaranIdAndPeriodeAnggaranTanggalMulaiPerencanaanBeforeAndPeriodeAnggaranTanggalSelesaiAnggaranAfterAndDepartemenIdInAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord statusRecord, String periodeAnggaran, LocalDate tanggalMulai, LocalDate tanggalSelesai, List<String> departemen, String departemen1, Pageable page);

    Page<PicAnggaran> findByStatusAndPeriodeAnggaranIdAndDepartemenIdInOrderByDepartemen(StatusRecord statusRecord, String periodeAnggaran, List<String> departemen, Pageable page);


    Page<PicAnggaran> findByStatusAndPeriodeAnggaranIdAndDepartemenIdInAndDepartemenNamaDepartemenContainingIgnoreCaseOrderByDepartemen(StatusRecord statusRecord, String periodeAnggaran, List<String> departemen, String departemen1, Pageable page);


    PicAnggaran findByStatusAndPeriodeAnggaranIdAndDepartemenId(StatusRecord statusRecord, String periodeAnggaran, String departemen);



}
