package id.ac.tazkia.aplikasikeuangan.dao.setting;


import id.ac.tazkia.aplikasikeuangan.dto.report.LaporanEstimasiPenerimaanDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.security.KeyStore;
import java.time.LocalDate;
import java.util.List;

public interface PenerimaanDao extends PagingAndSortingRepository<Penerimaan, String>, CrudRepository<Penerimaan, String> {

    Page<Penerimaan> findByStatusAndPeriodeAnggaranStatusOrderByTanggalPenerimaanDesc(StatusRecord statusRecord, StatusRecord statusPeriodeAnggaran, Pageable page);

    Page<Penerimaan> findByStatusAndPeriodeAnggaranAndMataAnggaranOrderByTanggalPenerimaanDesc(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, MataAnggaran mataAnggaran, Pageable page);
    Page<Penerimaan> findByStatusAndPeriodeAnggaranAndMataAnggaranAndKeteranganContainingIgnoreCaseOrderByTanggalPenerimaanDesc(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, MataAnggaran mataAnggaran, String keterangan, Pageable page);

    Page<Penerimaan> findByStatusAndPeriodeAnggaranOrderByTanggalPenerimaanDesc(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, Pageable page);
    Page<Penerimaan> findByStatusAndPeriodeAnggaranAndKeteranganContainingIgnoreCaseOrderByTanggalPenerimaanDesc(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, String keterangan, Pageable page);

    List<Penerimaan> findByStatusAndPeriodeAnggaranIdOrderByMataAnggaran(StatusRecord statusRecord, String periodeAnggaran);

    Page<Penerimaan> findByStatusAndPeriodeAnggaranIdOrderByMataAnggaran(StatusRecord statusRecord, String periodeAnggaran, Pageable page);

    @Query(value = "SELECT coalesce(sum(total-((total*deviasi)/100)),0) as total_penerimaan from penerimaan where  id_periode = ?1 and status='AKTIF' group by id_periode", nativeQuery = true)
    BigDecimal getTotalEstimasiPenerimaan(String idPeriode);


    @Query(value = "SELECT coalesce(sum(b.total-((b.total*deviasi)/100)),0) as total_penerimaan from penerimaan as a inner join penerimaan_real as b on a.id = b.id_penerimaan_estimasi where  id_periode = ?1 and a.status='AKTIF' and b.status = 'AKTIF' group by id_periode", nativeQuery = true)
    BigDecimal getTotalPenerimaan(String idPeriode);
    @Query(value = "SELECT coalesce(sum((total*deviasi)/100),0) as total_penerimaan from penerimaan where  id_periode = ?1 and status='AKTIF' group by id_periode", nativeQuery = true)
    BigDecimal getTotalEstimasiPenerimaanDeviasi(String idPeriode);

    @Query(value = "SELECT coalesce(sum(total-((total*deviasi)/100)),0) as total_penerimaan from penerimaan where  id_periode = ?1 and id_mata_anggaran = ?2 and status='AKTIF' group by id_periode", nativeQuery = true)
    BigDecimal getTotalEstimasiPenerimaanPerAnggaran(String idPeriode, String idMataAnggaran);

    @Query(value = "SELECT coalesce(sum((total*deviasi)/100),0) as total_penerimaan from penerimaan where  id_periode = ?1 and id_mata_anggaran = ?2 and status='AKTIF' group by id_periode", nativeQuery = true)
    BigDecimal getTotalEstimasiPenerimaanPerAnggaranDeviasi(String idPeriode, String idMataAnggaran);

    @Query(value = "select a.tanggal_penerimaan as tanggalPenerimaan, b.kode_mata_anggaran as kodeMataAnggaran, b.nama_mata_anggaran as  namaMataAnggaran, \n" +
            "x.deskripsi as keterangan, x.kuantitas as kuantitas, x.satuan as  satuan, x.jumlah_satuan as amount, x.total as total \n" +
            "from penerimaan as a\n" +
            "inner join estimasi_penerimaan as x on a.id = x.id_rencana_penerimaan\n" +
            "inner join mata_anggaran as b on a.id_mata_anggaran = b.id\n" +
            "inner join departemen as c on a.id_departemen = c.id\n" +
            "where a.status = 'AKTIF'\n" +
            "and x.tanggal >= ?1 and x.tanggal <= ?2",nativeQuery = true)
    List<LaporanEstimasiPenerimaanDto> laporanPenerimaanEstimasi(LocalDate tanggalMulai, LocalDate tanggalSelesai);

    @Query(value = "select sum(total)as total from\n" +
            "(select a.tanggal_penerimaan, b.kode_mata_anggaran, b.nama_mata_anggaran, b.keterangan, a.kuantitas, x.total from penerimaan as a\n" +
            "inner join estimasi_penerimaan as x on a.id = x.id_rencana_penerimaan\n" +
            "inner join mata_anggaran as b on a.id_mata_anggaran = b.id\n" +
            "inner join departemen as c on a.id_departemen = c.id\n" +
            "where a.status = 'AKTIF'\n" +
            "and x.tanggal >= ?1 and x.tanggal <= ?2)a", nativeQuery = true)
    BigDecimal totalPenerimaanEstimasi(LocalDate tanggalMulai, LocalDate tanggalSelesai);



}
