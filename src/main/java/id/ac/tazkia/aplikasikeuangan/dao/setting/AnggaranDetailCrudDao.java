package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;

public interface AnggaranDetailCrudDao extends PagingAndSortingRepository<AnggaranDetail, String>, CrudRepository<AnggaranDetail, String> {

    @Query(value = "SELECT coalesce(sum(kuantitas * amount),0)as total_anggaran from anggaran_detail where  id_anggaran= ?1 and status='AKTIF' group by id_anggaran", nativeQuery = true)
    BigDecimal getTotalAnggaran(String idAnggaran);

    @Query(value="select pagu_anggaran_amount-anggaran as sisa_pagu from\n" +
            "(select sum(a.amount*a.kuantitas)as anggaran, pagu_anggaran_amount from anggaran_detail as a inner join \n" +
            "anggaran as b on a.id_anggaran=b.id inner join\n" +
            "pagu_anggaran as c on b.id_departemen=c.id_departemen where b.id_departemen=?1 and a.status='AKTIF' and b.status='AKTIF' and b.id_periode_anggaran=?2 and c.id_periode_anggaran=?2\n" +
            "group by b.id_departemen)a",nativeQuery = true)
    BigDecimal getSisaPagu(String idDepartemen, String idPeriode);

    @Query(value="select (aaa.pagu_anggaran_amount-coalesce(bbb.realisasi,0))as sisa from\n" +
            "(select * from pagu_anggaran where id_departemen=?1 \n" +
            "and id_periode_anggaran=?2)aaa\n" +
            "left join\n" +
            "(select aa.id_departemen,sum(coalesce(dd.jumlah, aa.jumlah))as realisasi from\n" +
            "(select a.* from pengajuan_dana as a \n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail=b.id\n" +
            "inner join anggaran as c on b.id_anggaran=c.id \n" +
            "where c.id_departemen=?1 and c.id_periode_anggaran=?2 \n" +
            "and a.status not in('REJECTED','CANCELED','HAPUS'))aa\n" +
            "left join\n" +
            "(select a.id,a.id_pengajuan_dana, b.jumlah \n" +
            "from pencairan_dana_detail as a\n" +
            "inner join\n" +
            "laporan_dana as b on a.id=b.id_pencairan_dana_detail and b.status = 'AKTIF' where b.id_tahun_anggaran = ?2) as dd\n" +
            "on aa.id = dd.id_pengajuan_dana\n" +
            "group by aa.id_departemen)bbb\n" +
            "on aaa.id_departemen=bbb.id_departemen;", nativeQuery = true)
    BigDecimal getSisaPaguBudget(String idDepartemen, String idPeriode);


    @Query(value = "select sum(sisa) as sisa from\n" +
            "(select a.id_anggaran as id, a.id as id_anggaran_detail, a.kode_anggaran as kodeAnggaran, a.deskripsi as namaAnggaran,COALESCE(a.amount * a.kuantitas ,0) AS totalAnggaran, \n" +
            "sum(coalesce(b.jumlah,a.jumlah,0)) as totalPengeluaran, COALESCE(a.amount * a.kuantitas,0) - sum(coalesce(b.jumlah,a.jumlah,0)) AS sisa from\n" +
            "(select b.*,c.id as id_pengajuan_dana, c.jumlah, a.kode_anggaran from anggaran as a\n" +
            "inner join anggaran_detail as b on a.id = b.id_anggaran\n" +
            "left join (select * from pengajuan_dana where status not in ('HAPUS','CANCELED','REJECTED')) as c on b.id = c.id_anggaran_detail\n" +
            "where a.id_departemen = ?1 and a.status ='AKTIF' and a.id_periode_anggaran = ?2 \n" +
            "and b.status = 'AKTIF' )a\n" +
            "left join\n" +
            "(select a.id_pengajuan_dana,b.* from pencairan_dana_detail as a\n" +
            "inner join laporan_dana as b on a.id = b.id_pencairan_dana_detail) b on a.id_pengajuan_dana = b.id_pengajuan_dana\n" +
            "group by a.id)a", nativeQuery = true)
    BigDecimal getSisaAnggaranBudget(String idDepartemen, String idPeriode);

}
