package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AkunLevelTiga;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AkunLevelTigaDao extends PagingAndSortingRepository<AkunLevelTiga, String>, CrudRepository<AkunLevelTiga, String> {

    List<AkunLevelTiga> findByStatusOrderByAkunLevelDuaAkunLevelSatuAkunNamaAkun(StatusRecord statusRecord);

}
