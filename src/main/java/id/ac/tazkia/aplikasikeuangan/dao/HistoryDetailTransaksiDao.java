package id.ac.tazkia.aplikasikeuangan.dao;

import id.ac.tazkia.aplikasikeuangan.dto.report.DetailTransaksiDepartemenDto;
import id.ac.tazkia.aplikasikeuangan.entity.HistoryDetailTransaksi;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import jakarta.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

public interface HistoryDetailTransaksiDao extends PagingAndSortingRepository<HistoryDetailTransaksi, String>, CrudRepository<HistoryDetailTransaksi, String> {

    List<HistoryDetailTransaksi> findByStatusAndDepartemenAndTanggalUpdateOrderByTanggalPengajuan(StatusRecord statusRecord, Departemen departemen, LocalDateTime tanggalUpdate);

    @Transactional
    @Modifying
    @Query(value = "delete from history_detail_transaksi where date(tanggal_update) = date(?1)", nativeQuery = true)
    void deleteDataTransaksiYangSama(LocalDateTime tanggal);

    @Query(value = "select deskripsi_biaya as deskripsiBiaya, tanggal_pengajuan as tanggalPengajuan, pengajuan, tanggal_laporan as tanggalLaporan, realisasi, bulan, tahun,total,nomor from\n" +
            "((select 'Total Pada Bulan' as deskripsi_biaya,null as tanggal_pengajuan,null as pengajuan,null as tanggal_laporan,null as realisasi, \n" +
            "month(coalesce(tanggal_laporan,tanggal_pengajuan)) as bulan, year(coalesce(tanggal_laporan,tanggal_pengajuan)) as tahun, \n" +
            "sum(coalesce(realisasi,pengajuan)) as total, 2 as nomor\n" +
            "from history_detail_transaksi \n" +
            "where status = 'AKTIF' and id_departemen = ?1 \n" +
            "and  tanggal_update = ?2 group by month(coalesce(tanggal_laporan,tanggal_pengajuan)), year(coalesce(tanggal_laporan,tanggal_pengajuan))\n" +
            "order by year(coalesce(tanggal_laporan,tanggal_pengajuan)), month(coalesce(tanggal_laporan,tanggal_pengajuan)))\n" +
            "union\n" +
            "(select deskripsi_biaya, tanggal_pengajuan, pengajuan, tanggal_laporan, realisasi, \n" +
            "month(coalesce(tanggal_laporan,tanggal_pengajuan)) as bulan, year(coalesce(tanggal_laporan,tanggal_pengajuan)) as tahun, null as total, 1 as nomor from history_detail_transaksi\n" +
            "where status = 'AKTIF' and id_departemen = ?1 \n" +
            "and  tanggal_update = ?2 order by coalesce(tanggal_laporan,tanggal_pengajuan))) as a \n" +
            "order by tahun, bulan, nomor", nativeQuery = true)
    List<DetailTransaksiDepartemenDto> detailTransaksiDepartemen(String idDepartemen, LocalDateTime tanggalUpdate);


    @Query(value = "select deskripsi_biaya as deskripsiBiaya, tanggal_pengajuan as tanggalPengajuan, pengajuan, tanggal_laporan as tanggalLaporan, realisasi, bulan, tahun,total,nomor,idDepartemen from\n" +
            "((select 'Total Pada Bulan' as deskripsi_biaya,null as tanggal_pengajuan,null as pengajuan,null as tanggal_laporan,null as realisasi, \n" +
            "month(coalesce(tanggal_laporan,tanggal_pengajuan)) as bulan, year(coalesce(tanggal_laporan,tanggal_pengajuan)) as tahun, \n" +
            "sum(coalesce(realisasi,pengajuan)) as total, 2 as nomor, id_departemen as idDepartemen \n" +
            "from history_detail_transaksi \n" +
            "where status = 'AKTIF' \n" +
            "and  tanggal_update = ?1 group by month(coalesce(tanggal_laporan,tanggal_pengajuan)), year(coalesce(tanggal_laporan,tanggal_pengajuan))\n" +
            "order by year(coalesce(tanggal_laporan,tanggal_pengajuan)), month(coalesce(tanggal_laporan,tanggal_pengajuan)))\n" +
            "union\n" +
            "(select deskripsi_biaya, tanggal_pengajuan, pengajuan, tanggal_laporan, realisasi, \n" +
            "month(coalesce(tanggal_laporan,tanggal_pengajuan)) as bulan, year(coalesce(tanggal_laporan,tanggal_pengajuan)) as tahun, null as total, 1 as nomor, id_departemen as idDepartemen from history_detail_transaksi\n" +
            "where status = 'AKTIF' \n" +
            "and  tanggal_update = ?1 order by coalesce(tanggal_laporan,tanggal_pengajuan))) as a \n" +
            "order by tahun, bulan, nomor", nativeQuery = true)
    List<DetailTransaksiDepartemenDto> detailTransaksi(LocalDateTime tanggalUpdate);

}
