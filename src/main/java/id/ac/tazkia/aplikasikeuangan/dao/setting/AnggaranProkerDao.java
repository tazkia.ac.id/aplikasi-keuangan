package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranProker;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AnggaranProkerDao extends PagingAndSortingRepository<AnggaranProker, String>, CrudRepository<AnggaranProker, String> {

    List<AnggaranProker> findByStatusAndAnggaranPeriodeAnggaranIdAndAnggaranDepartemenId(StatusRecord statusRecord, String periodeAnggaran, String departemen);

}
