package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.dto.transaksi.PengembalianDanaDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDana;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface PengembalianDanaDao extends PagingAndSortingRepository<PengembalianDana, String>, CrudRepository<PengembalianDana, String> {

    PengembalianDana findByStatusAndPencairanDanaAndStatusPengembalian(StatusRecord statusRecord, PencairanDana pencairanDana, StatusRecord statusPencairan);

    Integer countByStatusAndStatusPengembalianAndPencairanDanaUserMintaPencairanId(StatusRecord statusRecord, StatusRecord statusPengembalian, String karyawan);

    @Query(value = "SELECT a.id, CAST(d.keterangan_pengajuan as CHAR) as keteranganPengajuan, a.nominal AS pengembalian, b.nominal_pencairan AS pencairan,c.jumlah_laporan as jumlahLaporan FROM pengembalian_dana AS a \n" +
            "INNER JOIN pencairan_dana AS b ON a.id_pencairan_dana = b.id\n" +
            "INNER JOIN\n" +
            "(SELECT bb.id_pencairan_dana,SUM(aa.jumlah)AS jumlah_laporan FROM laporan_dana AS aa \n" +
            "INNER JOIN pencairan_dana_detail AS bb ON aa.id_pencairan_dana_detail = bb.id GROUP BY id_pencairan_dana)c ON a.id_pencairan_dana=c.id_pencairan_dana\n" +
            "INNER JOIN\n" +
            "(SELECT id_pencairan_dana, Group_CONCAT(deskripsi_biaya SEPARATOR ', ') AS keterangan_pengajuan FROM pencairan_dana_detail AS cc INNER JOIN pengajuan_dana AS dd ON cc.id_pengajuan_dana = dd.id GROUP BY id_pencairan_dana)d ON a.id_pencairan_dana = d.id_pencairan_dana\n" +
            "WHERE b.user_minta_pencairan=?1 AND a.status=?2 AND a.status_pengembalian=?3 order by b.tanggal_pencairan \n",
            countQuery = "SELECT COUNT(id)AS page FROM\n" +
                    "(SELECT a.id, CAST(d.keterangan_pengajuan AS CHAR) AS keteranganPengajuan, a.nominal AS pengembalian, b.nominal_pencairan AS pencairan,c.jumlah_laporan AS jumlahLaporan \n" +
                    "FROM pengembalian_dana AS a \n" +
                    "INNER JOIN pencairan_dana AS b ON a.id_pencairan_dana = b.id\n" +
                    "INNER JOIN\n" +
                    "(SELECT bb.id_pencairan_dana,SUM(aa.jumlah)AS jumlah_laporan FROM laporan_dana AS aa \n" +
                    "INNER JOIN pencairan_dana_detail AS bb ON aa.id_pencairan_dana_detail = bb.id GROUP BY id_pencairan_dana)c ON a.id_pencairan_dana=c.id_pencairan_dana\n" +
                    "INNER JOIN\n" +
                    "(SELECT id_pencairan_dana, GROUP_CONCAT(deskripsi_biaya SEPARATOR ', ') AS keterangan_pengajuan FROM pencairan_dana_detail AS cc \n" +
                    "INNER JOIN pengajuan_dana AS dd ON cc.id_pengajuan_dana = dd.id GROUP BY id_pencairan_dana)d ON a.id_pencairan_dana = d.id_pencairan_dana\n" +
                    "WHERE b.user_minta_pencairan=?1 AND a.status=?2 AND a.status_pengembalian=?3 ORDER BY b.tanggal_pencairan)aa",nativeQuery = true)
    Page<PengembalianDanaDto> dataPengembalian2(String karyawan, String statusRecord, String statusAktif, Pageable page);

    @Query(value = "select a.id, CAST(d.deskripsi_biaya as char) as keteranganPengajuan, a.nominal as pengembalian, b.nominal_pencairan as pencairan,\n" +
            "sum(e.jumlah) as jumlahLaporan from pengembalian_dana as a\n" +
            "inner join pencairan_dana as b on a.id_pencairan_dana = b.id\n" +
            "inner join pencairan_dana_detail as c on b.id = c.id_pencairan_dana\n" +
            "inner join pengajuan_dana as d on c.id_pengajuan_dana = d.id\n" +
            "inner join laporan_dana as e on c.id = e.id_pencairan_dana_detail\n" +
            "where b.user_minta_pencairan=?1 AND a.status=?2 AND a.status_pengembalian=?3 group by a.id_pencairan_dana\n", countQuery = "select count(id) as nomor from\n" +
            "(select a.id, CAST(d.deskripsi_biaya as char) as keteranganPengajuan, a.nominal as pengembalian, b.nominal_pencairan as pencairan,\n" +
            "sum(e.jumlah) as jumlahLaporan from pengembalian_dana as a\n" +
            "inner join pencairan_dana as b on a.id_pencairan_dana = b.id\n" +
            "inner join pencairan_dana_detail as c on b.id = c.id_pencairan_dana\n" +
            "inner join pengajuan_dana as d on c.id_pengajuan_dana = d.id\n" +
            "inner join laporan_dana as e on c.id = e.id_pencairan_dana_detail\n" +
            "where b.user_minta_pencairan=?1 AND a.status=?2 AND a.status_pengembalian=?3 group by a.id_pencairan_dana) as a", nativeQuery = true)
    Page<PengembalianDanaDto> dataPengembalian(String karyawan, String statusRecord, String statusAktif, Pageable pageable);

    @Query(value = "SELECT a.id, CAST(d.keterangan_pengajuan as CHAR) as keteranganPengajuan, a.nominal AS pengembalian, b.nominal_pencairan AS pencairan,c.jumlah_laporan as jumlahLaporan FROM pengembalian_dana AS a \n" +
            "INNER JOIN pencairan_dana AS b ON a.id_pencairan_dana = b.id\n" +
            "INNER JOIN\n" +
            "(SELECT bb.id_pencairan_dana,SUM(aa.jumlah)AS jumlah_laporan FROM laporan_dana AS aa \n" +
            "INNER JOIN pencairan_dana_detail AS bb ON aa.id_pencairan_dana_detail = bb.id GROUP BY id_pencairan_dana)c ON a.id_pencairan_dana=c.id_pencairan_dana\n" +
            "INNER JOIN\n" +
            "(SELECT id_pencairan_dana, Group_CONCAT(deskripsi_biaya SEPARATOR ', ') AS keterangan_pengajuan FROM pencairan_dana_detail AS cc INNER JOIN pengajuan_dana AS dd ON cc.id_pengajuan_dana = dd.id GROUP BY id_pencairan_dana)d ON a.id_pencairan_dana = d.id_pencairan_dana\n" +
            "WHERE b.user_minta_pencairan=?1 AND a.status=?2 AND a.status_pengembalian=?3 order by b.tanggal_pencairan \n",nativeQuery = true)
    List<PengembalianDanaDto> dataPengembalianList(String karyawan, String statusRecord, String statusAktif);


    List<PengembalianDana> findByIdIn(List<String> id);

    @Query(value = "select sum(nominal) as total from pengembalian_dana where id in (?1)", nativeQuery =  true)
    BigDecimal totalNominal(List<String> id);


}
