package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;

public interface PenerimaanCrudDao extends CrudRepository<Penerimaan, String>, PagingAndSortingRepository<Penerimaan, String> {

    @Query(value = "SELECT coalesce(sum(amount),0)as total_penerimaan from penerimaan where id_periode= ?1 and status='AKTIF' group by id_periode", nativeQuery = true)
    BigDecimal getTotalPenerimaan(String periode);

}
