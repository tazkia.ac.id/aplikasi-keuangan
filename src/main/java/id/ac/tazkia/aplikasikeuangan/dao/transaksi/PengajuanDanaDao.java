package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.controller.transaksi.PengajuanDanaDetailController;
import id.ac.tazkia.aplikasikeuangan.dto.TransaksiSetahunDuaDto;
import id.ac.tazkia.aplikasikeuangan.dto.report.DetailTransaksiDto;
import id.ac.tazkia.aplikasikeuangan.dto.report.KasHarianDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.KeluarMasukDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.KeluarMasukTransaksiDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.TransaksiSetahunDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDanaDetail;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface PengajuanDanaDao extends PagingAndSortingRepository<PengajuanDana, String>, CrudRepository<PengajuanDana, String> {

    Page<PengajuanDana> findByStatusAndKaryawanPengajuOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Karyawan karyawan, Pageable page);



    @Query("select pd from PengajuanDana pd where pd.status in :status and pd.id <> :id and pd.requestFor = :karyawan")
    Page<PengajuanDana> lihatHistoryKaryawan(@Param("status") List<StatusRecord> statusRecords, @Param("id") String id, @Param("karyawan") Karyawan karyawan, Pageable pageable);

    @Query(value = "select id from pengajuan_dana where status in ?1 and id <> ?2 and requestFor = ?3", nativeQuery = true)
    List<String> listHistory(List<String> status, String id, String requestFor);


    Page<PengajuanDana> findByStatusInAndRequestForOrderByTanggalPengajuanDesc(List<StatusRecord> statusRecords, Karyawan karyawan, Pageable pageable);

    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.deskripsiBiaya = :deskripsi and pd.karyawanPengaju = :karyawan and pd.nomorUrut > pd.totalNomorUrut")
    Page<PengajuanDana> pengajuanDanaApprovedSearch(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, @Param("deskripsi")String deskripsiBiaya, Pageable page);

    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.karyawanPengaju = :karyawan and pd.nomorUrut > pd.totalNomorUrut")
    Page<PengajuanDana> pengajuanDanaApproved(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, Pageable page);

    @Query(value = "SELECT count(id) as approved from pengajuan_dana where status='AKTIF' and id_karyawan_pengaju = ?1 and nomor_urut > total_nomor_urut", nativeQuery = true)
    Integer jumlahPengajuanDanaApproved(String karyawan);

    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.karyawanPengaju = :karyawan and pd.nomorUrut > pd.totalNomorUrut")
    List<PengajuanDana> pengajuanDanaApprovedList(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan);


    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.karyawanPengaju = :karyawan and pd.nomorUrut <= pd.totalNomorUrut and pd.nomorUrut > 0")
    Page<PengajuanDana> pengajuanDanaWaiting(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, Pageable page);

    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.karyawanPengaju = :karyawan and pd.nomorUrut <= pd.totalNomorUrut and pd.nomorUrut > 0 and pd.anggaranDetail=:anggaranDetail")
    Page<PengajuanDana> pengajuanDanaWaitingAnggaranDetail(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan,@Param("anggaranDetail")AnggaranDetail anggaranDetail, Pageable page);

    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.karyawanPengaju = :karyawan and pd.nomorUrut <= pd.totalNomorUrut and pd.nomorUrut > 0 and deskripsiBiaya like %:search% ")
    Page<PengajuanDana> pengajuanDanaWaitingSearch(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, @Param("search")String search, Pageable page);

    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.karyawanPengaju = :karyawan and pd.nomorUrut <= pd.totalNomorUrut and pd.nomorUrut > 0 and pd.anggaranDetail=:anggaranDetail and pd.deskripsiBiaya like %:search% ")
    Page<PengajuanDana> pengajuanDanaWaitingSearchAnggaranDetail(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, @Param("anggaranDetail")AnggaranDetail anggaranDetail, @Param("search")String search, Pageable page);

    @Query(value = "SELECT count(id) as waiting from pengajuan_dana where status='AKTIF' and id_karyawan_pengaju = ?1 and nomor_urut <= total_nomor_urut and nomor_urut > 0", nativeQuery = true)
    Integer jumlahPengajuanDanaWaiting(String karyawan);

    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.karyawanPengaju = :karyawan and pd.nomorUrut = 0")
    Page<PengajuanDana> pengajuanDanaRejected(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, Pageable page);

    @Query(value = "SELECT count(id) as approved from pengajuan_dana where status='REJECTED' and id_karyawan_pengaju = ?1 and nomor_urut <= total_nomor_urut and nomor_urut = 0", nativeQuery = true)
    Integer jumlahPengajuanDanaRejected(String karyawan);

    @Query("select pd from PengajuanDana pd where pd.status = :status and pd.karyawanPengaju = :karyawan")
    Page<PengajuanDana> pengajuanDanaCair(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, Pageable page);

    @Query("select pd from PengajuanDana pd where pd.tanggalPengajuan >= :tanggalAwal and pd.tanggalPengajuan <= :tanggalAkhir and pd.nomorUrut > pd.totalNomorUrut and pd.status='AKTIF' order by pd.tanggalPengajuan")
    List<PengajuanDana> listPengajuanDanaApproveTanggal(@Param("tanggalAwal") LocalDateTime localDateAwal, @Param("tanggalAkhir") LocalDateTime localDateAkhir);

    @Query("select pd from PengajuanDana pd where pd.nomorUrut > pd.totalNomorUrut and pd.status='AKTIF' and pd.tanggalPengajuan <= :tanggal order by pd.tanggalPengajuan")
    List<PengajuanDana> listPengajuanDanaApprove(@Param("tanggal") LocalDateTime localDateTime);

    @Query(value = "select COALESCE (sum(jumlah),0)as total_pengajuan from pengajuan_dana where tanggal_pengajuan <= :tanggal and nomor_urut > total_nomor_urut and status='AKTIF'", nativeQuery = true)
    BigDecimal totalPengajuanDanaApprove(@Param("tanggal") LocalDateTime localDateTime);


    @Query(value = "select COALESCE (sum(jumlah),0)as total_pengajuan from pengajuan_dana where tanggal_pengajuan >= :tanggalAwal and tanggal_pengajuan <= :tanggalAkhir and nomor_urut > total_nomor_urut and status='AKTIF'", nativeQuery = true)
    BigDecimal totalPengajuanDanaApproveTanggal(@Param("tanggalAwal") LocalDateTime localDateAwal, @Param("tanggalAkhir") LocalDateTime localDateAkhir);

    @Query(value = "SELECT coalesce(sum(a.jumlah),0)as total_pengajuan from pengajuan_dana as a inner join anggaran as b on a.id_anggaran=b.id where b.id_periode_anggaran = ?1 and a.status in ('CAIR','LAPOR','CLOSED') group by b.id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalPengajuanDanaAktif(String periodeAnggaran);

    @Query(value = "SELECT coalesce(sum(a.jumlah),0)as total_pengajuan from pengajuan_dana as a inner join anggaran as b on a.id_anggaran=b.id where b.id_periode_anggaran = ?1 and a.status in ('CLOSED','CAIR','LAPOR') group by b.id_periode_anggaran", nativeQuery = true)
    BigDecimal getTotalPengajuanDana1(String periodeAnggaran);

//    @Query(value = "select sum(coalesce(d.jumlah,c.nominal_pencairan,a.jumlah,0))as Total_pengeluaran from pengajuan_dana as a\n" +
//            "Inner join anggaran_detail as g on a.id_anggaran_detail = g.id\n" +
//            "Inner join anggaran as h on g.id_anggaran = h.id\n" +
//            "left join pencairan_dana_detail as b on a.id = b.id_pengajuan_dana\n" +
//            "left join pencairan_dana as c on b.id_pencairan_dana = c.id \n" +
//            "left join laporan_dana as d on d.id_pencairan_dana_detail = b.id\n" +
//            "where a.status not in('REJECTED','CANCELED','HAPUS') and h.id_periode_anggaran = ?1", nativeQuery = true)
//    BigDecimal getTotalPengajuanDana2(String periodeAnggaran);


    @Query(value = "select sum(coalesce(if(c.jumlah > a.jumlah, a.jumlah, c.jumlah) ,a.jumlah,0)) as total_pengaluaran from\n" +
            "(select a.* from pengajuan_dana as a \n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran_proker as c on b.id_anggaran_proker = c.id\n" +
            "inner join anggaran as d on c.id_anggaran = d.id\n" +
            "where a.status not in ('HAPUS','CANCELED','REJECTED')\n" +
            "and d.id_periode_anggaran =?1) as a\n" +
            "left join pencairan_dana_detail as b on a.id = b.id_pengajuan_dana\n" +
            "left join laporan_dana as c on b.id = c.id_pencairan_dana_detail\n" +
            "where coalesce(c.status,'AKTIF') = 'AKTIF' and coalesce(c.status_lapor,'AKTIF') not in ('HAPUS','CANCELED','REJECTED')", nativeQuery = true)
    BigDecimal getTotalPengajuanDana22(String periodeAnggaran);

    @Query(value = "select sum(coalesce(if(b.jumlah > a.jumlah, a.jumlah, b.jumlah) ,a.jumlah,0)) as total_pengaluaran from \n" +
            "(select id,jumlah,id_tahun_anggaran from pengajuan_dana where status not in ('HAPUS','CANCELED','REJECTED') and id_tahun_anggaran = ?1 \n" +
            "group by id) as a \n" +
            "left join \n" +
            "(select id_pengajuan_dana, sum(a.jumlah) as jumlah from laporan_dana as a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id where a.status not in ('HAPUS','CANCELED','REJECTED') \n" +
            "and a.id_tahun_anggaran = ?1 group by id_pengajuan_dana) as b \n" +
            "on a.id = b.id_pengajuan_dana group by id_tahun_anggaran", nativeQuery = true)
    BigDecimal getTotalPengajuanDana2(String periodeAnggaran);

    @Query(value = "  select sum(coalesce(laporan,pencairan,minta_pencairan,pengajuan))as total_pengaluaran from\n" +
            "  (SELECT a.id as pengajuan_dana, c.id as pencairan_dana, sum(a.jumlah) as pengajuan, c.nominal_minta_pencairan as minta_pencairan, c.nominal_pencairan as pencairan, sum(d.jumlah) as laporan FROM pengajuan_dana as a \n" +
            "  inner join anggaran as e on a.id_anggaran=e.id left join pencairan_dana_detail as b on a.id = b.id_pengajuan_dana \n" +
            "  left join pencairan_dana as c on b.id_pencairan_dana = c.id left join laporan_dana as d on b.id = d.id_pencairan_dana_detail \n" +
            "  where e.id_periode_anggaran = ?1 and a.status not in ('HAPUS','CANCELED','REJECTED') group by coalesce(b.id_pencairan_dana,a.id))aa ", nativeQuery = true)
    BigDecimal getTotalPengajuanDana(String periodeAnggaran);

    @Query(value = "select coalesce(sum(a.jumlah),0)as total_pengajuan_approve from pengajuan_dana as a inner join anggaran as b on a.id_anggaran=b.id where a.status  not in ('HAPUS','REJECTED') and a.nomor_urut > a.total_nomor_urut and b.id_periode_anggaran = ?1 and b.id_departemen = ?2 group by b.id_periode_anggaran" , nativeQuery = true)
    BigDecimal getSisaPaguAnggaran(String periodeAnggaran, String idDepartemen);

    @Query(value = "select coalesce(total_pengajuan_approve,0) as total_pengajuan_approve from " +
            "(select coalesce(sum(a.jumlah),0)as total_pengajuan_approve from pengajuan_dana as a inner join " +
            "anggaran as b on a.id_anggaran=b.id where a.status not in ('HAPUS','REJECTED') and a.nomor_urut > a.total_nomor_urut and " +
            "b.id_periode_anggaran = ?1 and a.id_anggaran = ?2 group by b.id_periode_anggaran)a" , nativeQuery = true)
    BigDecimal getSisaPaguAnggaranDetail(String periodeAnggaran, String idAnggaranDetail);

    @Query(value = "select sum(coalesce(if(c.jumlah > a.jumlah, a.jumlah, c.jumlah) ,a.jumlah,0)) as total_pengajuan_approve from\n" +
            "(select a.* from pengajuan_dana as a \n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran_proker as c on b.id_anggaran_proker = c.id\n" +
            "inner join anggaran as d on c.id_anggaran = d.id\n" +
            "where a.status not in ('HAPUS','CANCELED','REJECTED')\n" +
            "and d.id_periode_anggaran = ?1 \n" +
            "and a.id_anggaran_detail = ?2) as a\n" +
            "left join pencairan_dana_detail as b on a.id = b.id_pengajuan_dana\n" +
            "left join laporan_dana as c on b.id = c.id_pencairan_dana_detail\n" +
            "where coalesce(c.status,'AKTIF') = 'AKTIF' and coalesce(c.status_lapor,'AKTIF') not in ('HAPUS','CANCELED','REJECTED')", nativeQuery = true)
    BigDecimal getTotalPengajuanDanaDetail(String periodeAnggaran, String idAnggaranDetail);


    @Query(value = "select coalesce(sum(a.jumlah),0)as total_pengajuan_approve from pengajuan_dana as a inner join anggaran as b on a.id_anggaran=b.id " +
            "where a.status  not in ('HAPUS','CANCELED','REJECTED') and a.id_anggaran = ?1 group by a.id_anggaran" , nativeQuery = true)
    BigDecimal getTotalPengajuanDanaAnggaran(String idAnggaran);


    @Query(value = "select coalesce(sum(coalesce(c.jumlah,a.jumlah)),0) as pengeluaran from pengajuan_dana as a\n" +
            "left join pencairan_dana_detail as b on a.id = b.id_pengajuan_dana\n" +
            "left join laporan_dana as c on b.id = c.id_pencairan_dana_detail\n" +
            "where a.status not in ('CANCELED','REJECTED','HAPUS','DELETED') and\n" +
            "coalesce(c.status,'AKTIF') = 'AKTIF' and a.id_anggaran_detail = ?1 \n" +
            "and month(tanggal_pengajuan) = month(now()) and year(tanggal_pengajuan) = year(tanggal_pengajuan)", nativeQuery = true)
    BigDecimal getTotalPengeluaranDetailAnggaran(String idDetailAnggaran);

    @Query(value = "select coalesce(sum(a.jumlah),0)as total_pengajuan_approve from pengajuan_dana as a \n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail=b.id \n" +
            "inner join anggaran_proker as c on b.id_anggaran_proker=c.id\n" +
            "where a.status  not in ('HAPUS','CANCELED','REJECTED') and b.id_anggaran_proker = ?1 group by b.id_anggaran_proker", nativeQuery = true)
    BigDecimal getTotalPengajuanDanaProker(String idAnggaranProker);

    Page<PengajuanDana> findByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord statusRecord, List<String> departemen, Pageable page);

    Integer countByStatusAndDepartemenIdInOrderByTanggalPengajuan(StatusRecord statusRecord, List<String> departemen);

    Page<PengajuanDana> findByStatusAndDepartemenIdInAndAnggaranKodeAnggaranContainingIgnoreCaseOrStatusAndDepartemenIdInAndAnggaranNamaAnggaranContainingIgnoreCaseOrderByTanggalPengajuan(StatusRecord statusRecord, List<String> departemen, String kode, StatusRecord statusRecord2, List<String> departemen2, String nama, Pageable page);

    Page<PengajuanDana> findByStatusOrderByTanggalPengajuan(StatusRecord statusRecord, Pageable page);

    List<PengajuanDana> findByStatusAndIdIn(StatusRecord statusRecord, List<String> pencairanDanaDetails);

    PengajuanDana findByStatusAndId(StatusRecord statusRecord, String idPengajuanDana);


    @Query(value="select tanggalpengajuan,tanggalpencairan, tanggalcair, tanggallaporan ,nama_karyawan as nama,GROUP_CONCAT(DISTINCT kode_anggaran SEPARATOR ', ')as kode,GROUP_CONCAT(DISTINCT nama_anggaran SEPARATOR ', ')as name,GROUP_CONCAT(DISTINCT deskripsi_biaya SEPARATOR ', ') as deskripsi,sum(jumlah) as pengajuan,nominal_minta_pencairan as disbursereq, \n" +
            "            nominal_pencairan as disburse,sum(lapor)as realisasi,nominal_pencairan - sum(lapor) as selisih from \n" +
            "            (select a.tanggal_pengajuan as tanggalpengajuan,b.tanggal_minta_pencairan as tanggalpencairan\n" +
            "            ,b.tanggal_pencairan as tanggalcair,c.tanggal_laporan as tanggallaporan,nama_karyawan,d.kode_anggaran,nama_anggaran,a.deskripsi_biaya,a.jumlah,nominal_minta_pencairan,nominal_pencairan,c.jumlah as lapor,coalesce(id_pencairan_dana,a.id)as id_pencairan_dana from \n" +
            "            (select uu.* from pengajuan_dana as uu inner join anggaran as vv on uu.id_anggaran=vv.id where uu.status not in ('HAPUS','REJECTED','CANCELED','CANCEL') and vv.id_periode_anggaran= ?1 order by uu.tanggal_pengajuan)a left join \n" +
            "            (select aa.id as pencairan_dana_detail,aa.id_pengajuan_dana,id_pencairan_dana,nominal_minta_pencairan,nominal_pencairan,bb.tanggal_minta_pencairan,bb.tanggal_pencairan from pencairan_dana_detail as aa inner join pencairan_dana as bb on aa.id_pencairan_dana=bb.id)b on a.id=b.id_pengajuan_dana left join \n" +
            "            (select cc.id as pencairan_dana_detail,dd.jumlah,dd.tanggal_laporan from pencairan_dana_detail as cc inner join laporan_dana as dd on cc.id=dd.id_pencairan_dana_detail where dd.status_lapor <> 'REJECTED')c on b.pencairan_dana_detail = c.pencairan_dana_detail inner join \n" +
            "            anggaran as d on a.id_anggaran=d.id inner join \n" +
            "            karyawan as e on a.id_karyawan_pengaju=e.id)aaa \n" +
            "            group by id_pencairan_dana \n" +
            "            order by tanggalpengajuan", nativeQuery = true)
    List<KasHarianDto> pageKasHarian1(String periodeAnggaran);

    @Query(value="select tanggal_pengajuan as tanggalpengajuan, tanggal_minta_pencairan as tanggalpencairan, tanggal_pencairan as tanggalcair, tanggal_laporan as tanggallaporan ,nama_karyawan as nama,GROUP_CONCAT(DISTINCT kode_anggaran SEPARATOR ', ')as kode,GROUP_CONCAT(DISTINCT nama_anggaran SEPARATOR ', ')as name,GROUP_CONCAT(DISTINCT deskripsi_biaya SEPARATOR ', ') as deskripsi,sum(jumlah) as pengajuan,nominal_minta_pencairan as disbursereq, \n" +
            "nominal_pencairan as disburse,sum(lapor)as realisasi,nominal_pencairan - sum(lapor) as selisih, aaa.id_pencairan_dana as idPencairanDana, bukti_pencairan as buktiPencairan, bukti_minta_pencairan as buktiMintaPencairan from \n" +
            "(select bukti_minta_pencairan,bukti_pencairan,a.tanggal_pengajuan,b.tanggal_minta_pencairan \n" +
            ",b.tanggal_pencairan,c.tanggal_laporan,nama_karyawan,d.kode_anggaran,nama_anggaran,a.deskripsi_biaya,a.jumlah,nominal_minta_pencairan,nominal_pencairan,c.jumlah as lapor,coalesce(id_pencairan_dana,a.id)as id_pencairan_dana from \n" +
            "(select uu.* from pengajuan_dana as uu inner join anggaran as vv on uu.id_anggaran=vv.id where uu.status not in ('HAPUS','REJECTED','CANCELED','CANCEL') and vv.id_periode_anggaran= ?1 and month(uu.tanggal_pengajuan) = month(now()) and year(uu.tanggal_pengajuan) = year(now()) order by uu.tanggal_pengajuan)a left join \n" +
            "(select aa.id as pencairan_dana_detail,aa.id_pengajuan_dana,id_pencairan_dana,nominal_minta_pencairan,nominal_pencairan,bb.tanggal_minta_pencairan,bb.tanggal_pencairan,bukti_pencairan,bb.file as bukti_minta_pencairan from pencairan_dana_detail as aa inner join pencairan_dana as bb on aa.id_pencairan_dana=bb.id)b on a.id=b.id_pengajuan_dana left join \n" +
            "(select cc.id as pencairan_dana_detail,dd.jumlah,dd.tanggal_laporan from pencairan_dana_detail as cc inner join laporan_dana as dd on cc.id=dd.id_pencairan_dana_detail where dd.status_lapor <> 'REJECTED')c on b.pencairan_dana_detail = c.pencairan_dana_detail inner join \n" +
            "anggaran as d on a.id_anggaran=d.id inner join \n" +
            "karyawan as e on a.id_karyawan_pengaju=e.id)aaa \n" +
            "group by id_pencairan_dana \n" +
            "order by tanggal_pengajuan", nativeQuery = true)
    List<KasHarianDto> pageKasHarian3(String periodeAnggaran);


    @Query(value="select GROUP_CONCAT(DATE_FORMAT(tanggal_pengajuan, '%d-%b-%Y') SEPARATOR ',\n') as tanggalpengajuan, tanggal_minta_pencairan as tanggalpencairan, tanggal_pencairan as tanggalcair, tanggal_laporan as tanggallaporan ,nama_karyawan as nama,GROUP_CONCAT(kode_anggaran SEPARATOR ', \n')as kode,GROUP_CONCAT(nama_anggaran SEPARATOR ',\n')as name,GROUP_CONCAT(deskripsi_biaya SEPARATOR ',\n') as deskripsi,sum(jumlah) as pengajuan,nominal_minta_pencairan as disbursereq, \n" +
            "nominal_pencairan as disburse,sum(lapor)as realisasi,nominal_pencairan - sum(lapor) as selisih, aaa.id_pencairan_dana as idPencairanDana, bukti_pencairan as buktiPencairan, bukti_minta_pencairan as buktiMintaPencairan from \n" +
            "(select bukti_minta_pencairan,bukti_pencairan,a.tanggal_pengajuan,b.tanggal_minta_pencairan \n" +
            ",b.tanggal_pencairan,c.tanggal_laporan,nama_karyawan,d.kode_anggaran,nama_anggaran,a.deskripsi_biaya,a.jumlah,nominal_minta_pencairan,nominal_pencairan,c.jumlah as lapor,coalesce(id_pencairan_dana,a.id)as id_pencairan_dana from \n" +
            "(select uu.* from pengajuan_dana as uu inner join anggaran as vv on uu.id_anggaran=vv.id where uu.status not in ('HAPUS','REJECTED','CANCELED','CANCEL') and month(uu.tanggal_pengajuan) = month(now()) and year(uu.tanggal_pengajuan) = year(now()) order by uu.tanggal_pengajuan)a left join \n" +
            "(select aa.id as pencairan_dana_detail,aa.id_pengajuan_dana,id_pencairan_dana,nominal_minta_pencairan,nominal_pencairan,bb.tanggal_minta_pencairan,bb.tanggal_pencairan,bukti_pencairan,bb.file as bukti_minta_pencairan from pencairan_dana_detail as aa inner join pencairan_dana as bb on aa.id_pencairan_dana=bb.id)b on a.id=b.id_pengajuan_dana left join \n" +
            "(select cc.id as pencairan_dana_detail,dd.jumlah,dd.tanggal_laporan from pencairan_dana_detail as cc inner join laporan_dana as dd on cc.id=dd.id_pencairan_dana_detail where dd.status_lapor <> 'REJECTED')c on b.pencairan_dana_detail = c.pencairan_dana_detail inner join \n" +
            "anggaran as d on a.id_anggaran=d.id inner join \n" +
            "karyawan as e on a.id_karyawan_pengaju=e.id)aaa \n" +
            "group by id_pencairan_dana \n" +
            "order by tanggal_pengajuan", nativeQuery = true)
    List<KasHarianDto> pageKasHarian();



    @Query(value="select GROUP_CONCAT(DATE_FORMAT(tanggal_pengajuan, '%d-%b-%Y') SEPARATOR ',\n') as tanggalpengajuan, tanggal_minta_pencairan as tanggalpencairan, tanggal_pencairan as tanggalcair, tanggal_laporan as tanggallaporan ,\n" +
            "nama_karyawan as nama,GROUP_CONCAT(kode_anggaran SEPARATOR ',\n')as kode,GROUP_CONCAT(nama_anggaran SEPARATOR ',\n')as name,\n" +
            "GROUP_CONCAT(deskripsi_biaya SEPARATOR ',\n') as deskripsi,sum(jumlah) as pengajuan,nominal_minta_pencairan as disbursereq, \n" +
            "nominal_pencairan as disburse,sum(lapor)as realisasi,nominal_pencairan - sum(lapor) as selisih, aaa.id_pencairan_dana as idPencairanDana, \n" +
            "bukti_pencairan as buktiPencairan, bukti_minta_pencairan as buktiMintaPencairan, status_akunting as statusAkunting from \n" +
            "(select bukti_minta_pencairan,bukti_pencairan,a.tanggal_pengajuan,b.tanggal_minta_pencairan \n" +
            ",b.tanggal_pencairan,c.tanggal_laporan,nama_karyawan,d.kode_anggaran,nama_anggaran,a.deskripsi_biaya,a.jumlah,nominal_minta_pencairan,nominal_pencairan,\n" +
            "c.jumlah as lapor,coalesce(id_pencairan_dana,a.id)as id_pencairan_dana, b.status_akunting from \n" +
            "(select uu.* from pengajuan_dana as uu inner join anggaran as vv on uu.id_anggaran=vv.id where uu.status not in ('HAPUS','REJECTED','CANCELED','CANCEL') \n" +
            "and month(uu.tanggal_pengajuan) = ?1 and year(uu.tanggal_pengajuan) = ?2 \n" +
            "order by uu.tanggal_pengajuan)a left join \n" +
            "(select aa.id as pencairan_dana_detail,aa.id_pengajuan_dana,id_pencairan_dana,nominal_minta_pencairan,nominal_pencairan,bb.tanggal_minta_pencairan,\n" +
            "bb.tanggal_pencairan,\n" +
            "bukti_pencairan,bb.file as bukti_minta_pencairan, bb.status_akunting from pencairan_dana_detail as aa inner join pencairan_dana as bb on aa.id_pencairan_dana=bb.id)b \n" +
            "on a.id=b.id_pengajuan_dana left join \n" +
            "(select cc.id as pencairan_dana_detail,dd.jumlah,dd.tanggal_laporan from pencairan_dana_detail as cc inner join laporan_dana as dd on \n" +
            "cc.id=dd.id_pencairan_dana_detail where dd.status_lapor <> 'REJECTED')c on b.pencairan_dana_detail = c.pencairan_dana_detail inner join \n" +
            "anggaran as d on a.id_anggaran=d.id inner join \n" +
            "karyawan as e on a.id_karyawan_pengaju=e.id)aaa \n" +
            "group by id_pencairan_dana \n" +
            "order by tanggal_pengajuan", nativeQuery = true)
    List<KasHarianDto> pageKasHarianDef(String bulan, String tahun);

    @Query(value="select tanggal_pengajuan as tanggalpengajuan, tanggal_minta_pencairan as tanggalpencairan, tanggal_pencairan as tanggalcair, tanggal_laporan as tanggallaporan ,\n" +
            "nama_karyawan as nama,GROUP_CONCAT(DISTINCT kode_anggaran SEPARATOR ', ')as kode,GROUP_CONCAT(DISTINCT nama_anggaran SEPARATOR ', ')as name,\n" +
            "GROUP_CONCAT(DISTINCT deskripsi_biaya SEPARATOR ', ') as deskripsi,sum(jumlah) as pengajuan,nominal_minta_pencairan as disbursereq, \n" +
            "nominal_pencairan as disburse,sum(lapor)as realisasi,nominal_pencairan - sum(lapor) as selisih, aaa.id_pencairan_dana as idPencairanDana, \n" +
            "bukti_pencairan as buktiPencairan, bukti_minta_pencairan as buktiMintaPencairan from \n" +
            "(select bukti_minta_pencairan,bukti_pencairan,a.tanggal_pengajuan,b.tanggal_minta_pencairan \n" +
            ",b.tanggal_pencairan,c.tanggal_laporan,nama_karyawan,d.kode_anggaran,nama_anggaran,a.deskripsi_biaya,a.jumlah,nominal_minta_pencairan,nominal_pencairan,\n" +
            "c.jumlah as lapor,coalesce(id_pencairan_dana,a.id)as id_pencairan_dana from \n" +
            "(select uu.* from pengajuan_dana as uu inner join anggaran as vv on uu.id_anggaran=vv.id where uu.status not in ('HAPUS','REJECTED','CANCELED','CANCEL') \n" +
            "and vv.id_periode_anggaran= ?1 and month(uu.tanggal_pengajuan) = ?2 and year(uu.tanggal_pengajuan) = ?3 \n" +
            "order by uu.tanggal_pengajuan)a left join \n" +
            "(select aa.id as pencairan_dana_detail,aa.id_pengajuan_dana,id_pencairan_dana,nominal_minta_pencairan,nominal_pencairan,bb.tanggal_minta_pencairan,\n" +
            "bb.tanggal_pencairan,\n" +
            "bukti_pencairan,bb.file as bukti_minta_pencairan from pencairan_dana_detail as aa inner join pencairan_dana as bb on aa.id_pencairan_dana=bb.id)b \n" +
            "on a.id=b.id_pengajuan_dana left join \n" +
            "(select cc.id as pencairan_dana_detail,dd.jumlah,dd.tanggal_laporan from pencairan_dana_detail as cc inner join laporan_dana as dd on \n" +
            "cc.id=dd.id_pencairan_dana_detail where dd.status_lapor <> 'REJECTED')c on b.pencairan_dana_detail = c.pencairan_dana_detail inner join \n" +
            "anggaran as d on a.id_anggaran=d.id inner join \n" +
            "karyawan as e on a.id_karyawan_pengaju=e.id)aaa \n" +
            "group by id_pencairan_dana \n" +
            "order by tanggal_pengajuan", nativeQuery = true)
    List<KasHarianDto> pageKasHarianDeff(String periodeAnggaran, String bulan, String tahun);

    @Query(value="SELECT tanggalpengajuan, COALESCE(tanggalpencairan,tanggalpengajuan)AS tanggalpencairan, COALESCE(tanggalcair,tanggalpengajuan)AS tanggalcair,COALESCE(tanggallaporan,tanggalpengajuan)AS tanggallaporan,\n" +
            "nama,kode,NAME,deskripsi,pengajuan,disbursereq,disburse,realisasi,selisih FROM            \n" +
            "(SELECT tanggalpengajuan, tanggalpencairan, tanggalcair, tanggallaporan ,nama_karyawan AS nama,GROUP_CONCAT(DISTINCT kode_anggaran SEPARATOR ', ')AS kode,GROUP_CONCAT(DISTINCT nama_anggaran SEPARATOR ', ')AS NAME,\n" +
            "GROUP_CONCAT(DISTINCT deskripsi_biaya SEPARATOR ', ') AS deskripsi,SUM(jumlah) AS pengajuan,nominal_minta_pencairan AS disbursereq, \n" +
            "nominal_pencairan AS disburse,SUM(lapor)AS realisasi,nominal_pencairan - SUM(lapor) AS selisih FROM \n" +
            "(SELECT a.tanggal_pengajuan AS tanggalpengajuan,b.tanggal_minta_pencairan AS tanggalpencairan\n" +
            ",b.tanggal_pencairan AS tanggalcair,c.tanggal_laporan AS tanggallaporan,nama_karyawan,d.kode_anggaran,nama_anggaran,a.deskripsi_biaya,a.jumlah,nominal_minta_pencairan,\n" +
            "nominal_pencairan,c.jumlah AS lapor,COALESCE(id_pencairan_dana,a.id)AS id_pencairan_dana FROM \n" +
            "(SELECT uu.* FROM pengajuan_dana AS uu INNER JOIN anggaran AS vv ON uu.id_anggaran=vv.id WHERE uu.status NOT IN ('HAPUS','REJECTED','CANCELED','CANCEL') AND vv.id_periode_anggaran= ?1 ORDER BY uu.tanggal_pengajuan)a LEFT JOIN \n" +
            "(SELECT aa.id AS pencairan_dana_detail,aa.id_pengajuan_dana,id_pencairan_dana,nominal_minta_pencairan,nominal_pencairan,bb.tanggal_minta_pencairan,bb.tanggal_pencairan FROM pencairan_dana_detail AS aa INNER JOIN pencairan_dana AS \n" +
            "bb ON aa.id_pencairan_dana=bb.id)b ON a.id=b.id_pengajuan_dana LEFT JOIN \n" +
            "(SELECT cc.id AS pencairan_dana_detail,dd.jumlah,dd.tanggal_laporan FROM pencairan_dana_detail AS cc INNER JOIN laporan_dana AS dd ON cc.id=dd.id_pencairan_dana_detail \n" +
            "WHERE dd.status_lapor <> 'REJECTED')c ON b.pencairan_dana_detail = c.pencairan_dana_detail INNER JOIN \n" +
            "anggaran AS d ON a.id_anggaran=d.id INNER JOIN \n" +
            "karyawan AS e ON a.id_karyawan_pengaju=e.id)aaa \n" +
            "GROUP BY id_pencairan_dana)a ORDER BY tanggalpengajuan", nativeQuery = true)
    List<KasHarianDto> pageKasHarian2(String periodeAnggaran);



    @Query(value="select tanggal_pengajuan as tanggalpengajuan, tanggal_minta_pencairan as tanggalpencairan, tanggal_pencairan as tanggalcair, tanggal_laporan as tanggallaporan ,nama_karyawan as nama,GROUP_CONCAT(DISTINCT kode_anggaran SEPARATOR ', ')as kode,GROUP_CONCAT(DISTINCT nama_anggaran SEPARATOR ', ')as name,GROUP_CONCAT(DISTINCT deskripsi_biaya SEPARATOR ', ') as deskripsi,sum(jumlah) as pengajuan,nominal_minta_pencairan as disbursereq, \n" +
            "nominal_pencairan as disburse,sum(lapor)as realisasi,nominal_pencairan - sum(lapor) as selisih, aaa.id_pencairan_dana as idPencairanDana, bukti_pencairan as buktiPencairan, bukti_minta_pencairan as buktiMintaPencairan from \n" +
            "(select bukti_minta_pencairan,bukti_pencairan,a.tanggal_pengajuan,b.tanggal_minta_pencairan \n" +
            ",b.tanggal_pencairan,c.tanggal_laporan,nama_karyawan,d.kode_anggaran,nama_anggaran,a.deskripsi_biaya,a.jumlah,nominal_minta_pencairan,nominal_pencairan,c.jumlah as lapor,coalesce(id_pencairan_dana,a.id)as id_pencairan_dana from \n" +
            "(select uu.* from pengajuan_dana as uu inner join anggaran as vv on uu.id_anggaran=vv.id where uu.status not in ('HAPUS','REJECTED','CANCELED','CANCEL') and vv.id_periode_anggaran= ?1 and uu.id_departemen= ?2 order by uu.tanggal_pengajuan)a left join \n" +
            "(select aa.id as pencairan_dana_detail,aa.id_pengajuan_dana,id_pencairan_dana,nominal_minta_pencairan,nominal_pencairan,bb.tanggal_minta_pencairan,bb.tanggal_pencairan,bukti_pencairan,bb.file as bukti_minta_pencairan from pencairan_dana_detail as aa inner join pencairan_dana as bb on aa.id_pencairan_dana=bb.id)b on a.id=b.id_pengajuan_dana left join \n" +
            "(select cc.id as pencairan_dana_detail,dd.jumlah,dd.tanggal_laporan from pencairan_dana_detail as cc inner join laporan_dana as dd on cc.id=dd.id_pencairan_dana_detail where dd.status_lapor <> 'REJECTED')c on b.pencairan_dana_detail = c.pencairan_dana_detail inner join \n" +
            "anggaran as d on a.id_anggaran=d.id inner join \n" +
            "karyawan as e on a.id_karyawan_pengaju=e.id)aaa \n" +
            "group by id_pencairan_dana \n" +
            "order by tanggal_pengajuan", nativeQuery = true)
    List<KasHarianDto> pageKasHarianDepartemen(String periodeAnggaran, String departemen);



    @Query(value="\n" +
            "SELECT id,tanggal_pengajuan AS tanggalPengajuan,nama_departemen AS namaDepartemen,nama_karyawan AS namaKaryawan,kode_anggaran AS kodeAnggaran, deskripsi AS detailAnggaran, deskripsi_biaya AS deskripsiBiaya,jumlah AS pengajuan,realisasi, jumlah - COALESCE(realisasi,0)AS selisih FROM\n" +
            "(SELECT a.id,tanggal_pengajuan,e.nama_departemen,d.nama_karyawan,c.kode_anggaran, b.deskripsi, a.deskripsi_biaya,a.jumlah FROM pengajuan_dana AS a \n" +
            "INNER JOIN anggaran_detail AS b ON a.id_anggaran_detail = b.id\n" +
            "INNER JOIN anggaran AS c ON b.id_anggaran = c.id\n" +
            "INNER JOIN karyawan AS d ON a.id_karyawan_pengaju = d.id\n" +
            "INNER JOIN departemen AS e ON c.id_departemen = e.id\n" +
            "WHERE c.id_periode_anggaran = ?1 AND a.status NOT IN ('HAPUS','CANCELED','REJECTED') AND b.status = 'AKTIF' AND c.status = 'AKTIF')a \n" +
            "LEFT JOIN\n" +
            "(SELECT b.id_pengajuan_dana,a.jumlah AS realisasi FROM laporan_dana AS a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id where a.status not in ('HAPUS','CANCELED','REJECTED') \n" +
            "and a.id_tahun_anggaran = ?1 group by id_pengajuan_dana)b\n" +
            "ON a.id = b.id_pengajuan_dana\n" +
            "ORDER BY tanggal_pengajuan" , nativeQuery = true)
    List<DetailTransaksiDto> listDetailTransaksi(String idPeriodeAnggaran);


    List<PengajuanDana> findByAnggaranDetailAnggaranDepartemenIdAndAnggaranDetailAnggaranPeriodeAnggaranIdOrderByTanggalPengajuan(String departemen, String periode);

    @Query(value = "select * from pengajuan_dana where status not in ('HAPUS','DELETE','REJECTED','CANCELED') and month(tanggal_pengajuan) = ?1 and year(tanggal_pengajuan) = ?2", nativeQuery = true)
    List<Object> dapatPengajuanDana(String bulan, String tahun);

    @Query(value = "select a.id from pengajuan_dana as a\n" +
            "inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "where b.id = ?1 and a.status not in ('HAPUS','CANCELED','REJECTED') and b.status = 'AKTIF' and date(tanggal_pengajuan) >= ?2", nativeQuery = true)
    List<String> cariPengajuanDanaLalu(String Id, LocalDate tanggal);


    @Query(value = "select round(sum(jumlah/1000),2) as total, MONTHNAME(tanggal_pengajuan) as bulan from pengajuan_dana \n" +
            "where year(tanggal_pengajuan) = year(now()) and status not in ('HAPUS','REJECTED','CANCELED') group by month(tanggal_pengajuan)", nativeQuery = true)
    List<TransaksiSetahunDto> jumlahTransaksiSetahun();

    @Query(value = "select round(sum(jumlah/1000),2) as total, MONTHNAME(tanggal_pengajuan) as bulan from pengajuan_dana \n" +
            "where year(tanggal_pengajuan) = year(now()) and status not in ('HAPUS','REJECTED','CANCELED') group by month(tanggal_pengajuan)", nativeQuery = true)
    List<TransaksiSetahunDuaDto> jumlahTransaksiSetahunDua();


    @Query(value = "select a.ada,a.nama, (coalesce(penerimaan,0)+coalesce(pengembalian,0)) as penerimaan,coalesce(pengeluaran,0) as pengeluaran from\n" +
            "(select 1 as ada,month(tanggal_pencairan) as bulan,year(tanggal_pencairan) as tahun, concat(left(monthname(tanggal_pencairan),3),'-',year(tanggal_pencairan)) as nama, sum(nominal_pencairan) as pengeluaran from pencairan_dana where \n" +
            "date(tanggal_pencairan) >= concat(year(DATE_SUB(now(), INTERVAL 5 month)),'-',month(DATE_SUB(now(), INTERVAL 5 month)),'-01') and status = 'AKTIF' group by month(tanggal_pencairan))as a\n" +
            "left join \n" +
            "(select 1 as ada,month(tanggal) as bulan,year(tanggal) as tahun, concat(left(monthname(tanggal),3),'-',year(tanggal)) as nama, sum(total) as penerimaan from penerimaan_real where \n" +
            "date(tanggal) >= concat(year(DATE_SUB(now(), INTERVAL 5 month)),'-',month(DATE_SUB(now(), INTERVAL 5 month)),'-01') and status = 'AKTIF' group by month(tanggal)) as b\n" +
            "on a.ada = b.ada and a.nama = b.nama\n" +
            "left join\n" +
            "(select 1 as ada,month(tanggal_terima) as bulan,year(tanggal_terima) as tahun, concat(left(monthname(tanggal_terima),3),'-',year(tanggal_terima)) as nama, sum(nominal_pengembalian) as pengembalian from pengembalian_dana_proses \n" +
            "where date(tanggal_terima) >= concat(year(DATE_SUB(now(), INTERVAL 5 month)),'-',month(DATE_SUB(now(), INTERVAL 5 month)),'-01') and status = 'AKTIF' \n" +
            "group by month(tanggal_terima)) as c\n" +
            "on a.ada = c.ada and a.nama = c.nama\n" +
            "order by a.tahun,a.bulan\n", nativeQuery = true)
    List<KeluarMasukDto> dataTransaksiEnamBulan();

}
