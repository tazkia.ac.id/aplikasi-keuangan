package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DepartemenDao extends PagingAndSortingRepository<Departemen, String>, CrudRepository<Departemen, String> {

        Page<Departemen> findByStatusOrderByKodeDepartemen(StatusRecord statusRecord, Pageable page);

        Page<Departemen> findByStatusAndInstansiIdInOrderByKodeDepartemen(StatusRecord statusRecord, List<String> idInstansi, Pageable page);

        Page<Departemen> findByStatusAndKodeDepartemenContainingOrNamaDepartemenContainingOrderByKodeDepartemen(StatusRecord statusRecord, String kode, String nama, Pageable page);

        Page<Departemen> findByStatusAndInstansiIdInAndKodeDepartemenContainingOrNamaDepartemenContainingOrderByKodeDepartemen(StatusRecord statusRecord, List<String> idInstansi, String kode, String nama, Pageable page);

        List<Departemen> findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord statusRecord, StatusRecord statusAktif);

        @Query(value = "select sum(blocking_pengajuan) as jml from departemen where id in (?1) and status = 'AKTIF'", nativeQuery = true)
        Integer jmlBlockingPengajuan(List<String> karyawanDepartemen);

        Departemen findByStatusAndId(StatusRecord statusRecord, String id);

        List<Departemen> findByStatusAndStatusAktifAndInstansi(StatusRecord statusRecord, StatusRecord statusRecord2, Instansi instansi);

        List<Departemen> findByStatusAndStatusAktif(StatusRecord statusRecord, StatusRecord statusRecord3);


}
