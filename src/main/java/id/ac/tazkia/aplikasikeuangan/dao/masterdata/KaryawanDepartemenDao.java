package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.KaryawanDepartemen;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface KaryawanDepartemenDao extends PagingAndSortingRepository<KaryawanDepartemen, String>, CrudRepository<KaryawanDepartemen, String> {

     List<KaryawanDepartemen> findByStatusAndKaryawanAndTanggalMulaiBefore(StatusRecord statusRecord, Karyawan karyawan, LocalDate tanggal);

     @Query("select kd.departemen.id from KaryawanDepartemen kd where kd.status = :status and kd.karyawan = :karyawan and kd.tanggalMulai <= :tanggal")
     List<String> cariId(@Param("status") StatusRecord statusRecord, @Param("karyawan")Karyawan karyawan, @Param("tanggal")LocalDate tanggal);




}
