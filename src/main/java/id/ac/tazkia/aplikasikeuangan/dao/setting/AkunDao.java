package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.setting.Akun;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AkunDao extends PagingAndSortingRepository<Akun, String>, CrudRepository<Akun, String> {



}
