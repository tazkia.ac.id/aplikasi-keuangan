package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.dto.transaksi.PengembalianDanaProsesAkuntingDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.PengembalianDanaProsesDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDanaProses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface PengembalianDanaProsesDao extends PagingAndSortingRepository<PengembalianDanaProses, String>, CrudRepository<PengembalianDanaProses, String> {

    Page<PengembalianDanaProses> findByStatusAndStatusPengembalianAndUserPengembaliOrderByTanggalPengembalian(StatusRecord statusRecord, StatusRecord statusPengembalian, Karyawan karyawan, Pageable page);

    @Query(value = "SELECT a.id,e.id AS pengembalianDanaProses, e.file, CAST(keterangan_pengajuan AS CHAR) AS keteranganPengajuan, a.nominal AS pengembalian, b.nominal_pencairan AS pencairan,c.jumlah_laporan as jumlahLaporan FROM pengembalian_dana AS a \n" +
            "INNER JOIN pencairan_dana AS b ON a.id_pencairan_dana = b.id\n" +
            "INNER JOIN\n" +
            "(SELECT bb.id_pencairan_dana,SUM(aa.jumlah)AS jumlah_laporan FROM laporan_dana AS aa \n" +
            "INNER JOIN pencairan_dana_detail AS bb ON aa.id_pencairan_dana_detail = bb.id GROUP BY id_pencairan_dana)c ON a.id_pencairan_dana=c.id_pencairan_dana\n" +
            "INNER JOIN\n" +
            "(SELECT id_pencairan_dana, CONCAT(deskripsi_biaya) AS keterangan_pengajuan FROM pencairan_dana_detail AS cc INNER JOIN pengajuan_dana AS dd ON cc.id_pengajuan_dana = dd.id GROUP BY id_pencairan_dana)d ON a.id_pencairan_dana = d.id_pencairan_dana\n" +
            "INNER JOIN pengembalian_dana_proses AS e ON a.id = e.id_pengembalian_dana\n" +
            "WHERE b.user_minta_pencairan = ?1 AND a.status = ?2 AND a.status_pengembalian = ?3 ORDER BY b.tanggal_pencairan", nativeQuery = true)
    Page<PengembalianDanaProsesDto> dataPengembalianProses(String karyawan, String statusRecord, String statusAktif, Pageable page);


//    @Query(value = "", nativeQuery = true)
//    Page<> dataPengembalianDanaOpen(String karyawan, String statusRecord, String statusAktif, Pageable page);


    Integer countByStatusAndUserPengembaliAndStatusPengembalian(StatusRecord statusRecord, Karyawan karyawan, StatusRecord status);

    Page<PengembalianDanaProses> findByStatusAndUserPengembaliOrderByTanggalPengembalianDesc(StatusRecord statusRecord, Karyawan karyawan, Pageable pageable);

    @Query(value = "SELECT COALESCE(COUNT(id),0) AS jml_refund FROM\n" +
            "(SELECT a.id, 1 AS ada FROM pengembalian_dana_proses AS a\n" +
            "INNER JOIN pengembalian_dana_proses_detail AS b ON a.id = b.id_pengembalian_dana_proses\n" +
            "INNER JOIN pengembalian_dana AS c ON b.id_pengembalian_dana = c.id\n" +
            "INNER JOIN pencairan_dana AS d ON c.id_pencairan_dana = d.id\n" +
            "INNER JOIN departemen AS e ON d.id_departemen = e.id\n" +
            "WHERE e.id_instansi IN (?1) AND a.status_pengembalian = ?2\n" +
            "GROUP BY a.id) aa GROUP BY ada" , nativeQuery = true)
    Integer jumlahRefundWaiting(List<String> idInstansi, String status);

    @Query(value = "SELECT a.*, nama_karyawan, e.nama_departemen, DATE_FORMAT(tanggal_pengembalian, '%a %D %b %Y %T')AS tanggal FROM pengembalian_dana_proses AS a\n" +
            "INNER JOIN pengembalian_dana_proses_detail AS b ON a.id = b.id_pengembalian_dana_proses\n" +
            "INNER JOIN pengembalian_dana AS c ON b.id_pengembalian_dana = c.id\n" +
            "INNER JOIN pencairan_dana AS d ON c.id_pencairan_dana = d.id\n" +
            "INNER JOIN departemen AS e ON d.id_departemen = e.id\n" +
            "INNER JOIN karyawan AS f ON a.user_pengembali = f.id\n" +
            "WHERE e.id_instansi IN (?1) AND a.status_pengembalian = ?2\n" +
            "GROUP BY a.id order by a.id", nativeQuery = true, countQuery = "SELECT COALESCE(COUNT(id),0) AS jml_refund FROM\n" +
            "(SELECT a.id, 1 AS ada FROM pengembalian_dana_proses AS a\n" +
            "INNER JOIN pengembalian_dana_proses_detail AS b ON a.id = b.id_pengembalian_dana_proses\n" +
            "INNER JOIN pengembalian_dana AS c ON b.id_pengembalian_dana = c.id\n" +
            "INNER JOIN pencairan_dana AS d ON c.id_pencairan_dana = d.id\n" +
            "INNER JOIN departemen AS e ON d.id_departemen = e.id\n" +
            "WHERE e.id_instansi IN (?1) AND a.status_pengembalian = ?2\n" +
            "GROUP BY a.id) aa GROUP BY ada\n")
    Page<Object> pageApprovalRefund(List<String> idLembaga, String status, Pageable pageable);

    @Query(value = "SELECT a.id AS id_pencairan_dana_proses,d.*,f.nama_karyawan, c.nominal FROM pengembalian_dana_proses AS a\n" +
            "INNER JOIN pengembalian_dana_proses_detail AS b ON a.id = b.id_pengembalian_dana_proses\n" +
            "INNER JOIN pengembalian_dana AS c ON b.id_pengembalian_dana = c.id\n" +
            "INNER JOIN pencairan_dana AS d ON c.id_pencairan_dana = d.id\n" +
            "INNER JOIN departemen AS e ON d.id_departemen = e.id\n" +
            "INNER JOIN karyawan AS f ON d.user_pencairan = f.id\n" +
            "WHERE e.id_instansi IN (?1) AND a.status_pengembalian = ?2 order by a.id", nativeQuery = true)

    List<Object> ListApprovalRefund(List<String> idLembaga, String status);


    @Query(value = "SELECT a.*, nama_karyawan, e.nama_departemen,DATE_FORMAT(tanggal_pengembalian, '%a %D %b %Y %T')AS tanggal  FROM pengembalian_dana_proses AS a\n" +
            "INNER JOIN pengembalian_dana_proses_detail AS b ON a.id = b.id_pengembalian_dana_proses\n" +
            "INNER JOIN pengembalian_dana AS c ON b.id_pengembalian_dana = c.id\n" +
            "INNER JOIN pencairan_dana AS d ON c.id_pencairan_dana = d.id\n" +
            "INNER JOIN departemen AS e ON d.id_departemen = e.id\n" +
            "INNER JOIN karyawan AS f ON a.user_pengembali = f.id\n" +
            "WHERE a.user_pengembali = ?1 AND a.status_pengembalian = ?2\n" +
            "GROUP BY a.id", nativeQuery = true, countQuery = "select coalesce(count(id),0) as jml_refund from\n" +
            "(select a.id, 1 as ada from pengembalian_dana_proses as a\n" +
            "inner join pengembalian_dana_proses_detail as b on a.id = b.id_pengembalian_dana_proses\n" +
            "inner join pengembalian_dana as c on b.id_pengembalian_dana = c.id\n" +
            "inner join pencairan_dana as d on c.id_pencairan_dana = d.id\n" +
            "inner join departemen as e on d.id_departemen = e.id\n" +
            "where a.user_pengembali = ?1 and a.status_pengembalian = ?2\n" +
            "group by a.id) aa group by ada")
    Page<Object> pageRefundKaryawan(String idKaryawan, String status, Pageable pageable);

    @Query(value = "SELECT a.id AS id_pencairan_dana_proses,d.*,f.nama_karyawan, c.nominal FROM pengembalian_dana_proses AS a\n" +
            "INNER JOIN pengembalian_dana_proses_detail AS b ON a.id = b.id_pengembalian_dana_proses\n" +
            "INNER JOIN pengembalian_dana AS c ON b.id_pengembalian_dana = c.id\n" +
            "INNER JOIN pencairan_dana AS d ON c.id_pencairan_dana = d.id\n" +
            "INNER JOIN departemen AS e ON d.id_departemen = e.id\n" +
            "INNER JOIN karyawan AS f ON d.user_pencairan = f.id\n" +
            "WHERE a.user_pengembali = ?1 AND a.status_pengembalian = ?2", nativeQuery = true)
    List<Object> listRefundKaryawanDetail(String idKaryawan, String status);

    @Query(value = "SELECT a.*, f.nama_karyawan, e.nama_departemen,DATE_FORMAT(tanggal_pengembalian, '%a %D %b %Y %T')AS tanggal, g.nama_karyawan AS pengesah,DATE_FORMAT(tanggal_terima, '%a %D %b %Y %T')AS tt  FROM pengembalian_dana_proses AS a\n" +
            "INNER JOIN pengembalian_dana_proses_detail AS b ON a.id = b.id_pengembalian_dana_proses\n" +
            "INNER JOIN pengembalian_dana AS c ON b.id_pengembalian_dana = c.id\n" +
            "INNER JOIN pencairan_dana AS d ON c.id_pencairan_dana = d.id\n" +
            "INNER JOIN departemen AS e ON d.id_departemen = e.id\n" +
            "INNER JOIN karyawan AS f ON a.user_pengembali = f.id\n" +
            "INNER JOIN karyawan AS g ON a.user_penerima = g.id\n" +
            "WHERE a.user_pengembali = ?1 AND a.status_pengembalian = ?2\n" +
            "GROUP BY a.id\n", nativeQuery = true, countQuery = "\n" +
            "SELECT COALESCE(COUNT(id),0) AS jml_refund FROM\n" +
            "(SELECT a.id, 1 AS ada FROM pengembalian_dana_proses AS a\n" +
            "INNER JOIN pengembalian_dana_proses_detail AS b ON a.id = b.id_pengembalian_dana_proses\n" +
            "INNER JOIN pengembalian_dana AS c ON b.id_pengembalian_dana = c.id\n" +
            "INNER JOIN pencairan_dana AS d ON c.id_pencairan_dana = d.id\n" +
            "INNER JOIN departemen AS e ON d.id_departemen = e.id\n" +
            "WHERE a.user_pengembali = ?1 AND a.status_pengembalian = ?2\n" +
            "GROUP BY a.id) aa GROUP BY ada")
    Page<Object> pageRefundKaryawanClose(String idKaryawan, String status, Pageable pageable);


    @Query(value = "select sum(nominal_pengembalian)as totalPengembalian from pengembalian_dana_proses where status = 'AKTIF' and status_pengembalian = 'CLOSED' and tanggal_pengembalian >= ?1", nativeQuery = true)
    BigDecimal totalPengembalian(LocalDate tanggalMulai);

    @Query(value = "select a.id as id, a.tanggal_pengembalian as tanggalPengembalian, a.keterangan_pengembalian as keteranganPengembalian, a.nominal_pengembalian as nominalPengembalian, \n" +
            "f.pilihan as pilihan, f.template_jurnal as templateJurnal from pengembalian_dana_proses as a\n" +
            "inner join pengembalian_dana_proses_detail as b on a.id = b.id_pengembalian_dana_proses \n" +
            "and b.status = 'AKTIF' and a.status_pengembalian = 'CLOSED' and year(a.tanggal_pengembalian) > '2023' and a.status_akunting = 'WAITING'\n" +
            "inner join pengembalian_dana as c on b.id_pengembalian_dana = c.id\n" +
            "inner join pencairan_dana_detail as d on c.id_pencairan_dana = d.id_pencairan_dana\n" +
            "inner join pengajuan_dana as e on d.id_pengajuan_dana = e.id\n" +
            "inner join anggaran_detail as f on e.id_anggaran_detail = f.id and template_jurnal is not null", nativeQuery = true)
    List<PengembalianDanaProsesAkuntingDto> listPengembalianDana();
}
