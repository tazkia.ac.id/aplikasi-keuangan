package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Bank;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BankDao extends PagingAndSortingRepository<Bank, String>, CrudRepository<Bank, String> {

    List<Bank> findByStatusOrderByNamaBank(StatusRecord statusRecord);

}
