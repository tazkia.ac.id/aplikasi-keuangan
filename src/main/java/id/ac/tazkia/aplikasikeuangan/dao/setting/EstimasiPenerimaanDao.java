package id.ac.tazkia.aplikasikeuangan.dao.setting;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPenerimaan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;

public interface EstimasiPenerimaanDao extends PagingAndSortingRepository<EstimasiPenerimaan, String>, CrudRepository<EstimasiPenerimaan, String> {

    Page<EstimasiPenerimaan> findByStatusAndPenerimaanOrderByTanggal(StatusRecord statusRecord, Penerimaan penerimaan, Pageable pageable);

    List<EstimasiPenerimaan> findByStatusAndPenerimaanOrderByTanggal(StatusRecord statusRecord, Penerimaan penerimaan);

    @Query(value = "select coalesce(sum(total),0.00) as total_amount from estimasi_penerimaan where status='AKTIF' and id_rencana_penerimaan = ?1", nativeQuery = true)
    BigDecimal getTotalEstimasiPenerimaan(String idRencanaPenerimaan);

    @Query(value = "select coalesce(sum(kuantitas),0.00) as kuantitas from estimasi_penerimaan where status='AKTIF' and id_rencana_penerimaan = ?1", nativeQuery = true)
    BigDecimal getKuantitasEstimasiPenerimaan(String idRencanaPenerimaan);


    @Query(value = "select sum(total) as total_estimasi_penerimaan from estimasi_penerimaan where status = 'AKTIF' and id_rencana_penerimaan = ?1", nativeQuery = true)
    BigDecimal cariTotalPenerimaan(String idRencanaPenerimaan);

}
