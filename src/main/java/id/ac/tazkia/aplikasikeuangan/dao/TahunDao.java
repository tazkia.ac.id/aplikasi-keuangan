package id.ac.tazkia.aplikasikeuangan.dao;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.Tahun;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TahunDao extends PagingAndSortingRepository<Tahun, String>, CrudRepository<Tahun, String> {

    List<Tahun> findByStatusOrderByTahunDesc(StatusRecord statusRecord);

    Tahun findByStatusAndTahun(StatusRecord statusRecord, String tahun);

}
