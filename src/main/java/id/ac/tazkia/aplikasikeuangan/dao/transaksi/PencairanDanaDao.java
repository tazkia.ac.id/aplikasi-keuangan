package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.dto.transaksi.PencairanDanaAkuntingDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
//import jdk.net.SocketFlow;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface PencairanDanaDao extends PagingAndSortingRepository<PencairanDana, String>, CrudRepository<PencairanDana, String> {

    PencairanDana findByStatusAndStatusPencairanAndPengajuanDanaId(StatusRecord statusRecord, StatusRecord statusPencairan, String pengajuanDana);

    PencairanDana findByStatusAndId(StatusRecord statusRecord, String id);
    List<PencairanDana> findByStatusAndStatusPencairanAndPengajuanDanaIn(StatusRecord statusRecord, StatusRecord statusPencairan, List<PengajuanDana> pengajuanDanas);

    Page<PencairanDana> findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi, Pageable page);
    Integer countByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi);
    Integer countByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi);


    Page<PencairanDana> findByStatusAndStatusPencairanAndTanggalMintaPencairanBetweenAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusCair, LocalDateTime dari, LocalDateTime sampai, List<String> instansi, Pageable page);


    Page<PencairanDana> findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByTanggalMintaPencairanDesc(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi, Pageable page);

    Page<PencairanDana> findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByNominalPencairan(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi, Pageable page);

    Page<PencairanDana> findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByNominalPencairanDesc(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi, Pageable page);

    Page<PencairanDana> findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByUserMintaPencairanNamaKaryawan(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi, Pageable page);

    Page<PencairanDana> findByStatusAndStatusPencairanAndDepartemenInstansiIdInOrderByPengajuanDanaDepartemenNamaDepartemen(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi, Pageable page);


    Page<PencairanDana> findByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalPencairan(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi, Pageable page);
    Page<PencairanDana> findByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdInOrderByTanggalPencairan(StatusRecord statusRecord, StatusRecord statusPencairan, StatusRecord statusCair, List<String> instansi, Pageable page);

    Page<PencairanDana> findByStatusAndStatusPencairanAndStatusCairOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusPencairan, StatusRecord statusCair, Pageable page);

    Integer countByStatusAndStatusPencairanAndStatusCairAndDepartemenInstansiIdIn(StatusRecord statusRecord, StatusRecord statusPencairan, StatusRecord statusCair, List<String> instansi);


    Page<PencairanDana> findByStatusAndStatusCairAndDepartemenInstansiIdInOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusCair, List<String> instansi, Pageable page);

    Page<PencairanDana> findByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord statusRecord, StatusRecord statusCair, Karyawan karyawan, Pageable page);
    Page<PencairanDana> findByStatusPencairanAndStatusAndUserMintaPencairanAndKeteranganPencairanContainingIgnoreCaseOrderByTanggalPencairan(StatusRecord statusRecord, StatusRecord statusCair, Karyawan karyawan,String search, Pageable page);
    Integer countByStatusPencairanAndStatusAndUserMintaPencairanOrderByTanggalPencairan(StatusRecord statusRecord, StatusRecord statusCair, Karyawan karyawan);

    Page<PencairanDana> findByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusCair, Karyawan karyawan, Pageable page);

    Page<PencairanDana> findByStatusAndStatusPencairanAndUserMintaPencairanAndKeteranganContainingIgnoreCaseOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusCair, Karyawan karyawan,String search, Pageable page);
    Integer countByStatusAndStatusPencairanAndUserMintaPencairanOrderByTanggalMintaPencairan(StatusRecord statusRecord, StatusRecord statusCair, Karyawan karyawan);

    Page<PencairanDana> findByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord statusRecord, StatusRecord statusCair, StatusRecord statusPengajuan , Karyawan karyawan, Pageable page);
    Integer countByStatusAndStatusCairAndPengajuanDanaStatusAndPengajuanDanaKaryawanPengajuOrderByTanggalPencairan(StatusRecord statusRecord, StatusRecord statusCair, StatusRecord statusPengajuan , Karyawan karyawan);



    @Query("select pd from PencairanDana pd where pd.tanggalMintaPencairan >= :tanggalAwal and pd.tanggalMintaPencairan <= :tanggalAkhir and pd.statusPencairan = :statusPencairan and  pd.statusCair = :statusCair and pd.status='AKTIF' order by pd.tanggalMintaPencairan")
    List<PencairanDana> listPencairanDanaApproveTanggal(@Param("tanggalAwal") LocalDateTime localDateAwal, @Param("tanggalAkhir") LocalDateTime localDateAkhir, @Param("statusPencairan") StatusRecord statusPencairan, @Param("statusCair") StatusRecord statsCair);

    @Query("select pd from PencairanDana pd where pd.statusPencairan = :statusPencairan and  pd.statusCair = :statusCair and pd.status ='AKTIF' and pd.tanggalMintaPencairan <= :tanggal order by pd.tanggalMintaPencairan")
    List<PencairanDana> listPencairanDanaApprove(@Param("tanggal") LocalDateTime localDateTime, @Param("statusPencairan") StatusRecord statusPencairan, @Param("statusCair") StatusRecord statsCair);

    @Query(value = "select COALESCE (sum(nominal_minta_pencairan),0)as total_pengajuan from pencairan_dana where tanggal_minta_pencairan <= :tanggal and status_pencairan = :statusPencairan and status_cair = :statusCair and status='AKTIF'", nativeQuery = true)
    BigDecimal totalPencairanDanaApprove(@Param("tanggal") LocalDateTime localDateTime, @Param("statusPencairan") String statusPencairan, @Param("statusCair") String statsCair);

    @Query(value = "select COALESCE (sum(nominal_minta_pencairan),0)as total_pengajuan from pencairan_dana where tanggal_minta_pencairan >= :tanggalAwal and tanggal_minta_pencairan <= :tanggalAkhir and status_pencairan = :statusPencairan and status_cair = :statusCair and status='AKTIF'", nativeQuery = true)
    BigDecimal totalPencairanDanaApproveTanggal(@Param("tanggalAwal") LocalDateTime localDateAwal, @Param("tanggalAkhir") LocalDateTime localDateAkhir, @Param("statusPencairan") String statusPencairan, @Param("statusCair") String statsCair);

    @Query(value = "select COALESCE (sum(nominal_pencairan),0)as total_pengajuan from pencairan_dana where tanggal_minta_pencairan <= :tanggal and status_pencairan in ('CAIR','LAPOR','CLOSED') and status_cair in ('WAITING','LAPOR','CLOSED') and status='AKTIF'", nativeQuery = true)
    BigDecimal totalPencairanDanaApprove2(@Param("tanggal") LocalDateTime localDateTime);

    @Query(value = "select COALESCE (sum(nominal_pencairan),0)as total_pengajuan from pencairan_dana where tanggal_minta_pencairan >= :tanggalAwal and tanggal_minta_pencairan <= :tanggalAkhir and status_pencairan in ('CAIR','LAPOR','CLOSED') and status_cair in ('WAITING','LAPOR','CLOSED') and status='AKTIF'", nativeQuery = true)
    BigDecimal totalPencairanDanaApproveTanggal2(@Param("tanggalAwal") LocalDateTime localDateAwal, @Param("tanggalAkhir") LocalDateTime localDateAkhir);



    @Query("select pd from PencairanDana pd where pd.tanggalMintaPencairan >= :tanggalAwal and pd.tanggalMintaPencairan <= :tanggalAkhir and pd.statusPencairan in ('CAIR','LAPOR','CLOSED') and  pd.statusCair in ('WAITING','LAPOR','CLOSED') and pd.status='AKTIF' order by pd.tanggalMintaPencairan")
    List<PencairanDana> listPencairanDanaApproveTanggal2(@Param("tanggalAwal") LocalDateTime localDateAwal, @Param("tanggalAkhir") LocalDateTime localDateAkhir);

    @Query("select pd from PencairanDana pd where pd.statusPencairan in ('CAIR','LAPOR','CLOSED') and  pd.statusCair in ('WAITING','LAPOR','CLOSED') and pd.status ='AKTIF' and pd.tanggalMintaPencairan <= :tanggal order by pd.tanggalMintaPencairan")
    List<PencairanDana> listPencairanDanaApprove2(@Param("tanggal") LocalDateTime localDateTime);


    @Query(value = "select sum(nominal_pencairan)as totalPencairan from pencairan_dana where status = 'AKTIF' and tanggal_pencairan >= ?1 and nominal_pencairan > 0", nativeQuery = true)
    BigDecimal totalPencairan(LocalDate tanggalMulai);


    @Query(value = "SELECT count(a.id) as jml FROM pencairan_dana as a\n" +
            "inner join pencairan_dana_detail as b on b.id_pencairan_dana = a.id\n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id inner join anggaran_detail as d \n" +
            "on c.id_anggaran_detail = d.id \n" +
            "inner join anggaran as e on d.id_anggaran = e.id \n" +
            "WHERE user_minta_pencairan = ?1 and e.id_departemen = ?2 \n" +
            "AND status_pencairan IN ('CAIR', 'CLOSED')\n" +
            "AND status_cair in ('WAITING','CAIR')\n" +
            "AND a.status = 'AKTIF'\n" +
            "AND tanggal_pencairan <= DATE_SUB(NOW(), INTERVAL 14 DAY)", nativeQuery = true)
    Integer cariLaporanMankrak(String idKaryawan, String idDepartemen);

    @Query(value = "select a.id as id, a.tanggal_pencairan as tanggalPencairan, concat(a.keterangan,', ',a.keterangan_pencairan) as deskripsi, a.nominal_pencairan as nominalPencairan, \n" +
            "if(pilihan = 'KEGIATAN','UMK-KGT-CAIR','UMK-PBJ-CAIR') as templateJurnal from pencairan_dana as a\n" +
            "inner join pencairan_dana_detail as b on a.id = b.id_pencairan_dana and a.status_cair in ('CAIR','CLOSED','LAPOR') \n" +
            "and a.status_pencairan in ('CLOSED','LAPOR')\n" +
            "and a.status_akunting = 'WAITING' and year(a.tanggal_pencairan) > '2023'\n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id and c.status not in ('HAPUS','CANCELED','REJECTED')\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id and d.pilihan is not null\n" +
            "group by a.id;", nativeQuery = true)
    List<PencairanDanaAkuntingDto> listPencairanAkuntingDto();


}
