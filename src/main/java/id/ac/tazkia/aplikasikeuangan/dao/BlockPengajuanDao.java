package id.ac.tazkia.aplikasikeuangan.dao;

import id.ac.tazkia.aplikasikeuangan.entity.BlockPengajuan;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface BlockPengajuanDao extends PagingAndSortingRepository<BlockPengajuan, String> {

    BlockPengajuan findByStatus(StatusRecord statusRecord);

}
