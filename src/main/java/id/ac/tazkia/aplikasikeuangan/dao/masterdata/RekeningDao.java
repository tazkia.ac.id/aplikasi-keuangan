package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Rekening;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RekeningDao extends PagingAndSortingRepository<Rekening, String>, CrudRepository<Rekening, String> {

    List<Rekening> findByStatusOrderByAtasNama(StatusRecord statusRecord);

}
