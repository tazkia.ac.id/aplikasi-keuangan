package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.setting.AkunLevelSatu;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AkunLevelSatuDao extends PagingAndSortingRepository<AkunLevelSatu, String>, CrudRepository<AkunLevelSatu, String> {



}
