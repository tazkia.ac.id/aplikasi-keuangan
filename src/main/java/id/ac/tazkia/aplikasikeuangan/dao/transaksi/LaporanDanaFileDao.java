package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDanaFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LaporanDanaFileDao extends CrudRepository<LaporanDanaFile, String>, JpaRepository<LaporanDanaFile, String> {
    List<LaporanDanaFile> findByLaporanDanaAndStatus(LaporanDana laporanDana, StatusRecord status);
}
