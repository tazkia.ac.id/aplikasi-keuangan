package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MataAnggaranDao extends PagingAndSortingRepository<MataAnggaran, String>, CrudRepository<MataAnggaran, String> {

    Page<MataAnggaran> findByStatusOrderByKodeMataAnggaran(StatusRecord statusRecord, Pageable page);

    Page<MataAnggaran> findByStatusAndKodeMataAnggaranContainingOrderByKodeMataAnggaran(StatusRecord statusRecord, String kode, Pageable page);

    MataAnggaran findByStatus(StatusRecord statusRecord);

    List<MataAnggaran> findByStatusOrderByKodeMataAnggaran(StatusRecord statusRecord);

}
