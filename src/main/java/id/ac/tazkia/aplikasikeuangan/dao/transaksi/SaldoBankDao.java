package id.ac.tazkia.aplikasikeuangan.dao.transaksi;


import id.ac.tazkia.aplikasikeuangan.dto.transaksi.SaldoBankDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.SisaSaldoBankDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.SaldoBank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SaldoBankDao extends PagingAndSortingRepository<SaldoBank, String>, CrudRepository<SaldoBank, String> {

    Page<SaldoBank> findByStatusOrderByTanggal(StatusRecord statusRecord, Pageable pageable);

    @Query(value = "select a.id as id,d.nama as instansi, a.tanggal as tanggal, amount, a.description as description, c.nama_karyawan as nama, e.nama_periode_anggaran as namaPeriode \n" +
            "from saldo_bank as a\n" +
            "inner join s_user as b on a.id_user = b.id\n" +
            "inner join karyawan as c on b.id = c.id_user inner join instansi as d on a.id_instansi = d.id inner join periode_anggaran as e on a.id_periode_anggaran = e.id \n" +
            "where month(a.tanggal) = ?1 \n" +
            "and year(a.tanggal) = ?2 \n" +
            "order by tanggal desc", countQuery = "select count(a.id) as jml\n" +
            "from saldo_bank as a\n" +
            "inner join s_user as b on a.id_user = b.id\n" +
            "inner join karyawan as c on b.id = c.id_user inner join instansi as d on a.id_instansi = d.id  \n" +
            "where month(a.tanggal) = ?1 \n" +
            "and year(a.tanggal) = ?2", nativeQuery = true)
    Page<SaldoBankDto> listSaldoBank(String bulan, String tahun, Pageable pageable);


    @Query(value = "select a.id as id,d.nama as instansi, a.tanggal as tanggal, amount, a.description as description, c.nama_karyawan as nama, e.nama_periode_anggaran as namaPeriode \n" +
            "from saldo_bank as a\n" +
            "inner join s_user as b on a.id_user = b.id\n" +
            "inner join karyawan as c on b.id = c.id_user inner join instansi as d on a.id_instansi = d.id inner join periode_anggaran as e on a.id_periode_anggaran = e.id \n" +
            "where month(a.tanggal) = ?1 \n" +
            "and year(a.tanggal) = ?2 and a.id_instansi = ?3 \n" +
            "order by tanggal desc", countQuery = "select count(a.id) as jml\n" +
            "from saldo_bank as a\n" +
            "inner join s_user as b on a.id_user = b.id\n" +
            "inner join karyawan as c on b.id = c.id_user inner join instansi as d on a.id_instansi = d.id \n" +
            "where month(a.tanggal) = ?1 \n" +
            "and year(a.tanggal) = ?2 and a.id_instansi = ?3", nativeQuery = true)
    Page<SaldoBankDto> listSaldoBankInstansi(String bulan, String tahun, String idInstansi, Pageable pageable);


    @Query(value = "select coalesce(masuk,0) as masuk, coalesce(keluar,0) as keluar, coalesce(masuk,0)-coalesce(keluar,0) as sisa from\n" +
            "(select sum(coalesce(amount,0)) as masuk, id_periode_anggaran from saldo_bank where id_periode_anggaran = ?1 and id_instansi = ?2 and status = 'AKTIF') as a\n" +
            "left join\n" +
            "(select sum(coalesce(jumlah,0)) as keluar,c.id_periode_anggaran \n" +
            "from pengajuan_dana as a inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id inner join departemen as d on c.id_departemen = d.id where c.id_periode_anggaran = ?1 and d.id_instansi = ?2 \n" +
            "and a.status not in ('CANCELED','REJECTED','HAPUS') and nomor_urut > total_nomor_urut) as b \n" +
            "on a.id_periode_anggaran = b.id_periode_anggaran", nativeQuery = true)
    SisaSaldoBankDto sisaSaldoBank(String idPeriodeAnggaran, String idInstansi);


    @Query(value = "select coalesce(masuk,0) as masuk, coalesce(keluar,0) as keluar, coalesce(masuk,0)-coalesce(keluar,0) as sisa, a.instansi from\n" +
            "(select sum(coalesce(amount,0)) as masuk, id_periode_anggaran, id_instansi, b.nama as instansi from saldo_bank as a \n" +
            "inner join instansi as b on a.id_instansi = b.id \n" +
            "where id_periode_anggaran = ?1 and id_instansi in (?2) and a.status = 'AKTIF') as a\n" +
            "left join\n" +
            "(select sum(coalesce(jumlah,0)) as keluar,c.id_periode_anggaran, id_instansi \n" +
            "from pengajuan_dana as a inner join anggaran_detail as b on a.id_anggaran_detail = b.id\n" +
            "inner join anggaran as c on b.id_anggaran = c.id inner join departemen as d on c.id_departemen = d.id \n" +
            "where c.id_periode_anggaran = ?1 and d.id_instansi in (?2)\n" +
            "and a.status not in ('CANCELED','REJECTED','HAPUS') and nomor_urut > total_nomor_urut) as b \n" +
            "on a.id_periode_anggaran = b.id_periode_anggaran and a.id_instansi = b.id_instansi group by a.id_instansi\n", nativeQuery = true)
    List<SisaSaldoBankDto> sisaSaldoBankList(String idPeriodeAnggaran, List<String> idInstansi);


}
