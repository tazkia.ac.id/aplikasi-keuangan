package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Jabatan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.SettingApprovalPengajuanDana;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface SettingApprovalPengajuanDanaDao extends PagingAndSortingRepository<SettingApprovalPengajuanDana, String>, CrudRepository<SettingApprovalPengajuanDana, String> {

    Page<SettingApprovalPengajuanDana> findByStatusAndJabatanKodeJabatanContainingIgnoreCaseOrJabatanNamaJabatanContainingIgnoreCaseOrderByJabatanKodeJabatan(StatusRecord statusRecord, String kodeJabatan, String namaJabatan, Pageable page);

    Page<SettingApprovalPengajuanDana> findByStatusOrderByJabatanKodeJabatan(StatusRecord statusRecord, Pageable page);

    List<SettingApprovalPengajuanDana> findByStatusAndJabatanOrderByNomorUrut(StatusRecord statusRecord, Jabatan jabatan);

    List<SettingApprovalPengajuanDana> findByStatusAndJabatanIdAndBatasPengajuanLessThanOrderByNomorUrut(StatusRecord statusRecord, String jabatan, BigDecimal batas);

    List<SettingApprovalPengajuanDana> findByStatusAndJabatanApproveIdIn(StatusRecord statusRecord, List<String> jabatan);

    @Query(value = "select coalesce(count(id),1) as jml from setting_approval_pengajuan_dana where status='AKTIF' and id_jabatan= ?1 and batas_pengajuan < ?2 group by id_jabatan", nativeQuery = true)
    Integer getJumlahApproval2(String jabatan, BigDecimal batas);

    @Query(value = "select coalesce(sum(nomor_urut),0)as nomor_urut from\n" +
            "(select * from setting_approval_pengajuan_dana where status='AKTIF' and id_jabatan=?1 and batas_pengajuan < ?2 order by nomor_urut desc limit 1)a", nativeQuery = true)
    Integer getJumlahApproval(String jabatan, BigDecimal batas);

    Integer countByStatusAndJabatan(StatusRecord statusRecord, Jabatan jabatan);

    Integer countByStatusAndJabatanApprove(StatusRecord statusRecord, Jabatan jabatan);

    @Query(value = "select coalesce(sum(nomor_urut),0)as nomor_urut from" +
            "(select * from pengajuan_dana_approve where id_pengajuan_dana=?1 and status_approve='WAITING' order by nomor_urut limit 1)a", nativeQuery = true)
    Integer nomorUrut(String pengajuanDana);

    @Query(value="select coalesce(sum(nomor_urut),0)as nomor_urut from\n" +
            "(select * from setting_approval_pengajuan_dana where status='AKTIF' and id_jabatan=?1 and batas_pengajuan < ?2 order by nomor_urut limit 1)a", nativeQuery = true)
    Integer nomorApproval(String idJabatan, BigDecimal jumlah);

}
