package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.dto.PrintLaporanDanaDto;
import id.ac.tazkia.aplikasikeuangan.dto.export.LaporanDanaDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.LaporanDanaAkuntingDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import jakarta.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.List;

public interface LaporanDanaDao extends PagingAndSortingRepository<LaporanDana, String>, CrudRepository<LaporanDana, String> {

    LaporanDana findByStatusAndId(StatusRecord statusRecord, String id);

    Page<LaporanDana> findByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(StatusRecord statusRecord, StatusRecord statusLapor, List<String> instansi, Pageable page);

    @Query(value = "select a.id,f.kode_anggaran as kodeAnggaran,f.nama_anggaran as namaAnggaran,a.deskripsi,z.jenis_pencairan as jenisPencairan,z.nomor_rekening as nomorRekening,\n" +
            "c.jumlah as jumlahPengajuan,z.nominal_pencairan,a.jumlah as jumlah_laporan,h.nama_karyawan as userPelapor,tanggal_laporan as tanggalLaporan,\n" +
            "g.nama_departemen as namaDepartemen, a.file \n" +
            "from laporan_dana as a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id and a.status = 'AKTIF' and a.status_lapor = 'WAITING'\n" +
            "inner join pencairan_dana as z on b.id_pencairan_dana = z.id \n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id and c.status in ('LAPOR','AKTIF')\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id and d.status = 'AKTIF'\n" +
            "inner join anggaran as f on d.id_anggaran = f.id and f.status = 'AKTIF'\n" +
            "inner join departemen as g on f.id_departemen = g.id and g.id_instansi in (?1)\n" +
            "inner join karyawan as h on a.user_pelapor = h.id order by tanggal_laporan", countQuery = "select count(a.id) as nomor\n" +
            "from laporan_dana as a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id and a.status = 'AKTIF' and a.status_lapor = 'WAITING'\n" +
            "inner join pencairan_dana as z on b.id_pencairan_dana = z.id \n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id and c.status in ('LAPOR','AKTIF')\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id and d.status = 'AKTIF'\n" +
            "inner join anggaran as f on d.id_anggaran = f.id and f.status = 'AKTIF'\n" +
            "inner join departemen as g on f.id_departemen = g.id and g.id_instansi in (?1)\n" +
            "inner join karyawan as h on a.user_pelapor = h.id", nativeQuery = true)
            Page<Object>tampilkanLaporanDana(List<String> idInstansi, Pageable page);


    @Query(value = "select count(a.id) as nomor\n" +
            "from laporan_dana as a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id and a.status = 'AKTIF' and a.status_lapor = 'WAITING'\n" +
            "inner join pencairan_dana as z on b.id_pencairan_dana = z.id \n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id and c.status in ('LAPOR','AKTIF')\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id and d.status = 'AKTIF'\n" +
            "inner join anggaran as f on d.id_anggaran = f.id and f.status = 'AKTIF'\n" +
            "inner join departemen as g on f.id_departemen = g.id and g.id_instansi in (?1)\n" +
            "inner join karyawan as h on a.user_pelapor = h.id", nativeQuery = true)
    Integer jumlahLaporanWaiting(List<String> idInstansi);


    @Query(value = "select a.id,f.kode_anggaran as kodeAnggaran,f.nama_anggaran as namaAnggaran,a.deskripsi,z.jenis_pencairan as jenisPencairan,z.nomor_rekening as nomorRekening,\n" +
            "c.jumlah as jumlahPengajuan,z.nominal_pencairan,a.jumlah as jumlah_laporan,h.nama_karyawan as userPelapor,tanggal_laporan as tanggalLaporan,\n" +
            "g.nama_departemen as namaDepartemen, a.file \n" +
            "from laporan_dana as a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id and a.status = 'AKTIF' and a.status_lapor = 'WAITING' and date(a.tanggal_laporan) >= ?2 and date(a.tanggal_laporan) <= ?3\n" +
            "inner join pencairan_dana as z on b.id_pencairan_dana = z.id \n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id and c.status in ('LAPOR','AKTIF')\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id and d.status = 'AKTIF'\n" +
            "inner join anggaran as f on d.id_anggaran = f.id and f.status = 'AKTIF'\n" +
            "inner join departemen as g on f.id_departemen = g.id and g.id_instansi in (?1)\n" +
            "inner join karyawan as h on a.user_pelapor = h.id order by tanggal_laporan", countQuery = "select count(a.id) as nomor\n" +
            "from laporan_dana as a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id and a.status = 'AKTIF' and a.status_lapor = 'WAITING' and date(a.tanggal_laporan) >= ?2 and date(a.tanggal_laporan) <= ?3\n" +
            "inner join pencairan_dana as z on b.id_pencairan_dana = z.id \n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id and c.status in ('LAPOR','AKTIF')\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id and d.status = 'AKTIF'\n" +
            "inner join anggaran as f on d.id_anggaran = f.id and f.status = 'AKTIF'\n" +
            "inner join departemen as g on f.id_departemen = g.id and g.id_instansi in (?1)\n" +
            "inner join karyawan as h on a.user_pelapor = h.id", nativeQuery = true)
    Page<Object>tampilkanLaporanDanaTanggal(List<String> idInstansi, LocalDate tanggalMulai, LocalDate tanggalSelesai, Pageable page);

//     Page<LaporanDana> findByStatusAndStatusLaporAndTanggalLaporanBetweenAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(
//         StatusRecord statusRecord, StatusRecord statusLapor, LocalDate startDate, LocalDate endDate, List<String> instansi, Pageable page);

     Page<LaporanDana> findByStatusAndStatusLaporAndTanggalLaporanBetweenAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(
        StatusRecord statusRecord, StatusRecord statusLapor, LocalDateTime startDateTime, LocalDateTime endDateTime, List<String> instansi, Pageable page);

    default Page<LaporanDana> findByStatusAndStatusLaporAndTanggalLaporanBetweenAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(
        StatusRecord statusRecord, StatusRecord statusLapor, LocalDate startDate, LocalDate endDate, List<String> instansi, Pageable page) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.atTime(LocalTime.MAX);
        return findByStatusAndStatusLaporAndTanggalLaporanBetweenAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdInOrderByTanggalLaporan(
            statusRecord, statusLapor, startDateTime, endDateTime, instansi, page);
    }

    Integer countByStatusAndStatusLaporAndPencairanDanaDetailPengajuanDanaDepartemenInstansiIdIn(StatusRecord statusRecord, StatusRecord statusLapor, List<String> instansi);

    Page<LaporanDana> findByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord statusRecord, StatusRecord statusLaporan, Karyawan karyawan, Pageable page);

    Integer countByStatusAndStatusLaporAndUserPelaporOrderByTanggalLaporan(StatusRecord statusRecord, StatusRecord statusLaporan, Karyawan karyawan);

    @Query(value = "SELECT (c.nominal_pencairan - COALESCE(SUM(a.jumlah),0)) AS sisa FROM laporan_dana AS a INNER JOIN pencairan_dana_detail AS b \n" +
            "ON a.id_pencairan_dana_detail = b.id INNER JOIN pencairan_dana AS c \n" +
            "ON b.id_pencairan_dana = c.id WHERE id_pencairan_dana= ?1 \n" +
            "GROUP BY b.id_pencairan_dana",nativeQuery = true)
    BigDecimal sisaPencairan(String pencairanDana);

    @Query(value = "select a.*,b.id_pencairan_dana from laporan_dana as a \n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id\n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id\n" +
            "inner join anggaran as e on d.id_anggaran = e.id\n" +
            "where e.id_departemen = ?1 \n" +
            "and e.id_periode_anggaran = ?2", nativeQuery = true)
    List<Object> attachmentLaporan(String idDepartemen, String idPeriodeAnggaran);


    @Query(value = "select a.*,b.id_pencairan_dana from laporan_dana as a \n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id\n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id\n" +
            "inner join anggaran as e on d.id_anggaran = e.id\n" +
            "where e.id_periode_anggaran = ?1", nativeQuery = true)
    List<Object> attachmentLaporanAll(String idPeriodeAnggaran);


    @Query(value = "select a.*,b.id_pencairan_dana from laporan_dana as a \n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id\n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id\n" +
            "inner join anggaran as e on d.id_anggaran = e.id\n" +
            "where month(c.tanggal_pengajuan) = ?1 and year(c.tanggal_pengajuan) = ?2", nativeQuery = true)
    List<Object> attachmentLaporanAll2(String bulan, String tahun);

    @Query(value = "select a.*,b.id_pencairan_dana from laporan_dana as a \n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id\n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id\n" +
            "inner join anggaran as e on d.id_anggaran = e.id\n" +
            "where month(c.tanggal_pengajuan) = month(now()) and year(c.tanggal_pengajuan) = year(now())", nativeQuery = true)
    List<Object> attachmentLaporanAll3();

    @Query(value = "select a.id from laporan_dana as a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id\n" +
            "inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id\n" +
            "inner join anggaran_detail as d on c.id_anggaran_detail = d.id\n" +
            "where d.id = ?1 and date(c.tanggal_pengajuan) >= ?2 and a.status = 'AKTIF' and a.status_lapor not in ('REJECTED','CANCELED','HAPUS')\n" +
            "and c.status not in ('REJECTED','CANCELED','HAPUS')", nativeQuery = true)
    List<String> cariLaporanDana(String idAnggaranDetail, LocalDate tanggal);


    List<LaporanDana> findByStatusAndPencairanDanaDetailPencairanDana(StatusRecord statusRecord, PencairanDana pencairanDana);


    @Query(value = "select g.nama_departemen as departemen,h.nama_karyawan as karyawan, group_concat(DISTINCT CONCAT(f.kode_anggaran,f.nama_anggaran) SEPARATOR ', ')as kode, \n" +
            "group_concat(DISTINCT CONCAT(e.deskripsi) SEPARATOR ', ')as detailAnggaran, sum(d.jumlah) as pengajuan, c.nominal_pencairan as pencairan, sum(a.jumlah) as laporan, \n" +
            "c.nominal_pencairan - sum(a.jumlah)as sisa, date(a.tanggal_laporan) as tanggal\n" +
            "from (select id, jumlah, id_pencairan_dana_detail, user_pelapor, tanggal_laporan from laporan_dana) as a\n" +
            "inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id\n" +
            "inner join (select id, nominal_pencairan from pencairan_dana) as c on b.id_pencairan_dana = c.id\n" +
            "inner join (select id, id_anggaran_detail,jumlah from pengajuan_dana) as d on b.id_pengajuan_dana = d.id\n" +
            "inner join (select id,id_anggaran,deskripsi from anggaran_detail) as e on d.id_anggaran_detail = e.id\n" +
            "inner join (select id,kode_anggaran, nama_anggaran, id_departemen from anggaran) as f on e.id_anggaran = f.id\n" +
            "inner join (select id, nama_departemen from departemen) as g on f.id_departemen = g.id\n" +
            "inner join (select id, nama_karyawan from karyawan) as h on a.user_pelapor = h.id\n" +
            "where id_pencairan_dana = ?1\n" +
            "group by c.id\n", nativeQuery = true)
    PrintLaporanDanaDto printLaporanDana(String idPencairan);

    @Query(value = "select a.id as id, concat(d.deskripsi,'-',c.deskripsi_biaya,'-',a.deskripsi) as deskripsi, a.tanggal_laporan as tanggalLaporan, a.jumlah as jumlah, a.posting as posting, d.pilihan as pilihan, template_jurnal as templateJurnal from laporan_dana as a\n" +
            "            inner join pencairan_dana_detail as b on a.id_pencairan_dana_detail = b.id and a.status = 'AKTIF' and a.status_lapor = 'CLOSED' and a.posting = 'WAITING' and year(a.tanggal_laporan) >'2023'\n" +
            "            inner join pengajuan_dana as c on b.id_pengajuan_dana = c.id\n" +
            "            inner join anggaran_detail as d on c.id_anggaran_detail = d.id and template_jurnal is not null", nativeQuery = true)
    List<LaporanDanaAkuntingDto> laporanDanaListAkunting();

}
