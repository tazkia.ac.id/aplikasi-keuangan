package id.ac.tazkia.aplikasikeuangan.dao.config;

import id.ac.tazkia.aplikasikeuangan.entity.config.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String>, CrudRepository<User, String> {
    User findByUsername(String username);
    User findByUsernameAndIdNot(String username, String id);
}
