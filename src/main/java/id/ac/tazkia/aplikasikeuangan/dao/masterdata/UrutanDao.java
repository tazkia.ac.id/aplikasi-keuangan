package id.ac.tazkia.aplikasikeuangan.dao.masterdata;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Urutan;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UrutanDao extends PagingAndSortingRepository<Urutan, String>, CrudRepository<Urutan, String> {

        Iterable<Urutan> findByStatusOrderByNomorUrut(StatusRecord statusRecord);

}
