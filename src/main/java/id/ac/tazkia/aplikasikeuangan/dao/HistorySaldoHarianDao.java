package id.ac.tazkia.aplikasikeuangan.dao;

import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardDto;
import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardKodeDto;
import id.ac.tazkia.aplikasikeuangan.entity.HistorySaldoHarian;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import jakarta.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface HistorySaldoHarianDao extends PagingAndSortingRepository<HistorySaldoHarian, String>, CrudRepository<HistorySaldoHarian, String> {

    @Query(value = "select id,nama_departemen as namaDepartemen, total_anggaran as totalAnggaran, used as pengeluaran, persentase, pagu_anggaran as paguAnggaran, sisa_pagu as sisaPagu from history_saldo_harian where tanggal_update = ?1 order by persentase desc", nativeQuery = true)
    List<SisaAnggaranDashboardDto> listSaldoHarian(LocalDateTime tanggal);

    @Query(value = "select a.id as id,a.nama_departemen as namaDepartemen, a.total_anggaran as totalAnggaran, a.used as pengeluaran, a.persentase, a.pagu_anggaran as paguAnggaran, a.sisa_pagu as sisaPagu, b.kode_departemen as kode, b.id as idDepartemen, a.tanggal_update as tanggalUpdate from history_saldo_harian as a inner join departemen as b on a.id_departemen = b.id where a.tanggal_update = ?1 order by a.persentase desc", nativeQuery = true)
    List<SisaAnggaranDashboardKodeDto> listSaldoHarianKode(LocalDateTime tanggal);

    @Query(value = "select id,nama_departemen as namaDepartemen, total_anggaran as totalAnggaran, used as pengeluaran, persentase, pagu_anggaran as paguAnggaran, sisa_pagu as sisaPagu from history_saldo_harian where tanggal_update = ?1 and nama_departemen like %?2% order by persentase desc", nativeQuery = true)
    List<SisaAnggaranDashboardDto> listSaldoHarianSearch(LocalDateTime tanggal, String namaDepartemen);

    @Query(value = "select id,nama_departemen as namaDepartemen, sum(total_anggaran) as totalAnggaran, sum(used) as pengeluaran,round((100 * sum(used))/sum(total_anggaran),2) as persentase, sum(pagu_anggaran) as paguAnggaran, sum(sisa_pagu) as sisaPagu from history_saldo_harian where tanggal_update = ?1 order by persentase desc", nativeQuery = true)
    List<SisaAnggaranDashboardDto> listSaldoHarianTotal(LocalDateTime tanggal);

    @Query(value = "select id,nama_departemen as namaDepartemen, sum(total_anggaran) as totalAnggaran, sum(used) as pengeluaran,round((100 * sum(used))/sum(total_anggaran),2) as persentase, sum(pagu_anggaran) as paguAnggaran, sum(sisa_pagu) as sisaPagu from history_saldo_harian where tanggal_update = ?1 and nama_departemen like %?2% order by persentase desc", nativeQuery = true)
    List<SisaAnggaranDashboardDto> listSaldoHarianTotalSearch(LocalDateTime tanggal, String namaDepartemen);

    @Query(value = "select tanggal_update from history_saldo_harian where status = 'AKTIF' group by tanggal_update order by tanggal_update desc limit 1", nativeQuery = true)
    LocalDateTime tanggalUpdateTerakhir();

    @Query(value = "select sum(used) as total_pengeluaran from history_saldo_harian where status = 'AKTIF' and tanggal_update= ?1  group by tanggal_update", nativeQuery = true)
    BigDecimal totalPengeluaran(LocalDateTime tanggalUpdate);

    @Transactional
    @Modifying
    @Query(value = "delete from history_saldo_harian where date(tanggal_update) = date(?1)", nativeQuery = true)
    void deleteDataYangSama(LocalDateTime tanggal);

    HistorySaldoHarian findByStatusAndId(StatusRecord statusRecord, String id);

}
