package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.dto.transaksi.ApprovalDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.ApprovedDto;
import id.ac.tazkia.aplikasikeuangan.dto.transaksi.EmailDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PencairanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDanaApprove;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import jakarta.persistence.QueryHint;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface PengajuanDanaApproveDao extends PagingAndSortingRepository<PengajuanDanaApprove, String >, CrudRepository<PengajuanDanaApprove, String> {

    Page<PengajuanDanaApprove> findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord,StatusRecord statusAju, StatusRecord StatusApprove, List<String> jabatanApprove, List<Integer> nomorUrut, List<Integer> nomor, Karyawan karyawan, List<String> departemen, Pageable page);

    @Query(value="select a.id as id,b.id as idpengajuandana,f.nama_departemen as namadepartemen,b.tanggal_pengajuan as tanggalpengajuan,b.file as file,e.kode_anggaran as kodeanggaran," +
            "e.nama_anggaran as namaanggaran,c.nama_karyawan as namakaryawan, " +
            "d.deskripsi as deskripsi,b.deskripsi_biaya as deskripsibiaya,b.kuantitas as kuantitas,b.satuan as satuan,b.jumlah as jumlah,a.nomor_urut as nomor,b.nomor_urut as nomorurut," +
            "b.amount as amount, g.nama_karyawan as requestFor, c.id as idFrom, g.id as idFor " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "left join karyawan as g on b.request_for=g.id " +
            "where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and id_jabatan_approve in (?3) " +
            "order by b.tanggal_pengajuan",nativeQuery = true, countQuery = "select count(a.id)as id from pengajuan_dana_approve as a " +
            "            inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut " +
            "            inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "            inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "            inner join anggaran as e on d.id_anggaran=e.id " +
            "            inner join departemen as f on e.id_departemen=f.id " +
            "            where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and id_jabatan_approve in (?3)")
    Page<ApprovalDto> pageApprovalPengajuanDana(String statusApprove, String karyawan, List<String> idJabatan, Pageable page);

    @Query(value="select a.id as id,b.id as idpengajuandana,f.nama_departemen as namadepartemen,b.tanggal_pengajuan as tanggalpengajuan,b.file as file,e.kode_anggaran as kodeanggaran," +
            "e.nama_anggaran as namaanggaran,c.nama_karyawan as namakaryawan, " +
            "d.deskripsi as deskripsi,b.deskripsi_biaya as deskripsibiaya,b.kuantitas as kuantitas,b.satuan as satuan,b.jumlah as jumlah,a.nomor_urut as nomor,b.nomor_urut as nomorurut," +
            "b.amount as amount, g.nama_karyawan as requestFor, b.status, b.total_nomor_urut as totalNomorUrut, b.nomor_urut as nomorUrut " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "left join karyawan as g on b.request_for=g.id " +
            "where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and id_jabatan_approve in (?3)" +
            "order by b.tanggal_pengajuan",nativeQuery = true, countQuery = "select count(a.id)as id from pengajuan_dana_approve as a " +
            "            inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "            inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "            inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "            inner join anggaran as e on d.id_anggaran=e.id " +
            "            inner join departemen as f on e.id_departemen=f.id " +
            "            where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and id_jabatan_approve in (?3)")
    Page<ApprovedDto> pageApprovalPengajuanDanaApproved(String statusApprove, String karyawan, List<String> idJabatan, Pageable page);


    @Query(value="select a.id as id,b.id as idpengajuandana,f.nama_departemen as namadepartemen,b.tanggal_pengajuan as tanggalpengajuan,b.file as file,e.kode_anggaran as kodeanggaran," +
            "e.nama_anggaran as namaanggaran,c.nama_karyawan as namakaryawan, " +
            "d.deskripsi as deskripsi,b.deskripsi_biaya as deskripsibiaya,b.kuantitas as kuantitas,b.satuan as satuan,b.jumlah as jumlah,a.nomor_urut as nomor,b.nomor_urut as nomorurut," +
            "b.amount as amount, g.nama_karyawan as requestFor " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "left join karyawan as g on b.request_for=g.id " +
            "where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and b.tanggal_pengajuan >= ?3 and b.tanggal_pengajuan <= ?4 and id_jabatan_approve in (?5) " +
            "order by b.tanggal_pengajuan",nativeQuery = true, countQuery = "select count(a.id)as id from pengajuan_dana_approve as a " +
            "            inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut " +
            "            inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "            inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "            inner join anggaran as e on d.id_anggaran=e.id " +
            "            inner join departemen as f on e.id_departemen=f.id " +
            "            where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and b.tanggal_pengajuan >= ?3 and b.tanggal_pengajuan <= ?4 and id_jabatan_approve in (?5) ")
    Page<ApprovalDto> pageApprovalPengajuanDanaWithDate(String statusApprove, String karyawan, LocalDateTime tanggalDepan, LocalDateTime tanggalBelakang, List<String> idJabatan, Pageable page);

    @Query(value="select a.id as id,b.id as idpengajuandana,f.nama_departemen as namadepartemen,b.tanggal_pengajuan as tanggalpengajuan,b.file as file,e.kode_anggaran as kodeanggaran," +
            "e.nama_anggaran as namaanggaran,c.nama_karyawan as namakaryawan, " +
            "d.deskripsi as deskripsi,b.deskripsi_biaya as deskripsibiaya,b.kuantitas as kuantitas,b.satuan as satuan,b.jumlah as jumlah,a.nomor_urut as nomor,b.nomor_urut as nomorurut," +
            "b.amount as amount, g.nama_karyawan as requestFor, b.status, b.total_nomor_urut as totalNomorUrut, b.nomor_urut as nomorUrut " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "left join karyawan as g on b.request_for=g.id " +
            "where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and b.tanggal_pengajuan >= ?3 and b.tanggal_pengajuan <= ?4 and id_jabatan_approve in (?5)" +
            "order by b.tanggal_pengajuan",nativeQuery = true, countQuery = "select count(a.id)as id from pengajuan_dana_approve as a " +
            "            inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "            inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "            inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "            inner join anggaran as e on d.id_anggaran=e.id " +
            "            inner join departemen as f on e.id_departemen=f.id " +
            "            where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and b.tanggal_pengajuan >= ?3 and b.tanggal_pengajuan <= ?4 and id_jabatan_approve in (?5)")
    Page<ApprovedDto> pageApprovalPengajuanDanaWithDateApproved(String statusApprove, String karyawan, LocalDateTime tanggalDepan, LocalDateTime tanggalBelakang, List<String> idJabatan, Pageable page);

    @Query(value="select a.id as id,b.id as idpengajuandana,f.nama_departemen as namadepartemen,b.tanggal_pengajuan as tanggalpengajuan,b.file as file,e.kode_anggaran as kodeanggaran," +
            "e.nama_anggaran as namaanggaran,c.nama_karyawan as namakaryawan, " +
            "d.deskripsi as deskripsi,b.deskripsi_biaya as deskripsibiaya,b.kuantitas as kuantitas,b.satuan as satuan,b.jumlah as jumlah,a.nomor_urut as nomor,b.nomor_urut as nomorurut," +
            "b.amount as amount, g.nama_karyawan as requestFor " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "left join karyawan as g on b.request_for=g.id " +
            "where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and (b.deskripsi_biaya like %?3% or d.deskripsi like %?3% or f.nama_departemen like %?3% or c.nama_karyawan like %?3%) and id_jabatan_approve in (?4)" +
            "order by b.tanggal_pengajuan",nativeQuery = true, countQuery = "select count(a.id)as id from pengajuan_dana_approve as a " +
            "            inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut " +
            "            inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "            inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "            inner join anggaran as e on d.id_anggaran=e.id " +
            "            inner join departemen as f on e.id_departemen=f.id " +
            "            where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and (b.deskripsi_biaya like %?3% or d.deskripsi like %?3% or f.nama_departemen like %?3% or c.nama_karyawan like %?3%) and id_jabatan_approve in (?4)")
    Page<ApprovalDto> pageApprovalPengajuanDanaWithSearch(String statusApprove, String karyawan, String search, List<String> idJabatan, Pageable page);

    @Query(value="select a.id as id,b.id as idpengajuandana,f.nama_departemen as namadepartemen,b.tanggal_pengajuan as tanggalpengajuan,b.file as file,e.kode_anggaran as kodeanggaran," +
            "e.nama_anggaran as namaanggaran,c.nama_karyawan as namakaryawan, " +
            "d.deskripsi as deskripsi,b.deskripsi_biaya as deskripsibiaya,b.kuantitas as kuantitas,b.satuan as satuan,b.jumlah as jumlah,a.nomor_urut as nomor,b.nomor_urut as nomorurut," +
            "b.amount as amount, g.nama_karyawan as requestFor, b.status, b.total_nomor_urut as totalNomorUrut, b.nomor_urut as nomorUrut " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "left join karyawan as g on b.request_for=g.id " +
            "where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and (b.deskripsi_biaya like %?3% or d.deskripsi like %?3% or f.nama_departemen like %?3% or c.nama_karyawan like %?3%) and id_jabatan_approve in (?4)" +
            "order by b.tanggal_pengajuan",nativeQuery = true, countQuery = "select count(a.id)as id from pengajuan_dana_approve as a " +
            "            inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "            inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "            inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "            inner join anggaran as e on d.id_anggaran=e.id " +
            "            inner join departemen as f on e.id_departemen=f.id " +
            "            where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and (b.deskripsi_biaya like %?3% or d.deskripsi like %?3% or f.nama_departemen like %?3% or c.nama_karyawan like %?3%) and id_jabatan_approve in (?4)")
    Page<ApprovedDto> pageApprovalPengajuanDanaWithSearchApproved(String statusApprove, String karyawan, String search, List<String> idJabatan, Pageable page);

    @Query(value="select a.id as id,b.id as idpengajuandana,f.nama_departemen as namadepartemen,b.tanggal_pengajuan as tanggalpengajuan,b.file as file,e.kode_anggaran as kodeanggaran," +
            "e.nama_anggaran as namaanggaran,c.nama_karyawan as namakaryawan, " +
            "d.deskripsi as deskripsi,b.deskripsi_biaya as deskripsibiaya,b.kuantitas as kuantitas,b.satuan as satuan,b.jumlah as jumlah,a.nomor_urut as nomor,b.nomor_urut as nomorurut," +
            "b.amount as amount, g.nama_karyawan as requestFor " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "left join karyawan as g on b.request_for=g.id " +
            "where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and b.tanggal_pengajuan >= ?3 and b.tanggal_pengajuan <= ?4 and (b.deskripsi_biaya like %?5% or d.deskripsi like %?5% or f.nama_departemen like %?5% or c.nama_karyawan like %?5%) and id_jabatan_approve in (?6)" +
            "order by b.tanggal_pengajuan",nativeQuery = true, countQuery = "select count(a.id)as id from pengajuan_dana_approve as a " +
            "            inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut " +
            "            inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "            inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "            inner join anggaran as e on d.id_anggaran=e.id " +
            "            inner join departemen as f on e.id_departemen=f.id " +
            "            where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and b.tanggal_pengajuan >= ?3 and b.tanggal_pengajuan <= ?4 and (b.deskripsi_biaya like %?5% or d.deskripsi like %?5% or f.nama_departemen like %?5% or c.nama_karyawan like %?5%) and id_jabatan_approve in (?6)")
    Page<ApprovalDto> pageApprovalPengajuanDanaWithDateSearch(String statusApprove, String karyawan, LocalDateTime tanggalA, LocalDateTime tanggalB, String search, List<String> idJabatan, Pageable page);

    @Query(value="select a.id as id,b.id as idpengajuandana,f.nama_departemen as namadepartemen,b.tanggal_pengajuan as tanggalpengajuan,b.file as file,e.kode_anggaran as kodeanggaran," +
            "e.nama_anggaran as namaanggaran,c.nama_karyawan as namakaryawan, " +
            "d.deskripsi as deskripsi,b.deskripsi_biaya as deskripsibiaya,b.kuantitas as kuantitas,b.satuan as satuan,b.jumlah as jumlah,a.nomor_urut as nomor,b.nomor_urut as nomorurut," +
            "b.amount as amount, g.nama_karyawan as requestFor, b.status, b.total_nomor_urut as totalNomorUrut, b.nomor_urut as nomorUrut " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "left join karyawan as g on b.request_for=g.id " +
            "where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and b.tanggal_pengajuan >= ?3 and b.tanggal_pengajuan <= ?4 and (b.deskripsi_biaya like %?5% or d.deskripsi like %?5% or f.nama_departemen like %?5% or c.nama_karyawan like %?5%) and id_jabatan_approve in (?6)" +
            "order by b.tanggal_pengajuan",nativeQuery = true, countQuery = "select count(a.id)as id from pengajuan_dana_approve as a " +
            "            inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "            inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "            inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "            inner join anggaran as e on d.id_anggaran=e.id " +
            "            inner join departemen as f on e.id_departemen=f.id " +
            "            where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and b.tanggal_pengajuan >= ?3 and b.tanggal_pengajuan <= ?4 and (b.deskripsi_biaya like %?5% or d.deskripsi like %?5% or f.nama_departemen like %?5% or c.nama_karyawan like %?5%) and id_jabatan_approve in (?6)")
    Page<ApprovedDto> pageApprovalPengajuanDanaWithDateSearchApproved(String statusApprove, String karyawan, LocalDateTime tanggalA, LocalDateTime tanggalB, String search, List<String> idJabatan, Pageable page);

    @Query(value="select count(a.id) as id " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut=b.nomor_urut  " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "where status_approve=?1 and user_approve=?2 and b.status='AKTIF' and id_jabatan_approve in (?3)" +
            "order by b.tanggal_pengajuan",nativeQuery = true)
    Integer countApprovalPengajuanDana(String statusApprove, String karyawan, List<String> idJabatan);

    @Query(value="select count(a.id) as id " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut<b.nomor_urut  " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "where status_approve=?1 and user_approve=?2 and b.status in ('AKTIF','OPEN','CAIR') and id_jabatan_approve in (?3)" +
            "order by b.tanggal_pengajuan desc",nativeQuery = true)
    Integer countApprovalPengajuanDanaApprove(String statusApprove, String karyawan, List<String> idJabatan);

    @Query(value="select count(a.id) as id " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut<b.nomor_urut  " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "where status_approve=?1 and user_approve=?2 and b.status='OPEN' " +
            "order by b.tanggal_pengajuan desc",nativeQuery = true)
    Integer countApprovalPengajuanDanaOpen(String statusApprove, String karyawan);

    @Query(value="select count(a.id) as id " +
            "from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id and a.nomor_urut<b.nomor_urut  " +
            "inner join karyawan as c on b.id_karyawan_pengaju=c.id " +
            "inner join anggaran_detail as d on b.id_anggaran_detail=d.id " +
            "inner join anggaran as e on d.id_anggaran=e.id " +
            "inner join departemen as f on e.id_departemen=f.id " +
            "where status_approve=?1 and user_approve=?2 and b.status='CAIR' " +
            "order by b.tanggal_pengajuan desc",nativeQuery = true)
    Integer countApprovalPengajuanDanaApproveCAIR(String statusApprove, String karyawan);

    Integer countByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdIn(StatusRecord statusRecord,StatusRecord statusAju, StatusRecord StatusApprove, List<String> jabatanApprove, List<Integer> nomorUrut, List<Integer> nomor, Karyawan karyawan, List<String> departemen);

    @Query(value="select * from pengajuan_dana_approve as a inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id " +
            "where a.status='AKTIF' and a.status_approve='WAITING' and b.id in ?1 and b.nomor_urut in ?2 and  " +
            "a.nomor_urut in ?3 group by b.id order by b.tanggal_pengajuan", nativeQuery = true)
    Page<Object[]> approvalPengajuanDana(List<String> jabatanApprove, List<Integer> nomorUrut, List<Integer> nomor, Pageable page);

    Page<PengajuanDanaApprove> findByStatusAndPengajuanDanaIdInAndStatusApproveAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord, List<String> jabatanApprove, StatusRecord StatusApprove, Karyawan karyawan, List<String> karyawanDepartemen, Pageable page);
    Integer countByStatusAndPengajuanDanaIdInAndStatusApproveAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdIn(StatusRecord statusRecord, List<String> jabatanApprove, StatusRecord StatusApprove, Karyawan karyawan, List<String> karyawanDepartemen);

    Page<PengajuanDanaApprove> findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanAfterAndPengajuanDanaTanggalPengajuanBeforeAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord, StatusRecord stattusAju, StatusRecord StatusApprove, List<String> jabatanApprove, List<Integer> nomorUrut, List<Integer> nomor, LocalDateTime dari, LocalDateTime sampai, Karyawan karyawan, List<String> karyawanDepartemen, Pageable page);

    Page<PengajuanDanaApprove> findByStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaTanggalPengajuanAfterAndPengajuanDanaTanggalPengajuanBeforeAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord, StatusRecord StatusApprove, List<String> jabatanApprove,  LocalDateTime dari, LocalDateTime sampai, Karyawan karyawan, List<String> karyawanDepartemen, Pageable page);

    Page<PengajuanDanaApprove> findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaTanggalPengajuanBetweenAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord,StatusRecord statusAjuan, StatusRecord StatusApprove, List<String> jabatanApprove, List<Integer> nomorUrut, List<Integer> nomor, LocalDateTime dari, LocalDateTime sampai, String namaKaryawan, Karyawan karyawan, List<String> karyawanDepartemen, Pageable page);

    Page<PengajuanDanaApprove> findByStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaTanggalPengajuanBetweenAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord, StatusRecord StatusApprove, List<String> jabatanApprove , LocalDateTime dari, LocalDateTime sampai, String namaKaryawan, Karyawan karyawan, List<String> karyawanDepartemen, Pageable page);

    Page<PengajuanDanaApprove> findByStatusAndPengajuanDanaStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaNomorUrutInAndNomorUrutInAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord, StatusRecord statusAju, StatusRecord StatusApprove, List<String> jabatanApprove, List<Integer> nomorUrut, List<Integer> nomor, String namaKaryawan, Karyawan karyawan, List<String> karyawanDepartemen, Pageable page);

    Page<PengajuanDanaApprove> findByStatusAndStatusApproveAndPengajuanDanaIdInAndPengajuanDanaKaryawanPengajuNamaKaryawanAndUserApproveAndPengajuanDanaAnggaranDetailAnggaranDepartemenIdInOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord, StatusRecord StatusApprove, List<String> jabatanApprove, String namaKaryawan, Karyawan karyawan, List<String> karyawanDepartenem, Pageable page);

    List<PengajuanDanaApprove> findByStatusAndPengajuanDanaIdOrderByNomorUrut(StatusRecord statusRecord, String pengajuanDana);

    @Query(value = "SELECT id_pengajuan_dana from pengajuan_dana_approve where id_jabatan_approve in ?1 and status='AKTIF' and status_approve='WAITING' group by id_pengajuan_dana", nativeQuery = true)
    List<String> getIdPengajuanDana(List<String> jabatan);

    @Query(value = "SELECT id_pengajuan_dana from pengajuan_dana_approve where id_jabatan_approve in ?1 and status='AKTIF' and status_approve='APPROVED' group by id_pengajuan_dana", nativeQuery = true)
    List<String> getIdPengajuanDanaApprove(List<String> jabatan);

    @Query(value = "SELECT id_pengajuan_dana from pengajuan_dana_approve where id_jabatan_approve in ?1 and status='AKTIF' and status_approve='APPROVED' group by id_pengajuan_dana", nativeQuery = true)
    List<String> getIdPengajuanDanaAll(List<String> jabatan);

    @Query(value = "SELECT nomor_urut from pengajuan_dana_approve where id_jabatan_approve in ?1 and status='AKTIF' and status_approve='WAITING' group by id_pengajuan_dana", nativeQuery = true)
    List<Integer> getIdNomorUrut(List<String> jabatan);

    @Query(value = "SELECT nomor_urut from pengajuan_dana_approve where id_jabatan_approve in ?1 and status='AKTIF' and status_approve='APPROVE' group by id_pengajuan_dana", nativeQuery = true)
    List<Integer> getIdNomorUrutApprove(List<String> jabatan);

    @Query(value = "SELECT a.* from pengajuan_dana_approve as a inner join pengajuan_dana as b on a.id_pengajuan_dana=b.id where a.status = ?1 and b.nomor_urut > b.total_nomor_urut group by id_pengajuan_dana", nativeQuery = true)
    List<PengajuanDanaApprove> getPengajuanDanaApproveWaiting(StatusRecord statusRecord);

    List<PengajuanDanaApprove> findByStatusAndPengajuanDanaStatusOrderByPengajuanDanaTanggalPengajuan(StatusRecord statusRecord, StatusRecord statusDana);

    List<PengajuanDanaApprove> findByStatusAndPengajuanDanaStatusOrderByNomorUrut(StatusRecord statusRecord, StatusRecord statuspengaju);

    List<PengajuanDanaApprove> findByPengajuanDanaId(String pengajuanDana);

    PengajuanDanaApprove findByStatusAndId(StatusRecord statusRecord, String id);



    @Query(value = "select c.nama_karyawan, b.deskripsi_biaya as item, b.kuantitas, b.amount, b.satuan, b.jumlah, c.email from pengajuan_dana_approve as a " +
            "inner join pengajuan_dana as b on a.id_pengajuan_dana = b.id " +
            "inner join karyawan as c on a.user_approve=c.id " +
            "where a.id_pengajuan_dana = ?1 " +
            "and a.status='AKTIF' " +
            "and a.status_approve='WAITING' order by a.nomor_urut asc limit 1", nativeQuery = true)
    EmailDto pengajuanDanaNotifikasi(String pengajuanDana);

    PengajuanDanaApprove findTopByPengajuanDanaAndNomorUrutAndStatusAndStatusApprove(PengajuanDana pengajuanDana, Integer nomorUrut, StatusRecord statusRecord, StatusRecord statusApprove);

}
