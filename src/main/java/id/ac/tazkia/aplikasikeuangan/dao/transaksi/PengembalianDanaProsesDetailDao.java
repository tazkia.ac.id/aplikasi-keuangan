package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDanaProses;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengembalianDanaProsesDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PengembalianDanaProsesDetailDao extends PagingAndSortingRepository<PengembalianDanaProsesDetail, String>, CrudRepository<PengembalianDanaProsesDetail, String> {


    List<PengembalianDanaProsesDetail> findByStatusAndPengembalianDanaProses(StatusRecord statusRecord, PengembalianDanaProses pengembalianDanaProses);

    PengembalianDanaProsesDetail findByStatusAndPengembalianDana(StatusRecord statusRecord, PengembalianDana pengembalianDana);

    @Query(value = "select id_pengembalian_dana from pengembalian_dana_proses_detail where id_pengembalian_dana_proses = ?1 and status = ?2", nativeQuery = true)
    List<String> idPengembalianDana(String idPengembalianDanaProses, String status);



}
