package id.ac.tazkia.aplikasikeuangan.dao.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KaryawanDao extends PagingAndSortingRepository<Karyawan, String>, CrudRepository<Karyawan, String> {

    Page<Karyawan> findByStatusOrderByNamaKaryawan(StatusRecord statusRecord, Pageable page);

    Page<Karyawan> findByStatusAndNikContainingIgnoreCaseOrStatusAndNamaKaryawanContainingIgnoreCaseOrderByNamaKaryawan(StatusRecord statusRecord, String Nik, StatusRecord statusRecord2, String nama, Pageable page);

    List<Karyawan> findByStatusOrderByNamaKaryawanAsc(StatusRecord statusRecord);

    Karyawan findByUser(User user);

}
