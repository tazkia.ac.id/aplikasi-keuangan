package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PendapatanTerkait;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PendapatanTerkaitDao extends PagingAndSortingRepository<PendapatanTerkait, String>, CrudRepository<PendapatanTerkait, String> {

    Page<PendapatanTerkait> findByStatusAndEstimasiPendapatanTerkaitOrderByTanggalDesc(StatusRecord statusRecord, EstimasiPendapatanTerkait estimasiPendapatanTerkait, Pageable pageable);

}
