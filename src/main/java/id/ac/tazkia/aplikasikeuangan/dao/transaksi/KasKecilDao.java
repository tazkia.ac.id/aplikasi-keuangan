package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.KasKecil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KasKecilDao extends PagingAndSortingRepository<KasKecil, String>, CrudRepository<KasKecil, String> {

    Page<KasKecil> findByStatusOrderByTanggalDesc(StatusRecord statusRecord, Pageable page);

    Page<KasKecil> findByStatusAndInstansiIdInOrderByTanggalDesc(StatusRecord statusRecord, List<String> instansi, Pageable page);

}
