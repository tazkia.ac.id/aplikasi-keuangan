package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface PeriodeAnggaranDao extends PagingAndSortingRepository<PeriodeAnggaran, String>, CrudRepository<PeriodeAnggaran, String> {

    Page<PeriodeAnggaran> findByStatusOrderByKodePeriodeAnggaran(StatusRecord statusRecord, Pageable page);
//
    Page<PeriodeAnggaran> findByStatusAndKodePeriodeAnggaranContainingIgnoreCaseOrNamaPeriodeAnggaranContainingIgnoreCaseOrderByKodePeriodeAnggaran(StatusRecord statusRecord,String kode,String nama,Pageable page);



    PeriodeAnggaran findByStatus(StatusRecord statusRecord);

    List<PeriodeAnggaran> findByStatusOrderByKodePeriodeAnggaranDesc(StatusRecord statusRecord);

    List<PeriodeAnggaran> findByStatusOrderByStatusAktif(StatusRecord statusRecord);

    PeriodeAnggaran findByStatusAndStatusAktif(StatusRecord statusRecord,StatusRecord statusAktif);

    List<PeriodeAnggaran> findByStatusAndIdNotInOrderByKodePeriodeAnggaran(StatusRecord statusRecord,List<String> idPeriode);

    List<PeriodeAnggaran> findByStatusAndIdOrderByTanggalMulaiAnggarAsc(StatusRecord statusRecord, String id);

    PeriodeAnggaran findByStatusAndId(StatusRecord statusRecord, String id);

    @Query(value = "SELECT COUNT(id) AS periode FROM periode_anggaran WHERE DATE(tanggal_mulai_perencanaan) <= DATE(NOW()) " +
            "AND DATE(tanggal_selesai_perencanaan) >= DATE(NOW()) AND status_aktif= ?1 AND STATUS='AKTIF'", nativeQuery = true)
    BigDecimal getPeriodeAnggaran(String status);


}
