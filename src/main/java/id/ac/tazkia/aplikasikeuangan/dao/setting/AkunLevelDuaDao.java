package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.setting.AkunLevelDua;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AkunLevelDuaDao extends PagingAndSortingRepository<AkunLevelDua, String>, CrudRepository<AkunLevelDua, String> {



}
