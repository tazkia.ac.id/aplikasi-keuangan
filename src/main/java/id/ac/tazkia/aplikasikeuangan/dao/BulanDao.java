package id.ac.tazkia.aplikasikeuangan.dao;

import id.ac.tazkia.aplikasikeuangan.entity.Bulan;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BulanDao extends PagingAndSortingRepository<Bulan, String>, CrudRepository<Bulan, String> {

    List<Bulan> findByStatusOrderByBulan(StatusRecord statusRecord);

    Bulan findByStatusAndBulan(StatusRecord statusRecord, String bulan);

}
