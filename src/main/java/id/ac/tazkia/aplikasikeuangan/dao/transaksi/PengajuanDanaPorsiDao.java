package id.ac.tazkia.aplikasikeuangan.dao.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDanaPorsi;
import org.hibernate.sql.Update;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PengajuanDanaPorsiDao extends PagingAndSortingRepository<PengajuanDanaPorsi, String>,CrudRepository<PengajuanDanaPorsi, String> {


    @Query(value = "update pengajuan_dana_porsi set status = 'NONAKTIF' where id_pengajuan_dana = ?1 and status = 'AKTIF'", nativeQuery = true)
    Update hapusDataPengajuanDanaPorsi(String idPengajuanDana);

}
