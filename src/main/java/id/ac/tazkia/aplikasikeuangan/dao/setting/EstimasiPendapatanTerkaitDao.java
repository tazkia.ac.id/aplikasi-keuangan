package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EstimasiPendapatanTerkaitDao extends PagingAndSortingRepository<EstimasiPendapatanTerkait, String>, CrudRepository<EstimasiPendapatanTerkait, String> {

    Page<EstimasiPendapatanTerkait> findByStatusAndPeriodeAnggaranAndInstansiIdInOrderByKeterangan(StatusRecord statusRecord, PeriodeAnggaran periodeAnggaran, List<String> idInstansi, Pageable pageable);

    Page<EstimasiPendapatanTerkait> findByStatusAndPeriodeAnggaranAndInstansiIdInAndKeteranganContainingIgnoreCaseOrderByKeterangan(
            StatusRecord statusRecord,PeriodeAnggaran periodeAnggaran, List<String> idInstansi, String keterangan, Pageable pageable);

}
