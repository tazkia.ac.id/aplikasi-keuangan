package id.ac.tazkia.aplikasikeuangan.dao.setting;

import id.ac.tazkia.aplikasikeuangan.dto.setting.DetailAnggaranPorsiDto;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetailPorsi;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;

public interface AnggaranDetailPorsiDao extends PagingAndSortingRepository<AnggaranDetailPorsi, String>, CrudRepository<AnggaranDetailPorsi, String> {

    List<AnggaranDetailPorsi> findByStatusAndAnggaranDetailOrderByDepartemen(StatusRecord statusRecord, AnggaranDetail anggaranDetail);

    @Query(value = "select id,nama_departemen as namaDepartemen,kode_anggaran as kodeAnggaran, nama_anggaran as namaAnggaran,deskripsi,jumlah,group_concat('-',porsi SEPARATOR '\\n') as porsi from\n" +
            "(select a.*, concat(b.porsi_departemen,' = ', persentase_porsi,'%') as porsi from\n" +
            "(select a.id,c.nama_departemen, b.kode_anggaran, b.nama_anggaran, a.deskripsi,a.kuantitas, a.amount, a.kuantitas * a.amount as jumlah  from anggaran_detail as a\n" +
            "inner join anggaran as b on a.id_anggaran = b.id\n" +
            "inner join departemen as c on b.id_departemen = c.id\n" +
            "where a.status = 'AKTIF' and b.id_periode_anggaran = ?1)a\n" +
            "left join \n" +
            "(select a.*, b.nama_departemen as porsi_departemen from anggaran_detail_porsi as a \n" +
            "inner join departemen as b on a.id_departemen = b.id\n" +
            "inner join anggaran_detail as c on a.id_anggaran_detail = c.id\n" +
            "inner join anggaran as d on c.id_anggaran = d.id\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and c.status = 'AKTIF' and d.status = 'AKTIF'\n" +
            "and d.id_periode_anggaran = ?1)b\n" +
            "on a.id = b.id_anggaran_detail)as a\n" +
            "group by id\n" +
            "order by nama_departemen, kode_anggaran", nativeQuery = true)
    List<Object[]> listPorsiAnggaranDetail(String idPeriodeAnggaran);

    @Query(value = "select sum(persentase_porsi) as totalPorsi from anggaran_detail_porsi where status = 'AKTIF' and id_anggaran_detail = ?1", nativeQuery = true)
    BigDecimal totalPorsiAnggaranDetail(String idAnggaranDetail);

}
