package id.ac.tazkia.aplikasikeuangan.export;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import id.ac.tazkia.aplikasikeuangan.entity.Terbilang;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;

import javax.imageio.ImageIO;
import jakarta.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static com.lowagie.text.Image.getInstance;

public class ExportBuktiPengajuanDana {


    public void generate(PengajuanDana pengajuanDana, String uploadFolder, String lokasiLogo, HttpServletResponse response) throws DocumentException, IOException {

        Document document = new Document(PageSize.A4);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, outputStream);
        document.open();
        String lokasiLogos = lokasiLogo + File.separator;

        Image background = getInstance(lokasiLogos+"logo_tr.png");
        background.setAbsolutePosition(400, 190);
        background.scaleAbsolute(400, 450);
        document.add(background);

        Font titleFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 12);
        titleFont.setStyle(Font.UNDERLINE);
        Font kodeFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 11);
        Font fStandard = FontFactory.getFont(FontFactory.defaultEncoding, 11);
        Font fontB = FontFactory.getFont(FontFactory.defaultEncoding, 11);
        Font fontI = FontFactory.getFont(FontFactory.TIMES_ITALIC, 11);


        Font fontTiltle = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle2 = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle3 = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle4 = FontFactory.getFont(FontFactory.defaultEncoding);

        fontTiltle.setSize(20);
        fontTiltle2.setSize(14);
        fontTiltle3.setSize(10);
        fontTiltle4.setSize(10);

        Paragraph paragraphKosong = new Paragraph("");


        Image img = getInstance(lokasiLogos+"bismillah.png");
        img.scaleToFit(140, 140);
        img.setAbsolutePosition(222, PageSize.A4.getHeight() - (img.getScaledHeight()) - 40);
        Image lTazkia = getInstance(lokasiLogos+"tazkia2.png");
        lTazkia.scaleToFit(100, 100);
        lTazkia.setAbsolutePosition(20, PageSize.A4.getHeight() - (lTazkia.getScaledHeight()) - 15);
        Paragraph newLine = new Paragraph("\n\n");
        document.add(img);
        document.add(lTazkia);
        document.add(newLine);
        Paragraph newLine2 = new Paragraph("\n");
        document.add(newLine2);
        Paragraph titleParagraph = new Paragraph("FRMULIR PENGAJUAN DANA", titleFont);
        titleParagraph.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(titleParagraph);
        document.add(newLine2);
        document.add(newLine2);

        BigDecimal jumlah = pengajuanDana.getJumlah();
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        String formattedJumlah = formatRupiah.format(jumlah);

        Paragraph totalPengajuan = new Paragraph("Total Pengajuan : "+ formattedJumlah, fontB);
        document.add(totalPengajuan);

        String terbilang = Terbilang.terbilang(jumlah);
//        Paragraph barisTerbilang = new Paragraph(terbilang + " rupiah");
//        document.add(barisTerbilang);

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.setWidths(new int[] {100});
        table.setSpacingBefore(10);
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        font.setColor(CMYKColor.BLACK);

        PdfPCell cell;
        cell = new PdfPCell(new Phrase(terbilang + " rupiah",fontB));
        cell.setPadding(5);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        document.add(table);

        PdfPTable tableIsi = new PdfPTable(4);
        tableIsi.setWidthPercentage(100);
        tableIsi.setWidths(new int[] {5,45,20,30});
        tableIsi.setSpacingBefore(10);
        tableIsi.setSpacingAfter(10);

        PdfPCell cellIsi;
        cellIsi = new PdfPCell(new Phrase("No",fontB));
        cellIsi.setPadding(5);
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Penggunaan",fontB));
        cellIsi.setPadding(5);
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Jumlah",fontB));
        cellIsi.setPadding(5);
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Rujukan",fontB));
        cellIsi.setPadding(5);
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableIsi.addCell(cellIsi);


        cellIsi = new PdfPCell(new Phrase("1",fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(pengajuanDana.getDeskripsiBiaya(),fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(formattedJumlah.toString(),fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("-",fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        tableIsi.addCell(cellIsi);


        cellIsi = new PdfPCell(new Phrase("",fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("",fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("",fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("-",fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        tableIsi.addCell(cellIsi);


        cellIsi = new PdfPCell(new Phrase("Total",fontB));
        cellIsi.setPadding(5);
        cellIsi.setColspan(2);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(formattedJumlah.toString(),fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("",fStandard));
        cellIsi.setPadding(5);
//        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableIsi.addCell(cellIsi);
        document.add(tableIsi);

        Paragraph isiRujukan = new Paragraph("Rujukan : SOP Internal / Harga Penawaran Vendor / Dll (Coret Yang Tidak Perlu)", kodeFont);
        document.add(isiRujukan);
        document.add(newLine2);
        Paragraph note = new Paragraph("Note : " +
                "\n     a. Pastikan Pengajuan Dana sesuai dengan Anggaran yang diajukan; " +
                "\n     b. Pastikan Pengajuan Dana diketahui oleh atasan; " +
                "\n     c. Pastikan Amanah dan Istiqomah terhadap Penggunaan Dana IAI Tazkia;", fontI);
        document.add(note);

        Image footer = getInstance(lokasiLogos+"footer.png");
        footer.scaleToFit(document.getPageSize().getWidth() - 20, 200);
        footer.setAbsolutePosition( 10, 10);
        document.add(footer);

//
//        document.newPage();



        String lokasiFile2 = uploadFolder + File.separator + pengajuanDana.getFile();
        System.out.println("File = "+ (pengajuanDana.getFile().substring(pengajuanDana.getFile().length()-3)));
        if ((pengajuanDana.getFile().substring(pengajuanDana.getFile().length()-3)).equalsIgnoreCase("png") || (pengajuanDana.getFile().substring(pengajuanDana.getFile().length()-3)).equalsIgnoreCase("jpg") || (pengajuanDana.getFile().substring(pengajuanDana.getFile().length()-4)).equalsIgnoreCase("jpeg")) {
            Image img2 = getInstance(lokasiFile2);
            if (img2.getWidth() * img2.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                if (img2.getWidth() > PageSize.A4.getWidth() - 40) {
                    img2.scalePercent((100 / img2.getWidth()) * (PageSize.A4.getWidth() - 40));
                } else {
                    if (img2.getHeight() > PageSize.A4.getHeight() - 90) {
                        img2.scalePercent((100 / img2.getHeight()) * (PageSize.A4.getHeight() - 50));
                    } else {
                        img2.scalePercent(100);
                    }
                }
            } else {
                if (img2.getWidth() > PageSize.A4.getWidth() - 40) {
                    img2.scalePercent((100 / img2.getWidth()) * (PageSize.A4.getWidth() - 40));
                } else {
                    if (img2.getHeight() > PageSize.A4.getHeight() - 90) {
                        img2.scalePercent((100 / img2.getHeight()) * (PageSize.A4.getHeight() - 90));
                    } else {
                        img2.scalePercent(100);
                    }
                }
            }

            img2.setAbsolutePosition(20, PageSize.A4.getHeight() - (img2.getScaledHeight()) - 70);
            document.newPage();
            paragraphKosong = new Paragraph("File Lampiran : " + pengajuanDana.getDeskripsiBiaya());
            document.add(paragraphKosong);
            document.add(img2);
        } else if ((pengajuanDana.getFile().substring(pengajuanDana.getFile().length()-3)).equalsIgnoreCase("pdf")) {
//            ClassPathResource pdfResource = new ClassPathResource(lokasiFile2);
//            byte[] pdfBytes = IOUtils.toByteArray(pdfResource.getInputStream());
//            document.newPage();
//            Image pdfImage = Image.getInstance(pdfBytes);
//            pdfImage.scaleToFit(400, 400);
//            pdfImage.setAlignment(Element.ALIGN_CENTER);
//            document.add(pdfImage);
            document.newPage();
            File pdfFile = new File(lokasiFile2);
            FileInputStream fileInputStream = new FileInputStream(pdfFile);
            byte[] pdfBytes = IOUtils.toByteArray(fileInputStream);
            PdfReader reader = new PdfReader(pdfBytes);
            int numPages = reader.getNumberOfPages();

            for (int i = 0; i < numPages; i++) {
                PdfImportedPage importedPage = writer.getImportedPage(reader, i + 1);
                PdfContentByte content = writer.getDirectContent();
                content.addTemplate(importedPage, 0, 0);
                document.newPage();
            }

            reader.close();
            fileInputStream.close();
        }
       document.close();
//        response.setContentType("application/pdf");
//        response.setHeader("Content-Disposition", "attachment; filename=\"nama_file.pdf\"");
        OutputStream servletOutputStream = response.getOutputStream();
        outputStream.writeTo(servletOutputStream);
        servletOutputStream.flush();
    }



}
