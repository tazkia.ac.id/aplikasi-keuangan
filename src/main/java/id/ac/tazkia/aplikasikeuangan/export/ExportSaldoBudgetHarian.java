package id.ac.tazkia.aplikasikeuangan.export;

import id.ac.tazkia.aplikasikeuangan.dao.HistoryDetailTransaksiDao;
import id.ac.tazkia.aplikasikeuangan.dao.HistorySaldoHarianDao;
import id.ac.tazkia.aplikasikeuangan.dto.report.DetailTransaksiDepartemenDto;
import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardDto;
import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardKodeDto;
import id.ac.tazkia.aplikasikeuangan.entity.HistorySaldoHarian;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hpsf.Decimal;
import org.apache.poi.sl.usermodel.Background;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.List;
import java.util.Map;

public class ExportSaldoBudgetHarian {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<SisaAnggaranDashboardKodeDto> sisaAnggaranDashboardKodeDtos;

    private List<DetailTransaksiDepartemenDto> detailTransaksiDepartemenDtos;

    public ExportSaldoBudgetHarian(List<SisaAnggaranDashboardKodeDto> sisaAnggaranDashboardKodeDtos, List<DetailTransaksiDepartemenDto> detailTransaksiDepartemenDtos) {
        this.sisaAnggaranDashboardKodeDtos = sisaAnggaranDashboardKodeDtos;
        this.detailTransaksiDepartemenDtos = detailTransaksiDepartemenDtos;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Dashboard");
        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(11);
        style.setFont(font);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        createCell(row, 0, "No", style);
        createCell(row, 1, "Departement", style);
        createCell(row, 2, "Code", style);
        createCell(row, 3, "Pagu", style);
        createCell(row, 4, "Budget", style);
        createCell(row, 5, "Pagu Balance", style);
        createCell(row, 6, "Used", style);
        createCell(row, 7, "Label", style);
        createCell(row, 8, "Remaining", style);

        writeDataLines();

        for (SisaAnggaranDashboardKodeDto sisaAnggaranDashboardKodeDto : sisaAnggaranDashboardKodeDtos){

            sheet = workbook.createSheet("K-" + sisaAnggaranDashboardKodeDto.getKode());

            CellStyle style1 = workbook.createCellStyle();
            XSSFFont font1 = workbook.createFont();
            font1.setBold(true);
            font1.setFontHeight(11);
            style.setFont(font1);
            style.setBorderTop(BorderStyle.THIN);
            style.setBorderRight(BorderStyle.THIN);
            style.setBorderLeft(BorderStyle.THIN);
            style.setBorderBottom(BorderStyle.THIN);

            style1.setFont(font1);
            style1.setBorderTop(BorderStyle.THIN);
            style1.setBorderRight(BorderStyle.THIN);
            style1.setBorderLeft(BorderStyle.THIN);
            style1.setBorderBottom(BorderStyle.THIN);
            style1.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
            style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);


            Row rowa = sheet.createRow(1);
            createCell(rowa, 0, "Code : ", style1);
            createCell(rowa, 1, sisaAnggaranDashboardKodeDto.getKode(), style1);

            Row rowb = sheet.createRow(2);
            createCell(rowb, 0, "Department : ", style1);
            createCell(rowb, 1, sisaAnggaranDashboardKodeDto.getNamaDepartemen(), style1);

            Row row1 = sheet.createRow(4);
            createCell(row1, 0, "No", style1);
            createCell(row1, 1, "Request Desc", style1);
            createCell(row1, 2, "Request Date", style1);
            createCell(row1, 3, "Request", style1);
            createCell(row1, 4, "Realized Date", style1);
            createCell(row1, 5, "Realized", style1);

                    int rowCount = 5;
                    CellStyle style2 = workbook.createCellStyle();
                    XSSFFont font2 = workbook.createFont();
                    font2.setFontHeight(11);
                    style.setFont(font2);
                    style.setBorderTop(BorderStyle.THIN);
                    style.setBorderRight(BorderStyle.THIN);
                    style.setBorderLeft(BorderStyle.THIN);
                    style.setBorderBottom(BorderStyle.THIN);


                    Integer angka = 1;
                    for (DetailTransaksiDepartemenDto detailTransaksiDepartemenDto : detailTransaksiDepartemenDtos) {
                        if(detailTransaksiDepartemenDto.getIdDepartemen().equals(sisaAnggaranDashboardKodeDto.getIdDepartemen())) {
                            if(detailTransaksiDepartemenDto.getNomor() == 1) {
                                Row row2 = sheet.createRow(rowCount++);
                                int columnCount = 0;
                                createCell(row2, columnCount++, angka, style2);
                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getDeskripsiBiaya(), style2);
                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getTanggalPengajuan(), style2);
                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getPengajuan(), style2);
                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getTanggalLaporan(), style2);
                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getRealisasi(), style2);
                                angka = angka + 1;
//                            }else{
//                                Row row2 = sheet.createRow(rowCount++);
//                                int columnCount = 0;
//                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getDeskripsiBiaya(), style);
//                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getTanggalPengajuan(), style);
//                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getPengajuan(), style);
//                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getTanggalLaporan(), style);
//                                createCell(row2, columnCount++, detailTransaksiDepartemenDto.getRealisasi(), style);
//                                angka = angka + 1;
                            }
                        }

                    }


        }

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof LocalDateTime) {
            // Memformat LocalDateTime menjadi "dd-mmm-yyyy"
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
            String formattedDateTime = ((LocalDateTime) value).format(formatter);
            cell.setCellValue(formattedDateTime);
        } else if (value instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) value).doubleValue());
        } else if (value instanceof String && ((String) value).startsWith("K-")) {
            // create hyperlink to open another sheet
            Hyperlink link = workbook.getCreationHelper().createHyperlink(HyperlinkType.DOCUMENT);
            link.setAddress("#'" + value + "'!A1");
            cell.setHyperlink(link);
            cell.setCellValue((String) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {

        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(11);
        style.setFont(font);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);

        Integer angka = 1;
        for (SisaAnggaranDashboardKodeDto sisaAnggaranDashboardKodeDto : sisaAnggaranDashboardKodeDtos) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, angka, style);
            createCell(row, columnCount++, sisaAnggaranDashboardKodeDto.getNamaDepartemen(), style);
            createCell(row, columnCount++, "K-"+sisaAnggaranDashboardKodeDto.getKode(), style);
            createCell(row, columnCount++, sisaAnggaranDashboardKodeDto.getPaguAnggaran(), style);
            createCell(row, columnCount++, sisaAnggaranDashboardKodeDto.getTotalAnggaran(), style);
            createCell(row, columnCount++, sisaAnggaranDashboardKodeDto.getSisaPagu(), style);
            createCell(row, columnCount++, sisaAnggaranDashboardKodeDto.getPengeluaran(), style);
            createCell(row, columnCount++, sisaAnggaranDashboardKodeDto.getPersentase() + "%", style);
            createCell(row, columnCount++, sisaAnggaranDashboardKodeDto.getTotalAnggaran().subtract(sisaAnggaranDashboardKodeDto.getPengeluaran()), style);

            angka = angka + 1;

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
