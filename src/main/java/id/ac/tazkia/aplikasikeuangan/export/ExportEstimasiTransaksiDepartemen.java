package id.ac.tazkia.aplikasikeuangan.export;

import id.ac.tazkia.aplikasikeuangan.dto.export.ExportAnggaranDetailEstimasiDepartemenDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class ExportEstimasiTransaksiDepartemen {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<ExportAnggaranDetailEstimasiDepartemenDto> exportEstimasiTransaksiDepartemenDtos;

    public ExportEstimasiTransaksiDepartemen(List<ExportAnggaranDetailEstimasiDepartemenDto> exportEstimasiTransaksiDepartemenDtos) {
        this.exportEstimasiTransaksiDepartemenDtos = exportEstimasiTransaksiDepartemenDtos;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Code", style);
        createCell(row, 1, "Budget_Name", style);
        createCell(row, 2, "Work_Program", style);
        createCell(row, 3, "Description", style);
        createCell(row, 4, "Estimate", style);
        createCell(row, 5, "Date", style);
        createCell(row, 6, "Qty", style);
        createCell(row, 7, "Unit", style);
        createCell(row, 8, "Amount", style);
        createCell(row, 9, "Total", style);

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
                cell.setCellValue((Double) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;


        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        CellStyle style2 = workbook.createCellStyle();
        CreationHelper createHelper = workbook.getCreationHelper();
        style2.setDataFormat(
                createHelper.createDataFormat().getFormat("dd/mm/yy"));
        XSSFFont font1 = workbook.createFont();
        font1.setFontHeight(14);
        style2.setFont(font1);


        for (ExportAnggaranDetailEstimasiDepartemenDto exportAnggaranDetailEstimasiDepartemenDto : exportEstimasiTransaksiDepartemenDtos) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//            LocalDate localDate = LocalDate.parse(exportAnggaranDetailEstimasiDepartemenDto.getTanggalEstimasi(), formatter);
////
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getKodeAnggaran(), style);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getNamaAnggaran(), style);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getProgramKerja(), style);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getDeskripsiDetail(), style);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getDeskripsiEstimasi(), style);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getTanggalEstimasi().format(formatter), style2);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getKuantitas().doubleValue(), style);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getSatuan(), style);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getAmount().doubleValue(), style);
            createCell(row, columnCount++, exportAnggaranDetailEstimasiDepartemenDto.getJumlah().doubleValue(), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }

}
