package id.ac.tazkia.aplikasikeuangan.export;

import id.ac.tazkia.aplikasikeuangan.dto.report.EstimasiPengeluaranDto;
import id.ac.tazkia.aplikasikeuangan.dto.report.LaporanEstimasiPenerimaanDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class ExportEstimasiRevenue {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<LaporanEstimasiPenerimaanDto> estimasiPenerimaanDtos;
    private BigDecimal totalEstimasi;

    public ExportEstimasiRevenue(List<LaporanEstimasiPenerimaanDto> estimasiPenerimaanDtos, BigDecimal totalEstimasi) {
        this.estimasiPenerimaanDtos = estimasiPenerimaanDtos;
        this.totalEstimasi = totalEstimasi;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Employes");

        org.apache.poi.ss.usermodel.Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Date", style);
        createCell(row, 1, "Code", style);
        createCell(row, 2, "Budget_name", style);
        createCell(row, 3, "Detail", style);
        createCell(row, 4, "Qty", style);
        createCell(row, 5, "Unit", style);
        createCell(row, 6, "Amount", style);
        createCell(row, 7, "Total", style);

    }

    private void createCell(org.apache.poi.ss.usermodel.Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (LaporanEstimasiPenerimaanDto est : estimasiPenerimaanDtos) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, est.getTanggalPenerimaan().toString(), style);
            createCell(row, columnCount++, est.getKodeMataAnggaran(), style);
            createCell(row, columnCount++, est.getNamaMataAnggaran(), style);
            createCell(row, columnCount++, est.getKeterangan(), style);
            createCell(row, columnCount++, est.getKuantitas().doubleValue(), style);
            createCell(row, columnCount++, est.getSatuan(), style);
            createCell(row, columnCount++, est.getAmount().doubleValue(), style);
            createCell(row, columnCount++, est.getTotal().doubleValue(), style);
        }
        Row row = sheet.createRow(rowCount+1);
        createCell(row, 6, "Total Amount", style);
        createCell(row, 7, totalEstimasi.doubleValue(), style);

    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }

}
