package id.ac.tazkia.aplikasikeuangan.export;

import id.ac.tazkia.aplikasikeuangan.dto.report.EstimasiPengeluaranDto;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class ExportEstimasiTransaksi {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<EstimasiPengeluaranDto> estimasiPengeluaranDtos;
    private BigDecimal totalEstimasi;

    public ExportEstimasiTransaksi(List<EstimasiPengeluaranDto> estimasiPengeluaranDtos, BigDecimal totalEstimasi) {
        this.estimasiPengeluaranDtos = estimasiPengeluaranDtos;
        this.totalEstimasi = totalEstimasi;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        System.out.println("buat header");
        sheet = workbook.createSheet("Employes");

        org.apache.poi.ss.usermodel.Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Date", style);
        createCell(row, 1, "Code", style);
        createCell(row, 2, "Budget_name", style);
        createCell(row, 3, "Detail", style);
        createCell(row, 4, "For", style);
        createCell(row, 5, "Qty", style);
        createCell(row, 6, "Unit", style);
        createCell(row, 7, "Amount", style);
        createCell(row, 8, "Total", style);

    }

    private void createCell(org.apache.poi.ss.usermodel.Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        System.out.println("Buat Data");
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (EstimasiPengeluaranDto est : estimasiPengeluaranDtos) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, est.getTanggalEstimasi().toString(), style);
            createCell(row, columnCount++, est.getKodeAnggaran(), style);
            createCell(row, columnCount++, est.getNamaAnggaran(), style);
            createCell(row, columnCount++, est.getDesDetail(), style);
            createCell(row, columnCount++, est.getDesEstimasi(), style);
            createCell(row, columnCount++, est.getKuantitas().doubleValue(), style);
            createCell(row, columnCount++, est.getSatuan(), style);
            createCell(row, columnCount++, est.getAmount().doubleValue(), style);
            createCell(row, columnCount++, est.getJumlah().doubleValue(), style);
        }
        Row row = sheet.createRow(rowCount+1);
        createCell(row, 7, "Total Amount", style);
        createCell(row, 8, totalEstimasi.doubleValue(), style);

    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
