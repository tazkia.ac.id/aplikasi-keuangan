package id.ac.tazkia.aplikasikeuangan.export;

import id.ac.tazkia.aplikasikeuangan.dto.report.DetailTransaksiDepartemenDto;
import id.ac.tazkia.aplikasikeuangan.dto.report.KasHarianDto;
import id.ac.tazkia.aplikasikeuangan.dto.setting.SisaAnggaranDashboardKodeDto;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class ExportDailyCash {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<KasHarianDto> pageKasHarian;

    public ExportDailyCash(List<KasHarianDto> pageKasHarian) {
        this.pageKasHarian = pageKasHarian;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Daily Cash");
        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(11);
        style.setFont(font);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        createCell(row, 0, "No", style);
        createCell(row, 1, "By", style);
        createCell(row, 2, "Code", style);
        createCell(row, 3, "Budget", style);
        createCell(row, 4, "Desc", style);
        createCell(row, 5, "Request Date", style);
        createCell(row, 6, "Request Amount", style);
        createCell(row, 7, "Disbursement Date", style);
        createCell(row, 8, "Disbursement Amount", style);
        createCell(row, 9, "Realized Date", style);
        createCell(row, 10, "Realized Amount", style);
        createCell(row, 11, "Balance", style);

        writeDataLines();


    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof LocalDateTime) {
            // Memformat LocalDateTime menjadi "dd-mmm-yyyy"
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
            String formattedDateTime = ((LocalDateTime) value).format(formatter);
            cell.setCellValue(formattedDateTime);
        } else if (value instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) value).doubleValue());
        } else if (value instanceof String && ((String) value).startsWith("K-")) {
            // create hyperlink to open another sheet
            Hyperlink link = workbook.getCreationHelper().createHyperlink(HyperlinkType.DOCUMENT);
            link.setAddress("#'" + value + "'!A1");
            cell.setHyperlink(link);
            cell.setCellValue((String) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {

        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(11);
        style.setFont(font);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);

        Integer angka = 1;
        for (KasHarianDto kasHarianDto : pageKasHarian) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, angka, style);
            createCell(row, columnCount++, kasHarianDto.getNama(), style);
            createCell(row, columnCount++, kasHarianDto.getKode(), style);
            createCell(row, columnCount++, kasHarianDto.getName(), style);
            createCell(row, columnCount++, kasHarianDto.getDeskripsi(), style);
            createCell(row, columnCount++, kasHarianDto.getTanggalpengajuan(), style);
            createCell(row, columnCount++, kasHarianDto.getPengajuan(), style);
            createCell(row, columnCount++, kasHarianDto.getTanggalcair(), style);
            createCell(row, columnCount++, kasHarianDto.getDisburse(), style);
            createCell(row, columnCount++, kasHarianDto.getTanggallaporan(), style);
            createCell(row, columnCount++, kasHarianDto.getRealisasi(), style);
            createCell(row, columnCount++, kasHarianDto.getSelisih(), style);

            angka = angka + 1;

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }
}
