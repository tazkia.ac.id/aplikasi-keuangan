package id.ac.tazkia.aplikasikeuangan.export;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import id.ac.tazkia.aplikasikeuangan.dto.PrintLaporanDanaDto;
import id.ac.tazkia.aplikasikeuangan.entity.Terbilang;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDana;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.LaporanDanaFile;
import org.apache.commons.compress.utils.IOUtils;

import jakarta.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Locale;
import java.util.List;
import java.time.format.DateTimeFormatter;

import static com.lowagie.text.Image.getInstance;

public class ExportBuktiLaporanDana {

    public void generateLaporanDana(PrintLaporanDanaDto printLaporanDanaDto, List<LaporanDana> laporanDanaList, String uploadFolder, String lokasiLogo, HttpServletResponse response) throws DocumentException, IOException {

        Document document = new Document(PageSize.A4);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, outputStream);
        document.open();
        String lokasiLogos = lokasiLogo + File.separator;

        Image background = getInstance(lokasiLogos+"logo_tr.png");
        background.setAbsolutePosition(400, 190);
        background.scaleAbsolute(400, 450);
        document.add(background);

        Font titleFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 12);
        titleFont.setStyle(Font.UNDERLINE);
        Font kodeFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 11);
        Font fStandard = FontFactory.getFont(FontFactory.defaultEncoding, 11);
        Font fontB = FontFactory.getFont(FontFactory.defaultEncoding, 11);
        Font fontI = FontFactory.getFont(FontFactory.TIMES_ITALIC, 11);


        Font fontTiltle = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle2 = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle3 = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle4 = FontFactory.getFont(FontFactory.defaultEncoding);

        fontTiltle.setSize(20);
        fontTiltle2.setSize(14);
        fontTiltle3.setSize(10);
        fontTiltle4.setSize(10);

        Paragraph paragraphKosong = new Paragraph("");


        Image img = getInstance(lokasiLogos+"bismillah.png");
        img.scaleToFit(140, 140);
        img.setAbsolutePosition(222, PageSize.A4.getHeight() - (img.getScaledHeight()) - 40);
        Image lTazkia = getInstance(lokasiLogos+"tazkia2.png");
        lTazkia.scaleToFit(100, 100);
        lTazkia.setAbsolutePosition(20, PageSize.A4.getHeight() - (lTazkia.getScaledHeight()) - 15);
        Paragraph newLine = new Paragraph("\n\n");
        document.add(img);
        document.add(lTazkia);
        document.add(newLine);
        Paragraph newLine2 = new Paragraph("\n");
        document.add(newLine2);
        Paragraph titleParagraph = new Paragraph("FORM LAPORAN PENGGUNAAN DANA", titleFont);
        titleParagraph.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(titleParagraph);
        document.add(newLine2);
        document.add(newLine2);

//        Paragraph nama = new Paragraph("Nama                                   : ", fontB);
//        document.add(nama);
//        Paragraph bagian = new Paragraph("Bagian                                  : ", fontB);
//        document.add(bagian);
//        Paragraph kode = new Paragraph("Kode & Nama Anggaran      : ", fontB);
//        document.add(kode);
//        Paragraph detail = new Paragraph("Detail Anggaran                   : ", fontB);
//        document.add(detail);

        PdfPTable tabelJudul = new PdfPTable(3);
        tabelJudul.setWidthPercentage(100);
        tabelJudul.setWidths(new int[] {30,5,65});
        tabelJudul.setSpacingBefore(10);
        tabelJudul.setSpacingAfter(10);

        PdfPCell cellIsi;
        cellIsi = new PdfPCell(new Phrase("Nama",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(":",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(printLaporanDanaDto.getKaryawan(),fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);


        cellIsi = new PdfPCell(new Phrase("Bagian",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(":",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(printLaporanDanaDto.getDepartemen(),fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);

        cellIsi = new PdfPCell(new Phrase("Kode & Nama Anggaran",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(":",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(printLaporanDanaDto.getKode(),fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);

        cellIsi = new PdfPCell(new Phrase("Detail Anggaran",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(":",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(printLaporanDanaDto.getDetailAnggaran(),fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);

        document.add(tabelJudul);


        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        String formatPengajuan = formatRupiah.format(printLaporanDanaDto.getPengajuan());
        String formatPencairan = formatRupiah.format(printLaporanDanaDto.getPencairan());
        String formatLaporan = formatRupiah.format(printLaporanDanaDto.getLaporan());
        String formatSisa = formatRupiah.format(printLaporanDanaDto.getSisa());
        String terbilang = Terbilang.terbilang(printLaporanDanaDto.getSisa());

        PdfPTable tableIsi = new PdfPTable(4);
        tableIsi.setWidthPercentage(100);
        tableIsi.setWidths(new int[] {10,30,30,30});
        tableIsi.setSpacingBefore(10);
        tableIsi.setSpacingAfter(10);

        cellIsi = new PdfPCell(new Phrase("No",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Total Pengajuan (Rp)",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Total Pencairan (Rp)",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Total Realisasi (Rp)",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);


        cellIsi = new PdfPCell(new Phrase("1",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(formatPengajuan,fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(formatPencairan,fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(formatLaporan,fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);

        document.add(tableIsi);

        Paragraph nominalSelisih = new Paragraph("Nominal Selisih : "+ formatSisa,kodeFont);
        document.add(nominalSelisih);

        Paragraph terbilangs = new Paragraph("Terbilang : " + terbilang, fontI);
        document.add(terbilangs);

        document.add(newLine2);
        document.add(newLine2);

        Paragraph tanggal = new Paragraph("Tanggal Pelaporan : ", fontB);
        document.add(tanggal);

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.setWidths(new int[] {50,50});
        table.setSpacingBefore(10);
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        font.setColor(CMYKColor.BLACK);

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd - MMM - yyyy"); // Ganti format tanggal sesuai keinginan (misalnya: "dd/MM/yyyy")
        LocalDate tanggals = printLaporanDanaDto.getTanggal();
        String tanggalFormatted = tanggals.format(dateFormatter);

        PdfPCell cell;
        cell = new PdfPCell(new Phrase(tanggalFormatted,fontB));
        cell.setPadding(5);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("",fontB));
        cell.setPadding(5);
        cell.setBorder(0);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        document.add(table);

        document.add(newLine2);

        Paragraph note = new Paragraph("Keterangan : " +
                "\n     a. Pastikan Penggunaan Dana sesuai dengan Anggaran yang diajukan; " +
                "\n     b. Pastikan Bukti Penggunaan dana SAH dan AKURAT; " +
                "\n     c. Pastikan Pelaporan Penggunaan diketahui oleh atasan; " +
                "\n     d. Pastikan Amanah dan Istiqomah terhadap Penggunaan Dana IAI Tazkia;", fontI);
        document.add(note);

        Image footer = getInstance(lokasiLogos+"footer.png");
        footer.scaleToFit(document.getPageSize().getWidth() - 20, 200);
        footer.setAbsolutePosition( 10, 10);
        document.add(footer);


        for(LaporanDana laporanDana : laporanDanaList){
            String lokasiFile2 = uploadFolder + File.separator + laporanDana.getFile();
            System.out.println("File = "+ (laporanDana.getFile().substring(laporanDana.getFile().length()-3)));
            if ((laporanDana.getFile().substring(laporanDana.getFile().length()-3)).equalsIgnoreCase("png") || (laporanDana.getFile().substring(laporanDana.getFile().length()-3)).equalsIgnoreCase("jpg") || (laporanDana.getFile().substring(laporanDana.getFile().length()-4)).equalsIgnoreCase("jpeg")) {
                Image img2 = getInstance(lokasiFile2);
                if (img2.getWidth() * img2.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                    if (img2.getWidth() > PageSize.A4.getWidth() - 40) {
                        img2.scalePercent((100 / img2.getWidth()) * (PageSize.A4.getWidth() - 40));
                    } else {
                        if (img2.getHeight() > PageSize.A4.getHeight() - 90) {
                            img2.scalePercent((100 / img2.getHeight()) * (PageSize.A4.getHeight() - 50));
                        } else {
                            img2.scalePercent(100);
                        }
                    }
                } else {
                    if (img2.getWidth() > PageSize.A4.getWidth() - 40) {
                        img2.scalePercent((100 / img2.getWidth()) * (PageSize.A4.getWidth() - 40));
                    } else {
                        if (img2.getHeight() > PageSize.A4.getHeight() - 90) {
                            img2.scalePercent((100 / img2.getHeight()) * (PageSize.A4.getHeight() - 90));
                        } else {
                            img2.scalePercent(100);
                        }
                    }
                }

                img2.setAbsolutePosition(20, PageSize.A4.getHeight() - (img2.getScaledHeight()) - 70);
                document.newPage();
                paragraphKosong = new Paragraph("File Lampiran : " + laporanDana.getDeskripsi());
                document.add(paragraphKosong);
                document.add(img2);
            } else if ((laporanDana.getFile().substring(laporanDana.getFile().length()-3)).equalsIgnoreCase("pdf")) {
//            ClassPathResource pdfResource = new ClassPathResource(lokasiFile2);
//            byte[] pdfBytes = IOUtils.toByteArray(pdfResource.getInputStream());
//            document.newPage();
//            Image pdfImage = Image.getInstance(pdfBytes);
//            pdfImage.scaleToFit(400, 400);
//            pdfImage.setAlignment(Element.ALIGN_CENTER);
//            document.add(pdfImage);
                document.newPage();
                File pdfFile = new File(lokasiFile2);
                FileInputStream fileInputStream = new FileInputStream(pdfFile);
                byte[] pdfBytes = IOUtils.toByteArray(fileInputStream);
                PdfReader reader = new PdfReader(pdfBytes);
                int numPages = reader.getNumberOfPages();

                for (int i = 0; i < numPages; i++) {
                    PdfImportedPage importedPage = writer.getImportedPage(reader, i + 1);
                    PdfContentByte content = writer.getDirectContent();
                    content.addTemplate(importedPage, 0, 0);
                    document.newPage();
                }

                reader.close();
                fileInputStream.close();
            }
        }

        document.close();

        OutputStream servletOutputStream = response.getOutputStream();
        outputStream.writeTo(servletOutputStream);
        servletOutputStream.flush();
    }

    public void generateLaporanDanaFile(PrintLaporanDanaDto printLaporanDanaDto, List<LaporanDanaFile> laporanDanaFiles, String uploadFolder, String lokasiLogo, HttpServletResponse response) throws DocumentException, IOException {

        Document document = new Document(PageSize.A4);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, outputStream);
        document.open();
        String lokasiLogos = lokasiLogo + File.separator;

        Image background = getInstance(lokasiLogos+"logo_tr.png");
        background.setAbsolutePosition(400, 190);
        background.scaleAbsolute(400, 450);
        document.add(background);

        Font titleFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 12);
        titleFont.setStyle(Font.UNDERLINE);
        Font kodeFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 11);
        Font fStandard = FontFactory.getFont(FontFactory.defaultEncoding, 11);
        Font fontB = FontFactory.getFont(FontFactory.defaultEncoding, 11);
        Font fontI = FontFactory.getFont(FontFactory.TIMES_ITALIC, 11);


        Font fontTiltle = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle2 = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle3 = FontFactory.getFont(FontFactory.defaultEncoding);
        Font fontTiltle4 = FontFactory.getFont(FontFactory.defaultEncoding);

        fontTiltle.setSize(20);
        fontTiltle2.setSize(14);
        fontTiltle3.setSize(10);
        fontTiltle4.setSize(10);

        Paragraph paragraphKosong = new Paragraph("");


        Image img = getInstance(lokasiLogos+"bismillah.png");
        img.scaleToFit(140, 140);
        img.setAbsolutePosition(222, PageSize.A4.getHeight() - (img.getScaledHeight()) - 40);
        Image lTazkia = getInstance(lokasiLogos+"tazkia2.png");
        lTazkia.scaleToFit(100, 100);
        lTazkia.setAbsolutePosition(20, PageSize.A4.getHeight() - (lTazkia.getScaledHeight()) - 15);
        Paragraph newLine = new Paragraph("\n\n");
        document.add(img);
        document.add(lTazkia);
        document.add(newLine);
        Paragraph newLine2 = new Paragraph("\n");
        document.add(newLine2);
        Paragraph titleParagraph = new Paragraph("FORM LAPORAN PENGGUNAAN DANA", titleFont);
        titleParagraph.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(titleParagraph);
        document.add(newLine2);
        document.add(newLine2);

//        Paragraph nama = new Paragraph("Nama                                   : ", fontB);
//        document.add(nama);
//        Paragraph bagian = new Paragraph("Bagian                                  : ", fontB);
//        document.add(bagian);
//        Paragraph kode = new Paragraph("Kode & Nama Anggaran      : ", fontB);
//        document.add(kode);
//        Paragraph detail = new Paragraph("Detail Anggaran                   : ", fontB);
//        document.add(detail);

        PdfPTable tabelJudul = new PdfPTable(3);
        tabelJudul.setWidthPercentage(100);
        tabelJudul.setWidths(new int[] {30,5,65});
        tabelJudul.setSpacingBefore(10);
        tabelJudul.setSpacingAfter(10);

        PdfPCell cellIsi;
        cellIsi = new PdfPCell(new Phrase("Nama",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(":",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(printLaporanDanaDto.getKaryawan(),fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);


        cellIsi = new PdfPCell(new Phrase("Bagian",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(":",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(printLaporanDanaDto.getDepartemen(),fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);

        cellIsi = new PdfPCell(new Phrase("Kode & Nama Anggaran",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(":",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(printLaporanDanaDto.getKode(),fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);

        cellIsi = new PdfPCell(new Phrase("Detail Anggaran",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(":",fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(printLaporanDanaDto.getDetailAnggaran(),fontB));
        cellIsi.setBorder(0);
        tabelJudul.addCell(cellIsi);

        document.add(tabelJudul);


        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        String formatPengajuan = formatRupiah.format(printLaporanDanaDto.getPengajuan());
        String formatPencairan = formatRupiah.format(printLaporanDanaDto.getPencairan());
        String formatLaporan = formatRupiah.format(printLaporanDanaDto.getLaporan());
        String formatSisa = formatRupiah.format(printLaporanDanaDto.getSisa());
        String terbilang = Terbilang.terbilang(printLaporanDanaDto.getSisa());

        PdfPTable tableIsi = new PdfPTable(4);
        tableIsi.setWidthPercentage(100);
        tableIsi.setWidths(new int[] {10,30,30,30});
        tableIsi.setSpacingBefore(10);
        tableIsi.setSpacingAfter(10);

        cellIsi = new PdfPCell(new Phrase("No",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Total Pengajuan (Rp)",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Total Pencairan (Rp)",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase("Total Realisasi (Rp)",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);


        cellIsi = new PdfPCell(new Phrase("1",fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(formatPengajuan,fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(formatPencairan,fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);
        cellIsi = new PdfPCell(new Phrase(formatLaporan,fontB));
        cellIsi.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellIsi.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellIsi.setPadding(5);
        tableIsi.addCell(cellIsi);

        document.add(tableIsi);

        Paragraph nominalSelisih = new Paragraph("Nominal Selisih : "+ formatSisa,kodeFont);
        document.add(nominalSelisih);

        Paragraph terbilangs = new Paragraph("Terbilang : " + terbilang, fontI);
        document.add(terbilangs);

        document.add(newLine2);
        document.add(newLine2);

        Paragraph tanggal = new Paragraph("Tanggal Pelaporan : ", fontB);
        document.add(tanggal);

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.setWidths(new int[] {50,50});
        table.setSpacingBefore(10);
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        font.setColor(CMYKColor.BLACK);

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd - MMM - yyyy"); // Ganti format tanggal sesuai keinginan (misalnya: "dd/MM/yyyy")
        LocalDate tanggals = printLaporanDanaDto.getTanggal();
        String tanggalFormatted = tanggals.format(dateFormatter);

        PdfPCell cell;
        cell = new PdfPCell(new Phrase(tanggalFormatted,fontB));
        cell.setPadding(5);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("",fontB));
        cell.setPadding(5);
        cell.setBorder(0);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        document.add(table);

        document.add(newLine2);

        Paragraph note = new Paragraph("Keterangan : " +
                "\n     a. Pastikan Penggunaan Dana sesuai dengan Anggaran yang diajukan; " +
                "\n     b. Pastikan Bukti Penggunaan dana SAH dan AKURAT; " +
                "\n     c. Pastikan Pelaporan Penggunaan diketahui oleh atasan; " +
                "\n     d. Pastikan Amanah dan Istiqomah terhadap Penggunaan Dana IAI Tazkia;", fontI);
        document.add(note);

        Image footer = getInstance(lokasiLogos+"footer.png");
        footer.scaleToFit(document.getPageSize().getWidth() - 20, 200);
        footer.setAbsolutePosition( 10, 10);
        document.add(footer);


        for(LaporanDanaFile laporanDanaFile : laporanDanaFiles){
            String lokasiFile2 = uploadFolder + File.separator + laporanDanaFile.getFile();
            System.out.println("File = "+ (laporanDanaFile.getFile().substring(laporanDanaFile.getFile().length()-3)));
            if ((laporanDanaFile.getFile().substring(laporanDanaFile.getFile().length()-3)).equalsIgnoreCase("png") || (laporanDanaFile.getFile().substring(laporanDanaFile.getFile().length()-3)).equalsIgnoreCase("jpg") || (laporanDanaFile.getFile().substring(laporanDanaFile.getFile().length()-4)).equalsIgnoreCase("jpeg")) {
                Image img2 = getInstance(lokasiFile2);
                if (img2.getWidth() * img2.getHeight() > (PageSize.A4.getHeight() - 90) * (PageSize.A4.getWidth() - 40)) {
                    if (img2.getWidth() > PageSize.A4.getWidth() - 40) {
                        img2.scalePercent((100 / img2.getWidth()) * (PageSize.A4.getWidth() - 40));
                    } else {
                        if (img2.getHeight() > PageSize.A4.getHeight() - 90) {
                            img2.scalePercent((100 / img2.getHeight()) * (PageSize.A4.getHeight() - 50));
                        } else {
                            img2.scalePercent(100);
                        }
                    }
                } else {
                    if (img2.getWidth() > PageSize.A4.getWidth() - 40) {
                        img2.scalePercent((100 / img2.getWidth()) * (PageSize.A4.getWidth() - 40));
                    } else {
                        if (img2.getHeight() > PageSize.A4.getHeight() - 90) {
                            img2.scalePercent((100 / img2.getHeight()) * (PageSize.A4.getHeight() - 90));
                        } else {
                            img2.scalePercent(100);
                        }
                    }
                }

                img2.setAbsolutePosition(20, PageSize.A4.getHeight() - (img2.getScaledHeight()) - 70);
                document.newPage();
                paragraphKosong = new Paragraph("File Lampiran : " + laporanDanaFile.getLaporanDana().getDeskripsi());
                document.add(paragraphKosong);
                document.add(img2);
            } else if ((laporanDanaFile.getFile().substring(laporanDanaFile.getFile().length()-3)).equalsIgnoreCase("pdf")) {
//            ClassPathResource pdfResource = new ClassPathResource(lokasiFile2);
//            byte[] pdfBytes = IOUtils.toByteArray(pdfResource.getInputStream());
//            document.newPage();
//            Image pdfImage = Image.getInstance(pdfBytes);
//            pdfImage.scaleToFit(400, 400);
//            pdfImage.setAlignment(Element.ALIGN_CENTER);
//            document.add(pdfImage);
                document.newPage();
                File pdfFile = new File(lokasiFile2);
                FileInputStream fileInputStream = new FileInputStream(pdfFile);
                byte[] pdfBytes = IOUtils.toByteArray(fileInputStream);
                PdfReader reader = new PdfReader(pdfBytes);
                int numPages = reader.getNumberOfPages();

                for (int i = 0; i < numPages; i++) {
                    PdfImportedPage importedPage = writer.getImportedPage(reader, i + 1);
                    PdfContentByte content = writer.getDirectContent();
                    content.addTemplate(importedPage, 0, 0);
                    document.newPage();
                }

                reader.close();
                fileInputStream.close();
            }
        }

        document.close();

        OutputStream servletOutputStream = response.getOutputStream();
        outputStream.writeTo(servletOutputStream);
        servletOutputStream.flush();
    }
}
