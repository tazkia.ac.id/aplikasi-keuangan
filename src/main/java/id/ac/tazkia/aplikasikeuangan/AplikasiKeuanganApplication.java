package id.ac.tazkia.aplikasikeuangan;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.MustacheFactory;

import jakarta.annotation.PostConstruct;
import nz.net.ultraq.thymeleaf.LayoutDialect;

@SpringBootApplication
public class AplikasiKeuanganApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiKeuanganApplication.class, args);
	}

	@Bean
	public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

	@Bean
	public MustacheFactory mustacheFactory(){
		return new DefaultMustacheFactory();
	}

	@PostConstruct
	public void setUTCTimeZone(){
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/api/pengajuandana/setahun").allowedOrigins("http://localhost:8080");
				registry.addMapping("/api/pengajuandana/setahun").allowedOrigins("https://spmi.tazkia.ac.id");
			}
		};
	}

}
