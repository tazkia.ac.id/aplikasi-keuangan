package id.ac.tazkia.aplikasikeuangan.entity.transaksi;


import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Rekening;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
public class KembalianDana {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_laporan_dana")
    private LaporanDana laporanDana;

    @NotNull
    private LocalDateTime tanggalKembalian;

    private String deskripsi;

    private String jenisKembalian;

    @ManyToOne
    @JoinColumn(name = "id_rekening")
    private Rekening rekening;

    @NumberFormat
    @Min(0)
    private BigDecimal nominalSelisih;

    @NumberFormat
    @Min(0)
    private BigDecimal nominalKembalian;



}
