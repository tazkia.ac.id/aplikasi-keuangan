package id.ac.tazkia.aplikasikeuangan.entity.setting;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
public class RencanaPenerimaan {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_periode")
    private PeriodeAnggaran periodeAnggaran;

    @ManyToOne
    @JoinColumn(name = "id_mata_anggaran")
    private MataAnggaran mataAnggaran;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal persentasePagu;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal pagu;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal persentaseAnggaran;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal anggaran;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String keterangan;

}
