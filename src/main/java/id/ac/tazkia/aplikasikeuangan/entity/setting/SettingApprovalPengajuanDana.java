package id.ac.tazkia.aplikasikeuangan.entity.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Jabatan;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@Entity
public class SettingApprovalPengajuanDana {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_jabatan")
    private Jabatan jabatan;

    @NotNull
    @NumberFormat
    @Min(0)
    private Integer nomorUrut;

    @ManyToOne
    @JoinColumn(name = "id_jabatan_approve")
    private Jabatan jabatanApprove;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal batasPengajuan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
