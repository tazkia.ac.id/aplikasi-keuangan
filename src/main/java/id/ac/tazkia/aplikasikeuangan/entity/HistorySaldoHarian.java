package id.ac.tazkia.aplikasikeuangan.entity;

import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class HistorySaldoHarian {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_departemen")
    private Departemen departemen;

    private String namaDepartemen;

    private BigDecimal totalAnggaran;

    private BigDecimal persentase;

    private BigDecimal paguAnggaran;

    private BigDecimal sisaPagu;

    private BigDecimal used;

    private LocalDateTime tanggalUpdate;

    private String keterangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
