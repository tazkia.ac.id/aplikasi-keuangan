package id.ac.tazkia.aplikasikeuangan.entity.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;

@Entity
@Data
public class Urutan {

    @Id
    private String id;

    private String nama;

    private String inisial;

    private Integer nomorUrut;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
