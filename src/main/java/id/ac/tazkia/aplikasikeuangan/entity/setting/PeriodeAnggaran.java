package id.ac.tazkia.aplikasikeuangan.entity.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDate;

@Data
@Entity
public class PeriodeAnggaran {

    @Id
    private String id;

    @NotNull
    @Size(min = 2, max = 20)
    private String kodePeriodeAnggaran;

    @NotNull
    @Size(min = 2, max = 200)
    private String namaPeriodeAnggaran;


    private LocalDate tanggalMulaiPerencanaan;


    private LocalDate tanggalSelesaiPerencanaan;


    private LocalDate tanggalMulaiAnggaran;


    private LocalDate tanggalSelesaiAnggaran;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @NotNull
    private String tanggalMulaiRencana;

    @NotNull
    private String tanggalSelesaiRencana;

    @NotNull
    private String tanggalMulaiAnggar;

    @NotNull
    private String tanggalSelesaiAnggar;

}
