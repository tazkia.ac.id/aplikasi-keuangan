package id.ac.tazkia.aplikasikeuangan.entity.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Jabatan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode(of = {"id", "nomorUrut", "pengajuanDana"})
@Entity
public class PengajuanDanaApprove {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pengajuan_dana")
    private PengajuanDana pengajuanDana;

    @NotNull
    @NumberFormat
    @Min(0)
    private Integer nomorUrut;

    @ManyToOne
    @JoinColumn(name = "id_jabatan_approve")
    private Jabatan jabatanApprove;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusApprove = StatusRecord.WAITING;

    @ManyToOne
    @JoinColumn(name = "user_approve")
    private Karyawan userApprove;

    private LocalDateTime tanggalApprove;

    private String tanggalApproval;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String keterangan;

    @NumberFormat
    private BigDecimal kuantitasRevisi;

    private String satuanRevisi;

    @NumberFormat
    private BigDecimal amountRevisi;

    @NumberFormat
    private BigDecimal jumlahRevisi;

}
