package id.ac.tazkia.aplikasikeuangan.entity.setting;

import com.fasterxml.jackson.annotation.JsonFormat;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.MataAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.RencanaPenerimaan;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
public class Penerimaan {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_periode")
    private PeriodeAnggaran periodeAnggaran;

    @ManyToOne
    @JoinColumn(name = "id_mata_anggaran")
    private MataAnggaran mataAnggaran;

    @ManyToOne
    @JoinColumn(name = "id_rencana_penerimaan")
    private RencanaPenerimaan rencanaPenerimaan;

    @ManyToOne
    @JoinColumn(name = "id_departemen")
    private Departemen departemen;

    @NotNull
    @Min(0)
    private BigDecimal kuantitas;

    @NotNull
    private String satuan;

    @NotNull
    @Min(0)
    private BigDecimal amount;

    @Min(0)
    private BigDecimal total;

    private String keterangan;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalPenerimaan;

    @NotNull
    private String tanggalPeneriman;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Min(0)
    private BigDecimal realisasi;

    @Min(0)
    private BigDecimal deviasi;

    private String userInsert;

    private LocalDateTime tanggalInsert;

    private String userEdit;

    private LocalDateTime tanggalEdit;

    private String userDelete;

    private LocalDateTime tanggalDelete;

}
