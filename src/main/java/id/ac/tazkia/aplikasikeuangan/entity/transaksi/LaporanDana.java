package id.ac.tazkia.aplikasikeuangan.entity.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
//@Getter
//@Setter
//@NoArgsConstructor
//@EqualsAndHashCode(of = "id")
//@Entity
public class LaporanDana {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pencairan_dana_detail")
    private PencairanDanaDetail pencairanDanaDetail;

    @NotNull
    private LocalDateTime tanggalLaporan;

    private String deskripsi;

    @NumberFormat
    @Min(0)
    private BigDecimal amount;

    @NumberFormat
    @Min(0)
    private BigDecimal jumlah;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusLapor = StatusRecord.WAITING;

    @ManyToOne
    @JoinColumn(name = "user_pelapor")
    private Karyawan userPelapor;

    @ManyToOne
    @JoinColumn(name = "user_konfirmasi")
    private Karyawan userKonfirmasi;

    private LocalDateTime tanggalKonfirmasi;

    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String keterangan;

    @ManyToOne
    @JoinColumn(name="id_tahun_anggaran")
    private PeriodeAnggaran periodeAnggaran;

//    @OneToMany(mappedBy = "laporanDana", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<LaporanDanaPorsi> laporanDanaPorsis = new HashSet<>();

    @Enumerated(EnumType.STRING)
    private Posting posting = Posting.WAITING;

    public enum Posting{
        WAITING, POSTED
    }

}
