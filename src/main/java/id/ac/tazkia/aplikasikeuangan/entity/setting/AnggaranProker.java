package id.ac.tazkia.aplikasikeuangan.entity.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.StandardDetail;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class AnggaranProker {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name="id_anggaran")
    private Anggaran anggaran;

    @ManyToOne
    @JoinColumn(name="id_standard_detail")
    private StandardDetail standardDetail;

    @ManyToOne
    @JoinColumn(name="id_akun_level_tiga")
    private AkunLevelTiga akunLevelTiga;

    private String programKerja;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
