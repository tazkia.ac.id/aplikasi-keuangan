package id.ac.tazkia.aplikasikeuangan.entity.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class EstimasiPendapatanTerkait {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_instansi")
    private Instansi instansi;

    @ManyToOne
    @JoinColumn(name = "id_periode_anggaran")
    private PeriodeAnggaran periodeAnggaran;


    private String keterangan;

    private String satuan;

    private BigDecimal nominalSatuan;

    private BigDecimal nominalTotal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private LocalDateTime tanggalInsert;

    private String userUpdate;

    private LocalDateTime tanggalUpdate;

    private String userDelete;

    private LocalDateTime tanggalDelete;


}
