package id.ac.tazkia.aplikasikeuangan.entity.setting;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
public class AnggaranDetailEstimasi {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_anggaran_detail")
    private AnggaranDetail anggaranDetail;

    private String deskripsi;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalEstimasi;

    @NotNull
    @Min(0)
    private BigDecimal kuantitas;

    @NotNull
    private String satuan;

    @NotNull
    @Min(0)
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String tanggalEstimasiString;

}
