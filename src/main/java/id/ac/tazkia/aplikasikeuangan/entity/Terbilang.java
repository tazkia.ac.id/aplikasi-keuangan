package id.ac.tazkia.aplikasikeuangan.entity;

import java.math.BigDecimal;

public class Terbilang {

    private static final String[] ANGKA = {
            "", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"
    };

    public static String terbilang(BigDecimal nominal) {
        if (nominal.compareTo(BigDecimal.ZERO) == 0) {
            return "nol";
        }

        if (nominal.compareTo(new BigDecimal("999999999999.99")) >= 0) {
            return "Nominal terlalu besar";
        }

        return terbilangRatusan(nominal.intValue());
    }

    private static String terbilangRatusan(int angka) {
        if (angka < 12) {
            return ANGKA[angka];
        }

        if (angka >= 12 && angka <= 19) {
            return ANGKA[angka % 10] + " belas";
        }

        if (angka >= 20 && angka <= 99) {
            return ANGKA[angka / 10] + " puluh " + ANGKA[angka % 10];
        }

        if (angka >= 100 && angka <= 199) {
            return "seratus " + terbilangRatusan(angka % 100);
        }

        if (angka >= 200 && angka <= 999) {
            return ANGKA[angka / 100] + " ratus " + terbilangRatusan(angka % 100);
        }

        if (angka >= 1000 && angka <= 1999) {
            return "seribu " + terbilangRatusan(angka % 1000);
        }

        if (angka >= 2000 && angka <= 999999) {
            return terbilangRatusan(angka / 1000) + " ribu " + terbilangRatusan(angka % 1000);
        }

        if (angka >= 1000000 && angka <= 999999999) {
            return terbilangRatusan(angka / 1000000) + " juta " + terbilangRatusan(angka % 1000000);
        }

        if (angka >= 1000000000 && angka <= 999999999999L) {
            return terbilangRatusan(angka / 1000000000) + " miliar " + terbilangRatusan(angka % 1000000000);
        }

        return "";
    }
}
