package id.ac.tazkia.aplikasikeuangan.entity;

import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;


@Entity
@Data
public class Bulan {

    @Id
    private String bulan;

    private String nama;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
