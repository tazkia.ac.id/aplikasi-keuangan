package id.ac.tazkia.aplikasikeuangan.entity.transaksi;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import java.math.BigDecimal;

@Entity
@Data
public class PencairanDanaDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pengajuan_dana")
    private PengajuanDana pengajuanDana;

    @ManyToOne
    @JoinColumn(name = "id_pencairan_dana")
    private PencairanDana pencairanDana;



}
