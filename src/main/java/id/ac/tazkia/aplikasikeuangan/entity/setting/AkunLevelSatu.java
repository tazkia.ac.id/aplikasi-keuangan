package id.ac.tazkia.aplikasikeuangan.entity.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
public class AkunLevelSatu {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name="id_akun")
    private Akun akun;

    private String namaAkunLevelSatu;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
