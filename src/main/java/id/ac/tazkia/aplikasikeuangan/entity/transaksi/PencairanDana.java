package id.ac.tazkia.aplikasikeuangan.entity.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusAccounting;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Bank;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data

@Entity
public class PencairanDana {

    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pengajuan_dana")
    private PengajuanDana pengajuanDana;

    @NotNull
    private LocalDateTime tanggalMintaPencairan = LocalDateTime.now().plusHours(7);

    @ManyToOne
    @JoinColumn(name = "user_minta_pencairan")
    private Karyawan userMintaPencairan;

    private String jenisPencairan;

    private BigDecimal nominalMintaPencairan;

    private String keterangan;

    private String nomorRekening;

    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusPencairan = StatusRecord.WAITING;

    private LocalDateTime tanggalPencairan;

    private String keteranganPencairan;

    @ManyToOne
    @JoinColumn(name = "user_pencairan")
    private Karyawan userPencairan;

    private String buktiPencairan;

    @NumberFormat
    @Min(0)
    private BigDecimal nominalPencairan;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusCair = StatusRecord.WAITING;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "bank")
    private Bank bank;

    @ManyToOne
    @JoinColumn(name = "id_departemen")
    private Departemen departemen;

    private String tanggalan;

    private String jaman;

    private String journalTemplate;

    @Enumerated(EnumType.STRING)
    private StatusAccounting statusAkunting = StatusAccounting.WAITING;

}
