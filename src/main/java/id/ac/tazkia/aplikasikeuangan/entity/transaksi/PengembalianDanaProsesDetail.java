package id.ac.tazkia.aplikasikeuangan.entity.transaksi;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Data
@EqualsAndHashCode(of = {"id", "pengembalianDanaProses"})
@Entity
public class PengembalianDanaProsesDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;


    @ManyToOne
    @JoinColumn(name = "id_pengembalian_dana")
    private PengembalianDana pengembalianDana;

    @ManyToOne
    @JoinColumn(name = "id_pengembalian_dana_proses")
    private PengembalianDanaProses pengembalianDanaProses;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
