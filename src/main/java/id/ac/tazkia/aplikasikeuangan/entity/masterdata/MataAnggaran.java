package id.ac.tazkia.aplikasikeuangan.entity.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;


@Data
@Entity
public class MataAnggaran {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Size(min = 2, max = 20)
    private String kodeMataAnggaran;

    private String namaMataAnggaran;

    private String keterangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
