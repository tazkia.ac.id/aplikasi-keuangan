package id.ac.tazkia.aplikasikeuangan.entity.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.EstimasiPendapatanTerkait;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Penerimaan;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class PendapatanTerkait {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    @ManyToOne
    @JoinColumn(name = "id_estimasi_pendapatan_terkait")
    private EstimasiPendapatanTerkait estimasiPendapatanTerkait;
    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;
    private LocalDate tanggal;
    private String deskripsi;
    private String satuan;
    private BigDecimal jumlah;
    private BigDecimal nominalSatuan;
    private BigDecimal total;
    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
    private String userInsert;
    private String userUpdate;
    private String userDelete;
    private LocalDateTime tanggalInsert;
    private LocalDateTime tanggalUpdate;
    private LocalDateTime tanggalDelete;
    private String file;
    private String tanggalString;
}
