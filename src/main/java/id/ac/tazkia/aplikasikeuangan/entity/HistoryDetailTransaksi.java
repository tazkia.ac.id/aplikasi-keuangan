package id.ac.tazkia.aplikasikeuangan.entity;

import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.transaksi.PengajuanDana;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class HistoryDetailTransaksi {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pengajuan_dana")
    private PengajuanDana pengajuanDana;

    private LocalDateTime tanggalPengajuan;

    private String deskripsiBiaya;

    private BigDecimal pengajuan;

    private LocalDateTime tanggalLaporan;

    private BigDecimal realisasi;


    private LocalDateTime tanggalUpdate;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;


    @ManyToOne
    @JoinColumn(name = "id_departemen")
    private Departemen departemen;

}
