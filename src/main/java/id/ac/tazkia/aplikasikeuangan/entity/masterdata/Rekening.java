package id.ac.tazkia.aplikasikeuangan.entity.masterdata;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Data
@Entity
public class Rekening {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String nomorRekening;

    private String atasNama;

    private String bank;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
