package id.ac.tazkia.aplikasikeuangan.entity.transaksi;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.Anggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.AnggaranDetail;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import id.ac.tazkia.aplikasikeuangan.entity.setting.SettingApprovalPengajuanDana;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

//@Data
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
public class PengajuanDana {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private LocalDateTime tanggalPengajuan = LocalDateTime.now();

    @ManyToOne
    @JoinColumn(name = "id_departemen")
    private Departemen departemen;

    @NotNull
    @ManyToOne
    @JoinColumn(name="id_anggaran")
    private Anggaran anggaran;

    @NotNull
    @ManyToOne
    @JoinColumn(name="id_anggaran_detail")
    private AnggaranDetail anggaranDetail;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "request_for")
    private Karyawan requestFor;

    @NotNull
    @Size(min = 1)
    private String deskripsiBiaya;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal kuantitas;

    @NotNull
    private String satuan;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal amount;

    private BigDecimal jumlah;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_karyawan_pengaju")
    private Karyawan karyawanPengaju;

    private String file;
    private Integer nomorUrut;
    private Integer totalNomorUrut;


    @ManyToOne
    @JoinColumn(name="id_tahun_anggaran")
    private PeriodeAnggaran periodeAnggaran;


    @OneToMany(mappedBy = "pengajuanDana", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PengajuanDanaApprove> approvalPengajuanDana = new HashSet<>();

//    @OneToMany(mappedBy = "pengajuanDana", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<PengajuanDanaPorsi> pengajuanDanaPorsis = new HashSet<>();

}
