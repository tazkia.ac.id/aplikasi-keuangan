package id.ac.tazkia.aplikasikeuangan.entity.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class EstimasiPendapatanTerkaitDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_estimasi_pendapatan_terkait")
    private EstimasiPendapatanTerkait estimasiPendapatanTerkait;

    private String keterangan;

    private String satuan;

    private BigDecimal jumlah;

    private BigDecimal nominalSatuan;

    private BigDecimal nominalTotal;

    private LocalDate tanggalEstimasi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private LocalDateTime tanggalInsert;

    private String userUpdate;

    private String tanggalUpdate;

    private String userDelete;

    private String tanggalDelete;


}
