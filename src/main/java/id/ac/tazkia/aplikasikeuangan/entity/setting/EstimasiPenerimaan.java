package id.ac.tazkia.aplikasikeuangan.entity.setting;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class EstimasiPenerimaan {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_rencana_penerimaan")
    private Penerimaan penerimaan;

    private String deskripsi;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggal;

    @NotNull
    @Min(0)
    private BigDecimal kuantitas;

    @NotNull
    private String satuan;

    @NotNull
    @Min(0)
    private BigDecimal jumlahSatuan;

    @NotNull
    @Min(0)
    private BigDecimal total;

    private String tanggalString;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
