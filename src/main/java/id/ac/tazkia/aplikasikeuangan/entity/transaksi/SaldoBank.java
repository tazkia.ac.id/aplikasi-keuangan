package id.ac.tazkia.aplikasikeuangan.entity.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.config.User;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class SaldoBank {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_instansi")
    private Instansi instansi;

    @ManyToOne
    @JoinColumn(name = "id_periode_anggaran")
    private PeriodeAnggaran periodeAnggaran;

    private LocalDate tanggal;

    private String description;

    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    private LocalDateTime tanggalInsert;

    private String userInsert;

    private LocalDateTime tanggalUpdate;

    private String userUpdate;

    private LocalDateTime tanggalDelete;

    private String userDelete;

    private String fileBukti;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
