package id.ac.tazkia.aplikasikeuangan.entity;

public enum StatusAccounting {
    WAITING,POSTED,DONE,IGNORE
}
