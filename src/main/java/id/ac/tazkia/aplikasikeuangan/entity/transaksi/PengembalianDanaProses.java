package id.ac.tazkia.aplikasikeuangan.entity.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusAccounting;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Rekening;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

//@Data

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
public class PengembalianDanaProses {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

//    @ManyToOne
//    @JoinColumn(name = "id_pengembalian_dana")
//    private PengembalianDana pengembalianDana;

    @NotNull
    private LocalDateTime tanggalPengembalian;

    @ManyToOne
    @JoinColumn(name = "user_pengembali")
    private Karyawan userPengembali;

    private String KeteranganPengembalian;

    @ManyToOne
    @JoinColumn(name = "cara_bayar")
    private Rekening caraBayar;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal nominalPengembalian;

    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusPengembalian = StatusRecord.WAITING;

    @ManyToOne
    @JoinColumn(name = "user_penerima")
    private Karyawan userPenerima;

    private LocalDateTime tanggalTerima;

    private String keteranganPenerima;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @OneToMany(mappedBy = "pengembalianDanaProses", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PengembalianDanaProsesDetail> pengembalianDanaProsesDetail = new HashSet<>();

    @Enumerated(EnumType.STRING)
    private StatusAccounting statusAkunting;

}
