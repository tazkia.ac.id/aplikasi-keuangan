package id.ac.tazkia.aplikasikeuangan.entity.setting;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
public class AkunLevelDua {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name="id_akun_level_satu")
    private AkunLevelSatu akunLevelSatu;

    private String kodeAkunLevelDua;

    private String namaAkunLevelDua;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
