package id.ac.tazkia.aplikasikeuangan.entity.config;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.security.Policy;

@Entity
@Table(name = "s_user")
//@Data
@Getter
@Setter
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String username;
    private Boolean active;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;


}
