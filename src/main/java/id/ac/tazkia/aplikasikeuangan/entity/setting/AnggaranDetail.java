package id.ac.tazkia.aplikasikeuangan.entity.setting;

import java.math.BigDecimal;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.NumberFormat;

import id.ac.tazkia.aplikasikeuangan.entity.PilihanEnum;
import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.StandardDetail;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
public class AnggaranDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name="id_anggaran")
    private Anggaran anggaran;

    @ManyToOne
    @JoinColumn(name="id_anggaran_proker")
    private AnggaranProker anggaranProker;

    @ManyToOne
    @JoinColumn(name="id_standard_detail")
    private StandardDetail standardDetail;

    @NotNull
    @Size(min=2)
    private String deskripsi;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal kuantitas;

    private String satuan;

    @NotNull
    @NumberFormat
    @Min(0)
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String idPeriodeAnggaran;

    @Enumerated(EnumType.STRING)
    private PilihanEnum pilihan;

    private String templateJurnal;

}
