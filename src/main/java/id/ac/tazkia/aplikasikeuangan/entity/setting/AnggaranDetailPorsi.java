package id.ac.tazkia.aplikasikeuangan.entity.setting;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Departemen;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
public class AnggaranDetailPorsi {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_anggaran_detail")
    private AnggaranDetail anggaranDetail;

    @ManyToOne
    @JoinColumn(name = "id_departemen")
    private Departemen departemen;

    @NotNull
    private BigDecimal persentasePorsi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private LocalDate tanggalBerlaku;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

    private LocalDateTime tanggalInsert;

    private LocalDateTime tanggalUpdate;

    private LocalDateTime tanggalDelete;

    private String tanggalBerlakuString;

}
