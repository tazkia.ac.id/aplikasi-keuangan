package id.ac.tazkia.aplikasikeuangan.entity.transaksi;


import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Instansi;
import id.ac.tazkia.aplikasikeuangan.entity.masterdata.Karyawan;
import id.ac.tazkia.aplikasikeuangan.entity.setting.PeriodeAnggaran;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
public class KasKecil {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_periode_anggaran")
    private PeriodeAnggaran periodeAnggaran;

    @NotNull
    private LocalDate tanggal;

    @NotEmpty
    @NotNull
    private String deskripsi;

    private BigDecimal nominal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    private String tanggalString;

    private String userInsert;

    private LocalDateTime tanggalInsert;

    private String UserEdit;

    private LocalDateTime tanggalEdit;

    @ManyToOne
    @JoinColumn(name = "id_instansi")
    private Instansi instansi;

}
