package id.ac.tazkia.aplikasikeuangan.entity.transaksi;

import id.ac.tazkia.aplikasikeuangan.entity.StatusRecord;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
public class LaporanDanaFile {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_laporan_dana")
    private LaporanDana laporanDana;

    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
